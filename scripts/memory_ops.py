#!/usr/bin/python3
from mbqcsim.memory_ops import MemoryOperations
import matplotlib.pyplot as plt

# First experiment - initial sweep of fidelity with time
e = MemoryOperations(seed = 6903564118784409788,
                     p_min=0, p_max=1, p_steps=21, n=1000,
                     bw_min=3, bw_max=15,
                     bw_step_size=1, d_max=100)
da = e.run()
print(da)
da.sel(algorithm="gbfs",mem_op="pred_writes").plot.line(x="prob", marker="o", markerfacecolor='none')
plt.show()
exit()
# # Export distance_writes data
# dw = da.sel(mem_op="distance_writes")
# dw_gbfs = dw.sel(algorithm="gbfs").drop(["algorithm","note"]).to_dataframe()
# dw_gbfs = dw_gbfs.pivot_table(values='data', index='prob',
#                               columns='block_width', aggfunc='first')
# dw_gbfs.to_csv("distance_writes_gbfs.csv")
# dw_ibfs = dw.sel(algorithm="ibfs").drop(["algorithm","note"]).to_dataframe()
# dw_ibfs = dw_ibfs.pivot_table(values='data', index='prob',
#                               columns='block_width', aggfunc='first')
# dw_ibfs.to_csv("distance_writes_ibfs.csv")

# # Export distance_writes data
# ps = da.sel(mem_op="path_succ_writes")
# ps_gbfs = ps.sel(algorithm="gbfs").drop(["algorithm","note"]).to_dataframe()
# ps_gbfs = ps_gbfs.pivot_table(values='data', index='prob',
#                               columns='block_width', aggfunc='first')
# ps_gbfs.to_csv("path_succ_writes_gbfs.csv")
# ps_ibfs = ps.sel(algorithm="ibfs").drop(["algorithm","note"]).to_dataframe()
# ps_ibfs = ps_ibfs.pivot_table(values='data', index='prob',
#                               columns='block_width', aggfunc='first')
# ps_ibfs.to_csv("path_succ_writes_ibfs.csv")

#
fn = da.sel(mem_op="failed_nodes",algorithm="ibfs")
fn = 100*fn/20 # Convert data to percentage of height
df = fn.drop(["algorithm","note"]).to_dataframe()
df = df.pivot_table(values='data', index='prob',
                    columns='block_width', aggfunc='first')
df.to_csv("failed_nodes.csv")
# ps_ibfs = ps.sel(algorithm="ibfs").drop(["algorithm","note"]).to_dataframe()
# ps_ibfs = ps_ibfs.pivot_table(values='data', index='prob',
#                               columns='block_width', aggfunc='first')
# ps_ibfs.to_csv("path_succ_writes_ibfs.csv")
