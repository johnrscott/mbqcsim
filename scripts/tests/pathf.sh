#!/usr/bin/env bats

@test "Repeat using pathf full output" {
    # Generate output file
    run ../../build/bin/pathf -H20 -W200 -B10 -p0.8 -o out.json

    # Regenerate output based on previous output
    run ../../build/bin/pathf -i out.json -o test.json

    # Check outputs are equal
    run diff out.json test.json
    [ "$status" -eq 0 ]
}

@test "Repeat using pathf minimal output" {
    # Generate minimal output file
    run ../../build/bin/pathf -H20 -W200 -B10 -p0.8 -m -o out-min.json

    # Generate several sets of data based on minimal output
    run ../../build/bin/pathf -i out-min.json -m -o test1.json
    run ../../build/bin/pathf -i out-min.json -m -o test2.json
    run ../../build/bin/pathf -i out-min.json -m -o test3.json

    # Check outputs are equal
    run diff test1.json test2.json
    [ "$status" -eq 0 ]
    run diff test1.json test3.json
    [ "$status" -eq 0 ]
}
