'''
This file contains the draft process data routines. These are really not
for processing data, but storing raw data, and will be put into the CachedSweep
class in some form.
'''

class ProcessData:

    def injectDimensions(da, num_repeats):
        dims = {
            "n" : num_repeats,
        }
        return da.expand_dims(dims).copy()

class AverageFidelity:
    '''
    Compute the average fidelity at each column depth.
    '''

    def setDims(self, da, params):
        '''
        Now need to add in an extra dimension for the column. The 
        params argument holds the values of the fixed params of the
        sweep.
        '''
        self._params = params
        dims = {
            "n" : self._params[EsimArg.NumRepeats],
            "Column" : self._params[EsimArg.Width]
        }
        return da.expand_dims(dims).copy()
    
    def process(self, results):
        data = []

        # Create a new xarray
        a = np.empty((self._params[EsimArg.NumRepeats],
                      self._params[EsimArg.Width]))
        a[:] = np.nan
        da = xr.DataArray(a, dims=("n", "Column"))
        
        # Loop over all the repeats in the experiment, computing the
        # average for each repeat.
        for n, rep in enumerate(results["repeat_data"]):
            for k, col in enumerate(rep["sim_results"]):
                try:
                    da[n,k] = col["distance"]
                except KeyError as e:
                    # Continue to the next value
                    pass
                
        return da

    
class EndColumn(ProcessData):
    '''
    Compute the end column reached by esim
    '''

    def process(results):
        data = [rep["end_column"] for rep in results["repeat_data"]]
        return xr.DataArray(data, dims=("n")).copy()
    
class MaxColumn(ProcessData):
    '''
    Compute the max column reached by pathf
    '''

    def process(results):
        data = [rep["max_column"] for rep in results["repeat_data"]]
        return xr.DataArray(data, dims=("n")).copy()

class AverageDistanceWrites(ProcessData):
    '''
    Compute the average distance writes computed by pathf
    '''

    def process(results):
        data = []

        # Loop over all the repeats in the experiment, computing the
        # average for each repeat.
        for rep in results["repeat_data"]:

            # Loop over all the searches in the repeat, ignoring the final
            # searches where the window width is less than the block width
            total_dist_writes = 0
            num_searches = 0
            for path_emulate in rep["path_emulate"]:

                search = path_emulate["search"]
                if search["width"] == results["block_width"]:
                    try:
                        total_dist_writes += search["counts"]["distance_writes"]
                        num_searches += 1
                    except KeyError as e:
                        # Continue to the next value
                        pass

            # Calculate the average distance writes
            if num_searches != 0:
                average_distance_writes = float(total_dist_writes)/num_searches
            else:
                average_distance_writes = 0
            data.append(average_distance_writes)

        return xr.DataArray(data, dims=("n")).copy()


class AverageDistanceWrites(ProcessData):
    '''
    Compute the average distance writes computed by pathf
    '''

    def process(results):
        data = []
        for rep in results["repeat_data"]:
            total_ps_writes = 0
            num_extends = 0
            for path_emulate in rep["path_emulate"]:
                try:
                    path_extend = path_emulate["path_extend"]
                    total_ps_writes += (path_extend["counts"]
                                        ["path_succ_writes"])
                    num_extends += 1
                except KeyError as e:
                    # Continue to the next value
                    pass

            # Calculate the average distance writes
            if num_extends != 0:
                average_ps_writes = float(total_ps_writes)/num_extends
            else:
                average_ps_writes = 0            
            data.append(average_ps_writes)
            
        return xr.DataArray(data, dims=("n")).copy()
