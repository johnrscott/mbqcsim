#!/usr/bin/python3
from mbqcsim.cache import Cache

# Set the name of the cache folder here
folder="cache"

# Print the contents of the cache
Cache(cache_folder=folder).summarise()
