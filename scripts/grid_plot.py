#!/usr/bin/python3

# Matplotlib deprecation warnings were annoying me
import warnings
warnings.filterwarnings("ignore", module = "matplotlib\..*" )

import networkx as nx
import matplotlib.pyplot as plt
import json
import sys

if(len(sys.argv) == 2):
    filename = sys.argv[1]
else:
    print("No file specified")
    exit(0)
    
with open(filename) as f:
    line = f.readline()
    data = json.loads(line)

G = nx.grid_2d_graph(data['width'],data['height'])
G = nx.create_empty_copy(G)
for nodes in data['edges']:
    G.add_edge(tuple(nodes[0]), tuple(nodes[1]))
             
pos = {(x,y):(x,y) for x,y in G.nodes()}
#nx.draw(G, pos=pos, with_labels=True, node_size=600, node_color='lightblue')
nx.draw(G, pos=pos, node_size=1)

for path_list in data['paths']:
    path = []
    for p in path_list:
        path.append(tuple(p))

    # Calculated by python
    # nx_path = nx.shortest_path(G,source=path[0],target=path[-1])
    # path_edges = list(zip(nx_path,nx_path[1:]))
    # nx.draw_networkx_edges(G,pos,edgelist=path_edges,edge_color='y', width=6)

    # Calculated by C++    
    path_edges = list(zip(path,path[1:]))
    nx.draw_networkx_edges(G,pos,edgelist=path_edges,edge_color='r', width=3)

    
plt.show()
