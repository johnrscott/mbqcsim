#!/usr/bin/python3
from mbqcsim.depth_with_prob import MaxDepthWithProb
from mbqcsim.cache import Cache
import matplotlib.pyplot as plt

c = Cache()
c.summarise()
exit()

e = MaxDepthWithProb(seed=20000, p_min=0.4,
                     p_max=0.9, p_steps=6, n=10, bw_min=3, bw_max=18,
                     bw_step_size=3, d_max=200)
da = e.run()
print(da)
da.plot(x="prob", col="algorithm")
#da.plot(x="prob", col="algorithm")
#da.sel(algorithm="ibfs").plot(x="prob")
plt.show()

