#!/usr/bin/python3
from mbqcsim.depth_with_prob import MaxDepthWithProb
import matplotlib.pyplot as plt

# First experiment - initial sweep of maximum depth reached with probability
e = MaxDepthWithProb(seed=12306134726338567098, p_min=0.4,
                     p_max=0.95, p_steps=23, n=1000, bw_min=3, bw_max=18,
                     bw_step_size=1, d_max=2000)
da = e.run()
print(da)
da.plot.line(x="prob", col="algorithm", marker="o", markerfacecolor='none')

# Second experiment - same as first, slightly adjusted params
# Decreased max block width to 15 and added p = 1 to sweep
e = MaxDepthWithProb(seed=12406134726338567098, p_min=0.4,
                     p_max=1, p_steps=25, n=1000, bw_min=3, bw_max=15,
                     bw_step_size=1, d_max=2000)
da = e.run()
print(da)
da.plot.line(x="prob", col="algorithm", marker="o", markerfacecolor='none')










plt.show()
exit()

#da.plot(x="prob", col="algorithm")
#da.sel(algorithm="ibfs").plot(x="prob")
#tplt.save("test.tex")
#plt.show()
df_gbfs = da.sel(algorithm="gbfs").drop(["algorithm","note"]).to_dataframe()
df_gbfs = df_gbfs.pivot_table(values='data', index='prob',
                              columns='block_width', aggfunc='first')
df_gbfs.to_csv("max_depth_gbfs.csv")

df_ibfs = da.sel(algorithm="ibfs").drop(["algorithm","note"]).to_dataframe()
df_ibfs = df_ibfs.pivot_table(values='data', index='prob',
                              columns='block_width', aggfunc='first')
df_ibfs.to_csv("max_depth_ibfs.csv")
