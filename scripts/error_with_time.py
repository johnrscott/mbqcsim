#!/usr/bin/python3
from mbqcsim.error_with_time import ErrorWithTime
import matplotlib.pyplot as plt

# First experiment - initial sweep of fidelity with time
# V_pi = 1V, error = 0.005 V (0.5% ov V_pi), 12 steps
# Clock frequency 1GHz, t_max = 1us
# 200 repeats
# Approximate runtime (HPE Microserver Gen10+): 15 minutes
# Approximate peak memory usage: approx 3GiB (before memory optimisation)
e = ErrorWithTime(seed=18229267364050288035, n=200, v_pi=1, v_sigma_max=0.5e-2,
                  v_sigma_steps=12, xp=1e9, t_max=1e-6)
da = e.run()
df = da.drop(["note"]).to_dataframe()
#df.to_csv("test.dat", sep=" ")
#da.plot.line(x="time")
#da.plot(x="time")
#plt.show()
#exit()

# Second experiment - same as experiment one, but more repeats
# V_pi = 1V, error = 0.005 V (0.5% ov V_pi), 12 steps
# Clock frequency 1GHz, t_max = 1us
# 500 repeats
# Approximate runtime (HPE Microserver Gen10+): 30 minutes
# Approximate peak memory usage: 1GiB (after memory optimisation)
e = ErrorWithTime(seed=18329267364050288035, n=500, v_pi=1, v_sigma_max=0.5e-2,
                  v_sigma_steps=12, xp=1e9, t_max=1e-6)
da = e.run()
#da.plot.line(x="time")
#da.plot()
#plt.show()
#exit()

# Third experiment - same as previous, but more repeats and smaller error steps
# V_pi = 1V, error = 0.005 V (0.5% ov V_pi), 12 steps
# Clock frequency 1GHz, t_max = 1us
# 1000 repeats
# Approximate runtime (HPE Microserver Gen10+):
# Approximate peak memory usage:
e = ErrorWithTime(seed=18429267364050288035, n=1000, v_pi=1, v_sigma_max=0.5e-2,
                  v_sigma_steps=24, xp=1e9, t_max=1e-6)
da = e.run()
#da.plot.line(x="time")
#da.plot.surface(x="time")
da = da.assign_coords(time=(1e6 * da.time))
da.time.attrs["units"] = "us"

# Scale voltage to millivolts
da = da.assign_coords(noise_sigma=(1e3 * da.noise_sigma))

da["note"] = "Fidelity with time, V_pi=1V"
da.plot(x="time")
print(da)
da = da.sel(time=slice(None,None,50))
df = da.drop(["note"]).to_dataframe()
#df = df.iloc[::100,:]
#df.to_csv("error_with_time.dat", sep=" ")
# df = df.pivot_table(values='data', index='time',
#                     columns='noise_sigma', aggfunc='first')
print(df.info())

plt.show()

#exit()
# print(df.info())
#df.to_csv("error_with_time.csv", sep=" ")

# with open("error_with_time.dat","w") as f:
#     for index, row in df.iterrows():
#         f.write(f"{index + (row['data'],)} ")


#exit()
#df.to_csv("error_with_time.csv")

# with open("error_with_time.dat","w") as f:
#     for index, row in df.iterrows():
#         print(row['time'], row['noise_sigma'], row['data'])
    
    # for i,line in enumerate(df.values):
    #     for j,val in enumerate(line):
    #         print(line)
    #         print(val)
    #         print(f"({val},{df.columns[j]},{df.index[j]})\n")

#plt.show()
