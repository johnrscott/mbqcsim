#!/usr/bin/python3
#
# To use this file: specify the relative file name of a sample from
# the normal distribution. Check to see that the histogram looks normally
# distributed, the probplot is a straight line, and the mean and variance
# agree with the JSON in the file.
#
import pandas as pd
import matplotlib.pyplot as plt
import json
import numpy as np
from scipy.stats import norm, normaltest, probplot

filename = "../tests/normal-data-3.csv" 

with open(filename) as f:
    line = f.readline()
    data = json.loads(line)

df = pd.read_csv(filename, comment="{")

# This can only prove that the distribution is not normal
x = df["samples"].tolist()

mu_fit, sigma_fit = norm.fit(x)
print(f"Fitted mean = {mu_fit}, true mean = {data['mu']}")
print(f"Fitted sigma = {sigma_fit}, true sigma = {data['sigma']}")

result = normaltest(x, axis=0, nan_policy='propagate')
print(result)

# View the histogram
df.hist(bins=17)

# View the probplot (should be a straight line)
plt.figure()
probplot(x, sparams=(), dist='norm',
         fit=True, plot=plt, rvalue=False)
print(df.describe())

plt.show()

