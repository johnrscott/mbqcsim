#!/usr/bin/python3
from mbqcsim.fidelity import Fidelity
from mbqcsim.max_depth import MaxDepth
from mbqcsim.path_emulate import PathEmulate

from mbqcsim.esim import EsimArg
from mbqcsim.pathf import PathfArg
from mbqcsim.cache import Cache

import numpy as np
import matplotlib.pyplot as plt

from mbqcsim.error_with_time import ErrorWithTime

e = ErrorWithTime(seed=0, n=200, v_pi=1, v_sigma_max=0.5e-2, xp=1e9, t_max=1e-6)
da = e.run()
da.plot.line(x="time")
plt.show()
exit()



# Uncomment to view cache
#c = Cache()
#c.summarise()
#exit()

##############################################
## PathEmulate

p = PathEmulate(seed=0, note="Test")
p.setFixed(PathfArg.Height, 10)
p.setFixed(PathfArg.Width, 15)
p.setSweep(PathfArg.BlockWidth, range(3,10,1))
p.setSweep(PathfArg.EdgeProbability, [round(p,3) for p in np.linspace(0, 1, 11)])
p.setSweep(PathfArg.Algorithm, ["gbfs","ibfs"])
p.setFixed(PathfArg.YStart, 5)
p.setFixed(PathfArg.NumRepeats, 50)

ds = p.run()
print(ds)
da = ds["data"]

# Average over repeats
da = da.mean(dim="n")

# Average over block results
da = da.mean(dim="block")

# Select distance writes
#da = da.sel(count="distance_writes")
#da.attrs["long_name"] = "Distance writes"

# Select path successor writes
#da = da.sel(count="path_succ_writes")
#da.attrs["long_name"] = "Path successor writes"

# Select prune reads
# da = da.sel(count="prune_reads", Algorithm="ibfs")
# da.attrs["long_name"] = "Prune reads"

# Select failed nodes
da = da.sel(count="prune_reads", Algorithm="ibfs")
da.attrs["long_name"] = "Failed"

# Select distance data
#da.plot.line(x="EdgeProbability", col="Algorithm")
da.plot.line(x="EdgeProbability")
plt.show()

################################################
## MaxDepth

# p = MaxDepth(seed=0,note="First proper test run")
# p.setFixed(PathfArg.Height, 10)
# p.setFixed(PathfArg.Width, 5000)
# p.setSweep(PathfArg.BlockWidth, range(2,15,1))
# p.setSweep(PathfArg.EdgeProbability, [round(p,2) for p in np.linspace(0,1,101)])
# p.setFixed(PathfArg.Algorithm, "gbfs")
# p.setFixed(PathfArg.YStart, 5)
# p.setFixed(PathfArg.NumRepeats, 100)

# ds = p.run()
# print(ds)
# da = ds["data"]

# # Average over repeats
# da = da.mean(dim="n")
# #da.plot().line(x="EdgeProbability")
# #da.plot(x="EdgeProbability")
# #da.plot.surface(x="EdgeProbability")
# da.plot.contourf(x="EdgeProbability")

# plt.show()

# p = Fidelity(seed=123, note="Test")
# p.setFixed(EsimArg.Height, 10)
# p.setFixed(EsimArg.Width, 100)
# p.setSweep(EsimArg.BlockWidth, range(3,10,2))
# p.setSweep(EsimArg.EdgeProbability, [round(p,2) for p in np.linspace(0,1,11)])
# p.setFixed(EsimArg.Algorithm, "gbfs")
# p.setFixed(EsimArg.YStart, 5)
# p.setFixed(EsimArg.NumRepeats, 5)
# p.setFixed(EsimArg.NoiseStdDev, 0)
# p.setFixed(EsimArg.NoiseMean, 0)

# ds = p.run()
# print(ds)
