#include "gtest/gtest.h"
#include "custom-vector.hpp"
#include <numeric>
#include <string>

// Tests the defaul constructed BaseVector
TEST(CustomVectorTest, DefaultConstructor)
{
    BaseVector<double> vec;
    EXPECT_EQ(vec.size(), 0);
    EXPECT_THROW(vec.at(0), std::out_of_range);   
}

// Test initialiser list construction
TEST(CustomVectorTest, InitialiserList)
{
    BaseVector<int> vec{1,2,3,4,5};
    EXPECT_EQ(vec.size(), 5);
    EXPECT_EQ(vec[0], 1);
    EXPECT_EQ(vec[1], 2);
    EXPECT_EQ(vec[2], 3);
    EXPECT_EQ(vec[3], 4);
    EXPECT_EQ(vec[4], 5);
    EXPECT_THROW(vec.at(5), std::out_of_range);
}

TEST(CustomVectorTest, GetSubVector)
{
    BaseVector<int> vec{1,2,3,4,5};    

    // Correctly construct subvector
    SubVector<int> sub{vec.getSub(2,4)};
    EXPECT_EQ(sub.size(), 2); // Check size
    EXPECT_EQ(sub[0], 3); // Check values
    EXPECT_EQ(sub[1], 4);
    EXPECT_THROW(sub.at(2), std::out_of_range);

    // Construct the full subvector
    SubVector<int> full{vec.getSub(0,5)};
    EXPECT_EQ(full.size(), 5);
    EXPECT_EQ(full[0], 1);
    EXPECT_EQ(full[1], 2);
    EXPECT_EQ(full[2], 3);
    EXPECT_EQ(full[3], 4);
    EXPECT_EQ(full[4], 5);
    EXPECT_THROW(full.at(5), std::out_of_range);

    // Try to obtain an out-of-range subvector (invalid end)
    EXPECT_THROW(vec.getSub(0,7), std::out_of_range);

    // Try to obtain an out-of-range subvector (invalid begin)
    EXPECT_THROW(vec.getSub(7,9), std::out_of_range);

    // Try to obtain subvector with out of order begin/end
    EXPECT_THROW(vec.getSub(3,2), std::logic_error);

    // Check that the empty object is accessible
    SubVector<int> empty{vec.getSub(3,3)};
    EXPECT_EQ(empty.size(), 0);
}

TEST(CustomVectorTest, CopyTest)
{
    BaseVector<int> vec{};
    vec.push_back(1);
    vec.push_back(2);
    vec.push_back(3);
    vec.push_back(4);
    EXPECT_EQ(vec[0], 1);
    EXPECT_EQ(vec[1], 2);
    EXPECT_EQ(vec[2], 3);
    EXPECT_EQ(vec[3], 4);
	
    // Copy Vector (create subvector)
    auto sub{vec};
    sub[0] = 100;
    sub[1] = 200;

    // Check sub has changed
    EXPECT_EQ(sub[0], 100);
    EXPECT_EQ(sub[1], 200);
    EXPECT_EQ(sub[2], 3);
    EXPECT_EQ(sub[3], 4);    
    
    // Check vec has also changed
    EXPECT_EQ(vec[0], 100);
    EXPECT_EQ(vec[1], 200);
    EXPECT_EQ(vec[2], 3);
    EXPECT_EQ(vec[3], 4);    

    // Now create a true copy
    auto diff{copy(vec)};

    diff[0] = 1000;
    diff[1] = 2000;

    // Check diff is correct
    EXPECT_EQ(diff[0], 1000);
    EXPECT_EQ(diff[1], 2000);
    EXPECT_EQ(diff[2], 3);
    EXPECT_EQ(diff[3], 4);    
    
    // Verify vec has not changed
    EXPECT_EQ(vec[0], 100);
    EXPECT_EQ(vec[1], 200);
    EXPECT_EQ(vec[2], 3);
    EXPECT_EQ(vec[3], 4);    
}


TEST(CustomVectorTest, MultipleSubVectors)
{
    BaseVector<int> vec{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};    

    // Correctly construct subvector (use end edge case)
    SubVector<int> sub{vec.getSub(4,vec.size())};
    EXPECT_EQ(sub.size(), vec.size()-4); // Check size
    EXPECT_EQ(sub[0], 5); // Check values
    EXPECT_EQ(sub[1], 6);
    EXPECT_EQ(sub[2], 7);
    EXPECT_EQ(sub[3], 8);
    EXPECT_EQ(sub[10], 15);
    EXPECT_THROW(sub.at(11), std::out_of_range);

    // Make a sub sub vector (use begin edge case)
    SubVector<int> subsub{sub.getSub(0,sub.size()-7)};
    EXPECT_EQ(subsub.size(), sub.size()-7); // Check size
    EXPECT_EQ(subsub[0], 5); // Check values
    EXPECT_EQ(subsub[1], 6);
    EXPECT_EQ(subsub[2], 7);
    EXPECT_EQ(subsub[3], 8);
    EXPECT_THROW(subsub.at(4), std::out_of_range);

    // Make a further sub sub sub vector with only 1 element
    SubVector<int> subsubsub{subsub.getSub(1,2)};
    EXPECT_EQ(subsubsub.size(), 1); // Check size
    EXPECT_EQ(subsubsub[0], 6); // Check values
    EXPECT_THROW(subsubsub.at(1), std::out_of_range);
    
}

TEST(CustomVectorTest, RangeBasedForLoop)
{
    // Const test
    ///\todo Modifying a const object here -- some implementation problem
    const BaseVector<int> vec_const(1000);
    std::iota(vec_const.begin(), vec_const.end(), 0);

    int val{0};
    for (const auto & elem : vec_const) {
	EXPECT_EQ(elem, val++);
    }

    // Non-const test
    BaseVector<int> vec(1000);
    std::iota(vec.begin(), vec.end(), 0);

    // Write new values
    for (auto & elem : vec) {
	elem = -1;
    }

    // Check values
    for (const auto & elem : vec) {
	EXPECT_EQ(elem, -1);
    }

    
}

TEST(CustomVectorTest, EmplaceBack)
{
    struct Type
    {
	int a;
	std::string b;
    };

    // Make vector and use emplace to fill it
    BaseVector<Type> vec;
    for (std::size_t n{0}; n < 1000; n++) {
	vec.emplace_back(n, "Test");
    }

    // Check vector values
    for (std::size_t n{0}; n < 1000; n++) {
	EXPECT_EQ(vec[n].a, n);
	EXPECT_EQ(vec[n].b.compare("Test"), 0);
    }
}

TEST(CustomVectorTest, PushBack)
{
    // Make vector and use push_back to fill it
    BaseVector<int> vec;
    for (std::size_t n{0}; n < 1000; n++) {
	vec.push_back(n);
    }

    // Check vector values
    for (std::size_t n{0}; n < 1000; n++) {
	EXPECT_EQ(vec[n], n);
    }
}
