#include "gtest/gtest.h"
#include "qubit-node.hpp"
#include "pattern-node-2.hpp"
#include "pattern-sim.hpp"
#include "node-window.hpp"
#include "make-node.hpp"
#include "cluster-sim.hpp"

using Node = MakeNode<PatternNode2, QubitNode>;

TEST(PatternSimTest, ThreeQubitIdentityPattern)
{
    BaseNodeWindow<Node> win{1,3};
    setAllEdges(win,true);
    
    // Identity pattern
    const std::size_t logical_qubit{0}; // Just an index

    // Set input qubit
    win(0,0).setAsInput();
    
    // Set byproduct operator update rules
    win(0,0).setBypUpdate(logical_qubit, Bits{0,1}); // Add to z
    win(0,0).setXY(0); // Add to X

    win(1,0).setBypUpdate(logical_qubit, Bits{1,0}); // Add to x
    win(1,0).setXY(0); // Add to X

    win(2,0).setAsOutput(logical_qubit); // Set as the output qubit

    qsl::Qubits<qsl::Type::Resize,double> q{1};
    q = patternSim(win,std::move(q));
	
    qsl::Qubits<qsl::Type::Default, double> q_correct{1};
    EXPECT_NEAR(qsl::fubiniStudy(q,q_correct), 0, 1e-7);
}

TEST(PatternSimTest, ArbitraryOneQubitGatePattern)
{
    for (std::size_t n{0}; n < 1000; n++) {

	BaseNodeWindow<Node> win{1,5};
	setAllEdges(win,true);
    
	// Arbitrary one-qubit gate
	const std::size_t logical_qubit{0}; // Just an index

	// Generate random phases
	qsl::Random<double> rand(0,2*M_PI);
	const double xi{rand.getNum()};
	const double eta{rand.getNum()};
	const double zeta{rand.getNum()};

	// Set input qubit
	win(0,0).setAsInput();
	
	// Set byproduct operator and adaptive dependency update rules
	win(0,0).setBypUpdate(logical_qubit, Bits{0,1}); // Add to z
	win(0,0).setXY(0); // Set initial X

	win(1,0).setBypUpdate(logical_qubit, Bits{1,0}); // Add to x
	win(1,0).setXY(-xi, {win(0,0)}); // Set base angle
    
	win(2,0).setBypUpdate(logical_qubit, Bits{0,1}); // Add to x
	win(2,0).setXY(-eta, {win(1,0)}); // Set base angle
    
	win(3,0).setBypUpdate(logical_qubit, Bits{1,0}); // Add to x
	win(3,0).setXY(-zeta, {win(0,0), win(2,0)}); // Set base angle
    
	win(4,0).setAsOutput(logical_qubit); // Set as the output qubit

	qsl::Qubits<qsl::Type::Resize,double> q{1};
	q = patternSim(win,std::move(q));

	qsl::Qubits<qsl::Type::Default, double> q_correct{1};
	q_correct.rotateX(0, xi);
	q_correct.rotateZ(0, eta);
	q_correct.rotateX(0, zeta);
    
	EXPECT_NEAR(qsl::fubiniStudy(q,q_correct), 0, 1e-7);
    }
}

TEST(PatternSimTest, ArbitraryOneQubitGateCutOutPattern)
{
    for (std::size_t n{0}; n < 1000; n++) {

	BaseNodeWindow<Node> win{3,5};
	setAllEdges(win,true);
    
	// Arbitrary one-qubit gate
	const std::size_t logical_qubit{0}; // Just an index

	// Generate random phases
	qsl::Random<double> rand(0,2*M_PI);
	const double xi{rand.getNum()};
	const double eta{rand.getNum()};
	const double zeta{rand.getNum()};

	// Set input qubit
	win(0,1).setAsInput();
	
	// Column 0 
	win(0,1).setBypUpdate(logical_qubit, Bits{0,1}); // Add to z
	win(0,1).setXY(0); // Set initial X
	win(0,0).setBypUpdate(logical_qubit, Bits{0,1});
	win(0,2).setBypUpdate(logical_qubit, Bits{0,1});
    
	win(1,1).setBypUpdate(logical_qubit, Bits{1,0}); // Add to x
	win(1,1).setXY(-xi, {win(0,0), win(0,1), win(0,2)}); // Set base angle
	win(1,0).setBypUpdate(logical_qubit, Bits{1,0});
	win(1,2).setBypUpdate(logical_qubit, Bits{1,0});
    
	win(2,1).setBypUpdate(logical_qubit, Bits{0,1}); // Add to x
	win(2,1).setXY(-eta, {win(1,0), win(1,1), win(1,2)}); // Set base angle
	win(2,0).setBypUpdate(logical_qubit, Bits{0,1});
	win(2,2).setBypUpdate(logical_qubit, Bits{0,1});
    
	win(3,1).setBypUpdate(logical_qubit, Bits{1,0}); // Add to x
	win(3,1).setXY(-zeta, {win(0,0), win(0,1), win(0,2),
			       win(2,0), win(2,1), win(2,2)}); // Set base angle
	win(3,0).setBypUpdate(logical_qubit, Bits{1,0});
	win(3,2).setBypUpdate(logical_qubit, Bits{1,0});
    
	win(4,1).setAsOutput(logical_qubit); // Set as the output qubit
	win(4,0).setBypUpdate(logical_qubit, Bits{0,1});
	win(4,2).setBypUpdate(logical_qubit, Bits{0,1});

	qsl::Qubits<qsl::Type::Resize,double> q{1};
	q = patternSim(win, std::move(q));
    
	qsl::Qubits<qsl::Type::Default, double> q_correct{1};
	q_correct.rotateX(0, xi);
	q_correct.rotateZ(0, eta);
	q_correct.rotateX(0, zeta);
   
	EXPECT_NEAR(qsl::fubiniStudy(q,q_correct), 0, 1e-7);
    }
}

TEST(PatternSimTest, ReducedCNOTGatePattern)
{
    for (std::size_t n{0}; n < 1000; n++) {

	BaseNodeWindow<Node> win{2,7};
	setHorizontalEdges(win,true);
	win.setEdge({3,0},{3,1}, true);
    
	// Arbitrary one-qubit gate
	const std::size_t control{12}; // Just an index
	const std::size_t target{143};

	// Set input qubits
	win(0,0).setAsInput();
	win(0,1).setAsInput();
	
	// Set the bases 
	win(0,1).setX(); // X
	win(1,1).setY(); // Y
	win(2,1).setY(); // Y
	win(3,1).setX(); // X
	win(4,1).setY(); // Y
	win(5,1).setY(); // Y
	win(0,0).setX(); // X
	win(1,0).setX(); // X
	win(2,0).setX(); // X
	win(3,0).setX(); // X
	win(4,0).setX(); // X
	win(5,0).setX(); // X

	const Bits Z{0,1};
	const Bits X{1,0};
    
	// Set the byproduct operators
	// Add constant
	win(0,0).setBypConstant(control, Z);
	// Top row
	win(0,1).setBypUpdate(control, Z);
	win(1,1).setBypUpdate(control, X);
	win(1,1).setBypUpdate(target, X);
	win(2,1).setBypUpdate(control, Z ^ X);
	win(2,1).setBypUpdate(target, X);
	win(3,1).setBypUpdate(control, Z);
	win(4,1).setBypUpdate(control, Z ^ X);
	win(5,1).setBypUpdate(control, X);
	// Bottom row
	win(0,0).setBypUpdate(target, Z);
	win(0,0).setBypUpdate(control, Z);
	win(1,0).setBypUpdate(target, X);
	win(2,0).setBypUpdate(target, Z);
	win(2,0).setBypUpdate(control, Z);
	win(3,0).setBypUpdate(target, X);
	win(4,0).setBypUpdate(target, Z);
	win(5,0).setBypUpdate(target, X);

	win(6,1).setAsOutput(control); // Top row
	win(6,0).setAsOutput(target); // Bottom row

	qsl::Qubits<qsl::Type::Resize,double> q{2};
	q = patternSim(win, std::move(q));

	qsl::Qubits<qsl::Type::Default, double> q_correct{2};
	q_correct.controlNot(1,0);
 	
	EXPECT_NEAR(qsl::fubiniStudy(q,q_correct), 0, 1e-7);
    }
}
