#include "gtest/gtest.h"
#include <iostream>
#include "pathfinding.hpp"
#include "global-bfs.hpp"

using Node = MakeNode<PathNode, DistanceNode>;

TEST(PathfindingTest, Exceptions)
{
    // Error: mismatch between height of pathf and height of win
    {
        Pathfinding<GBFS> pathf{8, false}; // Height 8
	BaseNodeWindow<Node> win{9,20}; // Height 9
	EXPECT_THROW(pathf.emulate(win, 2, 5, Seed<>{}), std::out_of_range);
    }

    // Error: block width is larger than the window width
    {
	Pathfinding<GBFS> pathf{8, false}; // Height 8
	BaseNodeWindow<Node> win{8,20}; // Height 9
	std::size_t block_width{300};
	EXPECT_THROW(pathf.emulate(win, 2, block_width, Seed<>{}),
		     std::out_of_range);
    }
    
    // Error: y-coordinate starting point is not in range
    {
	Pathfinding<GBFS> pathf{8, false}; // Height 8
	BaseNodeWindow<Node> win{8,20}; // Height 9
	std::size_t y_start{300};
	EXPECT_THROW(pathf.emulate(win, y_start, 5, Seed<>{}),
		     std::out_of_range);
    }
}

/**
 * This test checks that the interactive mode works. 
 *
 */
TEST(PathfindingTest, PrintTest)
{
    BaseNodeWindow<Node> win{5,10};
    
    Seed graph_seed{16551164106594522575U};//{0};
    graph_seed.print();
    setRandomEdges(win, 0.80, graph_seed);

    Seed path_seed{1870038693533921390U};//{1};
    path_seed.print();

    const std::size_t y_start{3};
    const std::size_t block_width{7};

    Node last{}; // Will store the final node
    constexpr bool press_enter{false}; // Change to true to step through
    Pathfinding<GBFS, press_enter> pathf{win.height(), true};
    try {
	last = pathf.emulate(win, y_start, block_width, path_seed);
    } catch (const PathTerminated<Node> & e) {
	std::cout << e.what() << std::endl;
	last = e.last();
    }

    // Check the search got to the last column
    EXPECT_EQ(last.x(), 9);

    // Check that the correct length data is returned
    EXPECT_EQ(pathf.json().size(), 9);
    
    
}

TEST(PathfindingTest, NoPathTest)
{
    BaseNodeWindow<Node> win{5,10};
    // No edges in graph
    
    Seed path_seed{1870038693533921390U};//{1};
    path_seed.print();

    const std::size_t y_start{3};
    const std::size_t block_width{7};

    Node last{}; // Will store the final node
    constexpr bool press_enter{false}; // Change to true to step through
    Pathfinding<GBFS, press_enter> pathf{win.height(), true};
    try {
	last = pathf.emulate(win, y_start, block_width, path_seed);
    } catch (const PathTerminated<Node> & e) {
	std::cout << e.what() << std::endl;
	last = e.last();
    }

    // Check the search got to the last column
    EXPECT_EQ(last.x(), 0);

    // Check that the correct length data is returned
    EXPECT_EQ(pathf.json().size(), 0);
}
