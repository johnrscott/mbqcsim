#include "gtest/gtest.h"
#include "node-window.hpp"

using Node = BaseNode;

template<NodeType Node, bool Base>
void checkEmpty(NodeWindow<Node,Base> & win)
{
    // Check all edges are empty
    for (std::size_t x = 0; x < win.width()-1; x++) {
	for (std::size_t y = 0; y < win.height()-1; y++) {

	    // Check the vertical edges
	    EXPECT_EQ(win.aboveEdge(x,y), false);
	    EXPECT_EQ(win.belowEdge(x,y+1), false);

	    // Check the horizontal edges
	    EXPECT_EQ(win.rightEdge(x,y), false);
	    EXPECT_EQ(win.leftEdge(x+1,y), false);
	}
    }
}

TEST(PathUtilsTest, SetPathSegments)
{
    BaseNodeWindow<Node> win{5,5};
    setPath(win, {}, true); // Edge case -- no edges

    checkEmpty(win);
    
    setPath(win, {{0,0},{0,1},{1,1}}, true); // Set two edges
    EXPECT_EQ(win.aboveEdge(0,0), true);
    EXPECT_EQ(win.rightEdge(0,1), true);

    setPath(win, {{0,0},{0,1},{1,1}}, false); // Clear two edges
    EXPECT_EQ(win.aboveEdge(0,0), false);
    EXPECT_EQ(win.rightEdge(0,1), false);

}

// Tests the default constructed NodeWindow
TEST(PathUtilsTest, SetPathSegmentsExceptions)
{
    BaseNodeWindow<Node> win{5,5};

    // Out-of-range edges
    EXPECT_THROW(setPath(win, {{4,4},{4,5}}, true), std::out_of_range);

    // Attempt to set invalid edge (diagonal)
    EXPECT_THROW(setPath(win, {{2,2},{3,3}}, true), std::logic_error);
}

TEST(PathUtilsTest, SetPathDirection)
{
    BaseNodeWindow<Node> win{10,10};
    
    setPath(win, Vec2{1,1}, {Dir::U(1),Dir::R(2),
			     Dir::D(1),Dir::L(1)}, true);
    EXPECT_EQ(win.aboveEdge(1,1), true);
    EXPECT_EQ(win.rightEdge(1,2), true);
    EXPECT_EQ(win.rightEdge(2,2), true);
    EXPECT_EQ(win.belowEdge(3,2), true);
    EXPECT_EQ(win.leftEdge(3,1), true);
}

TEST(PathUtilsTest, SetPathRightAngleStraight)
{
    BaseNodeWindow<Node> win{10,10};

    // Straight horizontal forwards
    setAllEdges(win,false);
    setPath(win, Vec2{1,1}, Vec2{3,1}, true, true);
    EXPECT_EQ(win.rightEdge(1,1), true);
    EXPECT_EQ(win.rightEdge(2,1), true);

    // Straight horizontal backwards
    setAllEdges(win,false);
    setPath(win, Vec2{3,1}, Vec2{1,1}, true, true);
    EXPECT_EQ(win.rightEdge(1,1), true);
    EXPECT_EQ(win.rightEdge(2,1), true);

    // Straight vertical forwards
    setAllEdges(win,false);
    setPath(win, Vec2{1,1}, Vec2{1,3}, true, true);
    EXPECT_EQ(win.aboveEdge(1,1), true);
    EXPECT_EQ(win.aboveEdge(1,2), true);

    // Straight vertical backwards
    setAllEdges(win,false);
    setPath(win, Vec2{1,3}, Vec2{1,1}, true, true);
    EXPECT_EQ(win.aboveEdge(1,1), true);
    EXPECT_EQ(win.aboveEdge(1,2), true);

    // Check empty path
    setAllEdges(win,false);
    setPath(win, Vec2{1,3}, Vec2{1,3}, true, true);
}

TEST(PathUtilsTest, SetPathRightAngleCorner)
{
    BaseNodeWindow<Node> win{10,10};

    // Corner horizontal first
    setAllEdges(win,false);
    setPath(win, Vec2{1,1}, Vec2{3,2}, true, true);
    EXPECT_EQ(win.rightEdge(1,1), true);
    EXPECT_EQ(win.rightEdge(2,1), true);
    EXPECT_EQ(win.aboveEdge(3,1), true);

    // Corner vertical first
    setAllEdges(win,false);
    setPath(win, Vec2{1,1}, Vec2{3,2}, false, true);
    EXPECT_EQ(win.aboveEdge(1,1), true);
    EXPECT_EQ(win.rightEdge(1,2), true);
    EXPECT_EQ(win.rightEdge(2,2), true);
}
