#include "gtest/gtest.h"
#include "node-window.hpp"
#include "path-node.hpp"
#include "distance-node.hpp"
#include "global-bfs.hpp"
#include "pathfinding.hpp"
#include "random.hpp"

using Node = MakeNode<PathNode, DistanceNode>;

/// Pseudo-random (deterministic) 5x5 test window
template<NodeType Node>
BaseNodeWindow<Node> testPattern1()
{
    ///\todo Replace this with the proper seeding method, and rework
    /// out an example for it. In fact, use setRandomEdges instead with
    /// a seed. Then this function can be deleted.
    Random<int,std::mt19937> rnd{0,999,Seed<std::mt19937>{143323}};
    auto rand = [&]() {
		    // Out of 1000. Compared to random number < 1000
		    double cutoff{650};
		    const auto val{rnd()};
		    if (val < cutoff) {
			return true;
		    } else {
			return false;
		    }
		};
    
    BaseNodeWindow<Node> win{10,10};
    for (std::size_t x{0}; x < win.width(); x++) {
	for (std::size_t y{0}; y < win.height(); y++) {
	    if (x != win.width()-1) {
		win.setEdge({x,y}, {x+1,y}, rand());
	    }
	    if (y != win.height()-1) {
		win.setEdge({x,y}, {x,y+1}, rand());
	    }
	}
    }
    return win;
}

// Tests the default constructed NodeWindow
TEST(GlobalBFSTest, CheckDistances)
{
    // Pseudo-random test pattern. This function contains
    // a seed which should not be modified
    auto win{testPattern1<Node>()};
    win.print();
    
    // Perform the search and assign distances
    GBFS gbfs{win.height()};//, win.width()};
    gbfs.search(win, 2);
    
    // True distances (each row is a column)
    std::vector<long> distances{ 6, 5, 0, 1, 4, 5, 6,-1, 8, 9,
				 5, 4, 3, 2, 3, 4, 5, 6, 7, 8,
				 6,-1,10, 5, 4, 5, 6, 7, 8, 9,
				 7, 8, 9, 6, 7, 6, 7, 8, 9,10,
				 8,11,10, 7, 8, 7, 8, 9,10,11,
				11,10, 9, 8, 9, 8, 9,10,11,12,
				12,11,10, 9,10, 9,10,11,12,13,
				13,14,13,12,11,10,11,14,15,-1,
				14,15,14,13,12,11,12,13,14,15,
				15,16,15,14,15,12,13,14,-1,-1};

    for (std::size_t n{0}; n < distances.size(); n++) {
	EXPECT_EQ(win[n].getDistance(), distances[n]);
    }    
}

TEST(GlobalBFSTest, CheckReversePathTrees)
{
    // Pseudo-random test pattern. This function contains
    // a seed which should not be modified
    auto win{testPattern1<Node>()};

    // Perform the search and assign distances
    GBFS gbfs{win.height()};//, win.width()};
    gbfs.search(win, 2);

    // Traverse reverse path trees from every node that is accessible
    for (std::size_t n{0}; n < win.size(); n++) {

	// Check if node is accessible
	if (win[n].getDistance() != -1) {

	    // Traverse predecessors back to root node
	    long distance{win[n].getDistance()};
	    PredecessorIterator node{win[n], win};
	    while (true) {
		
		// Check the distance is correct
		EXPECT_EQ(node->getDistance(), distance);

		// Check for the root node
		if (node->getDistance() == 0) {
		    EXPECT_EQ(node->index(), 2);
		    break;
		}
		
		distance--;
		++node;
	    }
	}
    }    
}

TEST(GlobalBFSTest, EmulatePathfindingRepeatability)
{
    BaseNodeWindow<Node> win{10,10};
    Seed s1;
    setRandomEdges(win, 0.65, s1);
    // Search for paths
    Seed s2;
    Node last{}; // Will store the final node

    try {
	Pathfinding<GBFS> pathf{win.height()};
        last = pathf.emulate(win, 4, 4, s2);
    } catch (const PathTerminated<Node> & e) {
	std::cout << e.what() << std::endl;
	last = e.last();
    }
    win.print<PathNode>();
    
    // Now repeat on another window
    BaseNodeWindow<Node> win2{10,10};
    Seed s3{s1};
    setRandomEdges(win2, 0.65, s3);
    
    // Search for paths
    Seed s4{s2};
    Node last2{}; // Will store the final node
    try {
	Pathfinding<GBFS> pathf{win.height()};
        last2 = pathf.emulate(win2, 4, 4, s4);
    } catch (const PathTerminated<Node> & e) {
    	std::cout << e.what() << std::endl;
    	last2 = e.last();
    }
    win2.print<PathNode>();

    EXPECT_EQ(last, last2);
    
    // Check all the nodes are equal
    for (std::size_t x{0}; x < win.width(); x++) {
	for (std::size_t y{0}; y < win.height(); y++) {
	    EXPECT_EQ(win(x,y), win2(x,y));
	}
    }
}

/// Simple test of function
TEST(GlobalBFSTest, TotalSuccWrites)
{
    EXPECT_THROW(totalSuccWrites(1,0), std::logic_error);
    EXPECT_EQ(totalSuccWrites(1,1), 0); // Only one node, no successors
    EXPECT_EQ(totalSuccWrites(3,1), 4); // Vertical line
    EXPECT_EQ(totalSuccWrites(1,5), 8); // Horizontal line
    EXPECT_EQ(totalSuccWrites(10,5), 4*2 + 8*3*2 + 3*3*2 + 4*8*3); // Block 
}

/**
 * \brief Check that the performance counters give accurate results
 *
 * This is an important test of the validity of the performance counters.
 * The check compares the results of the counters for a particular
 * pseudorandom graph with the hand-calculated counts for the same
 * graph and algorithm. It isn't a demonstration that the counters are
 * always correct (much more extensive testing is needed for that) but
 * it is an important initial step.
 *
 * For each search, the counts can be calculated relatively easily. The
 * distance_writes and pred_writes for a search are equal to each other,
 * and are (the number of accessible nodes + total nodes). The total_nodes
 * contribution is due to the need to reset the search window at the
 * start of the search. The queue_push is equal to the number of accessible
 * nodes only, because the queue starts out empty and finishes empty 
 * after each search.
 *
 * The succ_writes is equal to the sum of the distances of the exit nodes
 * (accessible nodes in the final column), which is equal to the sum of the
 * lengths of the reverse paths from each exit node to the root node.
 *
 */
TEST(GlobalBFSTest, PerformanceCounters)
{
        // Pseudo-random test pattern
    BaseNodeWindow<Node> win{10,10};
    Seed graph_seed{16697734252652770408U};
    setRandomEdges(win, 0.6, graph_seed);
    
    // Perform GBFS pathfinding algorithm
    Seed path_seed{6697734252652770408U};
    Generator<unsigned, 0, 1> path_gen{path_seed};    
    Node last;

    std::size_t y_start{2};
    std::size_t block_width{5};
    
    // Use the global BFS algorithm
    Pathfinding<GBFS> pathf{win.height()};	
    try {
	last = pathf.emulate(win, y_start, block_width, path_seed);
    } catch (const PathTerminated<Node> & p) {
	last = p.last();
    }

    // Store the data from the path emulation (search and path extension)
    auto results = pathf.json();

    // First search, block 0. Block clear takes 50 distance writes and
    // predecessor pushes, then search requires 38 distance writes and
    // predecessor pushes. The number of queue pushes is one for the root
    // node, and then one for every distance write (38 in total).
    EXPECT_EQ(results[0].at("search").at("width"), 5);
    EXPECT_EQ(results[0].at("search").at("height"), 10);
    EXPECT_EQ(results[0].at("search").at("counts").at("distance_writes"), 88);
    EXPECT_EQ(results[0].at("search").at("counts").at("distance_writes"), 88);
    EXPECT_EQ(results[0].at("search").at("counts").at("pred_writes"), 88);
    EXPECT_EQ(results[0].at("search").at("counts").at("queue_push"), 38);

    // First reverse path tree generation, block 0. Total number of
    // successor writes is equal to the sum of all the distances of the
    // exit nodes (accessible nodes in the final column) (40), plus a
    // constant number of writes to clear the window at the start (equal
    // to 170)
    EXPECT_EQ(results[0].at("search").at("counts").at("succ_writes"), 210);    

    // Block 1 search
    EXPECT_EQ(results[1].at("search").at("width"), 5);
    EXPECT_EQ(results[1].at("search").at("height"), 10);
    EXPECT_EQ(results[1].at("search").at("counts").at("distance_writes"), 87);
    EXPECT_EQ(results[1].at("search").at("counts").at("pred_writes"), 87);
    EXPECT_EQ(results[1].at("search").at("counts").at("queue_push"), 37);
    // Block 1 SPT
    EXPECT_EQ(results[1].at("search").at("counts").at("succ_writes"), 230);    

    // Block 2 search
    EXPECT_EQ(results[2].at("search").at("width"), 5);
    EXPECT_EQ(results[2].at("search").at("height"), 10);
    EXPECT_EQ(results[2].at("search").at("counts").at("distance_writes"), 87);
    EXPECT_EQ(results[2].at("search").at("counts").at("pred_writes"), 87);
    EXPECT_EQ(results[2].at("search").at("counts").at("queue_push"), 37);
    // Block 2 SPT
    EXPECT_EQ(results[2].at("search").at("counts").at("succ_writes"), 226);    

    // Block 3 search
    EXPECT_EQ(results[3].at("search").at("width"), 5);
    EXPECT_EQ(results[3].at("search").at("height"), 10);
    EXPECT_EQ(results[3].at("search").at("counts").at("distance_writes"), 86);
    EXPECT_EQ(results[3].at("search").at("counts").at("pred_writes"), 86);
    EXPECT_EQ(results[3].at("search").at("counts").at("queue_push"), 36);
    // Block 3 SPT
    EXPECT_EQ(results[3].at("search").at("counts").at("succ_writes"), 228);    

    // Block 4 search
    EXPECT_EQ(results[4].at("search").at("width"), 5);
    EXPECT_EQ(results[4].at("search").at("height"), 10);
    EXPECT_EQ(results[4].at("search").at("counts").at("distance_writes"), 85);
    EXPECT_EQ(results[4].at("search").at("counts").at("pred_writes"), 85);
    EXPECT_EQ(results[4].at("search").at("counts").at("queue_push"), 35);
    // Block 4 SPT
    EXPECT_EQ(results[4].at("search").at("counts").at("succ_writes"), 228);

    // Block 5 search
    EXPECT_EQ(results[5].at("search").at("width"), 5);
    EXPECT_EQ(results[5].at("search").at("height"), 10);
    EXPECT_EQ(results[5].at("search").at("counts").at("distance_writes"), 84);
    EXPECT_EQ(results[5].at("search").at("counts").at("pred_writes"), 84);
    EXPECT_EQ(results[5].at("search").at("counts").at("queue_push"), 34);
    // Block 5 SPT
    EXPECT_EQ(results[5].at("search").at("counts").at("succ_writes"), 215);

    // Block 6 search
    // First smaller block. 4x10. There are 40 nodes in total, and 132
    // successor write memory locations to clear.
    EXPECT_EQ(results[6].at("search").at("width"), 4);
    EXPECT_EQ(results[6].at("search").at("height"), 10);
    EXPECT_EQ(results[6].at("search").at("counts").at("distance_writes"), 66);
    EXPECT_EQ(results[6].at("search").at("counts").at("pred_writes"), 66);
    EXPECT_EQ(results[6].at("search").at("counts").at("queue_push"), 26);
    // Block 6 SPT
    EXPECT_EQ(results[6].at("search").at("counts").at("succ_writes"), 167);

    // Block 7 search
    // First smaller block. 3x10. There are 30 nodes in total, and 94
    // successor write memory locations to clear.
    EXPECT_EQ(results[7].at("search").at("width"), 3);
    EXPECT_EQ(results[7].at("search").at("height"), 10);
    EXPECT_EQ(results[7].at("search").at("counts").at("distance_writes"), 41);
    EXPECT_EQ(results[7].at("search").at("counts").at("pred_writes"), 41);
    EXPECT_EQ(results[7].at("search").at("counts").at("queue_push"), 11);
    // Block 6 SPT
    EXPECT_EQ(results[7].at("search").at("counts").at("succ_writes"), 108);

    // Block 8 search
    // First smaller block. 2x10. There are 20 nodes in total, and 56
    // successor write memory locations to clear.
    EXPECT_EQ(results[8].at("search").at("width"), 2);
    EXPECT_EQ(results[8].at("search").at("height"), 10);
    EXPECT_EQ(results[8].at("search").at("counts").at("distance_writes"), 27);
    EXPECT_EQ(results[8].at("search").at("counts").at("pred_writes"), 27);
    EXPECT_EQ(results[8].at("search").at("counts").at("queue_push"), 7);
    // Block 6 SPT
    EXPECT_EQ(results[8].at("search").at("counts").at("succ_writes"), 67);
}
