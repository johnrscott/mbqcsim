#include "gtest/gtest.h"
#include "distance-node.hpp"
#include "node-window.hpp"
#include "make-node.hpp"
#include <random>
#include <algorithm>

// Path node type
using Node = MakeNode<DistanceNode>;

// Tests the default DistanceNode object
TEST(DistanceNodeTest, DefaultObject)
{
    // Make a default object and check properties
    std::size_t index{20};
    std::size_t height{7};
    Node node{index,height};
    
    EXPECT_EQ(node.getDistance(), -1);
    EXPECT_EQ(node.getPredecessorOffset(), 0);
}

TEST(DistanceNodeTest, Predecessors)
{
    // Make a default object and check properties
    std::size_t height{5};

    std::vector<Node> nodes;
    for (std::size_t n{0}; n < 20; n++) {
	nodes.emplace_back(n,height);
    }

    // Add some predecessors
    nodes[0].setRoot();
    nodes[1].setPredecessor(nodes[0]);
    nodes[2].setPredecessor(nodes[1]);
    nodes[3].setPredecessor(nodes[2]);

    // Check distances
    EXPECT_EQ(nodes[0].getDistance(), 0);
    EXPECT_EQ(nodes[1].getDistance(), 1);
    EXPECT_EQ(nodes[2].getDistance(), 2);
    EXPECT_EQ(nodes[3].getDistance(), 3);

    // Check predecessors
    EXPECT_EQ(nodes[0].isPredecessor(nodes[1]), 1);    
    EXPECT_EQ(nodes[1].isPredecessor(nodes[0]), -1);    
    EXPECT_EQ(nodes[1].isPredecessor(nodes[2]), 1);    
    EXPECT_EQ(nodes[2].isPredecessor(nodes[1]), -1);    
    EXPECT_EQ(nodes[2].isPredecessor(nodes[3]), 1);    
    EXPECT_EQ(nodes[3].isPredecessor(nodes[2]), -1);
}

TEST(DistanceNodeTest, AddGetSuccessors)
{
    BaseNodeWindow<Node> win{10,10};
    setAllEdges(win, true);

    // Add successors in a square pattern and check
    win(0,0).addSuccessor(win(0,1));
    EXPECT_EQ(win(0,1).isSuccessor(win(0,0)), +1);
    EXPECT_EQ(win(0,0).isSuccessor(win(0,1)), -1);
    EXPECT_EQ(win(0,0).getSuccessorOffsets(), std::set<int>{1});
    win(0,1).addSuccessor(win(1,1));
    EXPECT_EQ(win(1,1).isSuccessor(win(0,1)), +1);
    EXPECT_EQ(win(0,1).isSuccessor(win(1,1)), -1);
    EXPECT_EQ(win(0,1).getSuccessorOffsets(), std::set<int>{10});
    win(1,1).addSuccessor(win(1,0));
    EXPECT_EQ(win(1,0).isSuccessor(win(1,1)), +1);
    EXPECT_EQ(win(1,1).isSuccessor(win(1,0)), -1);
    EXPECT_EQ(win(1,1).getSuccessorOffsets(), std::set<int>{-1});
    win(1,0).addSuccessor(win(0,0));
    EXPECT_EQ(win(0,0).isSuccessor(win(1,0)), +1);
    EXPECT_EQ(win(1,0).isSuccessor(win(0,0)), -1);
    EXPECT_EQ(win(1,0).getSuccessorOffsets(), std::set<int>{-10});

    // Add a branch from (1,1)
    win(1,1).addSuccessor(win(2,1));
    EXPECT_EQ(win(1,1).getSuccessorOffsets(), (std::set<int>{-1,10}));
    
    win.print<DistanceNode>();
}

TEST(DistanceNodeTest, IsSuccessors)
{
    BaseNodeWindow<Node> win{10,10};
    setAllEdges(win, true);

    // Add successors in a square pattern and check
    win(0,0).addSuccessor(win(0,1));
    EXPECT_EQ(win(0,1).isSuccessor(win(0,0)), +1);
    EXPECT_EQ(win(0,0).isSuccessor(win(0,1)), -1);
    win(0,1).addSuccessor(win(1,1));
    EXPECT_EQ(win(1,1).isSuccessor(win(0,1)), +1);
    EXPECT_EQ(win(0,1).isSuccessor(win(1,1)), -1);
    win(1,1).addSuccessor(win(1,0));
    EXPECT_EQ(win(1,0).isSuccessor(win(1,1)), +1);
    EXPECT_EQ(win(1,1).isSuccessor(win(1,0)), -1);
    win(1,0).addSuccessor(win(0,0));
    EXPECT_EQ(win(0,0).isSuccessor(win(1,0)), +1);
    EXPECT_EQ(win(1,0).isSuccessor(win(0,0)), -1);

    // Add a branch from (1,1)
    win(1,1).addSuccessor(win(2,1));
    EXPECT_EQ(win(2,1).isSuccessor(win(1,1)), +1);
    EXPECT_EQ(win(1,1).isSuccessor(win(2,1)), -1);
    
    win.print<DistanceNode>();
}




