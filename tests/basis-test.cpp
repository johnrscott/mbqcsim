#include "gtest/gtest.h"
#include "basis.hpp"

TEST(BasisTest, MeasureZ)
{
    // Check the object has the right label
    EXPECT_EQ(MeasureZ.label(), 'Z');

    // Check object prints correctly
    std::stringstream ss;
    MeasureZ.print(ss);
    EXPECT_EQ(ss.str(), std::string("(0,0)"));
}

TEST(BasisTest, MeasureXY)
{
    // Check the object has the right label
    EXPECT_EQ(MeasureXY{0.5}.label(), 'B');

    // Check object prints correctly
    std::stringstream ss;
    MeasureXY{0.5}.print(ss);
    EXPECT_EQ(ss.str(), std::string("XY(0.5)"));
}
