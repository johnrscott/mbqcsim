#include "gtest/gtest.h"
#include "path-node.hpp"
#include "make-node.hpp"
#include "node-window.hpp"
#include "path-sim.hpp"
#include "global-bfs.hpp"
#include "iterative-bfs.hpp"
#include "pathfinding.hpp"

#include <random>
#include <algorithm>

// Path node type
using Node = MakeNode<PathNode,DistanceNode>;

// Tests the default PathNode object
TEST(PathNodeTest, DefaultObject)
{
    // Make a default object and check properties
    std::size_t index{20};
    std::size_t height{7};
    Node node{index,height};
    EXPECT_EQ(node.getPathSuccessorOffset(), 0);
    EXPECT_EQ(node.getPathPredecessorOffset(), 0);
    EXPECT_EQ(node.onPath(), false);
    EXPECT_THROW(node.getPathIndex(), std::logic_error);
    EXPECT_EQ(node.right(), 0);
    EXPECT_EQ(node.left(), 0);
}

// Tests path successors and path predecessors between PathNodes
TEST(PathNodeTest, LongPathTest)
{
    // Create a vector of random index nodes
    const std::size_t height{10};
    const std::size_t test_length{1000};

    // Make a random node generator
    //std::random_device rd;
    //std::mt19937 engine{rd()};
    //std::uniform_int_distribution<std::size_t> dist{0,10000};
    //Random<std::size_t> dist{0,10000};
    Generator<std::size_t, 0, 10000> rnd{Seed<>{}};
    
    std::vector<std::size_t> indices(test_length);
    std::iota(indices.begin(), indices.end(), 0); // Fill with 0,1,..., test_length
    std::ranges::shuffle(indices, rnd); // Shuffle the values
    
    std::vector<Node> nodes;
    for (std::size_t n{0}; n < test_length; n++) {
	nodes.emplace_back(indices[n], height);
    }
    
    // Make the nodes successors of one another
    for (auto it = std::ranges::begin(nodes); it != std::ranges::end(nodes)-1;
	 ++it) {
	it->setPathSuccessor(*(it+1));
    }
    
    // Check that all the nodes are on the path, with the correct path index
    std::size_t path_index{0};
    for (const auto & node : nodes) {
	EXPECT_EQ(node.onPath(), true);
	EXPECT_EQ(node.getPathIndex(), path_index++);
    }

    
    // Now check that all the successors are correct
    for (auto it = std::ranges::begin(nodes); it != std::ranges::end(nodes)-1;
	 ++it) {

	// Check the offset is correct
	const long n1{static_cast<long>(it->index())};
	const long n2{static_cast<long>((it+1)->index())};
	const long succ{it->getPathSuccessorOffset()};
	EXPECT_EQ(succ, n2-n1);

	// Check the nodes are adjacent
	EXPECT_EQ(it->isAdjacent(*(it+1)), 1);
    }    

    // Now check that all the predecessors are correct
    for (auto it = std::ranges::rbegin(nodes); it != std::ranges::rend(nodes)-1;
	 ++it) {

	// Check the offset is correct
	const long n1{static_cast<long>(it->index())};
	const long n2{static_cast<long>((it+1)->index())};
	const long pred{it->getPathPredecessorOffset()};
	EXPECT_EQ(pred, n2-n1);

	// Check the nodes are adjacent
	EXPECT_EQ(it->isAdjacent(*(it+1)), -1);

    }

    // Now check that the edge cases
    EXPECT_EQ(std::ranges::rbegin(nodes)->getPathSuccessorOffset(), 0);
    EXPECT_EQ(std::ranges::begin(nodes)->getPathPredecessorOffset(), 0);
}

TEST(PathNodeTest, PathAdjacentToSelf)
{
    const std::size_t index{23};
    const std::size_t height{3};
    Node node{index,height};
    EXPECT_THROW(node.isAdjacent(node), std::logic_error);
}

TEST(PathNodeTest, SetSuccessorToSelf)
{
    const std::size_t index{23};
    const std::size_t height{3};
    Node node{index,height};
    EXPECT_THROW(node.setPathSuccessor(node), std::logic_error);
}

TEST(PathNodeTest, NotPathAdjacent)
{
    const std::size_t index{23};
    const std::size_t height{3};
    Node n1{index,height};
    Node n2{index+1,height};
    Node n3{index+2,height};
    EXPECT_EQ(n1.isAdjacent(n2), 0);
    EXPECT_EQ(n2.isAdjacent(n3), 0);
    EXPECT_EQ(n1.isAdjacent(n3), 0);
}

TEST(PathNodeTest, RightNode)
{
    // Make node and check it is not a right node
    const std::size_t index{23};
    const std::size_t height{3};
    Node node{index,height};
    EXPECT_EQ(node.right(), 0);

    // Make and check regular right node
    node.setRight(false);
    EXPECT_EQ(node.right(), 1);

    // Make and check minimal right node
    node.setRight(true);
    EXPECT_EQ(node.right(), 2);

    // Clear right node
    node.clearRight();
    EXPECT_EQ(node.right(), 0);
}

TEST(PathNodeTest, LeftNode)
{
    // Make node and check it is not a left node
    const std::size_t index{23};
    const std::size_t height{3};
    Node node{index,height};
    EXPECT_EQ(node.left(), 0);

    // Make and check regular left node
    node.setLeft(false);
    EXPECT_EQ(node.left(), 1);

    // Make and check maximal left node
    node.setLeft(true);
    EXPECT_EQ(node.left(), 2);

    // Clear left node
    node.clearLeft();
    EXPECT_EQ(node.left(), 0);
}

TEST(PathNodeTest, SubNodeWindows)
{
    BaseNodeWindow<Node> win{6,20};
    setAllEdges(win,true);

    // Create a path
    win(0,0).setPathSuccessor(win(0,1));
    win(0,1).setPathSuccessor(win(1,1));
    win(1,1).setPathSuccessor(win(2,1));
    win(2,1).setPathSuccessor(win(3,1));
    win(3,1).setPathSuccessor(win(4,1));
    win(4,1).setPathSuccessor(win(4,2));
    win(4,2).setPathSuccessor(win(3,2));
    win(3,2).setPathSuccessor(win(2,2));
    win(2,2).setPathSuccessor(win(1,2));
    win(1,2).setPathSuccessor(win(1,3));
    
    // Make a subwindow
    auto sub{win.sub(2,6,3)};

    // Check that the correct nodes have predecessors
    EXPECT_EQ(sub(-1,1).isAdjacent(sub(0,1)), 1);
    EXPECT_EQ(sub(0,1).isAdjacent(sub(1,1)), 1);
    EXPECT_EQ(sub(1,1).isAdjacent(sub(1,2)), 1);
    EXPECT_EQ(sub(1,2).isAdjacent(sub(0,2)), 1);
    EXPECT_EQ(sub(0,2).isAdjacent(sub(-1,2)), 1);
}

TEST(PathNodeTest, LeftRightNodesGBFS)
{
    BaseNodeWindow<Node> win{8,17};
    setCheckerSnakePattern(win, 2, 3, 3, 1);

    std::vector<Vec2> maxLeft{{ 0,0},{ 1,0},{ 2,0},{ 3,7},
			      { 4,7},{ 5,7},{ 6,7},{ 7,0},
			      { 8,0},{ 9,0},{10,0},{11,7},
			      {12,7},{13,7},{14,7},{15,0}};
    std::vector<Vec2> minRight{{ 1,7},{ 2,4},{ 3,7},{ 4,7},
			       { 5,0},{ 6,3},{ 7,0},{ 8,0},
			       { 9,7},{10,4},{11,7},{12,7},
			       {13,0},{14,3},{15,0},{16,0}};

    // Create a generator for use with getNextNode
    Seed path_seed;
    Generator<unsigned, 0, 1> path_gen{path_seed};
    
    std::size_t y{0};
    GBFS gbfs{win.height()};
    try {
	for (std::size_t x{0}; x < win.width()-1; x++) {
	    auto sub{win.sub(x,win.width(),x)};
	    gbfs.search(sub, y);
	    gbfs.makeSPT(sub);
	    PathExtension path;
	    y = path.extend(std::move(sub), y, path_gen);
	}
    } catch (const std::logic_error &) {
	// This is thrown when no more path successors can be
	// found
    }
    
    writeLeftNodes(win.sub(0,win.width(),0), win(0,0));
    std::cout << "Writing left nodes..." << std::endl;

    win.print<PathNode>();

    for (const auto & xy : maxLeft) {
	EXPECT_EQ(win(xy.x(),xy.y()).left(), 2); 
    }

    for (const auto & xy : minRight) {
	EXPECT_EQ(win(xy.x(),xy.y()).right(), 2); 
    }

}

TEST(PathNodeTest, LeftRightNodesIBFS)
{
    BaseNodeWindow<Node> win{8,17};
    setCheckerSnakePattern(win, 2, 3, 3, 1);

    std::vector<Vec2> maxLeft{{ 0,0},{ 1,0},{ 2,0},{ 3,7},
			      { 4,7},{ 5,7},{ 6,7},{ 7,0},
			      { 8,0},{ 9,0},{10,0},{11,7},
			      {12,7},{13,7},{14,7},{15,0}};
    std::vector<Vec2> minRight{{ 1,7},{ 2,4},{ 3,7},{ 4,7},
			       { 5,0},{ 6,3},{ 7,0},{ 8,0},
			       { 9,7},{10,4},{11,7},{12,7},
			       {13,0},{14,3},{15,0},{16,0}};

    // Create a generator for use with getNextNode
    Seed path_seed;
    Generator<unsigned, 0, 1> path_gen{path_seed};
    
    std::size_t y{0};
    IBFS search{win.height()};//, sub.width()};
    try {
	for (std::size_t x{0}; x < win.width()-1; x++) {
	    auto sub{win.sub(x,win.width(),x)};
	    search.search(sub, y);
	    search.makeSPT(sub);
	    PathExtension path;
	    y = path.extend(std::move(sub), y, path_gen);
	}
    } catch (const std::logic_error &) {
	// This is thrown when no more path successors can be
	// found
    }
    
    writeLeftNodes(win.sub(0,win.width(),0), win(0,0));
    std::cout << "Writing left nodes..." << std::endl;

    win.print<PathNode>();

    for (const auto & xy : maxLeft) {
	EXPECT_EQ(win(xy.x(),xy.y()).left(), 2); 
    }

    for (const auto & xy : minRight) {
	EXPECT_EQ(win(xy.x(),xy.y()).right(), 2); 
    }

}
