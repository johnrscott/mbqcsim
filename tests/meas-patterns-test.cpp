#include "gtest/gtest.h"
#include "qubit-node.hpp"
#include "pattern-node-2.hpp"
#include "pattern-sim.hpp"
#include "node-window.hpp"
#include "make-node.hpp"
#include "cluster-sim.hpp"
#include "meas-patterns.hpp"

using Node = MakeNode<PatternNode2, QubitNode>;

TEST(PatternSimTest, IdentityPattern)
{
    std::vector<std::tuple<std::size_t, std::size_t, std::size_t>> cases{
	{9,9,8}, {3,3,0}, {3,3,1}, {3,3,2}, {8,21,2}, 
    };

    for (const auto & [num_qubits, width, height] : cases) {
    
	auto win{identityPattern(num_qubits, width, height)};
	qsl::Qubits<qsl::Type::Resize,double> q{1};

	// Set arbitrary initial state
	q.rotateX(0, 1.2);
	q.rotateZ(0, -3.2);
	q.rotateX(0, 4.9);

	// Copy the state for checking
	auto q_copy{q};
    
	// Perform the gate using a measurement pattern
	q = patternSim(win, std::move(q));

	// Now perform the same gate on the copy using an ordinary simulator
	// Nothing to do
    
	// Check states are the same
	EXPECT_NEAR(qsl::fubiniStudy(q,q_copy), 0, 1e-7);
    }
}

TEST(PatternSimTest, IdentityPatternExceptions)
{
    // Height too big (memory problem, too many qubits)
    EXPECT_THROW(identityPattern(20,9,8), std::out_of_range);

    // Qubit position out of range
    EXPECT_THROW(identityPattern(10,9,20), std::out_of_range);

    // Can't make identity pattern with even width
    EXPECT_THROW(identityPattern(3,10,20), std::logic_error);
}
