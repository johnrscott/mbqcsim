#include "gtest/gtest.h"
#include "make-node.hpp"
#include "pattern-node-2.hpp"
#include "qubit-node.hpp"
#include "node-window.hpp"

using Node = MakeNode<PatternNode2, QubitNode>;

TEST(PatternNode2Test, BypRuleUpdateExceptions)
{
    Node node{3,4};
    std::map<std::size_t,Bits> byp{{0, Bits{0,1}}};

    // Check out-of-range byproduct rule update
    const std::size_t qubit_index{3};
    node.setBypUpdate(qubit_index, Bits{0,1});
    EXPECT_THROW(node.updateByp(byp, 1), std::out_of_range);
}


TEST(PatternNode2Test, BypConstantUpdateExceptions)
{
    Node node{3,4};
    std::map<std::size_t,Bits> byp{{0, Bits{0,1}}};
    
    // Check out-of-range byproduct rule update
    const std::size_t qubit_index{3};
    node.setBypConstant(qubit_index, Bits{0,1});
    EXPECT_THROW(node.updateByp(byp, 1), std::out_of_range);    
}

TEST(PatternNode2Test, OutputQubitExceptions)
{
    Node node{3,4};

    // Error: Cannot get logical qubit index unless node is marked
    // as an output qubit
    EXPECT_THROW(node.getLogicalQubitIndex(), std::logic_error);
}

TEST(PatternNode2Test, DependencyExceptions)
{
    Node node{3,4};

    // Error: cannot set same node as measurement dependency
    EXPECT_THROW(node.setXY(2.1, {node}), std::logic_error);
}

TEST(PatternNode2Test, GetAngleExceptions)
{
    BaseNodeWindow<Node> win{10,10};
    
    // Error: cannot get angle for node specified for the Z-basis
    EXPECT_THROW(win(1,1).getAngle(win), std::logic_error);
}

TEST(PatternNode2Test, MarkerColours)
{
    BaseNodeWindow<Node> win{10,10};

    // Check default marker does not return a colour
    EXPECT_EQ(win(0,1).markerColour(), "");

    // Check red when the marker is an output node
    win(0,1).setAsOutput(0);
    EXPECT_EQ(win(0,1).markerColour(), Colour::RED+Colour::BOLD);

    // Check purple when the marker is an input and output node
    win(0,1).setAsInput();
    EXPECT_EQ(win(0,1).markerColour(), Colour::PURPLE+Colour::BOLD);

    // Check purple when the marker is an input node
    win(1,1).setAsInput();
    EXPECT_EQ(win(1,1).markerColour(), Colour::BLUE+Colour::BOLD);

    // Check yellow when node has adaptive measurement settings
    win(2,1).setXY(0.5, {win(3,4)});
    EXPECT_EQ(win(2,1).markerColour(), Colour::YELLOW+Colour::BOLD);

    // Check green when node also has byproduct operators
    win(2,1).setBypUpdate(0, Bits{1,1});
    EXPECT_EQ(win(2,1).markerColour(), Colour::GREEN+Colour::BOLD);

    // Check cyan when node has only byproduct operators
    win(2,2).setBypUpdate(0, Bits{1,1});
    EXPECT_EQ(win(2,2).markerColour(), Colour::CYAN+Colour::BOLD);

}



