/**
 * \file bits.cpp
 * \brief Tests for the Bits class
 *
 */
#include <gtest/gtest.h>
#include "bits.hpp"
#include <sstream>

TEST(Bits, EmptyConstructor)
{
    // Check the default constructor
    Bits empty;
    EXPECT_EQ(empty.val(), 0);
    std::stringstream ss;
    ss << empty;
    EXPECT_EQ(ss.str(), "");
    EXPECT_EQ(empty.size(), 0);
}

TEST(Bits, InitListConstructor)
{
    // Check the default constructor
    Bits bits{0,1,1,0,1,0};
    EXPECT_EQ(bits.val(), 0b011010);

    // Check the exception for invalid bit
    EXPECT_THROW((Bits{0,1,0,0,2}), std::out_of_range);
}


TEST(Bits, ReadBits)
{
    // Do a simple test
    Bits bits(3, 3);
    std::stringstream ss;
    bits.print(ss);
    EXPECT_EQ(ss.str(), "011");
    EXPECT_EQ(bits.val(), 3);
    EXPECT_EQ(bits(0), 1);
    EXPECT_EQ(bits(1), 1);
    EXPECT_EQ(bits(2), 0);
    EXPECT_THROW(bits(3), std::out_of_range);
}

TEST(Bits, ConstReadBits)
{
    // Check the const context read exception
    const Bits const_bits(0b101011011, 5);
    EXPECT_EQ(const_bits(0), 1);
    EXPECT_EQ(const_bits(2), 0);
    EXPECT_THROW(const_bits(10), std::out_of_range);
}

TEST(Bits, PushBit)
{
    Bits bits;
    EXPECT_EQ(bits.val(), 0);

    bits.pushBit(1);
    EXPECT_EQ(bits.val(), 0b1);    

    bits.pushBit(1);
    EXPECT_EQ(bits.val(), 0b11);    

    bits.pushBit(0);
    EXPECT_EQ(bits.val(), 0b11);    
    
    bits.pushBit(1);
    EXPECT_EQ(bits.val(), 0b1011);    

    // Check out of range
    EXPECT_THROW(bits.pushBit(2), std::out_of_range);
    EXPECT_THROW(bits.pushBit(-1), std::out_of_range);   
}

TEST(Bits, ModifyBits)
{
    // Check that the bits can be modified
    Bits bits(0b011, 3);
    bits(2) = 1;
    std::stringstream ss;//ss.str(std::string());
    ss << bits;
    EXPECT_EQ(ss.str(), "111");
}

TEST(Bits, XorEquals)
{
    // Test the xor equals
    Bits more_bits(0b101, 3);
    Bits bits(0b111, 3);
    bits ^= more_bits;
    EXPECT_EQ(bits.val(), 0b010);

    // Test xor equals with literal
    EXPECT_EQ((bits ^= 0b010).val(), 0b000);

    // Test xor operation between two bitstrings
    Bits a(0b10101, 5);
    Bits b(0b11000, 5);
    EXPECT_EQ((a ^ b).val(), 0b01101);
}

TEST(Bits, AndEquals)
{
    // Test the and equals
    Bits more_bits(0b101, 3);
    Bits bits(0b111, 3);
    bits &= more_bits;
    EXPECT_EQ(bits.val(), 0b101);

    // Test and equals with literal
    //EXPECT_EQ((bits &= 0b010).val(), 0b000);

    // Test and operation between two bitstrings
    Bits a(0b10101, 5);
    Bits b(0b11000, 5);
    EXPECT_EQ((a & b).val(), 0b10000);
}


TEST(Bits, Concaternation)
{
    // Check concaternation of two bitstrings
    EXPECT_EQ((Bits(0b111, 3) + Bits(0b01, 2) + Bits(0b0, 1)).val(), 0b111010);
}

TEST(Bits, SetBits)
{
    // Check assignment operator
    Bits a;
    a.set(0b00100, 5);
    EXPECT_EQ(a.val(), 0b00100);
}

TEST(Bits, XorAll)
{
    // Check that xoring all the bits together works
    EXPECT_EQ(Bits(0b10101, 6).xorBits(), 1);
    EXPECT_EQ(Bits(0b10100, 6).xorBits(), 0);
    EXPECT_EQ(Bits(0b1, 1).xorBits(), 1);
}

TEST(Bits, BitRange)
{
    // Check that substring function works
    Bits c(0b1100101101, 10);
    EXPECT_EQ((c(4,0).val()), 0b01101);

    // Check out of bounds
    EXPECT_THROW(c(4,5), std::out_of_range);
    EXPECT_THROW(c(10,5), std::out_of_range);
}

