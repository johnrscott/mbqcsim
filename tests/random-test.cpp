#include "gtest/gtest.h"
#include "random.hpp"
#include <list>

template <typename T>
class RandomTest : public testing::Test {
public:
    using List = std::list<T>;
    static T shared_;
    T value_;
};

using Types = ::testing::Types<int, long int, std::size_t,
			       float, double, long double>;

TYPED_TEST_SUITE(RandomTest, Types);

TYPED_TEST(RandomTest, IntConstructor)
{
    using T = TypeParam;
    constexpr T min{static_cast<T>(10.4)}; // These will round to ints
    constexpr T max{static_cast<T>(22.45)};

    Random<TypeParam> rnd{min,max,Seed{}};
    EXPECT_EQ(rnd.min(), min);
    EXPECT_EQ(rnd.max(), max);

    EXPECT_THROW((Random<TypeParam>{max,min,Seed{}}), std::logic_error);

    // Check static generator
    if constexpr (std::is_integral_v<T>) {
	Generator<T, min, max> rnd2{Seed{}};
	EXPECT_EQ(rnd2.min(), min);
	EXPECT_EQ(rnd2.max(), max);
	EXPECT_THROW((Generator<TypeParam, max, min>{Seed{}}), std::logic_error);
    }

}

TYPED_TEST(RandomTest, SeedTest)
{
    using T = TypeParam;
    constexpr T min{static_cast<T>(10.4)}; // These will round to ints
    constexpr T max{static_cast<T>(22.45)};

    using Rnd = Random<TypeParam>;

    // Set the seed
    constexpr Seed seed{14324234};
    Rnd rnd{min,max,seed};

    // Check the seed is the same
    EXPECT_EQ(rnd.seed(), seed.seed());	

    // Check static generator
    if constexpr (std::is_integral_v<T>) {

	using Gen = Generator<T, min, max>;
	Gen rnd2{seed};

	// Check the seed is the same
	EXPECT_EQ(rnd2.seed(), seed.seed());
    }
}

/// Check the same seed gives the same random sequence
TYPED_TEST(RandomTest, SeedSequence)
{
    using T = TypeParam;
    T min{static_cast<T>(10.4)}; // These will round to ints
    T max{static_cast<T>(22.45)};

    using Rnd = Random<TypeParam>;
    Rnd rnd{min,max,Seed{}};

    // Store the sequence
    std::vector<T> seq;
    for (std::size_t n{0}; n < 10000; n++) {
	seq.push_back(rnd());
    }

    // Reseed with the same seed
    rnd.seed(Seed{rnd.seed()});

    // Check the same sequence is generated
    for (const auto val : seq) {
	EXPECT_NEAR(val, rnd(), 1e-12);
    }
}

/// Check the samples are in range
TYPED_TEST(RandomTest, RangeTest)
{
    using T = TypeParam;
    T min{static_cast<T>(10.4)}; // These will round to ints
    T max{static_cast<T>(22.45)};

    using Rnd = Random<TypeParam>;
    Rnd rnd{min,max,Seed{}};
    
    // Check the samples are in range
    for (std::size_t n{0}; n < 10000; n++) {
	EXPECT_EQ(rnd() <= max, true);
	EXPECT_EQ(rnd() >= min, true);
    }

    // Change seed
    rnd.seed(Seed{1231232123});

    // Check range again
    for (std::size_t n{0}; n < 10000; n++) {
	EXPECT_EQ(rnd() <= max, true);
	EXPECT_EQ(rnd() >= min, true);
    }
}

