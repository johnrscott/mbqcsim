#include "gtest/gtest.h"
#include "coord-arith.hpp"

TEST(CoordArithTest, DefaultConstructor)
{
    Vec2 vec;
    EXPECT_EQ(vec.x(), 0);
    EXPECT_EQ(vec.y(), 0);
}

TEST(CoordArithTest, PrintTest)
{
    Vec2 vec{0,-5};
    EXPECT_EQ(vec.str(), std::string("(0,-5)"));
}

TEST(CoordArithTest, SettingMembers)
{
    Vec2 vec{2,3};
    EXPECT_EQ(vec.x(), 2);
    EXPECT_EQ(vec.y(), 3);

    vec.x() = 7;
    vec.y() = -10;
    EXPECT_EQ(vec.x(), 7);
    EXPECT_EQ(vec.y(), -10);
}

TEST(CoordArithTest, VectorAdd)
{
    Vec2 v1{2,3};
    Vec2 v2{-12,5};
    EXPECT_EQ((v1+v2).x(), -10);
    EXPECT_EQ((v1+v2).y(), 8);
}

TEST(CoordArithTest, VectorSub)
{
    Vec2 v1{2,3};
    Vec2 v2{-12,5};
    EXPECT_EQ((v1-v2).x(), 14);
    EXPECT_EQ((v1-v2).y(), -2);
}

TEST(CoordArithTest, VectorScalarMult)
{
    Vec2 v1{2,3};
    EXPECT_EQ((-4*v1).x(), -8);
    EXPECT_EQ((-4*v1).y(), -12);
}

TEST(CoordArithTest, VectorScalarDiv)
{
    Vec2 v1{24,12};
    EXPECT_EQ((v1/12).x(), 2);
    EXPECT_EQ((v1/12).y(), 1);
    EXPECT_EQ((v1/5).x(), 4);
    EXPECT_EQ((v1/5).y(), 2);
}

TEST(CoordArithTest, VectorAbs)
{
    Vec2 v1{24,-12};
    auto v2{abs(v1)};
    EXPECT_EQ(v2.x(), 24);
    EXPECT_EQ(v2.y(), 12);

    Vec2 v3{-124,12};
    auto v4{abs(v3)};
    EXPECT_EQ(v4.x(), 124);
    EXPECT_EQ(v4.y(), 12);

}




