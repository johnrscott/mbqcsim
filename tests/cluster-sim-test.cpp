#include "gtest/gtest.h"
#include "qubit-node.hpp"
#include "node-window.hpp"
#include "make-node.hpp"
#include "cluster-sim.hpp"

using Node = MakeNode<QubitNode>;

TEST(ClusterSimTest, Constructor)
{
    Seed sim_seed;
    ClusterSim sim{4, sim_seed}; // Height = 4, no columns
    EXPECT_EQ(sim.state().getNumQubits(), 0);
    EXPECT_EQ(sim.size(), 0);
    EXPECT_EQ(sim.height(), 4);
}

/// Check the state resulting from valid uses of addColumn
TEST(ClusterSimTest, AddColumnNoEdges)
{
    Seed sim_seed;
    ClusterSim sim{4, sim_seed}; // Height = 4, no columns    

    // Window with five columns, no edges
    BaseNodeWindow<Node> win{4,5};

    // Add the first column and check state
    sim.addColumn(win.sub(0,1,0));
    qsl::Qubits<qsl::Type::Resize, double> q{4};
    for (std::size_t n{0}; n < q.getNumQubits(); n++) {
	q.hadamard(n);
    }
    EXPECT_NEAR(qsl::fubiniStudy(sim.state(), q), 0, 1e-11);

    // Add the second column and check state
    sim.addColumn(win.sub(0,2,1)); // Remember to index columns from -1
    q = qsl::Qubits<qsl::Type::Resize, double>{8};
    for (std::size_t n{0}; n < q.getNumQubits(); n++) {
	q.hadamard(n);
    }
    EXPECT_NEAR(qsl::fubiniStudy(sim.state(), q), 0, 1e-11);
}

/// Check the state resulting from addColumn with vertical edges
TEST(ClusterSimTest, AddColumnVerticalEdges)
{
    Seed sim_seed;
    ClusterSim sim{4, sim_seed}; // Height = 4, no columns    

    // Window with one three columns
    BaseNodeWindow<Node> win{4,3};
    setVerticalEdges(win, true);
    
    // Add the first column and check state
    sim.addColumn(win.sub(0,1,0));
    qsl::Qubits<qsl::Type::Resize, double> q{4};
    for (std::size_t n{0}; n < q.getNumQubits(); n++) {
	q.hadamard(n);
    }
    for (std::size_t n{0}; n < q.getNumQubits()-1; n++) {
    	q.controlZ(n,n+1);
    }    
    EXPECT_NEAR(qsl::fubiniStudy(sim.state(), q), 0, 1e-11);

    // Add another column and check state
    sim.addColumn(win.sub(0,2,1));
    q = qsl::Qubits<qsl::Type::Resize, double>{8};
    for (std::size_t n{0}; n < q.getNumQubits(); n++) {
	q.hadamard(n);
    }
    const std::size_t N{q.getNumQubits()/2};
    for (std::size_t n{0}; n < N-1; n++) {
    	q.controlZ(n,n+1);
    	q.controlZ(N+n,N+n+1);
    }    
    EXPECT_NEAR(qsl::fubiniStudy(sim.state(), q), 0, 1e-11);

    // Expect error attempting to add another column
    EXPECT_THROW(sim.addColumn(win.sub(1,3,2)), std::logic_error);    

    // Check edge case when each column has only one row 
    sim = ClusterSim{1, sim_seed};
    win = BaseNodeWindow<Node>{1,4};
    setVerticalEdges(win, true);
    sim.addColumn(win.sub(2,3,2));
    q = qsl::Qubits<qsl::Type::Resize, double>{1};
    q.hadamard(0);
    EXPECT_NEAR(qsl::fubiniStudy(sim.state(), q), 0, 1e-11);    
}

TEST(ClusterSimTest, AddColumnHorizontalEdges)
{
    Seed sim_seed;
    ClusterSim sim{4, sim_seed}; // Height = 4, no columns    

    // Window with one three columns
    BaseNodeWindow<Node> win{4,3};
    setHorizontalEdges(win, true);

    // Add two columns and check state
    sim.addColumn(win.sub(0,1,0));
    sim.addColumn(win.sub(0,2,1));
    qsl::Qubits<qsl::Type::Resize, double> q{8};
    const std::size_t N{q.getNumQubits()/2};
    for (std::size_t n{0}; n < 2*N; n++) {
	q.hadamard(n);
    }
    for (std::size_t n{0}; n < N; n++) {
    	q.controlZ(n,n+N);
    }    
    EXPECT_NEAR(qsl::fubiniStudy(sim.state(), q), 0, 1e-11);

    // Expect error attempting to add another column
    EXPECT_THROW(sim.addColumn(win.sub(1,3,2)), std::logic_error);

    // Check edge case when only a single column is added
    sim = ClusterSim{4, sim_seed};
    sim.addColumn(win.sub(2,3,2));
    q = qsl::Qubits<qsl::Type::Resize, double>{4};
    for (std::size_t n{0}; n < 4; n++) {
	q.hadamard(n);
    }
    EXPECT_NEAR(qsl::fubiniStudy(sim.state(), q), 0, 1e-11);    
}

/// Check invalid uses of addColumn
TEST(ClusterSimTest, AddColumnExceptions)
{
    Seed sim_seed;
    ClusterSim sim{4, sim_seed}; // Height = 4, no columns

    // Error: win contains 5 columns, must be at most 1 when sim is empty
    BaseNodeWindow<Node> win{4,5};
    EXPECT_THROW(sim.addColumn(win), std::logic_error);

    // Error: win must have the same height as sim (4)
    win = BaseNodeWindow<Node>{5,0};
    EXPECT_THROW(sim.addColumn(win), std::logic_error);    

    // Error: win must have width = 2 when one column is present
    win = BaseNodeWindow<Node>{4,5};
    sim.addColumn(win.sub(0,1,0)); // This is OK, adding one column to empty window
    EXPECT_THROW(sim.addColumn(win.sub(0,1,0)),
		 std::logic_error); // This is the error

    // Error: win has one columns, but first column does not have index -1
    EXPECT_THROW(sim.addColumn(win.sub(0,2,0)), std::logic_error);    

    // Error: cannot add a third column after adding two columns already   
    EXPECT_THROW(sim.addColumn(win.sub(0,2,1)), std::logic_error);

    // Error: cannot add a column with partially filled previous columns
    win(0,2).setDoNotMeasure(true);
    sim.measureColumn(win.sub(0,1,0));
    EXPECT_THROW(sim.addColumn(win.sub(0,1,0)), std::logic_error);
}

/// Quick test 
TEST(ClusterSimTest, MeasureXTest)
{
    Seed root_seed;
    for (std::size_t n{0}; n < 1000; n++) {
	
	// Identity pattern on three qubits
	BaseNodeWindow<Node> win{1,2};
	setHorizontalEdges(win,true);

	Seed sim_seed{root_seed.seed() + n};
	ClusterSim sim{1, sim_seed};

	// Set measurement pattern
	win(0,0).setBasis(MeasureX);
	win(1,0).setBasis(MeasureX);
    
	// Add first two columns
	sim.addColumn(win.sub(0,1,0));
	sim.addColumn(win.sub(0,2,1));

	// Measure first column
	sim.measureColumn(win.sub(0,1,0));
    
	// Get outcome from measurement
	const unsigned outcome{win(0,0).getOutcome()};
    
	// Check remaining state
	auto q{sim.state()};
    
	// Correct state
	qsl::Qubits<qsl::Type::Resize, double> q_correct{1};
	if (outcome == 1) {
	    q_correct.pauliX(0);
	}
	EXPECT_NEAR(qsl::fubiniStudy(q,q_correct), 0, 1e-11);
    }
}

/// Check cluster state X - Z - X has all equal outcomes
TEST(ClusterSimTest, ColumnCorrelations)
{
    Seed root_seed;
    for (std::size_t n{0}; n < 10; n++) {

	Seed sim_seed{root_seed.seed() + n};
	ClusterSim sim{3, sim_seed};
	BaseNodeWindow<Node> win{3,2};
	setVerticalEdges(win,true);

	// Add first two columns
	sim.addColumn(win.sub(0,1,0));
	sim.addColumn(win.sub(0,2,1));

	win[0].setBasis(MeasureX);
	win[2].setBasis(MeasureX);

	sim.measureColumn(win.sub(0,1,0));
	for (std::size_t n{0}; n < 3; n++) {
	    const unsigned outcome{win[0].getOutcome()};
	    EXPECT_EQ(outcome, win[1].getOutcome());
	    EXPECT_EQ(outcome, win[2].getOutcome());
	}
    }
}


/// Check cluster state X - Z - X has all equal outcomes
TEST(ClusterSimTest, ThreeQubitIdentityPattern)
{
    Seed root_seed;
    for (std::size_t n{0}; n < 1000; n++) {

	// Identity pattern on three qubits
	BaseNodeWindow<Node> win{1,3};
	setHorizontalEdges(win,true);

	Seed sim_seed{root_seed.seed() + n};
	ClusterSim sim{1, sim_seed};

	// Set measurement pattern
	win(0,0).setBasis(MeasureX);
	win(1,0).setBasis(MeasureX);
	win(2,0).setDoNotMeasure(true);

	// Add first two columns
	sim.addColumn(win.sub(0,1,0));
	sim.addColumn(win.sub(0,2,1));

	// Measure first column and add third column
	sim.measureColumn(win.sub(0,1,0));
	sim.addColumn(win.sub(1,3,2));

	// Measure second column
	sim.measureColumn(win.sub(1,2,1));    

	// Now only final column is left, get state
	auto q{sim.state()};

	// Compute the byproduct operators
	unsigned z{win(0,0).getOutcome()};
	unsigned x{win(1,0).getOutcome()};

	// Correct the state
	if (z == 1) {
	    q.pauliZ(0);
	}
	if (x == 1) {
	    q.pauliX(0);
	}

	// Correct state
	qsl::Qubits<qsl::Type::Resize, double> q_correct{1};
	q_correct.hadamard(0);
	EXPECT_NEAR(qsl::fubiniStudy(q,q_correct), 0, 1e-11);
    }
}


/// Check cluster state X - Z - X has all equal outcomes
TEST(ClusterSimTest, ArbitraryOneQubitGatePattern)
{
    Seed root_seed;
    for (std::size_t n{0}; n < 1000; n++) {

	// Generate random phases
	qsl::Random<double> rand(0,2*M_PI);
	const double xi{rand.getNum()};
	const double eta{rand.getNum()};
	const double zeta{rand.getNum()};
    
	// Identity pattern on three qubits
	BaseNodeWindow<Node> win{1,5};
	setHorizontalEdges(win,true);

	Seed sim_seed{root_seed.seed() + n};
	ClusterSim sim{1, sim_seed};

	// Set measurement pattern
	win(0,0).setBasis(MeasureX);
    
	// Add first two columns
	sim.addColumn(win.sub(0,1,0));
	sim.addColumn(win.sub(0,2,1));

	// Measure first column and add third column
	sim.measureColumn(win.sub(0,1,0));
	sim.addColumn(win.sub(1,3,2));

	// Set first adaptive basis
	const unsigned m_0{win(0,0).getOutcome()};
	win(1,0).setBasis(MeasureXY(m_0 == 0 ? -xi : xi));
    
	// Measure second column and add fourth column
	sim.measureColumn(win.sub(1,2,1));
	sim.addColumn(win.sub(2,4,3));

	// Set second adaptive basis
	const unsigned m_1{win(1,0).getOutcome()};
	win(2,0).setBasis(MeasureXY(m_1 == 0 ? -eta : eta));
    
	// Measure third column and add fifth column
	sim.measureColumn(win.sub(2,3,2));
	sim.addColumn(win.sub(3,5,4));

	// Set third adaptive basis
	const unsigned m_2{win(2,0).getOutcome()};
	win(3,0).setBasis(MeasureXY((m_0 ^ m_2) == 0 ? -zeta : zeta));
    
	// Measure fourth column
	sim.measureColumn(win.sub(3,4,3));    
	const unsigned m_3{win(3,0).getOutcome()};
    
	// Now only final column is left, get state
	auto q{sim.state()};

	// Compute the byproduct operators
	unsigned z{m_0 ^ m_2};
	unsigned x{m_1 ^ m_3};

	// Correct the state
	if (z == 1) {
	    q.pauliZ(0);
	}
	if (x == 1) {
	    q.pauliX(0);
	}

	// Correct state
	qsl::Qubits<qsl::Type::Resize, double> q_correct{1};
	q_correct.hadamard(0);
	q_correct.rotateX(0, xi);
	q_correct.rotateZ(0, eta);
	q_correct.rotateX(0, zeta);
    
	EXPECT_NEAR(qsl::fubiniStudy(q,q_correct), 0, 1e-7);
    }
}

TEST(ClusterSimTest, SetStateExceptions)
{
    Seed sim_seed;
    ClusterSim sim{5, sim_seed};
    qsl::Qubits<qsl::Type::Resize, double> q{6};

    // Error: Cannot set state with wrong height (not a multiple of 5)
    EXPECT_THROW(sim.setState(std::move(q)), std::logic_error);
}

TEST(ClusterSimTest, MeasureColumnException)
{
    Seed sim_seed;
    ClusterSim sim{5, sim_seed};

    // Error: measureColumn must be given a window with width 1
    EXPECT_THROW(sim.measureColumn(BaseNodeWindow<Node>{4,2}), std::logic_error);

    // Error: measureColumn must be given a window with height 5
    EXPECT_THROW(sim.measureColumn(BaseNodeWindow<Node>{4,1}), std::logic_error);
}

TEST(ClusterSimTest, EntangleColumnException)
{
    Seed sim_seed;
    ClusterSim sim{10, sim_seed};

    // Error: entangleColumn does not work if the simulator does not
    // contain exactly one column (here there are zero)
    EXPECT_THROW(sim.entangleColumn(BaseNodeWindow<Node>{4,2}), std::logic_error);

    // Add a column
    qsl::Qubits<qsl::Type::Resize, double> q{10};
    sim.setState(std::move(q));
    
    // Error: entangleColumn must be given a window of height 10
    EXPECT_THROW(sim.entangleColumn(BaseNodeWindow<Node>{4,1}), std::logic_error);

    // Error: entangleColumn must be given a window with width 1
    EXPECT_THROW(sim.entangleColumn(BaseNodeWindow<Node>{10,3}), std::logic_error);
}

TEST(ClusterSimTest, PrintTest)
{
    // Check default object
    {
	Seed sim_seed;
	ClusterSim sim{4, sim_seed}; // Height = 4, no columns
	std::stringstream s1;
	sim.print(s1);
	std::stringstream s2;
	s2 << "(csim:N=4,W=0)" << std::endl;
	EXPECT_EQ(s1.str(), s2.str());
    }

    // Check width calculation
    {
	Seed sim_seed;
	ClusterSim sim{2, sim_seed};

	BaseNodeWindow<Node> win{2,10};
	setVerticalEdges(win,true);

	// Add first two columns
	sim.addColumn(win.sub(0,1,0));
	sim.addColumn(win.sub(0,2,1));
	
	std::stringstream s1;
	sim.print(s1);
	std::stringstream s2;
	s2 << "(csim:N=2,W=2)" << std::endl;
	EXPECT_EQ(s1.str(), s2.str());
    }

    // Check state
    {
	Seed sim_seed;
	ClusterSim sim{3, sim_seed};

	// Make a state 
	qsl::Qubits<qsl::Type::Resize, double> q{3};

	// Store state print out in s1
	std::stringstream s1;
	q.print(s1);

	// Move the state into the cluster simulator
	sim.setState(std::move(q));

	// Print the cluster sim state
	std::stringstream s2;
	sim.printState(s2);

	// Check the print outs are the same
	EXPECT_EQ(s1.str(), s2.str());
    }
}
