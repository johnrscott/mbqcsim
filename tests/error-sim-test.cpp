#include "gtest/gtest.h"
#include <iostream>
#include "error-sim.hpp"
#include "global-bfs.hpp"

using Node = MakeNode<PathNode, DistanceNode, PatternNode, QubitNode>;

TEST(ErrorSimTest, Exceptions)
{
    // Error: cannot simulate array of height > 14
    EXPECT_THROW((ErrorSim<GBFS, NoNoise>{20, false}), std::logic_error);

    // Error: mismatch between height of esim and height of win
    {
	ErrorSim<GBFS, NoNoise> esim{8, false}; // Height 8
	BaseNodeWindow<Node> win{9,20}; // Height 9
	EXPECT_THROW(esim.simulate(win, 2, 5, Seed<>{}, Seed<>{}),
		     std::out_of_range);
    }

    // Error: block width is larger than the window width
    {
	ErrorSim<GBFS, NoNoise> esim{8, false}; // Height 8
	BaseNodeWindow<Node> win{8,20}; // Height 9
	std::size_t block_width{300};
	EXPECT_THROW(esim.simulate(win, 2, block_width, Seed<>{}, Seed<>{}),
		     std::out_of_range);
    }

    // Error: y-coordinate starting point is not in range
    {
	ErrorSim<GBFS, NoNoise> esim{8, false}; // Height 8
	BaseNodeWindow<Node> win{8,20}; // Height 9
	std::size_t y_start{300};
	EXPECT_THROW(esim.simulate(win, y_start, 5, Seed<>{}, Seed<>{}),
		     std::out_of_range);
    }
}

TEST(ErrorSimTest, EndColumnCheck)
{
    BaseNodeWindow<Node> win{5,10};
    setAllEdges(win, true);
    // Remove a column of edges
    for (std::size_t y{0}; y < 5; y++) {
	win.setEdge({6,y},{7,y},false);
    }
    
    ErrorSim<GBFS, NoNoise> esim{5, false};
    //ErrorSim<GBFS, NoNoise> esim{9, true};

    Seed path_seed{1870038693533921390U};//{1};
    path_seed.print();
    Seed sim_seed{13843120558048235281U};//{2};
    sim_seed.print();

    const std::size_t y_start{3};
    const std::size_t block_width{3};
    
    std::size_t end_col{0};
    try {
	end_col = esim.simulate(win, y_start, block_width, path_seed, sim_seed);
	//last = esim.simulate(win, y_start, block_width, path_seed, sim_seed);
    } catch (const ErrorSimTerminated<Node> & p) {
	end_col = p.end();
    }

    // Check it reached column 5 (remember to account for the block width)
    EXPECT_EQ(end_col, 5);
}


TEST(ErrorSimTest, NoNoise)
{
    BaseNodeWindow<Node> win{5,10};
    
    Seed graph_seed{16551164106594522575U};//{0};
    graph_seed.print();
    setRandomEdges(win, 0.80, graph_seed);
    
    ErrorSim<GBFS, NoNoise> esim{5, false};
    //ErrorSim<GBFS, NoNoise> esim{9, true};

    Seed path_seed{1870038693533921390U};//{1};
    path_seed.print();
    Seed sim_seed{13843120558048235281U};//{2};
    sim_seed.print();

    const std::size_t y_start{3};
    const std::size_t block_width{7};
    
    std::size_t end_col{0};
    try {
	end_col = esim.simulate(win, y_start, block_width, path_seed, sim_seed);
	//last = esim.simulate(win, y_start, block_width, path_seed, sim_seed);
    } catch (const ErrorSimTerminated<Node> & p) {
	end_col = p.end();
    }

    // Check it reached the end of the window
    EXPECT_EQ(end_col, 10);
    

    // Check that all the added noise was zero

    ///\todo Removed this test because I have temporarily removed
    /// node data from the output json, due to memory issues when
    /// runing esim.
    // auto columns(esim.json());
    // for (const auto & col : columns) {
    // 	// Check noise for each qubit
    // 	for (const auto & node : col["nodes"]) {
    // 	    // Check the noise recorded for each qubit was zero
    // 	    EXPECT_EQ(node["qubit"]["noise"]["z_0"], 0);
    // 	    EXPECT_EQ(node["qubit"]["noise"]["x_1"], 0);
    // 	}

    // 	// Check distance is zero (they should be exactly zero)
    // 	EXPECT_NEAR(col["distance"], 0, 1e-17);
    // }
}

TEST(ErrorSimTest, GaussianNoise)
{
    BaseNodeWindow<Node> win{5,10};
    
    Seed graph_seed{16551164106594522575U};//{0};
    graph_seed.print();
    setRandomEdges(win, 0.80, graph_seed);
    
    ErrorSim<GBFS, GaussianNoise> esim{5, false};
    //ErrorSim<GBFS, NoNoise> esim{9, true};

    Seed path_seed{1870038693533921390U};//{1};
    path_seed.print();
    Seed sim_seed{13843120558048235281U};//{2};
    sim_seed.print();
    Seed noise_seed{3};

    const std::size_t y_start{3};
    const std::size_t block_width{7};
    
    std::size_t end_col{0};
    double mu{0};
    double sigma{0.1};
    try {
	end_col = esim.simulate(win, y_start, block_width, path_seed, sim_seed,
			     mu, sigma, noise_seed);
	//last = esim.simulate(win, y_start, block_width, path_seed, sim_seed);
    } catch (const ErrorSimTerminated<Node> & p) {
	end_col = p.end();
    }

    // Check it reached the end of the window
    EXPECT_EQ(end_col, 10);

    // Distances
    std::vector<double> distances{
	0.06788539845592048,
	0.11744212716842053,
	0.08921232815743366,
	0.03680293112750653,
	0.03837944482774176,
	0.08707154481179202,
	0.12196912374134024,
	0.12293328144834223,
	0.15528912169064993,
	0.17234587376056845,
    };

    // Check the distances
    std::size_t count{0};
    auto columns(esim.json());
    for (const auto & col : columns) {
	// Check distance is zero (they should be exactly zero)
	EXPECT_NEAR(col["distance"], distances[count++], 1e-14);
    }
}

/**
 * This test checks that the interactive mode works. 
 *
 */
TEST(ErrorSimTest, PrintTest)
{
    BaseNodeWindow<Node> win{5,10};
    
    Seed graph_seed{16551164106594522575U};//{0};
    graph_seed.print();
    setRandomEdges(win, 0.80, graph_seed);

    constexpr bool press_enter{false}; // Change to true to step through
    ErrorSim<GBFS, GaussianNoise, press_enter> esim{5, true};

    Seed path_seed{1870038693533921390U};//{1};
    path_seed.print();
    Seed sim_seed{13843120558048235281U};//{2};
    sim_seed.print();
    Seed noise_seed{3};

    const std::size_t y_start{3};
    const std::size_t block_width{7};
    
    std::size_t end_col{0};
    double mu{0};
    double sigma{0.1};
    try {
	end_col = esim.simulate(win, y_start, block_width, path_seed, sim_seed,
			     mu, sigma, noise_seed);
	//last = esim.simulate(win, y_start, block_width, path_seed, sim_seed);
    } catch (const ErrorSimTerminated<Node> & p) {
	end_col = p.end();
    }

    // Check it reached the end of the window
    EXPECT_EQ(end_col, 10);

    // Distances
    std::vector<double> distances{
	0.06788539845592048,
	0.11744212716842053,
	0.08921232815743366,
	0.03680293112750653,
	0.03837944482774176,
	0.08707154481179202,
	0.12196912374134024,
	0.12293328144834223,
	0.15528912169064993,
	0.17234587376056845,
    };

    // Check the distances
    std::size_t count{0};
    auto columns(esim.json());
    for (const auto & col : columns) {
	// Check distance is zero (they should be exactly zero)
	EXPECT_NEAR(col["distance"], distances[count++], 1e-14);
    }
}
