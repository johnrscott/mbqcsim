#include "gtest/gtest.h"
#include "distance-node.hpp"
#include "make-node.hpp"
#include "node-window.hpp"

#include "predecessor-iterator.hpp"

#include <random>
#include <algorithm>

// Path node type
using Node = MakeNode<DistanceNode>;

// Tests the default PathNode object
TEST(PredecessorIterator, Normal)
{
    std::vector<std::pair<long int,long int>>
	coords{{0,1},{1,1},{2,1},{3,1},{4,1},{4,2},
	       {3,2},{2,2},{1,2},{1,3}};
    
    BaseNodeWindow<Node> win{6,20};
    setAllEdges(win,true);

    // Create a path
    win(0,1).setRoot();
    for (std::size_t n{0}; n < coords.size()-1; n++) {
	const auto xy1{coords[n]};
	const auto xy2{coords[n+1]};
	win(xy1.first,xy1.second).setPredecessor(win(xy2.first, xy2.second));	
    }
    
    // Iterate over path
    std::size_t count{0};
    for (auto & node : PredecessorRange(win(0,1), win)) {
	EXPECT_EQ(node.x(), coords[count].first);
	EXPECT_EQ(node.y(), coords[count].second);
	count++;
    }
    EXPECT_EQ(count, 10);
}

// Tests the default PathNode object
TEST(PredecessorIterator, WithSubWindow)
{
    std::vector<std::pair<long int,long int>>
	coords{{0,1},{1,1},{2,1},{3,1},{4,1},{4,2},
	       {3,2},{2,2},{1,2},{1,3}};
    
    BaseNodeWindow<Node> win{6,20};
    setAllEdges(win,true);

    // Create a path
    win(0,1).setRoot();
    for (std::size_t n{0}; n < coords.size()-1; n++) {
	const auto xy1{coords[n]};
	const auto xy2{coords[n+1]};
	win(xy1.first,xy1.second).setPredecessor(win(xy2.first, xy2.second));	
    }

    auto sub{win.sub(2,6,3)};
    // Iterate over path
    std::size_t count{0}; // Start from the first node in the window: (-1,1)
    try {
	for (auto & node : PredecessorRange(sub(-1,1), sub)) {
	    EXPECT_EQ(node.x(), coords[count+2].first);
	    EXPECT_EQ(node.y(), coords[count+2].second);
	    count++;
	}
	EXPECT_EQ(true, false); // Shouldn't get here
    } catch (const std::out_of_range &) {
	// Expect the out of range for index 6
	EXPECT_EQ(count, 6);
    }
}

// Tests the default PathNode object
TEST(PredecessorIterator, Exceptions)
{
    std::vector<std::pair<long int,long int>>
	coords{{0,1},{1,1},{2,1},{3,1},{4,1},{4,2},
	       {3,2},{2,2},{1,2},{1,3}};
    
    BaseNodeWindow<Node> win{6,20};

    // Create a path
    win(0,1).setRoot();
    for (std::size_t n{0}; n < coords.size()-1; n++) {
	const auto xy1{coords[n]};
	const auto xy2{coords[n+1]};
	win(xy1.first,xy1.second).setPredecessor(win(xy2.first, xy2.second));	
    }

    // Error: Cannot make range starting from non path node
    EXPECT_THROW(PredecessorRange(win(5,4), win), std::logic_error);
}


