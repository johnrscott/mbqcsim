#include "gtest/gtest.h"
#include "qubit-node.hpp"
#include "make-node.hpp"
#include <random>
#include <algorithm>

// Path node type
using Node = MakeNode<QubitNode>;

// Tests the default DistanceNode object
TEST(QubitNodeTest, DefaultObject)
{
    // Make a default object and check properties
    std::size_t index{20};
    std::size_t height{7};
    Node node{index,height};
    EXPECT_NEAR(node.getBasis().z_0(), 0, 1e-17);
    EXPECT_NEAR(node.getBasis().x_1(), 0, 1e-17);

    // Error: qubit has not not been measured yet
    EXPECT_THROW(node.getOutcome(), std::logic_error);
}

// Tests the default DistanceNode object
TEST(QubitNodeTest, CopyConstructor)
{
    // Make a default object and check properties
    std::size_t index{20};
    std::size_t height{7};
    Node node{index,height};

    // Make non-trivial changes to basis (the unique_ptr)
    MeasureXY m{3.4};
    node.setBasis(m);
    EXPECT_NEAR(node.getBasis().z_0(), m.z_0(), 1e-17);
    EXPECT_NEAR(node.getBasis().x_1(), m.x_1(), 1e-17);

    // Measure and get outcome
    qsl::Qubits<qsl::Type::Resize, double> qsl{3}; 
    Random<double> rnd{0,1,Seed<>{}};
    node.measure(qsl,0,rnd,0,0);
    const auto outcome{node.getOutcome()};
    
    // Copy construct another object and check equality again
    auto node2{node};

    // Check the angles are the same
    EXPECT_NEAR(node2.getBasis().z_0(), m.z_0(), 1e-17);
    EXPECT_NEAR(node2.getBasis().x_1(), m.x_1(), 1e-17);

    // Check the outcome is the same
    EXPECT_EQ(node2.getOutcome(), outcome); 
}

TEST(QubitNode, MarkerShape)
{
    Node node;

    // Check node shape is "Z"
    EXPECT_EQ(node.markerShape(), "Z");
    
    qsl::Qubits<qsl::Type::Resize, double> qsl{3};
    qsl.hadamard(0);

    // Check that the shape is equal to the outcome of measurement
    Random<double> rnd{0,1,Seed<>{}};
    for (std::size_t rep{0}; rep < 1000; rep++) {
	auto q{qsl};
	node.measure(q,0,rnd,0,0);
	EXPECT_EQ(std::to_string(node.getOutcome()), node.markerShape());
    }

    // For a non-trivial basis, check correct label
    Node node2;
    node2.setBasis(MeasureXY{0.5});
    EXPECT_EQ(node2.markerShape(), "B");
}

TEST(QubitNode, MarkerColours)
{
    Node node;

    // Check default marker is purple (unmeasured
    EXPECT_EQ(node.markerColour(), Colour::PURPLE+Colour::BOLD);

    // Check red when the marker is set to do-not-measure
    node.setDoNotMeasure(true);
    EXPECT_EQ(node.markerColour(), Colour::RED+Colour::BOLD);

    // Do a copy construction and check the same holds in the copied class
    auto copy{node};
    EXPECT_EQ(copy.markerColour(), Colour::RED+Colour::BOLD);    

    // Make a measurement and check green
    qsl::Qubits<qsl::Type::Resize, double> qsl{3};
    node.setDoNotMeasure(false);
    Random<double> rnd{0,1,Seed<>{}};
    node.measure(qsl,0,rnd,0,0);
    EXPECT_EQ(node.markerColour(), Colour::GREEN+Colour::BOLD);    
}

TEST(QubitNode, Measurement)
{
    ///\todo Writing this test is fiddly, and I can tell there are
    /// loads of "interface bugs" ( silently going wrong behind your
    /// back). This whole measuring thing needs a redesign.
    Node node;

    // By default, set to measure Z.
    qsl::Qubits<qsl::Type::Resize, double> qsl{3};

    Random<double> rnd{0,1,Seed<>{}};
    for (std::size_t rep{0}; rep < 1000; rep++) {
	auto q{qsl};
	node.measure(q,0,rnd,0,0);
	EXPECT_EQ(node.getOutcome(), 0);
	EXPECT_EQ(q.getNumQubits(), 2); // because 1 qubit was measured out
    }

    // Now set to do-not-measure
    node.setDoNotMeasure(true);
    EXPECT_EQ(node.measure(qsl,0,rnd,0,0), -1); // This time, nothing is measured
    EXPECT_EQ(qsl.getNumQubits(), 3); // because nothing was measured this time	
}

TEST(QubitNodeTest, PrintTest)
{
    // Test empty node
    {
	MakeNode<QubitNode> node{5,10};
	std::stringstream s1;
	node.print(s1);
	std::stringstream s2;
	s2 << "(0,5) (idx=5)" << std::endl
	   << "  (qnode:m=(0,0),out:-1)" << std::endl;
	EXPECT_EQ(s1.str(), s2.str());
    }

    // Test node with measured outcome
    {
	MakeNode<QubitNode> node{5,10};
	qsl::Qubits<qsl::Type::Resize, double> qsl{3}; 
	Random<double> rnd{0,1,Seed<>{}};
	node.measure(qsl,0,rnd,0,0);
	const auto outcome{node.getOutcome()};
	std::stringstream s1;
	node.print(s1);
	std::stringstream s2;
	s2 << "(0,5) (idx=5)" << std::endl
	   << "  (qnode:m=(0,0),out:" << outcome << ")" << std::endl;
	EXPECT_EQ(s1.str(), s2.str());
    }


    // Test node with specified basis
    {
	MakeNode<QubitNode> node{5,10};
	qsl::Qubits<qsl::Type::Resize, double> qsl{3}; 
	Random<double> rnd{0,1,Seed<>{}};
	node.setBasis(Measure{1.1,-2.2});
	node.measure(qsl,0,rnd,0,0);
	const auto outcome{node.getOutcome()};
	std::stringstream s1;
	node.print(s1);
	std::stringstream s2;
	s2 << "(0,5) (idx=5)" << std::endl
	   << "  (qnode:m=(1.1,-2.2),out:" << outcome << ")" << std::endl;
	EXPECT_EQ(s1.str(), s2.str());
    }
}
