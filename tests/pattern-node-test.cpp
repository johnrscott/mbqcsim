#include "gtest/gtest.h"
#include "make-node.hpp"
#include "pattern-node.hpp"
#include "qubit-node.hpp"
#include "node-window.hpp"

using Node = MakeNode<PatternNode, QubitNode>;

TEST(PatternNodeTest, GetAngleExceptions)
{
    Node node{3,4};

    // Error: cannot get angle from a node in the Z basis
    EXPECT_THROW(node.getAngle(), std::logic_error);
}

TEST(PatternNodeTest, PushBypUpdateExceptions)
{
    Node node{3,4};

    // Error: cannot add an update rule to a node without a path
    // index
    EXPECT_THROW(node.pushBypUpdate(Bits{0,1}), std::logic_error);

    // Error: cannot push update twice for the same index
    node.pushBypUpdate(Bits{0,1}, 2);
    EXPECT_THROW(node.pushBypUpdate(Bits{0,1}, 2), std::logic_error);
    
}

TEST(PatternNodeTest, GetPathBypUpdateExceptions)
{
    Node node{3,4};

    // Error: cannot get update for non-path nodes
    EXPECT_THROW(node.getPathBypUpdate(), std::runtime_error);
}

TEST(PatternNodeTest, MarkerColours)
{
    BaseNodeWindow<Node> win{10,10};

    // Check default marker does not return a colour
    EXPECT_EQ(win(0,1).markerColour(), "");

    // Check green when the marker is on the path
    win(0,1).setPatternIndex(10);
    EXPECT_EQ(win(0,1).markerColour(), Colour::GREEN+Colour::BOLD);

    // Check red when there are byproduct operators
    win(1,1).pushBypUpdate(Bits{0,1}, 22);
    EXPECT_EQ(win(1,1).markerColour(), Colour::RED+Colour::BOLD);
}

TEST(PatternNodeTest, PrintTest)
{
    // Test empty node
    {
	MakeNode<PatternNode> node{5,10};
	std::stringstream s1;
	node.print(s1);
	std::stringstream s2;
	s2 << "(0,5) (idx=5)" << std::endl
	   << "  (patnode:b=Z,byp={},idx=-1)" << std::endl;
	EXPECT_EQ(s1.str(), s2.str());
    }

    // Test node with XY measurement settings
    {
	MakeNode<PatternNode> node{5,10};
	node.setXY(2.1);
	std::stringstream s1;
	node.print(s1);
	std::stringstream s2;
	s2 << "(0,5) (idx=5)" << std::endl
	   << "  (patnode:b=XY(2.1),byp={},idx=-1)" << std::endl;
	EXPECT_EQ(s1.str(), s2.str());
    }

    // Test node with byproduct operators
    {
	MakeNode<PatternNode> node{5,10};
	node.pushBypUpdate(Bits{0,1}, 3);
	node.pushBypUpdate(Bits{1,0}, 0);
	node.pushBypUpdate(Bits{1,1}, 1);

	std::stringstream s1;
	node.print(s1);
	std::stringstream s2;
	s2 << "(0,5) (idx=5)" << std::endl
	   << "  (patnode:b=Z,byp={(0:x),(1:xz),(3:z),},idx=-1)" << std::endl;
	EXPECT_EQ(s1.str(), s2.str());
    }
}
