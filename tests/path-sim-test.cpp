#include "gtest/gtest.h"
#include "node-window.hpp"
#include "path-node.hpp"
#include "distance-node.hpp"
#include "pattern-node.hpp"
#include "qubit-node.hpp"
#include "make-node.hpp"
#include "path-sim.hpp"
#include "path-pattern.hpp"
#include "global-bfs.hpp"
#include "iterative-bfs.hpp"
#include "pathfinding.hpp"

using Node = MakeNode<PathNode, DistanceNode, PatternNode, QubitNode>;

TEST(PathSimTest, StraightIdentityPattern)
{
    unsigned height{5};
    std::size_t width{10};

    for (std::size_t y_start{0}; y_start < height; y_start++) {
    
	BaseNodeWindow<Node> win{height,width};

	setAllEdges(win,true);

	Seed sim_seed;
	PathSim sim{height, sim_seed};
	IdentityGate gate{};

	// Create a generator for use with getNextNode
	Seed path_seed;
	Generator<unsigned, 0, 1> path_gen{path_seed};
	
	std::size_t y{y_start};
	for (std::size_t n{0}; n < win.width()-1; n++) {
	    auto sub{win.sub(n,win.width(),n)};
	    GBFS gbfs{win.height()};//, sub.width()};    
	    gbfs.search(sub, y);
	    gbfs.makeSPT(sub);
	    PathExtension path;
	    y = path.extend(std::move(sub), y, path_gen);
	}

	writeLeftNodes(win.sub(0,win.width(),0), win(0,y_start));
	gate.generate(win.sub(0,win.width(),0), y_start);
    
	// Make initial all plus state
	qsl::Qubits<qsl::Type::Resize,double> initial{height};
	for (std::size_t n{0}; n < initial.getNumQubits(); n++) {
	    initial.hadamard(n);
	}

	// This should return (by move) the result of applying
	// the pattern to the first 2 columns.
	bool start{true};
	for (std::size_t x{0}; x < win.width()-1; x++) {
	    initial = sim.simulate(win.sub(x,std::min(x+2, win.width()),x),
				   std::move(initial), start);
	    start = false;	
	    auto left_height{sim.getLeftHeight()};
	    auto q_correct{gate.state(left_height)};
	    auto q{sim.state()};
	    EXPECT_NEAR(qsl::fubiniStudy(q,q_correct), 0, 1e-7);
	}
    }
}

TEST(PathSimTest, VerticalSnakeIdentityPattern)
{
    unsigned height{5};
    unsigned width{10};

    //for (std::size_t y_start{0}; y_start < height; y_start++) {
    std::size_t y_start{0};

    // Loop test 100 times 
    for (std::size_t rep{0}; rep < 100; rep++) {
    
	BaseNodeWindow<Node> win{height,width};

	//setAllEdges(win,true);
	snakePattern(win, {0,0}, {width-1,height-1}, false, true, 1);

	Seed sim_seed;
	PathSim sim{height, sim_seed};
	IdentityGate gate{};

	//std::size_t y_start{1};

	// Create a generator for use with getNextNode
	Seed path_seed;
	Generator<unsigned, 0, 1> path_gen{path_seed};
	
	std::size_t y{y_start};
	for (std::size_t n{0}; n < win.width()-1; n++) {
	    auto sub{win.sub(n,win.width(),n)};
	    GBFS gbfs{win.height()};//, sub.width()};
	    gbfs.search(sub, y);
	    gbfs.makeSPT(sub);
	    PathExtension path;
	    y = path.extend(std::move(sub), y, path_gen);
	}

	writeLeftNodes(win.sub(0,win.width(),0), win(0,y_start));
	gate.generate(win.sub(0,win.width(),0), y_start);
    
	// Make initial all plus state
	qsl::Qubits<qsl::Type::Resize,double> initial{height};
	for (std::size_t n{0}; n < initial.getNumQubits(); n++) {
	    initial.hadamard(n);
	}

	// This should return (by move) the result of applying
	// the pattern to the first 2 columns.
	bool start{true};
	for (std::size_t x{0}; x < win.width()-1; x++) {
	    initial = sim.simulate(win.sub(x,std::min(x+2, win.width()),x),
				   std::move(initial), start);
	    start = false;	
	    auto left_height{sim.getLeftHeight()};
	    auto q_correct{gate.state(left_height)};
	    auto q{sim.state()};
	    EXPECT_NEAR(qsl::fubiniStudy(q,q_correct), 0, 1e-7);
	}
    }
}

TEST(PathSimTest, PathLengthZeroEdgeCase)
{
    // Starting y-coordinate
    const std::size_t y_start{4};
    const std::size_t block_width{4};
    const std::size_t width{10};
    const unsigned height{10};

    BaseNodeWindow<Node> win{height,width};
    Seed s1{751162693886785575};
    setRandomEdges(win, 0.65, s1);
    
    // Search for paths [RANDOM]
    Seed s2{};
    Node last{}; // Will store the final node
    try {
	Pathfinding<GBFS> pathf{win.height()};
	last = pathf.emulate(win, block_width, y_start, s2);
    } catch (const PathTerminated<Node> & e) {
	std::cout << e.what() << std::endl;
	last = e.last();
    }

    // Compute left nodes for simulation
    writeLeftNodes(win.sub(0,win.width(),0), win(0,y_start));

    // Perform the simulation
    IdentityGate identity{};
    identity.generate(win.sub(0,win.width(),0), y_start);

    // Make initial all plus state
    qsl::Qubits<qsl::Type::Resize,double> col{height};
    for (std::size_t n{0}; n < col.getNumQubits(); n++) {
	col.hadamard(n);
    }

    Seed sim_seed;
    PathSim sim{height, sim_seed};
    col = sim.simulate(win.sub(0,last.x()+1,0), std::move(col), true);
    auto left_height{sim.getLeftHeight()};
    auto q_correct{identity.state(left_height)};
    auto q{sim.state()};
    
    EXPECT_NEAR(qsl::fubiniStudy(q,q_correct), 0, 1e-7);
}

/// Get this to only print seeds when it fails
TEST(PathSimTest, RandomSimulationGBFS)
{
    std::vector<std::tuple<std::size_t,std::size_t,std::size_t,unsigned>>
	cases{
	{4,12,35,7}, {0,11,35,7}, {6,10,35,7}, {6,9,35,7}, 
    };

    for (const auto & [y_start,block_width,width,height] : cases) {
	for (std::size_t rep{0}; rep < 10; rep++) {
	
	    BaseNodeWindow<Node> win{height,width};
	    Seed graph_seed;
	    setRandomEdges(win, 0.65, graph_seed);
    
	    // Search for paths [RANDOM]
	    Seed path_seed;//{18178443374515508180};
    
	    Node last{}; // Will store the final node
	    try {
		Pathfinding<GBFS> pathf{win.height()};
		last = pathf.emulate(win, y_start, block_width, path_seed);
	    } catch (const PathTerminated<Node> & e) {
		last = e.last();
	    }
	
	    // Compute left nodes for simulation
	    writeLeftNodes(win.sub(0,win.width(),0), win(0,y_start));

	    // Perform the simulation
	    IdentityGate gate{};
	    gate.generate(win.sub(0,win.width(),0), y_start);

	    // Make initial all plus state
	    qsl::Qubits<qsl::Type::Resize,double> col{height};
	    for (std::size_t n{0}; n < col.getNumQubits(); n++) {
		col.hadamard(n);
	    }
	    Seed sim_seed;
	    PathSim sim{height, sim_seed};

	    try {
		// This should return (by move) the result of applying
		// the pattern to the first 2 columns.
		bool start{true};
		for (std::size_t x{0}; x < last.x()+1; x++) {
		    col = sim.simulate(win.sub(x,std::min(x+2, last.x()+1),x),
				       std::move(col), start);
		    start = false;	
		    auto left_height{sim.getLeftHeight()};
		    auto q_correct{gate.state(left_height)};
		    auto q{sim.state()};
		    auto distance{qsl::fubiniStudy(q,q_correct)};
		    if (distance > 1e-7) {
			///\todo This is a hack to see the seeds
			throw std::runtime_error("Error in test");
		    }
		    EXPECT_NEAR(distance, 0, 1e-7);
		}
	    } catch (...) {
		graph_seed.print();
		path_seed.print();
		win.print<PathNode>();
		win.print<PatternNode>();
		win.print<QubitNode>();
		abort();
	    }
	}
    }
}

/// Get this to only print seeds when it fails
/// This test is failing at the moment with an exception
/// for advancing the path iterator, so there is probably a bug in IBFS.
TEST(PathSimTest, RandomSimulationIBFS)
{
    std::vector<std::tuple<std::size_t,std::size_t,std::size_t,unsigned>>
	cases{
	{4,12,35,7}, {0,11,35,7}, {6,10,35,7}, {6,9,35,7}, 
    };

    for (const auto & [y_start,block_width,width,height] : cases) {
	for (std::size_t rep{0}; rep < 10; rep++) {
	
	    BaseNodeWindow<Node> win{height,width};
	    Seed graph_seed;
	    setRandomEdges(win, 0.65, graph_seed);
    
	    // Search for paths [RANDOM]
	    Seed path_seed;//{18178443374515508180};
    
	    Node last{}; // Will store the final node
	    try {
		Pathfinding<IBFS> pathf{win.height()};
		last = pathf.emulate(win, y_start, block_width, path_seed);
	    } catch (const PathTerminated<Node> & e) {
		last = e.last();
	    }
	
	    // Compute left nodes for simulation
	    writeLeftNodes(win.sub(0,win.width(),0), win(0,y_start));

	    // Perform the simulation
	    IdentityGate gate{};
	    gate.generate(win.sub(0,win.width(),0), y_start);

	    // Make initial all plus state
	    qsl::Qubits<qsl::Type::Resize,double> col{height};
	    for (std::size_t n{0}; n < col.getNumQubits(); n++) {
		col.hadamard(n);
	    }
	    Seed sim_seed;
	    PathSim sim{height, sim_seed};

	    try {
		// This should return (by move) the result of applying
		// the pattern to the first 2 columns.
		bool start{true};
		for (std::size_t x{0}; x < last.x()+1; x++) {
		    col = sim.simulate(win.sub(x,std::min(x+2, last.x()+1),x),
				       std::move(col), start);
		    start = false;	
		    auto left_height{sim.getLeftHeight()};
		    auto q_correct{gate.state(left_height)};
		    auto q{sim.state()};
		    auto distance{qsl::fubiniStudy(q,q_correct)};
		    if (distance > 1e-7) {
			///\todo This is a hack to see the seeds
			throw std::runtime_error("Error in test");
		    }
		    EXPECT_NEAR(distance, 0, 1e-7);
		}
	    } catch (...) {
		graph_seed.print();
		path_seed.print();
		win.print<PathNode>();
		win.print<PatternNode>();
		win.print<QubitNode>();
		abort();
	    }
	}
    }
}



/// Get this to only print seeds when it fails
TEST(PathSimTest, CheckerSnakeDeadEndSimulation)
{
    const std::size_t y_start{0};
    const unsigned height{7};
    const std::size_t width{25};
    const std::size_t block_width{6};
    BaseNodeWindow<Node> win{height,width};

    //Seed graph_seed;
    //setRandomEdges(win, 0.65, graph_seed);
    setCheckerDeadEndPattern(win);
    
    // Search for paths [RANDOM]
    Seed path_seed;//{18178443374515508180};
    path_seed.print();
    
    Node last{}; // Will store the final node
    Pathfinding<GBFS> pathf{win.height()};
    try {
	last = pathf.emulate(win, y_start, block_width, path_seed);
    } catch (const PathTerminated<Node> & e) {
	last = e.last();
    }
	
    // Compute left nodes for simulation
    writeLeftNodes(win.sub(0,win.width(),0), win(0,y_start));


    // Perform the simulation
    IdentityGate gate{};
    gate.generate(win.sub(0,win.width(),0), y_start);

    // Make initial all plus state
    qsl::Qubits<qsl::Type::Resize,double> col{height};
    for (std::size_t n{0}; n < col.getNumQubits(); n++) {
	col.hadamard(n);
    }
    Seed sim_seed;
    PathSim sim{height, sim_seed};

    try {
	// This should return (by move) the result of applying
	// the pattern to the first 2 columns.
	bool start{true};
	for (std::size_t x{0}; x < last.x()+1; x++) {
	    col = sim.simulate(win.sub(x,std::min(x+2, last.x()+1),x),
			       std::move(col), start);
	    start = false;	
	    auto left_height{sim.getLeftHeight()};
	    auto q_correct{gate.state(left_height)};
	    auto q{sim.state()};
	    EXPECT_NEAR(qsl::fubiniStudy(q,q_correct), 0, 1e-7);
	}
    } catch (...) {
	path_seed.print();
	win.print<PathNode>();
	win.print<PatternNode>();
	win.print<QubitNode>();
	abort();
    }
}


/**
 * \brief Error in the calculation of maximal left nodes
 *
 * Before bug fix, in graphs where the path terminates in the middle of
 * the graph, but where the last node is not the rightmost node in the
 * path, the last node was incorrectly marked as maximal left node, causing
 * the simulation to fail.
 */
TEST(PathSimTest, SimulationBug1)
{
    std::size_t y_start{4};
    std::size_t width{35};
    unsigned height{7};
    std::size_t block_width{12};

    BaseNodeWindow<Node> win{height,width};
    Seed graph_seed{8007228381479810150};
    setRandomEdges(win, 0.65, graph_seed);
    
    // Search for paths [RANDOM]
    Seed path_seed{6805592162654696976};
    
    Node last{}; // Will store the final node
    try {
	Pathfinding<GBFS> pathf{win.height()};
	last = pathf.emulate(win, y_start, block_width, path_seed);
    } catch (const PathTerminated<Node> & e) {
	last = e.last();
    }

    // Compute left nodes for simulation
    writeLeftNodes(win.sub(0,win.width(),0), win(0,y_start));
    
    // Perform the simulation
    IdentityGate gate{};
    gate.generate(win.sub(0,win.width(),0), y_start);

    // Make initial all plus state
    qsl::Qubits<qsl::Type::Resize,double> col{height};
    for (std::size_t n{0}; n < col.getNumQubits(); n++) {
	col.hadamard(n);
    }
    
    Seed sim_seed;
    PathSim sim{height, sim_seed};

    // This should return (by move) the result of applying
    // the pattern to the first 2 columns.
    bool start{true};
    for (std::size_t x{0}; x < last.x()+1; x++) {
	col = sim.simulate(win.sub(x,std::min(x+2, last.x()+1),x),
			   std::move(col), start);
	start = false;	
	auto left_height{sim.getLeftHeight()};
	auto q_correct{gate.state(left_height)};
	auto q{sim.state()};
	auto distance{qsl::fubiniStudy(q,q_correct)};
	EXPECT_NEAR(distance, 0, 1e-7);
    }
}

/**
 * \brief Error in pattern generation in the presence of path loops
 *
 * This is really a pattern generation bug, but I couldn't figure out an
 * easy solution to it off the top of my head, so I'm leaving the case
 * here to be fixed later. The issue occurs when there is a loop in the
 * path, causing X measurements to be next to each other. This messes
 * up the simulation (because the measurement pattern is no longer one
 * line).
 */
TEST(PathSimTest, SimulationBug2)
{
    std::size_t y_start{0};
    std::size_t block_width{11};
    std::size_t width{35};
    unsigned height{7};
    
    BaseNodeWindow<Node> win{height,width};
    Seed graph_seed{6932965734845481015};
    setRandomEdges(win, 0.65, graph_seed);
    
    // Search for paths [RANDOM]
    Seed path_seed{7841540533562183854};
    
    Node last{}; // Will store the final node
    Pathfinding<GBFS> pathf{win.height()};
    try {
	last = pathf.emulate(win, y_start, block_width, path_seed);
    } catch (const PathTerminated<Node> & e) {
	last = e.last();
    }

    // Compute left nodes for simulation
    writeLeftNodes(win.sub(0,win.width(),0), win(0,y_start));
    
    // Perform the simulation
    IdentityGate gate{};

    // Exception thrown here for bug
    EXPECT_THROW(gate.generate(win.sub(0,win.width(),0), y_start),
		 std::logic_error);
}

/**
 * \brief Test the incremental method of simulation
 *
 * This is a bit rough and ready, but I wanted to get at least one
 * test in of this important simulation method (the basis for esim).
 * \todo Come back and tidy this up.
 */
TEST(PathSimTest, IncrementalSimulation)
{
    std::size_t y_start{5};
    std::size_t block_width{10};
    std::size_t width{200};
    unsigned height{8};
    	
    BaseNodeWindow<Node> win{height,width};
    Seed graph_seed{18237402459369202231U};
    graph_seed.print();
    //setRandomEdges(win, 0.85, graph_seed);
    setCheckerSnakePattern(win, 5, 5, 3, 1);
    
    // Search for paths [RANDOM]
    Seed path_seed{5186529528026697428};
    path_seed.print();
    // Create the generator for path randomness
    Generator<unsigned, 0, 1> path_gen{path_seed};
    
    // Set the initial node as the pathStartPoint
    std::size_t y{y_start}; // Variable holding current y-coordinate of path
    std::size_t y_next; // Variable holding next  y-coordinate pattern
    std::size_t y_gen{y_start};
    std::size_t y_max_left{y_start};
    win(0,y).setPathBegin();

    GBFS search{height};
    IdentityGate gate;

    // Make initial all plus state
    qsl::Qubits<qsl::Type::Resize,double> col{height};
    for (std::size_t n{0}; n < col.getNumQubits(); n++) {
	col.hadamard(n);
    }

    Seed sim_seed{2121388728286287960U};//{6113565041654536446U};
    sim_seed.print();
    PathSim sim{height, sim_seed};

    // Distance between simulation and correct state
    double distance{0};
    Bits byp{0,0}; // Current byproduct operators
    
    // Flag to mark the first simulation column
    bool sim_start{true};
    
    // Loop over all sub-blocks, performing a path search,
    // followed by pattern generation, followed by a simulation
    for (std::size_t x{0}; x < win.width(); x++) {
	
	// Compute the current block location

	const std::size_t start{x};
	const std::size_t end{std::min(x+block_width,win.width())};
		
	nlohmann::json block_entry;
	auto sub{win.sub(start,end,start)};

	// Search algorithm and RPT
	search.search(sub, y);
	search.makeSPT(sub);
	block_entry["search"] = search.json();

	// Extend the path
	PathExtension path;
	try {
	    y_next = path.extend(win.sub(start,end,start), y, path_gen);
	    block_entry["path_extend"] = path.json();
	} catch (const std::logic_error &) {
	    // This happens during the processing of column 52
	    EXPECT_EQ(x, 52);
	    break;
	}
	// Generate the pattern
	///\todo The amount the window extends backwards could be a parameter,
	/// but it is not of immediate interest to me how far back the window
	/// has to extend for realistic scenarios. Setting it to 10 should be
	/// large enough for all realistic backtracking.
	const long gen_start{std::max<long>(0, static_cast<long>(start)-10)};
	y_gen = gate.generate(win.sub(gen_start,end,start), y_gen, true);

	// Compute left nodes for simulation
	y_max_left = writeLeftNodes(win.sub(gen_start,start+2,start),
				    win(start, y_max_left));
	    
	// Simulate the column
	// Lag by one column so that the pattern endpoint is always one
	// column ahead.
	if (start > 0) {
	    col = sim.simulate(win.sub(start-1,std::min(start+1, end),start-1),
			       std::move(col), sim_start);
	    sim_start = false;	
	    auto left_height{sim.getLeftHeight()};
	    auto q_correct{gate.state(left_height)};
	    auto q{sim.state()};
	    byp = sim.getBypOps();
	    distance = qsl::fubiniStudy(q,q_correct);
	    EXPECT_NEAR(distance, 0, 1e-7);
	}

	// Update the starting y value
	y = y_next;
	    
    }
}
