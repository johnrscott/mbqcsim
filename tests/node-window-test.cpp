#include "gtest/gtest.h"
#include "node-window.hpp"
#include "path-node.hpp"
#include "distance-node.hpp"
#include "pattern-node.hpp"
#include "qubit-node.hpp"
#include "make-node.hpp"

using Node = BaseNode;

// Ostream that does not print
class NullBuffer : public std::streambuf
{
public:
    int overflow(int c) { return c; }
};

static NullBuffer null_buffer;

// Use this to surpress printed output in the tests
static std::ostream debug_stream(&null_buffer);

// Use this to view printed output
//static std::ostream & debug_stream{std::cout};

// Tests the default constructed NodeWindow
TEST(NodeWindowTest, DefaultConstructor)
{
    BaseNodeWindow<Node> win{5};
    win.checkConsistency(debug_stream);
    EXPECT_EQ(win.size(), 0);
    EXPECT_EQ(win.height(), 5);
    EXPECT_EQ(win.width(), 0);

    EXPECT_THROW(win[0], std::out_of_range);
    EXPECT_THROW(win(0,0), std::out_of_range);
}

// Tests the NodeWindow(height,width) constructor
TEST(NodeWindowTest, NonZeroWidthConstructor)
{
    BaseNodeWindow<Node> win{4,3};
    win.checkConsistency(debug_stream);
    EXPECT_EQ(win.size(), 4*3);
    EXPECT_EQ(win.height(), 4);
    EXPECT_EQ(win.width(), 3);
}

TEST(NodeWindowTest, NodeAccess)
{
    BaseNodeWindow<Node> win{5,6};
    win.checkConsistency(debug_stream);
    const std::size_t height{win.height()};
    const std::size_t width{win.width()};

    // Verify properties of nodes accessed by index
    for (std::size_t n = 0; n < win.size(); n++) {
	EXPECT_EQ(win[n].index(), n);
	EXPECT_EQ(win[n].x(), n/height);
	EXPECT_EQ(win[n].y(), n%height);
    }

    // Verify properties of nodes accessed by coordinates
    for (std::size_t x = 0; x < width; x++) {
	for (std::size_t y = 0; y < height; y++) {	    
	    EXPECT_EQ(win(x,y).index(), height*x + y);
	    EXPECT_EQ(win(x,y).x(), x);
	    EXPECT_EQ(win(x,y).y(), y);
	}
    }

    // Attempt out-of-range indexed access
    EXPECT_THROW(win[win.size()], std::out_of_range);
    
    // Attempt out-of-range access in the x-direction
    EXPECT_THROW(win(width,0), std::out_of_range);

    // Attempt out-of-range access in the y-direction
    EXPECT_THROW(win(0,height), std::out_of_range);
}

TEST(NodeWindowTest, AddColumn)
{
    BaseNodeWindow<Node> win{5};
    win.checkConsistency(debug_stream);
    // Start from 1 to simplify index calculation
    for (std::size_t n = 1; n < 20; n++) {
	win.addColumn();
	win.checkConsistency(debug_stream);

	EXPECT_EQ(win.size(), 5*n);
	EXPECT_EQ(win.height(), 5);
	EXPECT_EQ(win.width(), n);
    }
}

TEST(NodeWindowTest, SetEdge)
{
    BaseNodeWindow<Node> win{5,4};
    win.checkConsistency(debug_stream);

    // Out of range (x,y)
    EXPECT_THROW(win.setEdge({200,49},{201,49},true), std::out_of_range);

    // Not adjacent
    EXPECT_THROW(win.setEdge({1,1},{3,4},true), std::logic_error);

    // Diagonal edge
    EXPECT_THROW(win.setEdge({1,1},{2,2},true), std::logic_error);

    // Same coordinates
    EXPECT_THROW(win.setEdge({1,1},{1,1},true), std::logic_error);

    // Check horizontal right
    EXPECT_EQ(win.rightEdge(1,1), false);
    win.setEdge({1,1}, {2,1}, true);
    EXPECT_EQ(win.rightEdge(1,1), true);

    // Check horizontal left
    EXPECT_EQ(win.leftEdge(1,1), false);
    win.setEdge({1,1}, {0,1}, true);
    EXPECT_EQ(win.leftEdge(1,1), true);
    
    // Check vertical up
    EXPECT_EQ(win.aboveEdge(2,2), false);
    win.setEdge({2,2}, {2,3}, true);
    EXPECT_EQ(win.aboveEdge(2,2), true);

    // Check vertical down
    EXPECT_EQ(win.belowEdge(2,2), false);
    win.setEdge({2,2}, {2,1}, true);
    EXPECT_EQ(win.belowEdge(2,2), true);
}

TEST(NodeWindowTest, DefaultEdges)
{
    BaseNodeWindow<Node> win{5,4};
    win.checkConsistency(debug_stream);
    for (std::size_t n = 0; n < win.size(); n++) {
	EXPECT_EQ(win.leftEdge(n), false);
	EXPECT_EQ(win.rightEdge(n), false);
	EXPECT_EQ(win.aboveEdge(n), false);
	EXPECT_EQ(win.belowEdge(n), false);
    }
}

TEST(NodeWindowTest, EdgeByIndexExceptions)
{
    // Check the empty window
    BaseNodeWindow<Node> win{0,0};
    win.checkConsistency(debug_stream);

    EXPECT_THROW(win.leftEdge(0), std::out_of_range);
    EXPECT_THROW(win.rightEdge(0), std::out_of_range);
    EXPECT_THROW(win.aboveEdge(0), std::out_of_range);
    EXPECT_THROW(win.belowEdge(0), std::out_of_range);

    // Check non-empty window
    win = BaseNodeWindow<Node>(5,4);
    EXPECT_THROW(win.leftEdge(20), std::out_of_range);
    EXPECT_THROW(win.rightEdge(20), std::out_of_range);
    EXPECT_THROW(win.aboveEdge(20), std::out_of_range);
    EXPECT_THROW(win.belowEdge(20), std::out_of_range);
}

TEST(NodeWindowTest, EdgeByCoordinateExceptions)
{
    // Check the empty window
    BaseNodeWindow<Node> win{243,233};
    win.checkConsistency(debug_stream);

    // Check out of range to the left
    EXPECT_THROW(win.leftEdge(-1,0), std::out_of_range);
    EXPECT_THROW(win.rightEdge(-1,0), std::out_of_range);
    EXPECT_THROW(win.aboveEdge(-1,0), std::out_of_range);
    EXPECT_THROW(win.belowEdge(-1,0), std::out_of_range);

    // Check out of range to the right
    EXPECT_THROW(win.leftEdge(233,0), std::out_of_range);
    EXPECT_THROW(win.rightEdge(233,0), std::out_of_range);
    EXPECT_THROW(win.aboveEdge(233,0), std::out_of_range);
    EXPECT_THROW(win.belowEdge(233,0), std::out_of_range);

    // Check out of range above
    EXPECT_THROW(win.leftEdge(0,243), std::out_of_range);
    EXPECT_THROW(win.rightEdge(0,243), std::out_of_range);
    EXPECT_THROW(win.aboveEdge(0,243), std::out_of_range);
    EXPECT_THROW(win.belowEdge(0,243), std::out_of_range);

    // Check out of range below
    EXPECT_THROW(win.leftEdge(0,-1), std::out_of_range);
    EXPECT_THROW(win.rightEdge(0,-1), std::out_of_range);
    EXPECT_THROW(win.aboveEdge(0,-1), std::out_of_range);
    EXPECT_THROW(win.belowEdge(0,-1), std::out_of_range);
}

TEST(NodeWindowTest, ToggleAllEdges)
{
    // Make a window
    BaseNodeWindow<Node> win{12,32};
    win.checkConsistency(debug_stream);

    // Check all horizontal edges
    for (std::size_t x = 0; x < win.width(); x++) {
	for (std::size_t y = 0; y < win.height(); y++) {

	    // Test the right edges
	    if (x < win.width() - 1) {
		win.setEdge({x,y},{x+1,y}, 1); // Set the edge
		EXPECT_EQ(win.rightEdge(x,y), true); // Check the edge
		win.setEdge({x,y},{x+1,y}, 0); // Clear the right edge
	    }

	    // Test the left edges
	    if (x > 0) {
		win.setEdge({x,y},{x-1,y}, 1); // Set the edge
		EXPECT_EQ(win.leftEdge(x,y), true); // Clear the right edge
		win.setEdge({x,y},{x-1,y}, 0);
	    }

	    // Test the below-edges
	    if (y > 0) {
		win.setEdge({x,y},{x,y-1}, 1); // Set the edge
		EXPECT_EQ(win.belowEdge(x,y), true); // Clear the below-edge
		win.setEdge({x,y},{x,y-1}, 0);
	    }

	    // Test the above-edges
	    if (y < win.height()-1) {
		win.setEdge({x,y},{x,y+1}, 1); // Set the edge
		EXPECT_EQ(win.aboveEdge(x,y), true); // Clear the above-edge
		win.setEdge({x,y},{x,y+1}, 0);
	    }
	}
    }
}

TEST(NodeWindowTest, FullEdgeTest)
{
    // Make a window
    BaseNodeWindow<Node> win{12,32};
    win.checkConsistency(debug_stream);

    // Check all horizontal edges
    for (std::size_t x = 0; x < win.width()-1; x++) {
	for (std::size_t y = 0; y < win.height()-1; y++) {
	    win.setEdge({x,y},{x,y+1}, 1); // Set edge above
	    win.setEdge({x,y},{x+1,y}, 1); // Set to the right
	}
    }

    // Check that all internal edges are present
    for (std::size_t x = 0; x < win.width()-1; x++) {
	for (std::size_t y = 0; y < win.height()-1; y++) {

	    // Check the vertical edges
	    EXPECT_EQ(win.aboveEdge(x,y), true);
	    EXPECT_EQ(win.belowEdge(x,y+1), true);

	    // Check the horizontal edges
	    EXPECT_EQ(win.rightEdge(x,y), true);
	    EXPECT_EQ(win.leftEdge(x+1,y), true);
	}
    }

    // Check that the boundary node edges are false (the edges
    // that do not exist)
    for (std::size_t x = 0; x < win.width(); x++) {
	// Non-existent top and bottom edges	
	EXPECT_EQ(win.belowEdge(x,0), false);
	EXPECT_EQ(win.aboveEdge(x,win.height()-1), false);
    }

    for (std::size_t y = 0; y < win.height(); y++) {
	// Non-existent left and right edges	
	EXPECT_EQ(win.leftEdge(0,y), false);
	EXPECT_EQ(win.rightEdge(win.width()-1,y), false);
    }
}

TEST(NodeWindowTest, PrintExceptions)
{    
    // Make a window
    BaseNodeWindow<Node> win{12,32};
    win.checkConsistency(debug_stream);

    // Invalid bottom-left and top-right nodes (horizontal)
    EXPECT_THROW(win.print({1,2},{0,5}, debug_stream), std::logic_error);

    // Invalid bottom-left and top-right nodes (vertical)
    EXPECT_THROW(win.print({1,2},{4,1}, debug_stream), std::logic_error);

    // Out of range to the left
    EXPECT_THROW(win.print({-2,0},{2,3}, debug_stream), std::out_of_range);

    // Out of range to the right
    EXPECT_THROW(win.print({0,0},{200,3}, debug_stream), std::out_of_range);

    // Out of range above
    EXPECT_THROW(win.print({0,0},{0,30}, debug_stream), std::out_of_range);

    // Out of range below
    EXPECT_THROW(win.print({0,-3},{0,2}, debug_stream), std::out_of_range);
}

TEST(NodeWindowTest, SetAllEdges)
{
    // Make a window
    BaseNodeWindow<Node> win{12,32};
    win.checkConsistency(debug_stream);

    // Check all edges are empty
    for (std::size_t n = 0; n < win.size(); n++) {
	EXPECT_EQ(win.leftEdge(n),false);
	EXPECT_EQ(win.rightEdge(n),false);
	EXPECT_EQ(win.aboveEdge(n),false);
	EXPECT_EQ(win.belowEdge(n),false);
    }

    // Set all the edges to true
    setAllEdges(win, true);
    win.checkConsistency(debug_stream);

    // Check all edges are set
    for (std::size_t x = 0; x < win.width(); x++) {
	for (std::size_t y = 0; y < win.height(); y++) {
	    if (x > 0) {
		EXPECT_EQ(win.leftEdge(x,y), true);
	    }
	    if (x < win.width()-1) {
		EXPECT_EQ(win.rightEdge(x,y), true);
	    }
	    if (y > 0) {
		EXPECT_EQ(win.belowEdge(x,y), true);
	    }
	    if (y < win.height()-1) {
		EXPECT_EQ(win.aboveEdge(x,y), true);
	    }
	}
    }

    // Now clear all the edges again
    setAllEdges(win, false);
    win.checkConsistency(debug_stream);

    // Check all edges are empty
    for (std::size_t n = 0; n < win.size(); n++) {
	EXPECT_EQ(win.leftEdge(n),false);
	EXPECT_EQ(win.rightEdge(n),false);
	EXPECT_EQ(win.aboveEdge(n),false);
	EXPECT_EQ(win.belowEdge(n),false);
    }
}

TEST(NodeWindowTest, SubNodeWindowSizes)
{
    // Make a window
    BaseNodeWindow<Node> win{12,32};
    
    // Create subwindow
    SubNodeWindow<Node> sub{win.sub(2,4,2)};

    // Check that the subwindow has the correct size
    EXPECT_EQ(sub.size(), win.height()*(4-2));
    EXPECT_EQ(sub.height(), win.height());
    EXPECT_EQ(sub.width(), 4-2);
}

/**
 *
 *
 */
TEST(NodeWindowTest, SubNodeWindowCoordinates)
{
    BaseNodeWindow<Node> win{6,20};

    EXPECT_EQ(win(2,1).x(), 2);
    EXPECT_EQ(win(2,1).y(), 1);
    
    long int start{2};
    long int end{6};
    long int zero{3};
    auto sub{win.sub(start,end,zero)};

    // EXPECT_EQ(sub(-1,1).x(), 2);
    // EXPECT_EQ(sub(-1,1).y(), 1);
    // EXPECT_EQ(sub.getNode(-1,1).x(), 2);
    // EXPECT_EQ(sub.getNode(-1,1).y(), 1);
	
}

TEST(NodeWindowTest, GetNode)
{
    BaseNodeWindow<Node> win{6,20};
    const long int H{static_cast<long int>(win.height())};
    const long int W{static_cast<long int>(win.width())};

    // Check coordinates and indices
    for (long int x{0}; x < W; x++) {
	for (long int y{0}; y < H; y++) {
		EXPECT_EQ(win.getNode(x,y).x(), x);
		EXPECT_EQ(win.getNode(x,y).y(), y);
		EXPECT_EQ(win.getNode(x,y).index(), H*x+y);
		EXPECT_EQ(win(x,y).x(), x);
		EXPECT_EQ(win(x,y).y(), y);
		EXPECT_EQ(win(x,y).index(), H*x+y);
	}
    }
}

TEST(NodeWindowTest, GetNodeSubwindow)
{
    BaseNodeWindow<Node> win{6,20};
    
    // Create subwindow
    long int start{2};
    long int end{6};
    long int zero{3};
    SubNodeWindow<Node> sub{win.sub(start,end,zero)};

    const long int H{static_cast<long int>(sub.height())};
    //const long int W{static_cast<long int>(sub.width())};

    // Check coordinates and indices
    for (long int x{start-zero}; x < end-zero; x++) {
	for (long int y{0}; y < H; y++) {
	    EXPECT_EQ(sub.getNode(x,y).x(), x+zero);
	    EXPECT_EQ(sub.getNode(x,y).y(), y);
	    //EXPECT_EQ(win.getNode(x,y).index(), H*x+y);
	    EXPECT_EQ(sub(x,y).x(), x+zero);
	    EXPECT_EQ(sub(x,y).y(), y);
	    //EXPECT_EQ(win(x,y).index(), H*x+y);
	}
    }
}

/**
 * \brief Test repeatability of range edge generation
 */
TEST(NodeWindowTest, RandomEdgeGeneration)
{
    BaseNodeWindow<Node> win{10,10};

    Seed seed1; // Make random seed
    setRandomEdges(win, 0.65, seed1); // Create random graph

    // Perform the same operation on another window
    BaseNodeWindow<Node> win2{10,10};

    Seed seed2{seed1}; // Make the same random seed as before
    setRandomEdges(win2, 0.65, seed2); // Create random graph

    win.print();
    win2.print();
    
    // Check all edges are equal
    for (std::size_t x{0}; x < win.width(); x++) {
	for (std::size_t y{0}; y < win.height(); y++) {
	    EXPECT_EQ(win.aboveEdge(x,y), win2.aboveEdge(x,y));
	    EXPECT_EQ(win.belowEdge(x,y), win2.belowEdge(x,y));
	    EXPECT_EQ(win.leftEdge(x,y), win2.leftEdge(x,y));
	    EXPECT_EQ(win.rightEdge(x,y), win2.rightEdge(x,y));
	}
    }

    // Perform the same operation with manual seeding
}


/**
 * \brief Basic checking of SubNodeWindow edges
 */
TEST(NodeWindowTest, SubNodeWindowEdges)
{
    // Make a window
    BaseNodeWindow<Node> win{124,322};

    // Add random edges
    setRandomEdges(win, 0.65);
    
    // Create subwindow
    long int start{142};
    long int end{234};
    long int zero{start};
    SubNodeWindow<Node> sub{win.sub(start,end,zero)};
    sub.checkConsistency(debug_stream);

    // Check the edges agree between the window and subwindow
    for (long int y = 0; y < static_cast<long int>(win.height()); y++) {
	for (long int x = start; x < end; x++) {
	    if (x > start) {
		EXPECT_EQ(win.leftEdge(x,y), sub.leftEdge(x-start,y));
	    } else {
		EXPECT_EQ(sub.leftEdge(x-start,y), false);
	    }
	    if (x < end-1) {
		EXPECT_EQ(win.rightEdge(x,y), sub.rightEdge(x-start,y));
	    } else {
		EXPECT_EQ(sub.rightEdge(x-start,y), false);
	    }
	    if (y < static_cast<long int>(win.height())-1) {
		EXPECT_EQ(win.aboveEdge(x,y), sub.aboveEdge(x-start,y));
	    } else {
		EXPECT_EQ(sub.aboveEdge(x-start,y), false);
	    }
	    if (y > 0) {
		EXPECT_EQ(win.belowEdge(x,y), sub.belowEdge(x-start,y));
	    } else {
		EXPECT_EQ(sub.belowEdge(x-start,y), false);
	    }
	}
    }

    // Create another subwindow (a sub-subwindow)
    start = 1;
    end = 3;
    zero = start;
    SubNodeWindow<Node> subsub{sub.sub(start,end,zero)};
    subsub.checkConsistency(debug_stream);

    // Check the edges agree between the window and subwindow
    for (long int y = 0; y < static_cast<long int>(sub.height()); y++) {
	for (long int x = start; x < end; x++) {
	    if (x > start) {
		EXPECT_EQ(sub.leftEdge(x,y), subsub.leftEdge(x-start,y));
	    } else {
		EXPECT_EQ(subsub.leftEdge(x-start,y), false);
	    }
	    if (x < end-1) {
		EXPECT_EQ(sub.rightEdge(x,y), subsub.rightEdge(x-start,y));
	    } else {
		EXPECT_EQ(subsub.rightEdge(x-start,y), false);
	    }
	    if (y < static_cast<long int>(sub.height())-1) {
		EXPECT_EQ(sub.aboveEdge(x,y), subsub.aboveEdge(x-start,y));
	    } else {
		EXPECT_EQ(subsub.aboveEdge(x-start,y), false);
	    }
	    if (y > 0) {
		EXPECT_EQ(sub.belowEdge(x,y), subsub.belowEdge(x-start,y));
	    } else {
		EXPECT_EQ(subsub.belowEdge(x-start,y), false);
	    }

	}
    }
}

/**
 * \brief Checking SubNodeWindows with non-trivial zero-column
 *
 */ 
TEST(NodeWindowTest, SubNodeWindowZeroCol)
{
    // Make a window
    BaseNodeWindow<Node> win{124,322};

    // Add random edges
    setRandomEdges(win, 0.65);

    // Get a SubWindow
    // long int start{142};
    // long int end{234};
    // long int zero{150};
    long int start{142};
    long int end{147};
    long int zero{4};
    SubNodeWindow<Node> sub{win.sub(start,end,zero)};
    sub.checkConsistency(debug_stream);

    // Check the edges
    for (long int x = start - zero; x < end - zero; x++) {
	for (long int y = 0; y < static_cast<long int>(win.height()); y++) {
	    if (x > start - zero) {
		EXPECT_EQ(sub.leftEdge(x,y), win.leftEdge(x+zero,y));
	    } else {
		EXPECT_EQ(sub.leftEdge(x,y), false);
		EXPECT_THROW(sub.leftEdge(x-1,y), std::out_of_range);
	    }
	    if (x < end - zero - 1) {
		EXPECT_EQ(sub.rightEdge(x,y), win.rightEdge(x+zero,y));
	    } else {
		EXPECT_EQ(sub.rightEdge(x,y), false);
		EXPECT_THROW(sub.rightEdge(x+1,y), std::out_of_range);
	    }
	    if (y < static_cast<long int>(win.height())-1) {
	    	EXPECT_EQ(sub.aboveEdge(x,y), win.aboveEdge(x+zero,y));
	    } else {
	    	EXPECT_EQ(sub.aboveEdge(x,y), false);
		EXPECT_THROW(sub.aboveEdge(x,y+1), std::out_of_range);
	    }
	    if (y > 0) {
	    	EXPECT_EQ(sub.belowEdge(x,y), win.belowEdge(x+zero,y));
	    } else {
	    	EXPECT_EQ(sub.belowEdge(x,y), false);
		EXPECT_THROW(sub.belowEdge(x,y-1), std::out_of_range);
	    }
	}
    }
    
}

TEST(NodeWindowTest, BasePrint)
{
    // Make a window
    BaseNodeWindow<Node> win{12,32};

    // Should work
    win.print({0,0}, {32,12}, debug_stream);

    // Expect error due to wrongly specified rectangle (y direction)
    EXPECT_THROW(win.print({0,12}, {32,0}, debug_stream), std::logic_error);

    // Expect error due to wrongly specified rectangle (x direction)
    EXPECT_THROW(win.print({32,0}, {0,12}, debug_stream), std::logic_error);

    // Expect error due to wrongly specified rectangle (x/y directions)
    EXPECT_THROW(win.print({32,12}, {0,0}, debug_stream), std::logic_error);

    // Expect error due to out of range coordinates
    EXPECT_THROW(win.print({-1,0}, {32,12}, debug_stream), std::out_of_range);
    EXPECT_THROW(win.print({0,-1}, {32,12}, debug_stream), std::out_of_range);
    EXPECT_THROW(win.print({0,0}, {33,12}, debug_stream), std::out_of_range);
    EXPECT_THROW(win.print({0,0}, {32,13}, debug_stream), std::out_of_range);

    // Create a subwindow
    long int start{4};
    long int end{11};
    long int zero{7};
    SubNodeWindow<Node> sub{win.sub(start,end,zero)};
    sub.checkConsistency(debug_stream);

    // Check edge cases
    sub.print({-3,0}, {4,12}, debug_stream); // Valid
    EXPECT_THROW(sub.print({-4,0}, {4,12}, debug_stream), std::out_of_range);
    EXPECT_THROW(sub.print({-3,0}, {5,12}, debug_stream), std::out_of_range);
    EXPECT_THROW(sub.print({-3,-1}, {4,12}, debug_stream), std::out_of_range);
    EXPECT_THROW(sub.print({-3,0}, {4,13}, debug_stream), std::out_of_range);
}

TEST(NodeWindowTest, SubWindowExceptions)
{
    // Make a window
    BaseNodeWindow<Node> win{124,322};

    // Create subwindow (should work)
    auto sub{win.sub(3,12,5)};

    // Expect error due to begin > end
    EXPECT_THROW(win.sub(12,0,0), std::logic_error);

    // This edge case should work
    auto subsub{sub.sub(-2,7,-2)};
    
    // Expect error due to wrong begin index
    EXPECT_THROW(sub.sub(-3,7,0), std::logic_error);

    // Expect error due to wrong end index
    EXPECT_THROW(sub.sub(-2,8,0), std::logic_error);

}

TEST(NodeWindowTest, NoNeighbours)
{
    // Make a window
    BaseNodeWindow<Node> win{124,322};    

    // Check that no node has any neighbour
    for (std::size_t n{0}; n < win.size(); n++) {
	EXPECT_EQ(win.neighbours(n).size(), 0);
    }
}

TEST(NodeWindowTest, AllNeighbours)
{
    // Make a window
    BaseNodeWindow<Node> win{124,322};    

    // Add all edges
    setAllEdges(win, true);

    long int W{static_cast<long int>(win.width())};
    long int H{static_cast<long int>(win.height())};
    
    // Check numbers of neighbours
    for (long int x{0}; x < W; x++) {
	for (long int y{0}; y < H; y++) {

	    if ((x == 0 or x == W-1) and (y == 0 or y == H-1)) {
		// Check corner nodes have two neighbours
		EXPECT_EQ(win.neighbours(x,y).size(), 2);
	    } else if (x == 0 or x == W-1 or y == 0 or y == H-1) {
		// Check side nodes have three neighbours
		EXPECT_EQ(win.neighbours(x,y).size(), 3);
	    } else {
		// Check other nodes have four neighbours
		EXPECT_EQ(win.neighbours(x,y).size(), 4);
	    }
	}
    }
}

TEST(NodeWindowTest, NeighbourTest)
{
    // Make a window
    BaseNodeWindow<Node> win{124,322};    

    //long int W{static_cast<long int>(win.width())};
    long int H{static_cast<long int>(win.height())};
    
    // Right neighbour
    win.setEdge({12,23}, {13,23}, true);
    EXPECT_EQ(win.neighbours(12,23).size(), 1);
    EXPECT_EQ(win.neighbours(12,23)[0], 12*H+23+H);

    // Left neighbour
    win.setEdge({12,24}, {11,24}, true);
    EXPECT_EQ(win.neighbours(12,24).size(), 1);
    EXPECT_EQ(win.neighbours(12,24)[0], 12*H+24-H);

    // Neighbour above
    win.setEdge({45,23}, {45,24}, true);
    EXPECT_EQ(win.neighbours(45,23).size(), 1);
    EXPECT_EQ(win.neighbours(45,23)[0], 45*H+23+1);

    // Neighbour below
    win.setEdge({35,23}, {35,22}, true);
    EXPECT_EQ(win.neighbours(35,23).size(), 1);
    EXPECT_EQ(win.neighbours(35,23)[0], 35*H+23-1);
}

TEST(NodeWindowTest, GetColumnIndex)
{
    BaseNodeWindow<Node> win{32,54};
    win.checkConsistency(debug_stream);
    const long int height{static_cast<long int>(win.height())};
    //const std::size_t width{win.width()};

    // Check basic column indices are correct
    for (std::size_t n = 0; n < win.size(); n++) {
	const Node & node{win[n]};
	EXPECT_EQ(win.getColumnIndex(node), n/height);
    }

    // Create a subwindow with trivial zero column
    auto sub{win.sub(23,40,23)};

    // Check shifted column indices are correct
    for (std::size_t n = 0; n < sub.size(); n++) {
	const Node & node{sub[n]};
	EXPECT_EQ(sub.getColumnIndex(node), (n/height));
    }

    // Create a subwindow with non-trivial zero column
    sub = win.sub(23,40,31);

    // Check shifted column indices are correct
    for (std::size_t n = 0; n < sub.size(); n++) {
	const Node & node{sub[n]};
	EXPECT_EQ(sub.getColumnIndex(node), (n/height)+(23-31));
    }

    
}

TEST(NodeWindowTest, CopyBaseNodeWindow)
{
    BaseNodeWindow<Node> win{4,5};
    setRandomEdges(win,0.5);

    auto win_copy{copy(win)};

    // Check everything is equal
    EXPECT_EQ(win_copy.height(), win.height());
    EXPECT_EQ(win_copy.width(), win.width());
    EXPECT_EQ(win_copy.size(), win.size());

    for (std::size_t n{0}; n < win_copy.size(); n++) {
	EXPECT_EQ(win_copy.leftEdge(n), win.leftEdge(n));
	EXPECT_EQ(win_copy.rightEdge(n), win.rightEdge(n));
	EXPECT_EQ(win_copy.aboveEdge(n), win.aboveEdge(n));
	EXPECT_EQ(win_copy.belowEdge(n), win.belowEdge(n));
    }

    // Check the windows are independent
    setAllEdges(win, true);
    setAllEdges(win_copy, false);

    for (std::size_t x{1}; x < win.width()-1; x++) {
	for (std::size_t y{1}; y < win.height()-1; y++) {
	    EXPECT_EQ(win.leftEdge(x,y), true);
	    EXPECT_EQ(win.rightEdge(x,y), true);
	    EXPECT_EQ(win.aboveEdge(x,y), true);
	    EXPECT_EQ(win.belowEdge(x,y), true);
	}
    }
    
    for (std::size_t n{0}; n < win_copy.size(); n++) {
	EXPECT_EQ(win_copy.leftEdge(n), false);
	EXPECT_EQ(win_copy.rightEdge(n), false);
	EXPECT_EQ(win_copy.aboveEdge(n), false);
	EXPECT_EQ(win_copy.belowEdge(n), false);
    }
}

TEST(NodeWindowTest, CopySubNodeWindow)
{
    BaseNodeWindow<Node> win{4,10};
    setRandomEdges(win,0.5);

    auto sub{win.sub(1,8,2)};
    
    auto sub_copy{copy(sub)};
    win.print();
    sub.print();
    sub_copy.print();
    // Check everything is equal
    EXPECT_EQ(sub_copy.height(), sub.height());
    EXPECT_EQ(sub_copy.width(), sub.width());
    EXPECT_EQ(sub_copy.size(), sub.size());

    for (std::size_t n{0}; n < sub_copy.size(); n++) {
	std::cout << n << std::endl;
	EXPECT_EQ(sub_copy.leftEdge(n), sub.leftEdge(n));
	EXPECT_EQ(sub_copy.rightEdge(n), sub.rightEdge(n));
	EXPECT_EQ(sub_copy.aboveEdge(n), sub.aboveEdge(n));
	EXPECT_EQ(sub_copy.belowEdge(n), sub.belowEdge(n));
    }

    // Check the subdows are independent
    setAllEdges(sub, true);
    setAllEdges(sub_copy, false);

    for (long x{0}; x < 5; x++) {
    	for (long y{1}; y < static_cast<long>(sub.height())-1; y++) {
    	    EXPECT_EQ(sub.leftEdge(x,y), true);
    	    EXPECT_EQ(sub.rightEdge(x,y), true);
    	    EXPECT_EQ(sub.aboveEdge(x,y), true);
    	    EXPECT_EQ(sub.belowEdge(x,y), true);
    	}
    }
    
    for (std::size_t n{0}; n < sub_copy.size(); n++) {
    	EXPECT_EQ(sub_copy.leftEdge(n), false);
    	EXPECT_EQ(sub_copy.rightEdge(n), false);
    	EXPECT_EQ(sub_copy.aboveEdge(n), false);
    	EXPECT_EQ(sub_copy.belowEdge(n), false);
    }
}
