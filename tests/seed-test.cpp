#include "gtest/gtest.h"
#include "seed.hpp"

// Tests the constructors of Seed
TEST(SeedTest, Constructors)
{
    Seed s1; // Random seed
    Seed s2{s1.seed()}; // Copy the same seed
    EXPECT_EQ(s1.seed(), s2.seed());
    EXPECT_EQ(s1.json().at("seed"), s1.seed()); // Check the JSON is correct
    EXPECT_EQ(s2.json().at("seed"), s1.seed()); // Check the JSON is correct
    
    // Check copy constructor
    Seed s3{s1};
    EXPECT_EQ(s3.seed(), s1.seed());
    EXPECT_EQ(s3.json().at("seed"), s1.seed());
}
