#include "gtest/gtest.h"
#include "cmdline.hpp"

/**
 * \brief Missing cmdline tests
 *
 * The CommandLine object is based on getopt, and currently can't
 * easily be tested becasue of the way it is designed. (Badly.)
 * For now, the CommandLine is tested implicitely by checking that
 * the JSON output parameters correspond to the parameters passed
 * on the command line.
 *
 */

/*
TEST(CmdlineTest, ArgsValues)
{
    CommandLine cmd;
    
    cmd.addOption<std::string>('s', "string", "string test");
    cmd.addOption<bool>('o', "bool", "bool test");
    cmd.addOption<int>('i', "int", "int test");
    cmd.addOption<unsigned>('u', "unsigned-int", "unsigned int test");
    cmd.addOption<long int>('q', "long", "long test");
    cmd.addOption<long long int>('Q', "long long", "long long test");
    cmd.addOption<std::size_t>('w', "size_t", "Size_t test");
    cmd.addOption<double>('d', "double", "double test");
    cmd.addOption<long double>('L', "long-double", "long doubletest");
    cmd.addOption<float>('f', "float", "float test");

    char arg0[] {"prog"};
    char arg1[] {"-sa-string"};
    char arg2[] {"-o"};
    char arg3[] {"-i123"};
    char arg4[] {"-d1.23e-45"};
    char arg5[] {"-f-0.875e-16"};    
    char arg6[] {"-L0.125e-98"};
    char arg7[] {"-w12345"};
    char arg8[] {"-u0"};
    char arg9[] {"-q12634879"};
    char arg10[] {"-Q-12634879"};
    
    char * argv[] {arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7,
	    arg8, arg9, arg10, NULL};

    cmd.parse(11, argv);

    // Check string
    EXPECT_EQ(cmd.get<std::string>('s').has_value(), true);
    EXPECT_EQ(cmd.get<std::string>('s').value(), "a-string");

    // Check bool option
    EXPECT_EQ(cmd.get<bool>('o').has_value(), true);
    EXPECT_EQ(cmd.get<bool>('o').value(), true);

    // Check int
    EXPECT_EQ(cmd.get<int>('i').has_value(), true);
    EXPECT_EQ(cmd.get<int>('i').value(), 123);

    // Check unsigned 
    EXPECT_EQ(cmd.get<unsigned>('u').has_value(), true);
    EXPECT_EQ(cmd.get<unsigned>('u').value(), 0);

    // Check long int
    EXPECT_EQ(cmd.get<long>('q').has_value(), true);
    EXPECT_EQ(cmd.get<long>('q').value(), 12634879);

    // Check long long int
    EXPECT_EQ(cmd.get<long long>('Q').has_value(), true);
    EXPECT_EQ(cmd.get<long long>('Q').value(), -12634879);

    // Check size_t 
    EXPECT_EQ(cmd.get<std::size_t>('w').has_value(), true);
    EXPECT_EQ(cmd.get<std::size_t>('w').value(), 12345);
    
    // Check double
    EXPECT_EQ(cmd.get<double>('d').has_value(), true);
    EXPECT_NEAR(cmd.get<double>('d').value(), 1.23e-45, 1e-50);

    // Check long double
    EXPECT_EQ(cmd.get<long double>('L').has_value(), true);
    EXPECT_NEAR(cmd.get<long double>('L').value(), 0.125e-98, 1e-103);
    
    // Check float
    EXPECT_EQ(cmd.get<float>('f').has_value(), true);
    EXPECT_NEAR(cmd.get<float>('f').value(), -0.875e-16, 1e-21);

}

TEST(CmdlineTest, UnrecognisedOption)
{
    CommandLine cmd;
    
    cmd.addOption<std::string>('s', "string", "string test");

    char arg0[] {"prog"};
    char arg1[] {"-o"};
    
    char * argv[] {arg0, arg1, NULL};

    EXPECT_EQ(cmd.parse(2, argv), -1);
}

TEST(CmdlineTest, UnsignedIntRange)
{
    CommandLine cmd;    
    cmd.addOption<unsigned>('u', "unsigned-int", "unsigned int test");
    char arg0[] {"prog"};
    char arg1[] {"-u9999999999999999999999999999999999999999"};
    char * argv[] { arg0, arg1, NULL};
    EXPECT_EQ(cmd.parse(2, argv), -1);
}
*/
