#include "gtest/gtest.h"
#include "distance-node.hpp"
#include "path-node.hpp"
#include "make-node.hpp"
#include "node-window.hpp"

#include "advance-iterator.hpp"

#include <random>
#include <algorithm>

// Path node type
using Node = MakeNode<DistanceNode, PathNode>;

TEST(AdvanceIterator, EndSentinel)
{
    Generator<unsigned, 0, 1> gen{Seed{}};
    BaseNodeWindow<Node> win{10,10};

    // Create some successors
    win(0,0).addSuccessor(win(1,0));
    win(1,0).addSuccessor(win(2,0));

    // Make a subwindow
    auto sub{win.sub(0,2,0)};
    RandomAdvanceIterator it{sub(0,0), sub, gen};

    ++it; // Advance (no successors, out of range access)
    EXPECT_EQ(*it, sub(1,0));

    ++it;
    EXPECT_EQ(it, RandomAdvanceSentinel{});    
}
