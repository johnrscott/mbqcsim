#include "gtest/gtest.h"
#include "base-node.hpp"

// Tests the constructor and basic members of BaseNode
TEST(BaseNodeTest, Constructor)
{
    // Check that using height zero throws an exception
    std::size_t height{0};
    std::size_t index{5};
    EXPECT_THROW((BaseNode{index,height}), std::logic_error);

    // Check construction when index less than height
    height = 3;
    index = 2;
    BaseNode node{index,height};
    EXPECT_EQ(node.index(), index);
    EXPECT_EQ(node.x(), 0);
    EXPECT_EQ(node.y(), 2);

    // Check construction when index is zero
    height = 10;
    index = 0; 
    node = BaseNode{index,height};
    EXPECT_EQ(node.index(), 0);
    EXPECT_EQ(node.x(), 0);
    EXPECT_EQ(node.y(), 0);

    // Check construction when index is zero
    height = 10;
    index = 0; 
    node = BaseNode{index,height};
    EXPECT_EQ(node.index(), 0);
    EXPECT_EQ(node.x(), 0);
    EXPECT_EQ(node.y(), 0);
}
