# Fixed output data from pathf and esim to test against

These are the command line arguments used to generate each file. Copy and paste the block to produce the data in this folder (make sure esim and pathf are in your path first). The data in this folder are used to check that builds produce the correct output (the data contained in this folder). To find out what binary created the data, look at the build key in the json output.

```bash
# out0
pathf -s15591010429174318617 -H10 -W200 -B11 -p0.9 -m -o out0_min.json
pathf -s15591010429174318617 -H10 -W200 -B11 -p0.9 -o out0_full.json
# out1
pathf -s4879641877954071836 -aibfs -H10 -W200 -B11 -p0.65 -m -o out1_min.json
pathf -s4879641877954071836 -aibfs -H10 -W200 -B11 -p0.65 -o out1_full.json
# out2
pathf -s12472593332314317353 -agbfs -H10 -W10 -B5 -p0.85 -n5 -m -o out2_min.json
pathf -s12472593332314317353 -agbfs -H10 -W10 -B5 -p0.85 -n5 -o out2_full.json

# out3
esim -s10743329286908062025 -H3 -W10 -B4 -p0.95 -n2 -m -o out3_min.json
esim -s10743329286908062025 -H3 -W10 -B4 -p0.95 -n2 -o out3_full.json
# out4
esim -s12802111341919126133 -H4 -W8 -B3 -p0.98 -e0.01 -f-0.01 -m -o out4_min.json
esim -s12802111341919126133 -H4 -W8 -B3 -p0.98 -e0.01 -f-0.01 -o out4_full.json
# out5
esim -s13800442061982097838 -H5 -W5 -B3 -p0.99 -e2 -f1 -n2 -m -o out5_min.json
esim -s13800442061982097838 -H5 -W5 -B3 -p0.99 -e2 -f1 -n2 -o out5_full.json
```
