#!/usr/bin/python3
import subprocess
import json

# Up a directory from the pytest folder in build/
PATHF = "@PATHF_PATH@"

# Test fixed data outputs from pathf (stored in testdata)
def test_pathf_fixed():

    # Create a dictionary of arguments and known output files
    arg_dict = {
        "out0_min.json" : ["-s15591010429174318617", "-H10", "-W200",
                           "-B11", "-p0.9", "-m"],
        "out0_full.json" : ["-s15591010429174318617", "-H10", "-W200",
                            "-B11", "-p0.9"],
        "out1_min.json" : ["-s4879641877954071836", "-aibfs", "-H10",
                           "-W200", "-B11", "-p0.65", "-m"],
        "out1_full.json" : ["-s4879641877954071836", "-aibfs", "-H10",
                            "-W200", "-B11", "-p0.65"],
        "out2_min.json" : ["-s12472593332314317353", "-agbfs", "-H10",
                           "-W10", "-B5", "-p0.85", "-n5", "-m"],
        "out2_full.json" : ["-s12472593332314317353", "-agbfs", "-H10",
                            "-W10", "-B5", "-p0.85", "-n5"]
    }

    for testfile, args in arg_dict.items():
    
        # Run the command and get the results directly
        stdout = subprocess.check_output([PATHF] + args)
        result_json = json.loads(stdout)

        # Load the known results from a file
        with open("pytest/testdata/" + testfile) as f:
            correct_json = json.load(f)

        # Remove the build key from each file
        del result_json["build"]
        del correct_json["build"]

        # Check that the json objects are the same
        assert(result_json == correct_json)

# Check that the next seed stored in the json file is correct
def test_pathf_next_seed():

    # Store the difference between root_seed and next_seed for different args
    arg_dict = {("-m","-n1") : 2,
                ("-m","-n5") : 10,
                ("-s1","-n2") : 4,}

    # Check seed differences
    for args, seed_diff in arg_dict.items():
    
        # Run the command and get the results directly
        stdout = subprocess.check_output([PATHF] + list(args))
        j = json.loads(stdout)
        
        # Check that the seed values are correct
        assert(j["next_seed"] == j["root_seed"] + seed_diff)
