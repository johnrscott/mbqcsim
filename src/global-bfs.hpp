/**
 * \file global-bfs.hpp
 * \brief Global breadth-first search algorithm 
 *
 */

#ifndef GLOBAL_BFS_HPP
#define GLOBAL_BFS_HPP

#include <queue>
#include "node-window.hpp"
#include "predecessor-iterator.hpp"
#include "advance-iterator.hpp"
#include "utils.hpp"
#include <algorithm>
#include <map>

#include <nlohmann/json.hpp>

/**
 * \brief Helper to compute the number of successor write locations
 *
 * This function calculates the number of writes that are needed to
 * clear the successors in a block, provided that the simplest scheme
 * is used where each node stores a separate memory location for each
 * successor.
 *
 * The reason this calculation is not simply 4*numNodes is that the
 * nodes on the corners and edges do not store memory locations for 
 * four nodes. This function provides the correct calculation for
 * any size grid (containing at least one node). Nodes at the edge
 * contribute 3 possible memory locations, and nodes at the corner
 * contribute two. Edge cases are covered.
 * 
 */
std::size_t totalSuccWrites(std::size_t H, std::size_t W);

/**
 * \brief Global breadth-first search (GBFS) to calculate potential path 
 * successors
 *
 * This class implements the GBFS and shortest-path tree (SPT) calculations
 * needed to extend a path according to this search method.
 *
 * It also contains performance counters to track how many times certain
 * operations are performed, such as writes to the node window.
 */
class GBFS
{
public:
    enum class Count
    {
	/**
	 * \brief Count the number of times a predecessor offset is written
	 */
	PRED_WRITES,

	/**
	 * \brief Number of times a successor is written while making the SPT
	 */
	SUCC_WRITES,
	
	/**
	 * \brief Count the number of times a distance is written
	 *
	 * During the BFS algorithm, distance data is written to the nodes.
	 * This count keeps track of how many times a distance was written.
	 */
	DISTANCE_WRITES,

	/**
	 * \brief Number of pushes to the queue
	 */
	QUEUE_PUSHES,
    };

    GBFS(std::size_t height) : H_{height} {}
    
    /**
     * \brief Reset all the performance counters
     */
    void resetCounts() {
	counts_.clear();
    }

    void print() const
	{
	    std::cout << "H=" << H_ << ";W=" << W_
		      << ";pred_writes=" << getCount(Count::PRED_WRITES)
		      << ";succ_writes=" << getCount(Count::SUCC_WRITES)
		      << ";distance_writes=" << getCount(Count::DISTANCE_WRITES)
		      << ";queue_pushes=" << getCount(Count::QUEUE_PUSHES)
		      << std::endl;
	}

    /// Get data contents of object (performance counters) as JSON
    nlohmann::json json() const
	{
	    nlohmann::json j;

    	    // Add header information
    	    j["height"] = H_;
    	    j["width"] = W_;

	    auto & cnt{j["counts"]};
	    cnt["pred_writes"] = getCount(Count::PRED_WRITES);
	    cnt["succ_writes"] = getCount(Count::SUCC_WRITES);
	    cnt["distance_writes"] = getCount(Count::DISTANCE_WRITES);
	    cnt["queue_push"] = getCount(Count::QUEUE_PUSHES);
	    return j;
	}

    /**
     * \brief Get the performance counters
     */
    std::map<Count,std::size_t> counts() const
	{
	    return counts_;
	}
    
    
    
    /**
     * \brief Perform a global breadth-first-search over the graph
     *
     * Pass the coordinates of the starting point. The routine will
     * clear all the old path data from the nodes and perform a new
     * search from scratch. Pass the coordinates of the starting node
     * for the search.
     *
     * This function should be moved to the global breadth-first search
     * object. Performing a full breadth-first search on the whole cluster
     * should ne seen as just performing a regular global breadth-first 
     * search with an active window over the whole cluster. 
     */
    template<typename Node, bool Base>
    void search(NodeWindow<Node,Base> & win, const std::size_t start)
	{
	    if (win.height() not_eq H_) {
		throw std::logic_error("Window with invalid height "
				       "passed to GBFS::search()");
	    }

	    // Reset counters
	    ///\todo The counter data should be encapsulated inside this
	    /// function. To be fixed later.
	    resetCounts();
	    W_ = win.width();
	    
	    // Reset the search data in the nodes
	    ///\todo Replace with iterator
	    for (std::size_t n{0}; n < win.size(); n++) {
		win[n].searchReset();
	    }

	    // Add the writes caused due to the search reset
	    counts_[Count::PRED_WRITES] += win.size();

	    // This is multiplied by four because each node could have
	    // a successor at each of its four neighbours. The simplest
	    // way to clear this data is to write four values per node.
	    // The exact calculation is performed by the totalSuccWrites(H,W)
	    // function which takes care of the nodes around the edge
	    counts_[Count::SUCC_WRITES] += totalSuccWrites(H_, W_);
	    counts_[Count::DISTANCE_WRITES] += win.size();	    
	    
	    // Queue to store the indices of nodes that need processing
	    // Indices are for the active window
	    std::queue<std::size_t> queue;

	    // Push the first node to the queue
	    queue.push(start);
	    counts_[Count::QUEUE_PUSHES]++;
		
	    // Set the initial node to visited
	    // This counts as one predecessor write and one distance write
	    win[start].setRoot();
	    counts_[Count::PRED_WRITES]++;
	    counts_[Count::DISTANCE_WRITES]++;
	    
	    // Loop over the entries remaining in the queue
	    while (not queue.empty()) {
		// Get node being processed, and remove from queue
		std::size_t current = queue.front();
		queue.pop();
		    
		// Loop over the neighbour nodes
		for (std::size_t index : win.neighbours(current)) {
		    Node & next{win[index]};
		    if (next.getDistance() == -1) {
			// Push the (logical index of) node to the queue
			queue.push(next.index() - win[0].index());
			counts_[Count::QUEUE_PUSHES]++;
			
			// Set the distance and predecessor
			// This counts as a distance and pred write
			next.setPredecessor(win[current]);
			counts_[Count::PRED_WRITES]++;
			counts_[Count::DISTANCE_WRITES]++;
		    }
		}
	    }
	}

    /**
     * \brief Construct the shortest path tree to accessible exit nodes
     *
     * Call this function after the searchWindow function to add
     * the shortest path tree to the window. The shortest path tree
     * is stored in the successor list for each node.
     *
     * An important aspect of this algorithm is that the next-column
     * node for each path is recorded. In the naive version of the
     * algorithm, where only the shortest-path trees are tracked, a
     * situation can arise where a path re-enters the left-most column
     * of the active window. If this path is chosen, and the active
     * window is moved one step to the right, then the portion of the
     * path in the left-most column will be lost, invalidating the
     * path. (At this point the algorithm will fail because it has
     * reached a dead-end.) To prevent this, it is necessary to advance
     * along the path (forwards) far enough that there is no portion
     * of the path remaining in the left-most column. This cannot be
     * achieved by searching forward along the paths in the next
     * step of the algorithm, because an unbounded amount of the path
     * may need to be visited. Instead, the 'next-column node' (the
     * one to which the algorithm should advance for the next active
     * window) is recorded in this function.
     *
     * The calculation of the 'next' node is based on the fact that
     * a path cannot cross from the left-most column to the right-most
     * column without passing through the second column on the left. 
     * As a result, when traversing the path backwards, there will
     * be a first time that this second column is visited. This node
     * is recorded as the 'next' node. When traversing the path forwards,
     * this node is the last time the second column (or any column to
     * the left of it) is visited. 
     *
     * \todo Note that there is an edge case if the window is of width two
     * which is not handled yet. A window this narrow is not feasible
     * anyway, but it would be good to fix it.
     *
     */
    template<typename Node, bool Base>
    void makeSPT(NodeWindow<Node,Base> & win)
	{	
	    // Find all the accessible nodes in the final column
	    for (std::size_t y{0}; y < win.height(); y++) {

		// Check if there is a path to the node
		auto exit_node{win(win.width()-1,y)};
		if (exit_node.getDistance() != -1) {

		    // Has the next node been found for this path?
		    // This is not necessarily the minimal right
		    // node. Any right node will serve
		    bool found_minimal_right_node{false};
		    
		    // If so, iterate backwards to the root node
		    for (PredecessorIterator node{win(win.width()-1,y), win};
			 not node->isRoot();
			 ++node) {

			const std::size_t next_column{1}; 
			
			// Check if the current node is in the second column
			if (not found_minimal_right_node) {
			    if (win.getColumnIndex(*node) == next_column) {
				if (win.getColumnIndex(*(node+1))
				    == next_column - 1) {
				    node->setRight(true); // Minimal right-node
				    found_minimal_right_node = true;
				} else {
				    node->setRight(false); // Regular right-node
				}
			    }
			}
		
			// Get the predecessor and add the successors 
			auto predecessor{node};
			(++predecessor)->addSuccessor(*node);
			counts_[Count::SUCC_WRITES]++;
		    }
		}
	    }
	}

private:
    const std::size_t H_{0}; ///< The window height
    std::size_t W_{0}; ///< The window width

    /**
     * \brief Store the performance counter values 
     */ 
    std::map<Count,std::size_t> counts_{};
        
    std::size_t getCount(Count c) const {
	try {
	    return counts_.at(c);
	} catch (const std::out_of_range &) {
	    return 0;
	}
    }

};

#endif
