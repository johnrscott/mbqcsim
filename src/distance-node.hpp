/**
 * \file distance-node.hpp
 * \brief Contains the node that is used for breadth-first search
 *
 */

#ifndef DISTANCE_NODE_HPP
#define DISTANCE_NODE_HPP

#include <vector>
#include <algorithm>
#include <set>
#include "colour.hpp"

/**
 * \brief The node object used for the global breadth-first search
 *
 * The breadth first search algorithm works in two stages. First, the
 * shortest path tree of nodes is found by assigning distances to all
 * reachable nodes from a particular root node. Each node that gets
 * a distance is "accessible", and stores its distance and also its
 * (unique) predecessor. Paths to desired destination nodes are
 * found by traversing the shortest path tree backwards using the 
 * predecessor information.
 *
 * Whenever reference is made to another node, an index offset is used.
 * This makes it possible to identity the correct node both when using
 * the full node vector (for the full cluster) or when using the active
 * window. Using this scheme, the value 0 means is the invalid-value 
 * marker (because that would refer to the current node).
 *
 */
template<typename Previous>
class DistanceNode : public Previous
{
    /** 
     * \brief The distance to the node from some starting point
     * 
     * The distance is a positive number or zero. The value -1 indicates
     * that the node has not been visited yet. In the global breadth-first
     * algorithm, the distances are recalculated at every interation of
     * the active window from some node in the first column of that window.
     * In the interative breadth-first search, the distances persist from
     * a starting node on the left-most column of the full cluster.
     */
    int distance{-1};
    
    /**
     * \brief Index of the previous node assigned during breadth-first search
     *
     * A non-negative number indicates a valid parent index. A value of 0
     * indicates that there is no predecessor recorded for this node.
     */
    int pred{0};

    /** 
     * \brief Indices of successor nodes
     *
     * The successor nodes are determined during the second stage of the
     * breadth-first algorithm, by traversing the shortest-path tree
     * backwards from the destination nodes. 
     *
     * The successors are stored as index offsets. 
     */
    std::set<int> successors{};

    /// Exit node flag (used in IBFS)
    bool exit{false};

    /// Exit node flag (used in IBFS)
    bool reverse_accessible{false};

    
protected:
    using Previous::index_;
    using Previous::x_;
    using Previous::y_;
    
public:

    bool operator == (const DistanceNode &) const & = default;
    
    /**
     * \brief Construct a default object
     *
     * This constructs a default DistanceNode. This is not what gets used
     * most of the time, because it sets the BaseNode properties to zero.
     * Instead, classes derived from this class are initialised by calling
     * the BaseNode constructor directly (then the trivial BaseNode here
     * is ignored).
     *
     * Implementation node: If I could find a way to do it, I would delete
     * this function, and make it impossible to directly construct this
     * class from this constructor. Although maybe there is a use case for
     * being able to construct this class -- I'm not sure.
     */
    using Previous::Previous;
    //DistanceNode() = default;

    /// Marker colour for printing
    std::string markerColour() const
	{
	    // Check for exit node that is accessible
	    if (exit and reverse_accessible) {
		return Colour::CYAN + Colour::BOLD;		    
	    }
	    // Check for exit node
	    if (exit) {
		return Colour::PURPLE + Colour::BOLD;		    
	    }
	    // Check node is accessible
	    if (distance != -1) {
		return Colour::GREEN + Colour::BOLD;
	    }
	    return "";
	}

    
    std::string markerShape() const
    	{
    	    return "•";
    	}

    template<typename Node>
    std::string edgeColour(const Node & node) const
	{
	    if (isSuccessor(node)) {
		return Colour::RED + Colour::BOLD;
	    }
	    if (isPredecessor(node)) {
		return Colour::CYAN + Colour::BOLD;
	    }
	    return "";
	}
    
    template<typename Node>
    std::string edgeShape(const Node & node) const
	{
	    if (node.index() == index_) {
		throw std::logic_error("Cannot get edgeShape between a "
				       "node and itself (DistanceNode). ");
	    }
	    if (x_+1 == node.x()) {
		switch(isPredecessor(node)) {
		case 1:
		    return "←";
		case -1:
		    return "→";
		default:
		    return "―";		    
		}
	    } else if (y_-1 == node.y()) {
		switch(isPredecessor(node)) {
		case 1:
		    return "↑";
		case -1:
		    return "↓";
		default:
		    return "|";		    
		}
	    }
	    return " ";
	}

    
    void print(std::ostream & os = std::cout) const 
	{
	    Previous::print(os);
	    os << "  (dnode:d=" << distance
	       << ",po=" << pred
	       << ",ss=";
	    for (const auto val : successors) {
		os << val << ",";
	    }
	    os << "exit:" << exit << ","; 
	    os << ")" << std::endl;  
	}

    
    /**
     * \brief Reset the state of the node
     *
     * This brings the node back into the default "unvisited" state. In this
     * state, there is no predecessor, and the distance to the node is -1.
     */
    void fullReset()
	{
	    distance = -1;
	    pred = 0;
	    successors.clear();
	    //path_successor = 0;
	}

    /**
     * \brief Reset the search information in the node
     *
     * Use this function to reset the nodes in the active window ready for
     * a new search. Everything is reset (the predecessor information), the
     * distances, successors, etc; only the path_successor is not reset,
     * because that contains persistent information about the path that
     * has been selected.
     *
     * \todo Figure out what other information should persist. What information
     * needs to be reset to do a search, and what can be left? What is the state
     * of the nodes after the active window has passed over them?
     *
     */
    void searchReset()
	{
	    distance = -1;
	    pred = 0;
	    successors.clear();
	    //path_successor = 0;
	}

    
    /**
     * \brief Get the distance to the node
     *
     * Result -1 means that the distance has not been assigned
     */
    int getDistance() const
	{
	    return distance;	
	}
    
    /**
     * \brief Set predecessor node
     *
     * Sets the predecessor of this node to be the node passed as
     * the argument.
     *
     * This function is used as part of the breadth-first search.
     * The distance to the current node is one greater than the
     * distance to the parent node.
     *
     * You can use this function to define a root node by passing
     * the same node as the argument. It will be recorded as a
     * predecessor of itself, which is no change, but the distance
     * will be incremented from -1 to 0.
     *
     */
    void setPredecessor(const DistanceNode & predecessor)
	{
	    // Set the parent node offset
	    pred = predecessor.index_ - index_;

	    // Set the distances
	    distance = predecessor.distance + 1;
	}

    /**
     * \brief Set this node as the root node (distance == 0)
     */
    void setRoot() {
	distance = 0;
	pred = 0;
    }

    /**
     * \brief Check if this node is the root node
     */
    bool isRoot() const {
	///\todo Are both of these necessary
	return (distance == 0) and (pred == 0);
    }

    /**
     * \brief Set this node as an exit node (used in IBFS)
     */
    void setExit(bool state) {
        exit = state;
    }

    /**
     * \brief Check if this node is the root node
     */
    bool isExit() const {
	return exit;
    }


    /**
     * \brief Used for finding failed paths (IBFS)
     */
    void setReverseAccessible(bool state) {
        reverse_accessible = state;
    }

    /**
     * \brief Check if this node is reverse accessible.
     */
    bool isReverseAccessible() const {
	return reverse_accessible;
    }

    /**
     * \brief Check whether a node is a successor of this node
     * 
     * Use this function to check if a node is a successor of this node,
     * or vice versa. The result can be treated as a bool, or an int which
     * determines which node is the predecessor of the other. 
     * 
     * \return The function returns +1 if this node is a successor
     * of the node passed in the argument. Returns -1 if the argument
     * node is a successor of this node. Returns 0 if neither is
     * a successor of the other.
     */
    template<typename Node>
    int isSuccessor(const Node & node) const 
	{
	    // Calculate offset between nodes
	    long int this_index{static_cast<long int>(index_)};
	    long int node_index{static_cast<long int>(node.index())};
	    long int offset{this_index - node_index};

	    // If the offset is in the successor set of the
	    // argument node, then this node is a successor of
	    // the argument node (return +1)
	    auto node_successors{node.successors};
	    auto it1 = std::ranges::find(node_successors, offset);
	    if (it1 != std::ranges::end(node_successors)) {
		return 1;
	    }

	    // Else, if -offset is in the successor set of the
	    // this node, then the argument node is a successor of
	    // this node (return -1)
	    auto it2 = std::ranges::find(successors, -offset);
	    if (it2 != std::ranges::end(successors)) {
		return -1;
	    }

	    // Else, return 0 (neither is a successor of the other)
	    return 0;
	}
    
    
    /**
     * \brief Check whether a node is a predecessor of this node
     *
     * Use this function to check if a node is a predecessor of this node,
     * or vice versa. The result can be treated as a bool, or an int which
     * determines which node is the predecessor of the other. 
     *
     * \return The function returns +1 if this node is the predecessor
     * of the node passed in the argument. Returns -1 if the argument
     * node is the predecessor of this node. Returns 0 if neither is
     * a predecessor of the other.
     *
     */
    template<typename Node>
    int isPredecessor(const Node & node) const 
	{
	    // Calculate offset between nodes
	    long int this_index{static_cast<long int>(index_)};
	    long int node_index{static_cast<long int>(node.index())};
	    long int offset{this_index - node_index};

	    // Get predecessor offsets
	    long int node_pred{node.getPredecessorOffset()};
	    long int this_pred{static_cast<long int>(pred)};

	    if (offset == node_pred) {
		return 1;
	    } else if (-offset == this_pred) {
		return -1;
	    } else {
		return 0;
	    }
	}
    
    /**
     * \brief Get offset to the predecessor. 
     *
     * Returns 0 for a node with node predecessor
     *
     */
    int getPredecessorOffset() const
	{
	    return pred;
	}

    std::size_t numSuccessors() const
	{
	    return successors.size();
	}
    
    /** 
     * \brief Add a successor
     * 
     * This function is part of the path indexing that occurs at
     * the end of the breadth-first search. Paths to the final column
     * are stored by recording the nodes that can come after this
     * particular node (the successor). This allows the path to be
     * traversed forwards.
     *
     */
    void addSuccessor(const DistanceNode & successor_node)
	{
	    // Cast to avoid subtraction errors
	    long int a = successor_node.index_;
	    long int b = /* this node's */ index_;
	    int offset = static_cast<int>(a - b);
	    // Add the successor node to the list
	    addSuccessor(offset);
	}

    /** 
     * \brief Remove a successor
     * 
     * Use this function to remove a node from the list of successors
     * for this node. If the node is not already a successor, the state
     * of the node remains unchanged.
     *
     */
    void removeSuccessor(const DistanceNode & successor_node)
	{
	    // Cast to avoid subtraction errors
	    long int a = successor_node.index_;
	    long int b = /* this node's */ index_;
	    int offset = static_cast<int>(a - b);
	    // Add the successor node to the list
	    removeSuccessor(offset);
	}

    
    /**
     * \brief Add a successor by offset
     */
    void addSuccessor(const int offset)
	{
	    if (offset == 0) {
		throw std::logic_error("Cannot set the successor offset to zero");
	    }
	    successors.insert(offset);
	}

    /**
     * \brief Remove a successor by offset
     * 
     * If the offset is not present, the object remains unchanged
     */
    void removeSuccessor(const int offset)
	{
	    successors.erase(offset);
	}

    
    /// Get the list of successors
    std::set<int> getSuccessorOffsets() const {
	return successors;
    }
};

#endif
