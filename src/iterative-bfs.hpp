/**
 * \file iterative-bfs.hpp
 * \brief Iterative breadth-first search algorithm 
 *
 */

#ifndef ITERATIVE_BFS_HPP
#define ITERATIVE_BFS_HPP

#include <queue>
#include "node-window.hpp"
#include "predecessor-iterator.hpp"
#include "advance-iterator.hpp"
#include "utils.hpp"
#include <algorithm>
#include <map>

#include <nlohmann/json.hpp>

/**
 * \brief Iterative breadth-first search (IBFS) to calculate potential path 
 * successors
 *
 * This class implements the IBFS and shortest-path tree (SPT) calculations
 * needed to extend a path according to this search method.
 *
 * Iterative breadth-first search is like GBFS, except that all distance
 * to already visited nodes is preserved, and only the newly added column
 * on the right is searched. Although only this column is searched, the
 * algorithm may still visit nodes in the previous columns if a new region
 * becomes accessible due to the new right-most column
 * 
 * The class contains performance counters to track how many times certain
 * operations are performed, such as writes to the node window.
 *
 * As a result of the incremental search (one column at a time), the distances
 * assigned to nodes are not the shortest distances, and therefore the reverse
 * path trees are not shortest path trees. This is not important for MBQC,
 * where the goal is to find accessible nodes (not shortest paths to nodes).
 */
class IBFS
{
public:
    enum class Count
    {
	/**
	 * \brief Count the number of times a predecessor offset is written
	 */
	PRED_WRITES,

	/**
	 * \brief Number of times a successor is written while making the SPT
	 */
	SUCC_WRITES,
	
	/**
	 * \brief Count the number of times a distance is written
	 */
	DISTANCE_WRITES,

	/**
	 * \brief Number of pushes to the queue
	 */
	QUEUE_PUSHES,

	/**
	 * \brief Number of failed exit nodes in previous column
	 *
	 * When the next block is searched, it is necessary to attempt
	 * to advance paths from all the exit nodes from the previous
	 * block. If some of these exit nodes do not lead to valid
	 * paths, they are marked as failed nodes. This counts the number
	 * of failed nodes from the previous block.
	 *
	 * The value of this counter determines the number of trees that
	 * must be pruned from the previous block.
	 */
	FAILED_NODES,

	/**
	 * \brief Number of times a successor is deleted from a node
	 *
	 * The reverse path tree must be pruned to remove paths that
	 * now point to failed nodes. This pruning operation is achieved
	 * by removing a successor node at the root of the subtree that
	 * must be deleted. (This makes that tree inaccessible.)
	 */
	SUCC_DELETES,

	/**
	 * \brief Number of steps required to traverse the pruned subtree
	 *
	 * In discovering the base of the subtree that is being pruned,
	 * it is necessary to step through the tree towards the root and
	 * check how many successors each node has along the way. This
	 * read operation is counted here
	 *
	 * Implementation note: this could be improved by storing the
	 * tree branch points as a linked list, making it possible to
	 * jump to the base of the subtree directly. A draft implementation
	 * would be needed to establish the overhead of creating and
	 * storing this structure.
	 */
	PRUNE_READS,
    };

    IBFS(std::size_t height)
	: H_{height} {}
        
    /**
     * \brief Reset all the performance counters
     */
    void resetCounts() {
	counts_.clear();
    }

    void print() const
	{
	    std::cout << "H=" << H_ << ";W=" << W_
		      << ";pred_writes=" << getCount(Count::PRED_WRITES)
		      << ";succ_writes=" << getCount(Count::SUCC_WRITES)
		      << ";distance_writes=" << getCount(Count::DISTANCE_WRITES)
		      << ";queue_pushes=" << getCount(Count::QUEUE_PUSHES)
		      << std::endl;
	}

    /// Get data contents of object (performance counters) as JSON
    nlohmann::json json() const
	{
	    nlohmann::json j;

    	    // Add header information
    	    j["height"] = H_;
    	    j["width"] = W_;

	    auto & cnt{j["counts"]};
	    cnt["pred_writes"] = getCount(Count::PRED_WRITES);
	    cnt["succ_writes"] = getCount(Count::SUCC_WRITES);
	    cnt["distance_writes"] = getCount(Count::DISTANCE_WRITES);
	    cnt["queue_push"] = getCount(Count::QUEUE_PUSHES);
	    cnt["failed_nodes"] = getCount(Count::FAILED_NODES);
	    cnt["succ_deletes"] = getCount(Count::SUCC_DELETES);
	    cnt["prune_reads"] = getCount(Count::PRUNE_READS);
	    return j;
	}

    /**
     * \brief Get the performance counters
     */
    std::map<Count,std::size_t> counts() const
	{
	    return counts_;
	}
    
    /**
     * \brief Perform a iterative breadth-first-search over the graph
     *
     * The routine will _not_ clear all the old path data from the 
     * nodes (unlike GBFS). The start parameter is used for the first search.
     * Instead, the first step in the algorithm is to push all these
     * starting nodes to the queue, and then perform a breadth-first
     * search as normal.
     *
     * The first time the algorithm is called, the start node vector
     * contains only one node, which is the starting node for the path
     * in the left-most column. This case is identified by setting the
     * "first" flag. If first is not set, then the start vector is 
     * assumed to contain exit nodes for the penultimate column that
     * will be used as the starting point for this search.
     *
     * \todo Fix the function signature to not need start (see the
     * templates used in PathFinding).
     */
    template<typename Node, bool Base>
    void search(NodeWindow<Node,Base> & win, const std::size_t start)
	{
	    if (win.height() not_eq H_) {
		throw std::logic_error("Window with invalid height "
				       "passed to IBFS::search()");
	    }

	    // Reset counters
	    ///\todo The counter data should be encapsulated inside this
	    /// function. To be fixed later.
	    resetCounts();
	    W_ = win.width();
	    
	    // Push the starting nodes to the queue
	    if (first_) {
		// This is not counted in the performance counters
		// because it should not really be here (part of class
		// initialisation/reset)
		///\todo Fix the initialisation. The presence of this
		/// indicates a design problem (class holds state in a
		/// way that will easily lead to class misuse).
		/// No clear method, so assign empty queue (move semantics
		/// will make this as efficient as anything else)
		queue_ = std::queue<std::size_t>{};

		// This is part of the algorithm, and must be counted
		queue_.push(start);
		counts_[Count::QUEUE_PUSHES]++;

		// In this special case, it is necessary to set the root
		// node as visited, because no other graph node is visited
		win[start].setRoot();
		counts_[Count::PRED_WRITES]++;
		counts_[Count::DISTANCE_WRITES]++;

		// Set the starting node as an exit node as a special case
		// so that the reverse path tree construction will work
		// for the initial search
		win[start].setExit(true);

	    } else {

		// Check that the window has width at least two
		if (W_ < 2) {
		    // Window has width one or zero, return the
		    // starting vector (there is nothing to search)
		    return;
		}
		
	    }

	    // Check the queue contains elements TODO
	    	    
	    // Loop over the entries remaining in the queue
	    while (not queue_.empty()) {
		// Get node being processed, and remove from queue
		std::size_t current = queue_.front();
		queue_.pop();
		    
		// Loop over the neighbour nodes
		for (std::size_t index : win.neighbours(current)) {
		    Node & next{win[index]};
		    if (next.getDistance() == -1) {
			// Push the (logical index of) node to the queue
			queue_.push(next.index() - win[0].index());
			counts_[Count::QUEUE_PUSHES]++;
			
			// Set the distance and predecessor
			// This counts as a distance and pred write
			next.setPredecessor(win[current]);
			counts_[Count::PRED_WRITES]++;
			counts_[Count::DISTANCE_WRITES]++;
		    }
		}
	    }

	    // Queue is empty here. Now repopulate the queue with
	    // all the exit nodes ready for the next search
	    ///\todo This can surely be optimised further -- there
	    /// might exist some way to establish the exit nodes
	    /// before they are popped from the queue? This would
	    /// reduce pops and writes to the queue.
	    
	    // Find all the accessible nodes in the final column, and
	    // add them to the queue. Subtract height from the 
	    for (std::size_t y = 0; y < win.height(); y++) {
		// Check if there is a path to the node
		if (win(W_-1,y).getDistance() != -1) {
		    
		    // Make index for next window
		    const std::size_t index{H_*(W_-2)+y};
		    queue_.push(index);
		}
	    }

	    // Clear the flag to indicate that the first
	    // search is done
	    first_ = false;
	}
    
    /**
     * \brief Construct the shortest path tree to accessible exit nodes
     *
     * Call this function after the searchWindow function to add
     * the shortest path tree to the window. The shortest path tree
     * is stored in the successor list for each node.
     *
     * The construction of the shortest path tree only traverses as far
     * back as the last exit node along any given path. 
     *
     */
    template<typename Node, bool Base>
    void makeSPT(NodeWindow<Node,Base> & win)
	{
	    // Find all nodes in the final column
	    for (std::size_t y = 0; y < win.height(); y++) {

		// Check if there is a path to the node (if node has a distance)
		auto exit_node{win(win.width()-1,y)};
		if (exit_node.getDistance() != -1) {
		
		    for (PredecessorIterator node{win(win.width()-1,y),win};
			 ;//not node->isExit()
			 ++node) {
			
			// If the node is the exit node, break
			if (node->isExit()) {
			    // Mark this node as reverse accessible,
			    // meaning that it is possible to get to
			    // it from an exit node by following the
			    // reverse path tree.
			    node->setReverseAccessible(true);
			    break;
			} else {
			    // Add the successor pointing to this node
			    (node+1)->addSuccessor(*node);
			    counts_[Count::SUCC_WRITES]++;
			}
		    }

		    // Mark the node as an exit node. It is important
		    // to do this at the end so that the algorithm
		    // above does not get stuck on the initial node
		    // (exit_node)
		    win(win.width()-1,y).setExit(true);
		}
	    }
	    
	    // Now iterate over all the failed paths in the previous
	    // column. These are paths ending at nodes that were exit
	    // nodes, but have not been marked as reverse accessible.
	    // The paths must be removed from the successor sets of
	    // each node
	    for (std::size_t y = 0; y < win.height(); y++) {

		// Check if there is a path to the node
		auto exit_node{win(win.width()-2,y)};
		if (exit_node.isExit() and not exit_node.isReverseAccessible()) {
	
		    // Increment the failed nodes counter
		    counts_[Count::FAILED_NODES]++;

		    // Reverse iterate through the path, starting at
		    // the failed node, until reaching a branch point
		    // with at least one other successor. At this
		    // node, remove the successor corresponding to
		    // the failed path
		    //
		    // In traversing this path, it may be that a failed
		    // path is part of another failed path. In this case,
		    // whichever is processed first prunes the successor,
		    // and the next one stops at PredecessorSentinel.
		    ///\todo This is an optimisation opportunity, because
		    /// the failed path could somehow mark itself as
		    /// pruned, and then the second failed path would
		    /// not need to traverse the whole list. One way to
		    /// do this would be to remove all successors, so that
		    /// the PredecessorSentinel occurs straight away, however
		    /// a flag at the top failed node might work just as
		    /// well.
		    for (PredecessorIterator node{exit_node,win};
			 node != PredecessorSentinel{};
			 ++node) {

			// Check for out_of_range for node+1
			// This can happen if the pruned path extends
			// further to the left than the window extends.
			//if ((node+1) != PredecessorSentinel{}) {
			//
			// Now: Expecting out of range for node+1 possibly
			try {

			    // Exception line
			    const auto succ_count{(node+1)->numSuccessors()};

			    // Increment this counter to track the number
			    // of reads that must be accomplished in
			    // traversing the subtree that must be pruned.
			    // Note: The counter does not take account of
			    // reads of the end sentinel, because it is
			    // likely that an improved system could be
			    // obtained for detecting this end condition
			    // without a further read.
			    // Main point: this line goes after the point that
			    // could through the exeption (node+1)
			    counts_[Count::PRUNE_READS]++;
			    
			    // If (n+1) is not the end, check whether it
			    // is a branch point (if so, remove the
			    // successor from (node+1) to node).
			    if (succ_count > 1) {
				// Reached a branch point. Remove the current
				// node as a successor of (node+1)
				// This successor removal is necessary, so
				// it can be performance-counted
				(node+1)->removeSuccessor(*node);
				counts_[Count::SUCC_DELETES]++;
				break;
			    }
			    
			} catch (const std::out_of_range &) {
			    // If (node+1) is the end sentinel, then
			    // break, because there are no more
			    // candidate nodes to remove successors
			    // from
			    break;
			}
		    }
		}

		// Now iterate over the RPT to find right nodes
		for (std::size_t y{0}; y < win.height(); y++) {

		    // Check if there is a path to the node
		    auto exit_node{win(win.width()-2,y)};
		    if (exit_node.getDistance() not_eq -1) {
			
			// Has the next node been found for this path?
			// This is not necessarily the minimal right
			// node. Any right node will serve
			bool found_minimal_right_node{false};
			
			// If so, iterate backwards to the root node
			try {
			    // Iterate from the current node to the nearest
			    // exit node
			    ///\todo If you replace "win(win.width()-1,y)" with
			    /// exit_node on the next line, the tests fail, which
			    /// means there is _definitely_ a bug in this
			    /// interface. I don't know what it is yet.
			    /// To come back to this later: the issue is that
			    /// the line above should be auto &, not auto,
			    /// because it needs to point to the actual node
			    /// in the window (possibly).
			    for (PredecessorIterator node{win(win.width()-1,y),
							  win};
				 node != PredecessorSentinel{}; // or out of range
				 ++node) {

				const std::size_t next_column{1};
				
				// Check if the current node is in the
				// second column
				if (not found_minimal_right_node) {
				    if (win.getColumnIndex(*node) == next_column) {
					if (win.getColumnIndex(*(node+1))
					    == next_column - 1) {
					    // Minimal right-node
					    node->setRight(true); 
					    found_minimal_right_node = true;
					} else {
					    // Regular right-node
					    node->setRight(false); 
					}
				    }
				}
			    }
			    
			} catch (const std::out_of_range & e) {
			    // The out of range exception is not an error,
			    // it just means that the edge of the window has
			    // been reached. In this case, there is no more
			    // of the RPT to explore.
			}
		    }
		}
	    }
	}

private:
    const std::size_t H_; ///< The window height
    std::size_t W_; ///< The window width

    /// Flag to indicate first search
    bool first_{true};
    
    /// The queue used in the BFS
    std::queue<std::size_t> queue_;
    
    /**
     * \brief Store the performance counter values 
     */ 
    std::map<Count,std::size_t> counts_{};
        
    std::size_t getCount(Count c) const {
	try {
	    return counts_.at(c);
	} catch (const std::out_of_range &) {
	    return 0;
	}
    }

};





#endif
