/**
 * \file path-sim.hpp
 * \brief Simulate a path specified by PathNodes containing left nodes
 *
 *
 */

#ifndef PATH_SIM_HPP
#define PATH_SIM_HPP

#include "node-window.hpp"
#include "path-iterator.hpp"
#include "base-node.hpp"
#include "cluster-sim.hpp"
#include "bits.hpp"
#include "qubit-node.hpp"

#include "utils.hpp"

/**
 * \brief Set left-nodes along a path specified by PathNodes
 *
 * A left-node in a column x is a node all of whose path predecessors are
 * in column x or to the left.
 *
 * Maximal left-nodes in a column x are those nodes whose immediate
 * path successor is in column x+1, and all of whose path predecessors
 * are in column x or to the left of column x. They are very important in
 * the simulation of logical qubit paths, because a valid intermediate state
 * of the simulation can be defined for paths ending on left-nodes.
 *
 * This function traverses a path forwards and writes all the left nodes and
 * maximal left nodes that it finds.
 *
 * Left nodes are found as follows. First, assume that start is a left node
 * (because it is the start of the path). Make a note of the current column,
 * x. Advance along the path until a node is reached with column x+1. This
 * is the first left-node for column x+1. Continue to advance through the nodes
 * until column x+2 is reached. The previous node is the maximal left-node. Any
 * intervening nodes that are in column x+1 are marked as left-nodes.
 *
 * It is really important you pass a left node as the starting node. This
 * is because the algorithm assumes that the starting node is a left node
 * in order to be sure that the nodes it finds that are successors to the
 * starting node are also left nodes. (If the starting node was not a left
 * node, how could this function be sure that the predecessor set of any
 * node it finds lies entirely to the left?)
 *
 * \todo Change interface to take y-coordinate start, not node (consistency
 * with other functions).
 */
template<NodeType Node, bool Base>
static std::size_t writeLeftNodes(NodeWindow<Node,Base> && win, Node & start)
{
    // Check that the starting node is on a path
    if (not start.onPath()) {
	start.print();
	throw std::logic_error("Start node is not the start of a path in "
			       "call to writeLeftNodes()");
    }
    
    // Variable to track the column under investigation
    std::size_t x_current{win.getColumnIndex(start)};

    // This is the current node being processed
    std::size_t y_last{start.y()};
    
    try {
	for (PathIterator<Node,Base> node{start, win};
	     node != PathSentinel{}; ++node) {

	    // Set the y-coordinate of the node
	    y_last = node->y();
	    
	    // Set this node as a left node if it is in the current column
	    if (win.getColumnIndex(*node) == x_current) {
		node->setLeft(false);
	    }

	    // Check whether the next node is in the next column
	    // If so, set as maximal left node, and advance the current column.
	    // Alternatively, if the current node is the end of the path, set
	    // it as the maximal left node.
	    //
	    // \todo This is a bug. If the node is the end of the path, it is
	    // not necessarily a maximal left node.
	    if (not node->isEnd()) {
		if (win.getColumnIndex(*(node+1)) == x_current+1) {
		    node->setLeft(true);
		    x_current++;
		}
	    } else {
		// If the current node is the end node, it may or may not be
		// a maximal left node, depending on whether its x-coordinate
		// is large enough. If it is equal to x_current, then it is
		// a maximal left node. Else, it is not.
		if (win.getColumnIndex(*node) == x_current) {
		    node->setLeft(true);
		}
	    }
	}

	return y_last;
	
    } catch (const std::out_of_range & e) {
	// This means that the path has tried to leave the window.
	// This is fine, just return, because you can assume that this bit
	// of the path will be processed later.
	return y_last;
    }

}


/**
 * \brief Path simulator
 *
 * Unlike the simulation of basis measurement patterns, this simulation is
 * a class because it holds the state of the pending byproduct operators --
 * the ones that have been measured but not applied yet. 
 *
 */
template<typename NoiseSource = NoNoise>
class PathSim
{
    ClusterSim<NoiseSource> sim;

    /// The current logical state of the path
    qsl::Qubits<qsl::Type::Resize, double> q{1};
    
    /**
     * \brief Pending byproduct operator updates
     *
     * Byproduct operators are updated once per column, according to rules
     * specified by nodes in the current column. Two cases can arise
     * regarding these rules:
     *
     * 1) The rules apply to path nodes whose indices are not greater 
     * than the current path height. In this case, the rules are directly 
     * incorporated into the current column's byproduct operators
     *
     * 2) The rules apply to path nodes whose indices are greater than the
     * current column's path height. In this case, the rules are stored
     * in this pending_byp structure ready to be incorporated into the
     * byproduct operators when the path becomes long enough. 
     *
     * The key of the map stores the path index to which the update applies.
     * The second argument stores the byproduct term which should be added
     * to the byproduct operators. Once it has been added, the term is
     * removed from the map.
     *
     * Note: multimap because keys may not be unique. Actually, it might
     * not even be necessary to search it, in which case a vector would
     * be better maybe. To think about.
     *
     * \todo Maybe a vector would be better.
     */
    std::multimap<std::size_t, Bits> pending_byp{};
	
    /**
     * \brief Byproduct operators for the logical qubit
     *
     * These are the main running byproduct operators for the measurement
     * pattern. They are updated at each new column using byproduct operator
     * update rules that apply to path indices less than or equal to the
     * current path height. In addition, 
     *
     * OLD COMMENTS:
     *
     * The byproduct operators (x,z) for the logical qubit. They are
     * updated according to rules given by the measurement pattern
     * stored in the nodes.
     * 
     * \todo At the moment, only the simulation of one logical qubit is
     * supported. This should be generalised to a vector of byproduct
     * operators, one per logical qubit
     *
     */
    Bits byp_ops{0,0};

    Bits byp_ops_verify{0,0};

    
    /**
     * \brief Stored byproduct operators
     *
     * The stored byproduct operators are sometimes necessary for 
     * adaptive measurement settings which need byproduct operators
     * from several steps in the past.
     */
    Bits stored_byp_ops{0,0};

    /// The last left-height
    int left_height{-1};
    
public:

    /**
     * \brief Construct an empty window simulator object
     *
     * The object initially has no columns. Columns are added using
     * the addColumn method.
     *
     */
    PathSim(unsigned height, const Seed<> & seed,
	    NoiseSource && noise_src = NoiseSource{})
	: sim{height, seed, std::move(noise_src)} {}

    /**
     * \brief Get the current value of the byproduct operators
     */
    Bits getBypOps() const {
	return byp_ops;
    }

    Bits getBypOpsVerify() const {
	return byp_ops_verify;
    }

    
    /**
     * \brief Get the internal state logical
     *
     */
    qsl::Qubits<qsl::Type::Resize,double> state() const
	{
	    return q;
	}
    
	
    /**
     * \brief Use the measurement pattern to set measurement bases
     *
     * This function should be thought of as the interface between
     * the pattern node and the qubit node. Information about the
     * measurement pattern (e.g. basis, angle, etc) is taken from
     * the pattern node and used to set a Measurement basis object
     * in the qubit node which is used for the actual measurement.
     * 
     */
    void setMeasurementBases(std::ranges::range auto && column)
	{
	    // Loop over the nodes setting the measurement basis
	    for (auto & node : column) {
		// For an XY basis, set the basis and angle
		if (node.isXYBasis()) {
		    node.setBasis(MeasureXY(node.getAngle()));
		}
		// For Z-measurement, do nothing (Z-measurement is default)
	    }
	}

    /**
     * \brief Apply pending byproduct operators depending on path height
     *
     * This function reads and accumulates the pending byproduct operator
     * rules which apply to nodes up to and including path_height. 
     * Applied rules are removed from the pending list. The net byproduct
     * operator is returned.
     *
     */
    Bits getNetPending(std::size_t path_height)
	{
	    Bits byp{0,0};
		
	    // Note the increment condition is missing
	    for (auto it = std::begin(pending_byp);
		 it != std::end(pending_byp); /* see end of loop */) {

		const std::size_t index = it->first;
		const Bits byp_rule = it->second;
		if (index <= path_height) {
		    byp ^= byp_rule;

		    // This function will return the
		    // iterator to the next position
		    it = pending_byp.erase(it);

		} else {	
		    // Manually increment
		    ++it;
		}
	    }
	    return byp;
	}
	
    /**
     * \brief Process the outcomes of measurements into byproduct operators
     *
     * After a column has been measured, it is necessary to use 
     * the measurement outcomes to calculate byproduct operators. 
     * Information about what byproduct operators to update is stored
     * in the pattern node: each node stores what should be done with
     * its measurement outcome.
     *
     * This function takes the byproduct operators as an argument, because
     * it is sometimes desirable to modify a different copy of the
     * byproduct operators.
     *
     * The update_pending flag is a hack to check that the algorithm works.
     * It enables this function to be used for the verification processing
     * too, where you don't want any of the byproduct operator rules to be
     * pushed to the pending rules variable, because they were already 
     * stored in the previous call.
     *
     * \todo I'm amazed this interface compiles! Need to give it the node
     * window properly
     */
    void processOutcomes(std::ranges::range auto && column, Bits & byp,
			 std::size_t left_height, bool verification)
	{
	    
	    // Loop over the contents of the pending
	    // byproduct operators, adding the rules
	    // that apply to qubits below the path height,
	    // incoporating (and erasing from the multimap)
	    // those whose index is now less than the path
	    // height
	    //
	    // This operation can be performed first because
	    // the path_height is fixed throughout this function,
	    // and does not depend on any node data.
	    //
	    // Loop over just-measured column adding outcomes to
	    // the correct byproduct operators
	    for (auto & node : column) {

		// If the measurement outcome is non-zero,
		// get the byproduct operator rules and either
		// add them to the argument byproduct operators,
		// or push them to the pending byproduct operator
		// variable, depending on the path height.
		if (not verification or not (node.left() == 2)) {
		    if (node.getOutcome() != 0) {
			    
			// Find all the byproduct operator update
			// rules stored in this node. For path nodes,
			// this just contains one update (the update
			// associated to this position on the path).
			// For cut-out qubits, the map contains all
			// the rules applying to surrounding path
			// nodes, with key given by the index of the
			// path node. If the index is "in the past",
			// then the update rule can be applied directly
			// to the byproduct operators. If the the index
			// is "in the future", the rule must be added to
			// the pending rules variable.
			const auto map = node.getBypUpdates();
			for (const auto & [index, byp_rule] : map) {

			    // Check if the rule is within the path height
			    // and either update the current byproduct
			    // operators or push to the pending rules
			    // variable
			    if (index <= left_height) {
				byp ^= byp_rule;
			    } else {
				if (not verification) {
				    pending_byp.insert({index, byp_rule});
				}
			    }				
			}
		    }   
		}
	    }
	}
	
    /**
     * \brief Simulate the window
     *
     * This function performs the simulation of a two-column window
     * of the cluster state. In the case, where there are currently
     * no columns in the simulator, an initial column is created based
     * on the left-most column in the window. 
     *
     * In the steady state case, the second column of the window is 
     * added to the cluster state simulation, and then the previous
     * column is measured out according to rules specified by the
     * nodes in the active window. Measurement outcomes are stored in
     * the nodes.
     *
     * The return type is a pair containing the index of the qubit
     * along the measurement pattern that has been simulated, and the
     * state of the simulation at that qubit. 
     *
     * 
     */
    template<NodeType Node, bool Base>
    qsl::Qubits<qsl::Type::Resize,double>
    simulate(NodeWindow<Node,Base> && win,
	     qsl::Qubits<qsl::Type::Resize,double> && initial,
	     bool first)
	{	    
	    // This function expects a window of width 1 or 2
	    if ((win.width() not_eq 1) and (win.width() not_eq 2)) {
		throw std::logic_error("Cannot simulate window that is not "
				       "width 1 or 2. Window has width "
				       + std::to_string(win.width()));
	    }

	    // Move the initial quantum state into the simulator
	    sim.setState(std::move(initial));
	    
	    // Perform the vertical entanglement required for the column
	    if (first) {
		sim.entangleColumn(win.sub(0,1,0));
	    }
	    
	    // Now the simulator has only the initial column
	    
	    // Make a copy of the simulator state here, before a
	    // new column has been added. This will be used to
	    // verify the state of the measurement pattern, before
	    // adding a new column to continue the pattern
	    auto sim_copy{sim};
	    
	    // Interpret the instructions in the pattern nodes
	    // and generate measurements
	    setMeasurementBases(win.sub(0,1,0));

	    // Make a copy of the column for verification purposes
	    // (all data associated with this column is eventually
	    // discarded).
	    SubNodeWindow<Node> col_copy{copy(win.sub(0,1,0))};
	    
	    // In this column of nodes, mark the "next-node" as
	    // not to be measured. This resulting unmeasured
	    // qubit will contain the state of the logical qubit
	    left_height = -1;
	    //Bits top_byp_correct{0,0};
	    for (auto & node : col_copy) {
		///\todo Check maximal left? Any left node works for the
		/// simulation, because it has the property that there are
		/// no forward paths. Maximal left has the property that the
		/// next edge is to the right. Is that useful for anything?
		/// UPDATE: yes, the crucial point is that it is unique! This
		/// is very important to end up with only one qubit.
		/// 
		if (node.left() == 2) {
		    node.setDoNotMeasure(true);
		    left_height = node.getPathIndex();	
		}
	    }

	    // Check there is an unmeasured node
	    if (left_height == -1) {
		throw std::runtime_error("Failed to find maximal left-node "
					 "in the current column.");
	    }

	    // Apply the pending byproduct rules to the byproduct
	    // operators based on the new left_height. 
	    byp_ops ^= getNetPending(left_height);
		
	    // Make a copy of the byproduct operators for
	    // verification purposes
	    byp_ops_verify = byp_ops;
		
	    // Measure the column of nodes, using the basis settings
	    // obtained in the previous step. This will measure all
	    // but the maximal left node.
	    ///\todo This use of sub is only to placate the measureColumn
	    /// rvalue argument
	    sim_copy.measureColumn(col_copy.sub(0,1,0));

	    // Check that there is exactly one qubit left
	    q = sim_copy.state();
	    if (q.getNumQubits() != 1) {
		col_copy.template print<QubitNode>();
		std::cout << q.getNumQubits() << std::endl;
		throw std::runtime_error("Number of qubits remaining after "
					 "measuring out copied column must "
					 "be one");
	    }
		
	    // Use the outcomes stored in the nodes to process
	    // byproduct operators
	    // At this point, the byproduct operators have only
	    // been modified by update rules which affect path
	    // nodes whose indices are less than or equal to the
	    // path height. This means that the byproduct operator
	    // can be used to obtain byproduct operators for the
	    // current column's path (after potentially swapping
	    // depending if the path height is even or odd). The
	    // only modification which is necessary is to undo the
	    // affect of the final (unmeasured) qubit.
	    processOutcomes(col_copy, byp_ops_verify, left_height, true);
	    // At this point, the simulator contains one qubit and
	    // the byproduct operators are valid. See the end of
	    // the function for how they are processed and returned.

	    //////////////////////////////////////////////////////////
	    // Now add the next column and continue with the pattern
		
	    // Check whether the last column has been reached. This
	    // is the case when the window has width 1
	    if (win.width() > 1) {
		// Add the new column on the right
		sim.addColumn(win.sub(0,2,1));
	    }
		
	    // Measure the column of nodes, using the basis settings
	    // obtained in the previous step.
	    // std::cout << "here" << std::endl;
	    // auto test{win.sub(0,1,0)};
	    // win.print();
	    // test.print();
	    // test.checkConsistency();
	    // std::cout << test.width() << std::endl;
	    // std::cout << test.height() << std::endl;
	    // for (const auto & node : test) {
	    // 	node.print();
	    // }
	    sim.measureColumn(win.sub(0,1,0));
	    // std::cout << "there" << std::endl;

	    // Use the outcomes stored in the nodes to process
	    // byproduct operators
	    // At this point, the byproduct operators have only
	    // been modified by update rules which affect path
	    // nodes whose indices are less than or equal to the
	    // path height. This means that the byproduct operator
	    // can be used to obtain byproduct operators for the
	    // current column's path (after potentially swapping
	    // depending if the path height is even or odd). The
	    // only modification which is necessary is to undo the
	    // affect of the final (unmeasured) qubit.
	    processOutcomes(win.sub(0,1,0), byp_ops, left_height, false);

	    // Correct the byproduct operators in light of the
	    // unmeasured qubit, for the simulator verification
	    // state.
	    //const Bits byp_copy = byp_ops ^ top_byp_correct;

	    // UNCOMMENT NEXT LINE TO VIEW THE VERIFICATION COLUMN
	    //col_copy.template print<QubitNode>();
	    
	    // Postselect the state leaving one qubit behind
	    //sim_copy.postselectColumn(col_copy);
	    // Apply the copied byproduct operators to correct the state
	    if (left_height % 2 == 0) {
		if (byp_ops_verify(0) == 1) {
		    q.pauliZ(0);
		}
		if (byp_ops_verify(1) == 1) {
		    q.pauliX(0);
		}
	    } else {
		if (byp_ops_verify(1) == 1) {
		    q.pauliZ(0);
		}
		if (byp_ops_verify(0) == 1) {
		    q.pauliX(0);
		}
	    }

	    return std::move(sim.getStateByMove());	
	}

    /// Get the last left height
    std::size_t getLeftHeight() {
	return left_height;
    }
};


/**
 * \brief Simulate a path measurement pattern*
 *
 * \todo This function is still work in progress. Don't use it yet.
 */
template<NodeType Node, bool Base>
qsl::Qubits<qsl::Type::Resize,double>
simulatePath(NodeWindow<Node,Base> && win)
{
    // Make initial all plus state
    qsl::Qubits<qsl::Type::Resize,double> col{win.height()};
    for (std::size_t n{0}; n < col.getNumQubits(); n++) {
	col.hadamard(n);
    }
    PathSim sim{win.height()};

    // This should return (by move) the result of applying
    // the pattern to the first 2 columns.
    bool start{true};
    for (std::size_t x{0}; x < win.width(); x++) {
	col = sim.simulate(win.sub(x,std::min(x+2, win.width()),x),
			   std::move(col), start);
	start = false;	
    }

    return sim.state();
}



#endif
