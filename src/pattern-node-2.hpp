/**
 * \file pattern-node-2.hpp
 *
 * New implementation of pattern-node
 *
 */

#ifndef PATTERN_NODE_2_HPP
#define PATTERN_NODE_2_HPP

#include <map>
#include "bits.hpp"
#include "node-window.hpp" // For NodeType

template<typename Previous>
class PatternNode2 : public Previous
{
    char label{'Z'}; ///< Label for use in marker shape for printing
    
    bool z_basis_{true}; ///< True if Z-basis, false for XY-basis

    /**
     * \brief Basis angle
     *
     * This is not necessarily the basis angle that is used. The true
     * measurement basis angle is this angle multiplied by a sign which
     * is determined by the adaptive measurement setting.
     *
     */
    double angle_{0};

    /**
     * \brief Byproduct operator update rules
     *
     * Stores what logical qubit byproduct operators are updated from
     * the outcome of this cluster qubit.
     *
     * The map associates a logical-qubit index to a byproduct operator update
     * rule for this cluster qubit. Once the outcome for this cluster qubit
     * is determined, the byproduct operator updates can be computed.
     *
     * The value in the map is a Bits object, which should be added to the
     * byproduct operators if the outcome is 1.
     *
     * Implementation note: 
     *
     * There is no concept of _when_ the byproduct operator is updated, which
     * is very important for using this class. In a complete measurement 
     * pattern, executed for its affect, it is perfectly valid to add up all
     * the byproduct operators at the end of the pattern. Alternatively, they
     * may be accumulated as the cluster qubits are measured. In particular, 
     * they can be accumulated as the cluster qubit outcomes become available,
     * which is how the PatternSim works. 
     *
     * However, in a pattern that is partially executed, such as a logical 
     * qubit path, it is important to know _when_ a byproduct operator should
     * be included, because it should only be included if it applies to the
     * "past" part of the measurement pattern, not the "future". Determining
     * whether a given node is in the past or future of a pattern is not a local
     * property, and also depends on the sub-window that is being simulated.
     * As a result, it is not stored in this class. This class assumes that
     * byp_update contains only updates that are supposed to be added. It is
     * on the user of the class to ensure only such byproduct operator updates
     * are added.
     *
     */
    std::map<std::size_t,Bits> byp_update{};

    /// Constants to add to the byproduct operators
    std::map<std::size_t,Bits> byp_constants{};

    bool input_{false}; ///< True if input qubit, false otherwise
    bool output_{false}; ///< True if output qubit, false otherwise

    /**
     * \brief Output logical qubit index
     *
     * If the qubit is an output qubit, this variable stores what index
     * that logical qubit has.
     *
     */
    std::size_t logical_qubit_;

    /**
     * \brief Offsets to nodes contributing to the adaptive measurement
     * setting of this node
     */
    std::vector<long> adapt_offsets_{};
    
protected:
    using Previous::index_;
    using Previous::x_;
    using Previous::y_;
    
public:

    using Previous::Previous;

    bool operator == (const PatternNode2 &) const & = default;
    
    /// Marker colour for printing
    std::string markerColour() const
	{
	    ///\todo Choosing the right colours here is a bit
	    /// tricky and I haven't got it right yet. The issue is that
	    /// the input node can contribute to byproduct operators,
	    /// which makes the two colour schemes dependent.

	    ///\todo These colour mixing operations are quite common
	    /// and can easily be moved to a helper function. That would
	    /// also help to organise the intent of the colours. For example,
	    /// any time two boolean values need to be expressed, the
	    /// red-purple-blue scheme may be used.
	    
	    // Input/output represented by red-purple-blue colour system
	    if (input_ and output_) {
		return Colour::PURPLE + Colour::BOLD;
	    }
	    if (input_) {
		return Colour::BLUE + Colour::BOLD;
	    }
	    if (output_) {
		return Colour::RED + Colour::BOLD;
	    }

	    // Use 
	    if ((adapt_offsets_.size() > 0) and (byp_update.size() > 0)) {
		return Colour::GREEN + Colour::BOLD;
	    }
	    if (adapt_offsets_.size() > 0) {
		return Colour::YELLOW + Colour::BOLD;
	    }
	    if (byp_update.size() > 0) {
		return Colour::CYAN + Colour::BOLD;
	    }
	    return "";
	}
    
    std::string markerShape() const
    	{
	    if (z_basis_) {
		return "Z";
	    } else {
		return std::string(1,label);
	    }
	}
    
    /// Print the node
    void print(std::ostream & os = std::cout) const
	{
	    Previous::print(os);
	    os << "  (p2node:";
	    if (z_basis_) {
		os << "Z;";
	    } else {
		os << "XY;angle=" << angle_ << ";";
	    }
	    os << "out=";
	    if (output_) {
		os << "y(" << logical_qubit_ << ");";
	    } else {
		os << "n;";
	    }
	    os << "byp={";
	    for (const auto & [logical_qubit, byp_rule] : byp_update) {
		os << "(" << logical_qubit << ":" << byp_rule << "),";
		    }
	    os << "};";
	    os << "dep={";
	    for (const long offset : adapt_offsets_) {
		os << "(" << offset << "),";
	    }
	    os << "}"
	       << ")" << std::endl;
	}

    /**
     * \brief Set the byproduct update for a given logical qubit
     *
     * \param index The index of the logical qubit
     * \param the byproduct operator update to use if the outcome of this
     * cluster qubit is 1.
     *
     */
    void setBypUpdate(std::size_t index, const Bits & update)
	{
	    byp_update.insert({index,update});
	}

    /**
     * \brief Set a constant to add to the byproduct operator
     *
     * In some measurement patterns, it is necessary to add a constant to
     * the byproduct operators. Although this is not associated to any
     * node, for consistency of interface it can be attached to any node.
     * In processing the byproduct operators, any node that holds a constant
     * byproduct operator will contribute this constant to the update of
     * the byproduct operators.
     *
     */
    void setBypConstant(std::size_t index, const Bits & update)
	{
	    byp_constants.insert({index,update});
	}

    
    /// Set as Z measurement
    void setZ()
	{
	    label = 'Z';
	    z_basis_ = true;
	}

    /**
     * \brief Accumulate byproduct operators to the argument
     *
     * This function is used to update byproduct operators passed as an
     * argument based on the byproduct operator rules stored in this node.
     * Call this function if the outcome of this qubit is a 1. All the
     * rules relating to this qubit will be added to the byp argument.
     *
     */
    void updateByp(std::map<std::size_t,Bits> & byp, unsigned outcome)
	{
	    // If outcome is 1 then add byproduct operator updates
	    if (outcome == 1) {
		for (const auto & [logical_qubit, byp_rule] : byp_update) {
		    ///\todo I don't really like this, but it will do for the
		    /// moment because it is correct. The issue is that operator[]
		    /// default constructs an empty Bits object, and then due to
		    /// what is really a bug in Bits, adding a non-zero bits object
		    /// always results in a zero length bits object. This should be
		    /// fixed in Bits.
		    try {
			byp.at(logical_qubit) ^= byp_rule;
		    } catch (const std::out_of_range & e) {
			std::cout << "Cannot update byproduct operator "
				  << "because affected logical qubit is not "
				  << "present in the argument" << std::endl;
			throw e;
		    }
		}
	    }

	    /// Add any constant contributions
	    for (const auto & [logical_qubit, byp_rule] : byp_constants) {
		try {
		    byp.at(logical_qubit) ^= byp_rule;
		} catch (const std::out_of_range & e) {
		    std::cout << "Cannot add constant byproduct operator "
			      << "because affected logical qubit is not "
			      << "present in the argument" << std::endl;
		    throw e;
		}
	    }
	    
	}
    
    /**
     * \brief Set as an output cluster qubit
     *
     * Output qubits are the cluster qubits which hold the result of the
     * measurement pattern after the pattern has been executed.
     *
     * The argument holds the index of the logical qubit corresponding to
     * this output node.
     *
     * \todo Consider a way to turn off output node 
     */
    void setAsOutput(std::size_t logical_qubit_index)
	{
	    output_ = true;
	    logical_qubit_ = logical_qubit_index; 
	}

    bool isOutput() const { return output_; }

    /**
     * \brief Set as an input cluster qubit
     *
     * Input qubits are the cluster qubits which hold the initial state that
     * will be processed by the measurement pattern.
     *
     * As part of setting a PatternNode as an input qubit, its measurement
     * basis is implicitly set to X, because input qubits for measurement
     * patterns are measured in the X basis (unless they are also output
     * qubit, in which case they are not measured so the basis does not
     * matter)
     *
     * \todo Double check that the X measurement is really a general rule.
     *
     * The input qubit node does not need to hold a logical qubit index,
     * because it is assumed that the input logical qubits are indexed
     * according to their positions in the first column of the NodeWindow. 
     * This is not true of output qubits, which may be re-ordered in general.
     */
    void setAsInput()
	{
	    setX();
	    input_ = true;
	}


    bool isInput() const { return input_; }

    
    std::size_t getLogicalQubitIndex() const
	{
	    if (not output_) {
		throw std::logic_error("Cannot get logical qubit index for a "
				       "PatternNode2 that is not an output "
				       "qubit.");
	    }
	    return logical_qubit_;
	}
    
    /// Set as XY measurement and basis angle
    void setXY(double angle)
	{
	    z_basis_ = false;
	    angle_ = angle;
	}


    /**
     * \brief Set an XY basis with adaptive measurement settings
     *
     * This function sets an XY base angle, and also provides a list of nodes
     * whose outcomes are added up modulo-2, forming a sign for the angle.
     *
     * The nodes affecting the measurement setting of this node are stored in
     * a vector of index offsets. These are used to retrieve outcomes from
     * those nodes to form the sign.
     *
     * Implementation notes:
     *
     * \todo The presence of a std::vector for nodes here is a design flaw
     * that also shows up in a couple of other places. It shouldn't be
     * necessary to construct the nodes just to pass to the function, when
     * they live in a NodeWindow anyway (and std::vector<Node&> is not valid).
     * A vector of pointers would work, but I would like a safer mechanism
     * for this kind of semantic (passing collections of references to nodes).
     *
     * UPDATE: I found std::reference_wrapper might be what I want. I'm going
     * to test it out here to see how it works, then potentially consider it
     * for use elsewhere in the code.
     */
    //template<NodeType Node>
    void setXY(double angle,
	       const std::vector<std::reference_wrapper<PatternNode2>> & depend)
	{
	    setXY(angle);

	    // Now set the measurement dependencies
	    for (const auto & node : depend) {
		if (node.get().index_ == index_) {
		    throw std::logic_error("Cannot set self-measurement setting "
					   "dependency in PatternNode2::setXY");
		} else {
		    const long adapt_index{static_cast<long>(node.get().index_)};
		    adapt_offsets_.push_back(adapt_index - /* this */ index_);
		}
	    }
	    label = 'B';
	}

    /// X basis does not have dependencies
    void setX()
	{
	    setXY(0);
	    label = 'X';
	}

    /// Y basis does not have dependencies
    void setY()
	{
	    setXY(M_PI/2);
	    label = 'Y';
	}
    
    /// Returns true for Z basis, false for XY basis
    bool isZBasis() const
	{
	    return z_basis_;
	}

    /// Return XY-basis angle 
    template<NodeType Node, bool Base>
    double getAngle(NodeWindow<Node,Base> & win) const
	{
	    // Throw error for Z basis
	    if (z_basis_) {
		throw std::logic_error("Cannot getAngle for a PatternNode "
				       "specified for the Z-basis");
	    }

	    // Modify the angle according to the adaptive measurement settings
	    ///\todo It isn't quite right that this function needs getOutcome(),
	    /// because that places a dependency between PatternNode2 and
	    /// QubitNode. Need to have a think about what has happened here.
	    int sign{1};
	    for (const long offset : adapt_offsets_) {
		Node & node{win.getNode(*this, offset)};
		if (node.getOutcome() == 1) {
		    sign *= -1;
		}
	    }
	    
	    // Correct angle 
	    return sign * angle_;
	}
};

#endif
