/**
 * \file pathfinding.hpp
 * \brief Class for emulating pathfinding
 *
 */

#ifndef PATHFINDING_HPP
#define PATHFINDING_HPP

#include "distance-node.hpp"
#include "path-node.hpp"
#include "advance-iterator.hpp"
#include "utils.hpp"

class PathExtension
{
public:

    enum class Count
    {
	/**
	 * \brief How many times a path successor is written
	 *
	 * This is also equal to the length of the path extension
	 */
	PATH_SUCC_WRITES,
    };
    
    /**
     * \brief Perform the global breadth-first search on the active window
     *
     * This function performs the breadth-first search using the active
     * window, starting from a particular entry node in the first column
     * of the window.
     *
     * An important aspect of this function is that path information for
     * the path predecessors of the starting node must be preserved. This
     * means it is incorrect to erase path data in the active window at
     * the start of this function, because the path may partially lie in
     * the active window in cases where the path backtracks. 
     * 
     * \todo This function is expecting the start node to be the right-node
     * for the left-most column of the active window. This causes a 
     * problem for the start of the algorithm, because the starting
     * node is actually the left-node. Need to fix this somehow.
     * 
     * Returns the index which is to be used as the start of the next
     * window search.
     */
    template<typename Node, bool Base>
    std::size_t extend(NodeWindow<Node,Base> && win,
		       const std::size_t y,
		       Generator<unsigned, 0, 1> & path_gen)
	{
	    // Check that there are at least two column
	    if (win.width() < 2) {
		throw std::logic_error("Cannot call extend() with 0 or 1 "
				       "column. "
				       "For zero columns, there is nothing "
				       "to process, "
				       "and with one column there is no need "
				       "to advance "
				       "the path.");
	    }

	    // Clear the counters
	    counts_.clear();
	    
	    // Point to the starting node
	    Node * node = &win[y];
	    
	    // Initialise counters
	    counts_[Count::PATH_SUCC_WRITES] = 0;
	    
	    // Advance along the shortest-path tree, making random choices
	    // at branch points, until you get to a node in the next column.
	    // At each stage, prune every successor apart from the chosen
	    // successor
	    RandomAdvanceIterator it{*node, win, path_gen};
	    while ((not it->right()) or (win.getColumnIndex(*it) not_eq 1)) {

		// Advance to next node
		auto old{it};
		try {
		    // Advance the iterator only once (each advance is random)
		    ++it;
		} catch (const std::runtime_error & e) {
		    std::stringstream ss;
		    ss << "Cannot get next node because "
		       << "no paths are accessible: ";
		    ss << e.what();
		    throw std::logic_error(ss.str());
		}

		// In addition, if *it is the end sentinel, also through
		// the error that the path must be terminated
		///\todo This is an error in the interface of
		/// RandomAdvanceIterator -- there should be one
		/// consistent approach to establishing whether or not
		/// the path iterator can be advanced.
		if (it == RandomAdvanceSentinel{}) {
		    std::stringstream ss;
		    ss << "Cannot get next node because "
		       << "no paths are accessible: end_sentinel reached";
		    throw std::logic_error(ss.str());		    
		}
		
		old->setPathSuccessor(*it);
		counts_[Count::PATH_SUCC_WRITES]++;
	    }

	    // Return the (y-coordinate) index of the next node
	    return it->y();
	}

    /// Get data contents of object (performance counters) as JSON
    nlohmann::json json() const
	{
	    nlohmann::json j;
	    auto & cnt{j["counts"]};
	    cnt["path_succ_writes"] = getCount(Count::PATH_SUCC_WRITES);
	    return j;
	}
    
private:

    /**
     * \brief Store the performance counter values 
     */ 
    std::map<Count,std::size_t> counts_{};
        
    std::size_t getCount(Count c) const {
	try {
	    return counts_.at(c);
	} catch (const std::out_of_range &) {
	    return 0;
	}
    }
};

/**
 * \brief Exception for when a path cannot be continued
 *
 * This exception is designed for use with the emulatePathfinding function.
 */
template<typename Node>
class PathTerminated
{
    const Node last_; ///< The last node in the path
public:
    explicit PathTerminated(const Node & last) : last_{last} {}
    Node last() const noexcept { return last_; }
    const char * what() const noexcept { return "Path terminated"; }
};

/**
 * \brief Class emulate pathfinding
 */
template<typename Search, bool press_enter = true>
class Pathfinding
{
    std::size_t height_;
    
    /**
     * \brief Store the performance counters from the each search window
     */
    std::vector<nlohmann::json> block_results;    

    Search search;

    /**
     * \brief Whether the print the search process
     */
    bool interactive_{false};

    std::size_t print_width_{20};
    
public:

    /// Constructor for normal use
    Pathfinding(std::size_t height)
	: height_{height}, search{height_}, interactive_{false} {}

    /// Constructor for interactive printing
    Pathfinding(std::size_t height, bool interactive)
	: Pathfinding(height)
	{
	    interactive_ = interactive;
	}

    /**
     * \brief Perform the global breadth-first search on a window
     *
     * Traverses the NodeWindow passed as the argument from left to right,
     * one block of width W at a time, starting from the left-most block. At
     * each iteration, the block is searched using the global BFS algorithm,
     * and a path is randomly selected from the candidates which would advance
     * the path from the first column of the sub-block to the second column.
     * The process repeats until a path is found through the full window, or
     * an exception occurs. The blocks on the far right have width < W so that
     * they fit in the full window, and so that a path all the way through the
     * window can be found. 
     *
     * The argument y is the y-coordinate of the starting node in the left-most
     * column of the window.
     * 
     * If the NodeWindow is traversed successfully, then the function returns the
     * last node in the path.
     *
     * If a block is discovered with no viable paths through it, then a 
     * PathTerminated exception is thrown. The exception contains the last
     * node reached by the path, as e.last(). You are guaranteed to get a last
     * node from this function, either as the return value or in the exception.
     * This last node may be anywhere in the window, including the start node
     * (in column zero)
     *
     * This function is called emulatePathfinding because it finds a path using
     * one sub-block at a time. These sub-blocks mimic the behaviour of the
     * ring-buffer that an implemtation might use for the storage of the
     * currently visible window. The algorithm is _not equivalent_ to running 
     * the global breadth first search on the entire NodeWindow, because the
     * path might reach a dead-end due to picking the wrong path-successor.
     * (In contrast, a global BFS will never fail if there are paths through
     * the NodeWindow.)
     *
     * Assumes NodeWindow zero-column is 0.
     *
     * 
     *
     */
    template<typename Node, bool Base>
    Node emulate(NodeWindow<Node,Base> & win, std::size_t y,
		 const std::size_t W, const Seed<> & path_seed)
	{
	    if (win.height() not_eq height_) {
		throw std::out_of_range("Window height must match Pathfinding "
					"height in emulate() function");
	    }
	    
	    if (W > win.width()) {
		throw std::out_of_range("Width of sub-block must be less than or "
					"equal to the NodeWindow width in.");
	    }

	    if (y >= win.height()) {
		throw std::out_of_range("Starting y-coordinate is not in range "
					"in emulatePathfinding");	
	    }

	    // Create the generator for path randomness
	    Generator<unsigned, 0, 1> path_gen{path_seed};
    
	    // Set the initial node as the pathStartPoint
	    win(0,y).setPathBegin();
	    
	    // Loop over all sub-blocks
	    for (std::size_t x{0}; x < win.width(); x++) {

		// Compute the current block location
		const std::size_t start{x};
		const std::size_t end{std::min(x+W,win.width())};
		
		try {
		    nlohmann::json block_entry;
		    auto sub{win.sub(start,end,start)};

		    // Search algorithm and RPT
		    search.search(sub, y);
		    search.makeSPT(sub);
		    block_entry["search"] = search.json();

		    // Extend the path
		    PathExtension path;
		    y = path.extend(std::move(sub), y, path_gen);
		    block_entry["path_extend"] = path.json();

		    // Store the two results in the JSON list
		    block_results.push_back(block_entry);
		    
		    /*} catch (const std::out_of_range & e) {
		    std::cout << e.what() << std::endl;
		    std::cout << "This is a fatal error here (pathfinding). "
			      << "Aborting" << std::endl;
			      abort();*/
		} catch (const std::logic_error & e) {
		    throw PathTerminated<Node>{win(x,y)};
		}

		// Print if interactive mode
		if (interactive_) {

		    // long int block_start{static_cast<long int>(start)};
		    // long int block_end{static_cast<long int>(end)};
		    
		    long win_start{std::max<long>(static_cast<long>(start)-10,0)};
		    long win_end{std::min<long>(win_start+20,win.width())};
		    
		    // Compute printable window
		    const std::size_t width{end-start};
		    std::cout << "Completed search of block " << x << ", "
			      << "width " << width << std::endl;
		    std::cout << "Distance nodes:" << std::endl;
		    win.template print<DistanceNode>(win_start,win_end);
		    std::cout << "Path nodes:" << std::endl;
		    win.template print<PathNode>(win_start,win_end);
		    if constexpr (press_enter) {
			pressEnterToContinue();
		    }
		}
	    }
	    return win(win.width()-1,y);
	}

    /**
     * \brief Get JSON performance counter data
     * 
     * Contains two keys: gbfs and path_extend. Both contains lists of the
     * performance counters relating to those two parts of the process.
     *
     * This JSON object aggregates the data from the search and the path
     * extension into one list, so that they can be processed easily together
     * (i.e. the path extension corresponding to a search is known)
     */ 
    nlohmann::json json() const {
	return block_results;
    }
};

#endif
