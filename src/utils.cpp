/**
 * \file utils.cpp
 * \brief Utility function implementations
 *
 */

#include <iostream>

void pressEnterToContinue(std::ostream & os, std::istream & is) {
    os << "Press enter to continue..." << std::endl;
    is.ignore();
}

