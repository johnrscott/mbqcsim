/**
 * \file normal.cpp
 * \brief Implementation of the Normal class
 *
 */

#include "normal.hpp"

template<std::floating_point T, typename Gen>
Normal<T, Gen>::Normal(T mu, T sigma, const Seed<Gen> & seed)
    : rnd_int_{0, 1, seed}, rnd_fp_{0, 1, seed}, mu_{mu}, sigma_{sigma},
      extra_{0}, extra_present_{false}
{
    if (sigma < 0) {
	throw std::logic_error("Cannot create Normal class when "
			       "sigma < 0");
    }
}

template<Numeric T, typename Gen>
void Normal<T, Gen>::seed(const Seed<Gen> & seed)
{
    ///\todo Check this is valid. The logic for seeding both with the
    /// same value is that they perform completely different processes
    /// (one performs floating point division to obtain a number in the
    /// range 0 to 1, and one checks if the random number is odd or even
    /// to produce 0 or 1. On the other hand, it would be a bug to use,
    /// say, seed and (seed+1), because the external program may use
    /// seed+1 for another purpose. This relates the the overall use
    /// of seeds in the program, which requires thinking about properly.
    rnd_int_.seed(seed); // Set the new seed
    rnd_fp_.seed(seed); // Set the new seed
}

template<Numeric T, typename Gen>
Normal<T, Gen>::seed_t Normal<T, Gen>::seed() const
{
    return rnd_int_.seed(); // Same as rnd_double_.seed_  
}

template<Numeric T, typename Gen>
T Normal<T, Gen>::mu() const
{
    return mu_;
}

template<Numeric T, typename Gen>
T Normal<T, Gen>::sigma() const 
{
    return sigma_;
}

/// Get a random value in [lower, upper)
template<Numeric T, typename Gen>
T Normal<T, Gen>::operator() ()
{
    if (extra_present_) {
	extra_present_ = false;
	return extra_;
    }
    while (true) {
	T a{rnd_fp_()};
	T b{rnd_fp_()};
	if (rnd_int_() == 0) {
	    a = 0 - a;
	}
	if (rnd_int_() == 0) {
	    b = 0 - b;
	}
	T c{a * a + b * b};
	if (c != 0 and c <= 1) {
	    c = std::sqrt(-std::log(c) * 2 / c);
	    // Store the additional generated value for next time
	    extra_present_ = true;
	    extra_ = b * sigma_ * c + mu_;
	    // Return main sample
	    return a * sigma_ * c + mu_;
	}
    }
}

// Instantiations
template class Normal<float, std::mt19937_64>;
template class Normal<double, std::mt19937_64>;
template class Normal<long double, std::mt19937_64>;

template class Normal<float, std::mt19937>;
template class Normal<double, std::mt19937>;
template class Normal<long double, std::mt19937>;
