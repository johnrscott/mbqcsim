/**
 * \file spt-example.cpp
 * \brief Contains the example calculation used in the docs
 *
 */

#include <iostream>
#include "distance-node.hpp"
#include "path-node.hpp"
#include "pattern-node.hpp"
#include "qubit-node.hpp"
#include "make-node.hpp"
#include "node-window.hpp"
#include "global-bfs.hpp"
#include "path-sim.hpp"
#include "path-pattern.hpp"
#include "utils.hpp"

using Node = MakeNode<DistanceNode, PathNode>;

int main()
{
    BaseNodeWindow<Node> win{5,10};

    Seed graph_seed{8584347314192243879};
    Seed path_seed{2504502762997221682};
    
    setRandomEdges(win, 0.63, graph_seed);

    const std::size_t y{3};
    Generator<unsigned, 0, 1> path_gen{path_seed};
    getNextNode(win.sub(0,10,0), y, path_gen);

    win.print<DistanceNode>();
    //win.print<PathNode>();
    
}
