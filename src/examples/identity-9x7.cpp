/**
 * \file identity-9x7.cpp
 * \brief Contains an example use of the identityPattern function
 */

#include <iostream>
#include "pattern-sim.hpp"
#include "meas-patterns.hpp"

using Node = MakeNode<PatternNode2, QubitNode>;

int main()
{
    auto win{identityPattern(7,9,4)};

    // Print the pattern
    win.print<PatternNode2>();

    qsl::Qubits<qsl::Type::Resize,double> q{1}; // Make 1-qubit all-zero state
    auto q_copy{q}; // Copy the state
    q = patternSim(win,std::move(q)); // Apply pattern to q
    win.print<QubitNode>();
    std::cout << "Distance = " << qsl::fubiniStudy(q, q_copy) << std::endl;
}
