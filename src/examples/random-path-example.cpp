/**
 * \file random-path-example.cpp
 * \brief Contains the code used to generate the random path example in the docs
 *
 */

#include <iostream>
#include "distance-node.hpp"
#include "path-node.hpp"
#include "pattern-node.hpp"
#include "qubit-node.hpp"
#include "make-node.hpp"
#include "node-window.hpp"
#include "global-bfs.hpp"
#include "path-sim.hpp"
#include "path-pattern.hpp"
#include "utils.hpp"

using Node = MakeNode<DistanceNode, PathNode, PatternNode>;

int main()
{
    BaseNodeWindow<Node> win{5,15};

    Seed graph_seed{10063563046112571763};
    Seed path_seed{3841411545980233071};
    setRandomEdges(win, 0.60, graph_seed);
    Generator<unsigned, 0, 1> path_gen{path_seed};

    //y = getNextNode(win.sub(0,10,0), y, path_gen);


    std::size_t y{3};
    std::size_t block_width{10};    

    // Find a path through the graph
    emulatePathfinding(win, y, block_width, path_seed);

    // Generate the measurement pattern
    IdentityGate gate;
    gate.generate(win.sub(0,win.width(),0), y);

    win.print<PathNode>();
    //win.print<PatternNode>();

}    
