/**
 * \brief Iterator for generating a new path segment
 *
 * After the search algorithm is complete in the active window, it is necessary
 * to create a new path segment by advancing along a path discovered in the
 * search process. This file contains iterators designed for that process.
 */

#ifndef ADVANCE_ITERATOR
#define ADVANCE_ITERATOR

#include "node-window.hpp"
#include <set>
#include <algorithm>

class RandomAdvanceSentinel {};

class RandomAdvanceException {};

/**
 * \brief Iterator that advances along path successors making a random
 *
 * Use this iterator to advance randomly forward along DistanceNodes. The
 * operator++ chooses a random successor out of the set of forward 
 * successors. The forward successors are successors (from DistanceNode)
 * which are not path predecessors (from PathNode). This prevents this
 * iterator from advancing backwards along a path.
 *
 */
template<NodeType Node, bool Base>
class RandomAdvanceIterator
{
    Node * node;
    NodeWindow<Node,Base> & win_;

    /**
     * \brief Node object to point to as the end sentinel
     *
     * The purpose of this object is to act as the pointee for the
     * interator that has been incremented to the end sentinel.
     * It should never be dereferenced.
     *
     * Implementation notes:
     *
     * A height of one is used because height zero will throw an
     * exception. Without the exception checking in BaseNode, this
     * causes illegal instruction.
     */
    Node end_sentinel;

    /// Generator for picking random successors
    Generator<unsigned, 0, 1> & gen_;
    
public:
    RandomAdvanceIterator(Node & node_in, NodeWindow<Node,Base> & win,
			  Generator<unsigned, 0, 1> & gen)
	: node{&node_in}, win_{win}, gen_{gen} {}

    Node & operator * ()
	{
	    return *node;
	}

    Node * operator -> ()
	{
	    if (node == &end_sentinel) {
		// std::cerr << "Internal error: Cannot perform operator-> on "
		// 	  << "RandomAdvanceIterator end_sentinel";
		// abort();
		throw RandomAdvanceException{};
	    } else {
		return node;
	    }
	}

    /** 
     * \brief Prefix increment
     *
     * Provides the strong exception guarantee with respect to 
     * std::runtime_error for no path successors, provided that
     * NodeWindow provides the strong guarantee std::out_of_range
     * in getNode(base_node, offset).
     *
     */
    RandomAdvanceIterator & operator++()
	{
	    // Get all the successor offsets
	    std::set<int> successors{node->getSuccessorOffsets()};

	    // Get path predecessor
	    ///\todo The fact that this is needed here is an issue -- what
	    // node type does RandomAdvanceIterator need to be defined? Here
	    /// it is two types, DistanceNode and PathNode, but it feels
	    /// like it should probably just be 1.
	    //
	    const long pred{node->getPathPredecessorOffset()};
	    
	    // Remove path predecessor (if present)
	    successors.erase(pred);

	    // This is the first way that 
	    if (successors.size() == 0) {
		throw std::runtime_error("Cannot advance RandomAdvanceIterator "
					 "at node (" + std::to_string(node->x())
					 + "," + std::to_string(node->y()) + ") "
					 "because there are no eligible "
					 "successors to pick from.");
	    }
	    
	    // Pick a random successor
	    std::vector<int> sample;

	    std::ranges::sample(successors, std::back_inserter(sample), 1, gen_);

	    // Assume sample[0] != 0, because zero successors are not valid
	    
	    // Advance to the next node
	    try {
		node = &win_.getNode(*node, sample[0]);
	    } catch (const std::out_of_range & e) {
		node = &end_sentinel;
	    }
	    return * this;
	} 
    
    bool operator==(const RandomAdvanceIterator & other) const {
	return (node == other.node);
    }

    /**
     * \brief Sentinel for end of path
     *
     * Check if the offset is zero. If it is, return true (this is
     * the end of the path).
     */
    bool operator==(const RandomAdvanceSentinel &) const
	{
	    //const long int offset{node->getRandomAdvanceSuccessorOffset()};
	    if (node == &end_sentinel) {
		return true;
	    } else {
		return false;
	    }
	}
};

template<typename Node, bool Base>
class RandomAdvanceRange
{
    const RandomAdvanceIterator<Node,Base> it;
    
public:

    RandomAdvanceRange(Node & node, NodeWindow<Node,Base> & win)
	: it{node, win}
	{
	    const long int offset{node.getRandomAdvanceSuccessorOffset()};
	    if (offset == 0) {
		throw std::logic_error("Node is not on the path. Cannot "
				       "use it as the start of RandomAdvanceRange.");
	    }
	}
    
    auto begin() const
    {
        // The begin is just the pointer to our string.
        return it;
    }
    auto end() const
    {
        // The end is a different type, the sentinel.
        return RandomAdvanceSentinel{};
    }
};

// No need for this is C++20
// template<typename Node, bool Base>
// bool operator==(const RandomAdvanceSentinel &, const RandomAdvanceIterator<Node,Base> & it)
// {
//     return (it == RandomAdvanceSentinel{});
// }

#endif
