/**
 * \file global-bfs.cpp
 * \brief Implementations for GBFS
 *
 */

#include <stdexcept>

std::size_t totalSuccWrites(std::size_t H, std::size_t W)
{
    if ((H == 0) or (W == 0)) {
	throw std::logic_error("Cannot call totalSuccWrites with an empty block");
    }
    if ((H == 1) and (W == 1)) {
	// Only 1 node, no need to write any successors
	return 0;
    }
    if (H == 1) {
	return 2 + 2*(W-2); 
    }
    if (W == 1) {
	return 2 + 2*(H-2); 
    }

    // This was derived by writing it out and factorising
    return (2*W-1)*(2*H-1)-1;
}
