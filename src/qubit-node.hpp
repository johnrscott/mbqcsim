/**
 * \file qubit-node.hpp
 * \brief Contains the cluster qubit node class.
 *
 * The class QubitNode is designed for use with the ClusterSim. The ClusterSim
 * only stores two columns of simulated qubits, but these are made to mimic
 * the behaviour of an arbitrary number of cluster qubits, associated to nodes
 * in a NodeWindow. The QubitNode stores how to make the measurements of the
 * qubits in the ClusterSim, so that the results correspond to cluster qubits
 * in the NodeWindow.
 * 
 */

#ifndef QUBIT_NODE_HPP
#define QUBIT_NODE_HPP

#include <memory>
#include "basis.hpp"
#include "colour.hpp"

/**
 * \brief Cluster qubit node
 * 
 * This node type holds information that is used by the ClusterSim class
 * for simulating an array of cluster qubits. The class holds the basis
 * in which the measurement should be made, and the measurement outcome after
 * the measurement has been made. It is also possible to exclude a node from
 * measurement.
 *
 * \todo Rule of five probably applies here -- write the proper constructors 
 *
 */
template<typename Previous>
class QubitNode : public Previous
{
    /** 
     * \brief Measurement outcome
     *
     * Stores the measurement outcome associated with this qubit. If the
     * qubit has not been measured, the value is -1. Otherwise the outcome
     * 0 or 1 is stored.
     */
    int outcome{-1};

    /**
     * \brief Measurement basis
     *
     * The logic for holding the measurement basis in a pointer is that
     * only non-default measurement settings will take up more memory.
     * This may be important due to the potentially large number of nodes.
     */
    std::unique_ptr<Measure> meas{nullptr};

    /**
     * \brief Whether to measure the qubit or leave it unmeasured
     *
     * Set to true if the qubit _should not_ be measured
     */
    bool do_not_measure{false};

    double z_0_noise_{0}; ///< The first modulator noise value 
    double x_1_noise_{0}; ///< The second modulator noise value
    
    
protected:
    using Previous::index_;
    using Previous::x_;
    using Previous::y_;
    
public:

    bool operator == (const QubitNode &) const & = default;
    
    /**
     * \brief Construct a default cluster qubit node
     */
    //QubitNode() = default;
    using Previous::Previous;

    /**
     * \brief Copy constructor implementing deep copy of the basis
     *
     */
    QubitNode(const QubitNode & old) : Previous{old}
	{
	    ///\todo This feels a bit risky 
	    do_not_measure = old.do_not_measure;
	    outcome = old.outcome;
	    if (old.meas) {
		meas = std::make_unique<Measure>(*old.meas);
	    }
	}

    /**
     * \brief Copy constructor implementing deep copy of the basis
     *
     */
    QubitNode & operator= (const QubitNode & old)
	{
	    ///\todo This feels a bit risky
	    Previous::operator=(old);
	    do_not_measure = old.do_not_measure;
	    outcome = old.outcome;
	    if (old.meas) {
		meas = std::make_unique<Measure>(*old.meas);
	    }

	    return * this;
	}

    
    /// Set the qubit to not be measured
    void setDoNotMeasure(bool state)
	{
	    do_not_measure = state;
	}

    /// Return true if the qubit should not be measured
    [[nodiscard]] bool doNotMeasure() const
	{
	    return do_not_measure;
	}

    /// Marker colour for printing
    std::string markerColour() const
	{
	    if (do_not_measure) {
		return Colour::RED + Colour::BOLD;
	    } else {
		if (outcome != -1) {
		    return Colour::GREEN + Colour::BOLD;
		} else {
		    return Colour::PURPLE + Colour::BOLD;
		}
	    }
	    return "";
	}

    std::string markerShape() const
    	{
	    if (outcome != -1) {
		return std::to_string(outcome);
	    } else {
		if (meas) {
		    return std::string(1, meas->label());
		} else {
		    return "Z";
		}
	    }
    	}
    
    /**
     * \brief Measure the cluster qubit
     *
     * This function measures qubit 0 of the simulator q 
     * (passed in the argument) and stores the result in the outcome
     * variable. The function also returns the measurement outcome.
     *
     * If there is not defined measurement, then the function measures
     * the qubit in the computational basis (MeasureZ).
     *
     * If the qubit is marked do_not_measure, the function returns -1.
     * 
     */
    int measure(qsl::Qubits<qsl::Type::Resize, double> & q, unsigned targ,
		Random<double> & rnd, double z_0_noise, double x_1_noise)
	{
	    if (not do_not_measure) {
		z_0_noise_ = z_0_noise;
		x_1_noise_ = x_1_noise;

		if (meas) {
		    outcome = meas->operator()(q, targ, rnd, z_0_noise, x_1_noise);
		} else {
		    outcome = MeasureZ(q, targ, rnd, z_0_noise, x_1_noise);
		}
		return outcome;
	    } else {
		return -1;
	    }
	}
    
    unsigned getOutcome() const {
	if (outcome == -1) {
	    throw std::logic_error("No outcome: the qubit has not "
				   "been measured yet");
	}
	return static_cast<unsigned>(outcome);
    }
    
    nlohmann::json json() const
	{
	    // Be careful to use (), not {} (to avoid init list constructor) 
	    nlohmann::json j(Previous::json());
	    auto & qubit_json{j["qubit"]};
	    if (meas) {
		qubit_json["meas"] = meas->json();
	    } else {
		qubit_json["meas"] = MeasureZ.json();
	    }
	    // Store noise values
	    qubit_json["noise"]["z_0"] = z_0_noise_;	    
	    qubit_json["noise"]["x_1"] = x_1_noise_;
	    qubit_json["outcome"] = outcome; 
	    return j;
	}
    
    /**
     * \brief Set the measurement basis
     *
     * Implementation note: This function takes an rvalue reference because
     * it is assumed that objects will only every be constructed as
     * temporaries (e.g. MeasureXY{0.2})
     *
     */
    template<typename MeasureType>
    void setBasis(const MeasureType & basis) {
	meas = std::make_unique<MeasureType>(basis);
    }

    /// Get the measurement basis
    Measure getBasis() const
	{
	    if (meas) {
		return * meas;
	    } else {
		return MeasureZ;
	    }
	}
    
    
    /**
     * \brief Print the qubit node details
     *
     */
    void print(std::ostream & os = std::cout) const {

	Previous::print(os);
	os << "  (qnode:m=";
	if (meas) {
	    meas->print(os);
	} else {
	    MeasureZ.print(os);
	}
	os << ",out:" << outcome;
	os << ")" << std::endl;

    }
    
    
};


#endif
