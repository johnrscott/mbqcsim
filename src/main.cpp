/**
 * \file main.cpp
 * \brief Contains main function
 *
 */

#include <iostream>
#include "pattern-node.hpp"
#include "path-node.hpp"
#include "pathfinding.hpp"
#include "make-node.hpp"
#include "global-bfs.hpp"
#include "path-sim.hpp"
#include "path-pattern.hpp"

using Node = MakeNode<PathNode, DistanceNode, PatternNode>;

int main()
{
    Pathfinding<GBFS> pathf{8, false};
    BaseNodeWindow<Node> win{8,12};
    Seed graph_seed{16579013659693138810U};
    graph_seed.print();
    Seed path_seed{10037827752562515194U};
    path_seed.print();
    
    setRandomEdges(win, 0.65, graph_seed);

    //Use this to get path
    try {
    	pathf.emulate(win, 2, 5, path_seed);
    } catch (...) {
	writeLeftNodes(win.sub(0,12,0), win(0,2));
    	//win.print<PathNode>();
    }

    // /// Use this to get RPT and right nodes
    // GBFS gbfs(8);
    // auto sub{win.sub(0,12,0)};
    // gbfs.search(sub, 2);
    // gbfs.makeSPT(sub);
    // win.print<DistanceNode>();
    // //win.print<PathNode>();

    IdentityGate gate;
    gate.generate(win.sub(0,12,0), 2);
    win.print<PatternNode>();
    
}
