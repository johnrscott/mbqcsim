/**
 * \file custom-vector.hpp
 * \brief A std::vector wrapper that supports subvector objects
 *
 * The Vector class is a class that can own a std::vector itself,
 * or can act as a view into a std::vector owned by another
 * Vector object. The purpose is to allow read-only or
 * read-write access to only a portion of a std::vector. Copying
 * the std::vector is not possible because of its size, and 
 * passing around unprotected references feels like a bad idea.
 *
 * This class might be replaceable by a standard library 
 * construction, but I'm not sure what. In the previous code,
 * I used std::ranges::subrange, but that feels just as bad
 * as unprotected references.
 */

#ifndef CUSTOM_VECTOR
#define CUSTOM_VECTOR

#include <vector>
#include <stdexcept>
#include <memory>
#include <iostream>

/**
 * \brief A class for accessing ranges of elements in vectors
 *
 * This program contains a large std::vector of nodes, which many parts of
 * the program either manipulate or read. This class is an attempt to allow
 * controlled access to portions of the vector without needing to pass 
 * around references or copies.
 *
 * There are two ways to construct this class. It can be constructed in a
 * similar way to a std::vector, in which case it creates a vector that it
 * owns. Alternatively, a new Vector can be obtained from an already
 * existing Vector. In this case, the new Vector stores a reference
 * to the previous Vector; no copying is done. However, the new object
 * may only expose a range of the old Vector, protecting parts of the
 * original vector that should not be modified. Users of the new Vector
 * can perform indexed access as if they were dealing with the a full vector.
 *
 * \todo The situation when const objects of this class are created needs some
 * thinking about. As it is, it is possible to modify the contents of the
 * wrapped vector, which feels wrong (or there is a bug, but I can't see it).
 * Have a look at the gtests for the problem. 
 * 
 */
template<typename T, bool Base>
class Vector
{
    std::shared_ptr<std::vector<T>> cont{std::make_shared<std::vector<T>>()};
    std::size_t begin_offset{0}; ///< Starting element (true index into cont)
    std::size_t end_offset{0}; ///< (One past) the end_offset element (true index into cont)
    
    /// Convert a logical index to a true index into cont
    inline std::size_t convertIndex(std::size_t index) const
	{
	    return begin_offset + index;
	}

    /// Allow access between Base = true/false class types 
    //template<typename,bool> friend_offset class Vector;
    friend class Vector<T, not Base>;
    
public:

    std::size_t size() const {
	return end_offset - begin_offset;
    }
    
    Vector() = default;

    Vector(const std::initializer_list<T> & init)
	: cont{std::make_shared<std::vector<T>>(init)},
	  begin_offset{0}, end_offset{cont->size()}
	{}
    
    Vector(std::size_t size, const T & init = T())
	: cont{std::make_unique<std::vector<T>>(size,init)},
	  begin_offset{0}, end_offset{cont->size()}
	{}

    /**
     * \brief Construct a Vector<T,false> from another Vector
     *
     * This constructor is the means for making a Vector that exposes
     * access to a subrange of another Vector. The Vectors constructed
     * using this constructor do not own their contents; logically, they
     * point to the same data contained in the Vector object that is the
     * first argument to the constructor. (Since the class contains a
     * shared_ptr, the new Vector does actually own the data, so that if
     * the old Vector is deleted, this object remains valid; however, this
     * is an edge case, not the intended purpose).
     *
     * This constructor is not a valid method for BaseVector objects, which
     * do own their own data.
     * 
     */
    template<bool B>
    Vector(const Vector<T,B> & other,
	      std::size_t begin_offset_in, std::size_t end_offset_in)
	: cont{other.cont}, begin_offset{other.convertIndex(begin_offset_in)},
	  end_offset{other.convertIndex(end_offset_in)}
	{
	    static_assert(not Base,
			  "The constructor Vector(Vector, "
			  "std::size_t, std::size_t) is not "
			  "available for Base == true Vectors");
	    if (begin_offset > end_offset) {
		throw std::logic_error("Cannot make SubVector using "
				       "begin_offset > end_offset");
	    }
	    if (begin_offset > cont->size()) {
		throw std::out_of_range("Begin_Offset index is out of range for "
					"Vector");
	    }
	    if (end_offset > cont->size()) {
		throw std::out_of_range("End_Offset index is out of range for "
					"Vector");
	    }
	}
    
    /// Begin for range-based for loop
    auto begin() { return cont->begin() + begin_offset; }
    auto begin() const { return cont->begin() + begin_offset; }

    /// End for range-based for loop
    auto end() { return cont->begin() + end_offset; }
    auto end() const { return cont->begin() + end_offset; }
    
    /** 
     * \brief Get a Vector which holds a range of elements from
     * this Vector
     */
    Vector<T,false>
    getSub(std::size_t begin_offset_in, std::size_t end_offset_in) const
	{
	    return Vector<T,false>{*this, begin_offset_in, end_offset_in};
	}
    
    /**
     * \brief Access an element at a particular index
     *
     * Access a particular element with bounds checking
     */ 
    const T & at(std::size_t index) const
	{
	    const std::size_t true_index{convertIndex(index)};
	    if (true_index < end_offset) {
		return (*cont)[true_index];
	    } else {
		throw std::out_of_range("Index is out of range for "
					"Vector operator[]. Trying to access "
					"index = " + std::to_string(index) + " "
					"when size = " + std::to_string(size()));
	    }
	}

    T & at (std::size_t index)
	{
	    return const_cast<T &>(
		static_cast<const Vector &>(*this).at(index));
	}

    /**
     * \brief Element access without bounds checking
     *
     * \todo Add a memory check (e.g. from valgrind) to the tests
     */
    const T & operator[] (std::size_t index) const
	{
	    const std::size_t true_index{convertIndex(index)};
	    return (*cont)[true_index];
	}

    T & operator[] (std::size_t index)
	{
	    return const_cast<T &>(
		static_cast<const Vector &>(*this)[index]);
	}

    
    /**
     * \brief Add an element to the end_offset of the Vector
     *
     * Wrapper around the internal std::vector::push_back. Be aware that
     * in the general case, where this object is a Vector derived from
     * another Vector, such a push_back may add an element to the 
     * middle of the larger vector. This may be a very expensive operation
     * if the underlying vector is long and the push_back end_offsets up inserting
     * near the start. In general, try to avoid using push_back on
     * Vectors derived from Vectors. (This is not the normal use case
     * for this class anyway, so it shouldn't be a problem. This class will
     * mainly be used for providing access to fixed length subranges of 
     * vectors.)
     * 
     */
    void push_back(const T & val)
	{
	    static_assert(Base,
			  "Member function push_back is only available "
			  "in base Vectors");
	    // Insert at end_offset
	    cont->push_back(val);
	    // Update the new end_offset
	    end_offset++;	    
	}

    template<typename... Args>
    void emplace_back(Args&&... args)
	{
	    static_assert(Base,
			  "Member function emplace_back is only available "
			  "in base Vectors");
	    // Insert at end_offset
	    cont->emplace_back(std::forward<Args>(args)...);
	    // Update the new end_offset
	    end_offset++;	    
	}
    
    void print(std::ostream & os = std::cout) const
	{
	    os << "Subvector begin_offset=" << begin_offset
	       << ", end_offset=" << end_offset
	       << std::endl;
	    for (std::size_t n = begin_offset; n < end_offset; n++) {
		///\todo Is this the best way to do operator[]?
		/// Another option is cont->operator[](n). Pros
		/// and cons?
		os << (*cont)[n] << std::endl;
	    }
	    os << std::endl;
	}

};

template<typename T>
using BaseVector = Vector<T, true>;

template<typename T>
using SubVector = Vector<T, false>;

template<typename T, bool Base>
Vector<T,Base> copy(const Vector<T,Base> & vec)
{
    Vector<T,Base> dest(vec.size()); // Create vector of the correct size
    for (std::size_t n{0}; n < vec.size(); n++) {
	dest[n] = vec[n];
    }
    return dest;
}


#endif
