/**
 * \file cluster-sim.hpp
 * \brief A recycling cluster state using two columns for simulating MBQC
 *
 */

#ifndef CLUSTER_SIM_HPP
#define CLUSTER_SIM_HPP

#include <qsl/qubits.hpp>
#include <qsl/utils/quantum.hpp>
#include <qsl/utils/random.hpp>
#include <ranges>
#include "basis.hpp"
#include "node-window.hpp"
#include "random.hpp"
#include "seed.hpp"
#include "normal.hpp"

/// Class for providing a source of no noise for ClusterSim
struct NoNoise
{
    constexpr double get() { return 0.0; }
};

/// Class for providing a source of guassian noise for ClusterSim
class GaussianNoise
{
    Normal<double> rnd_;
public:
    GaussianNoise(double mu, double sigma, const Seed<> & seed)
	: rnd_{mu,sigma,seed} {}
    Normal<double>::seed_t seed() const { return rnd_.seed(); }
    double get() { return rnd_(); }
};

    
/**
 * \brief A recycling cluster state simulator
 *
 * This class is a rectangular cluster state simulator with the 
 * ability to re-allocate measured columns of qubits on the 
 * other side of the grid. This makes it possible to simulate
 * arbitrary depth cluster state circuits containing N logical
 * qubits using a statevector of 2N simulated qubits.
 *
 * The statevector is arranged as follows:
 *
 * \verbatim
    Q_{N-1} --  Q_{2N-1}
      |            |
     ...          ...
      |            |
    Q_{2}   --  Q_{N+2}
      |            |
    Q_{1}   --  Q_{N+1}
      |            |
    Q_{0}   --  Q_{N}  
   \endverbatim
 *
 * 
 * The indices represent the position in the true linear state vector,
 * and the position in the grid show the logical position of each
 * qubit in the cluster state. To match the (x,y) coordinate system used 
 * in the random graph model, qubits are indexed in column-major order,
 * and the print function visualises the cluster in the xy plane.
 *
 * The left-most column contains qubits that are measured. One by
 * one, the lowest index qubit is measured out, reducing the size of
 * the state vector by one each time, until the total length of the
 * state vector is N. Then qubits are re-allocated on the right hand
 * side (at the high index end of the linear state vector), and
 * re-entangled into the cluster state.
 *
 * The pattern of entanglement is specified when a column is added. 
 * 
 */
template<typename NoiseSource = NoNoise>
class ClusterSim
{
    unsigned N; ///< The height of the cluster state 
    using Sim = qsl::Qubits<qsl::Type::Resize, double>;
    Sim q; ///< The quantum simulator
    Random<double> rnd; ///< Source of randomness for measurements
    NoiseSource noise_src_; ///< Source of noise (has a get() method)
    
public:

    /**
     * \brief Initialise the cluster state object
     *
     * This constructor creates an empty cluster state. To add columns,
     * use the addColumn function.
     *
     * \param N The number of qubits to include in the column
     * \param seed The seed to use for the source of randomness
     */
    ClusterSim(unsigned N, const Seed<> & seed,
	       NoiseSource && noise_src_ = NoiseSource{});

    void setState(qsl::Qubits<qsl::Type::Resize, double> && new_q)
	{
	    if ((new_q.getNumQubits() % N) != 0) {
		throw std::logic_error("Setting ClusterSim state to a value "
				       "with a number of qubits that is not "
				       "a multiple of the height N is not "
				       "allowed.");
	    }
	    q = std::move(new_q);
	}

    /**
     * \brief Move the simulator out of this class
     * \todo This could probably be implemented as a kind of move converter
     */
    qsl::Qubits<qsl::Type::Resize,double> && getStateByMove()
	{
	    return std::move(q);
	}
    
    unsigned height() const
	{
	    return N;
	}
    
    /// Get the number of qubits currently unmeasured
    unsigned size() const {
	return q.getNumQubits();
    }

    void print(std::ostream & os = std::cout) const
	{
	    os << "(csim:N=" << N << ",W=" << q.getNumQubits()/N
	       <<")" << std::endl;
	}

    void printState(std::ostream & os = std::cout) const
	{
	    q.print(os);
	}
    
    /**
     * \brief Measure a column of nodes
     *
     * Measure a range (a column) of cluster qubit nodes. The measurement bases
     * are stored in the nodes, and the outcomes are recorded in the nodes.
     *
     * \todo Provide the a concept for the node (cluster qubit)
     */
    template<NodeType Node, bool Base>
    void measureColumn(NodeWindow<Node,Base> && win)
	{
	    /// CHECK WINDOW CONTAINS JUST ONE COLUMN
	    if (win.width() not_eq 1) {
		throw std::logic_error("Measurement window must contain "
				       "only one column");
	    }
	    
	    if (win.size() not_eq N) {
		throw std::logic_error("Measurement column window "
				       "height must be N");
	    }

	    // Keep track of which qubit to measure
	    unsigned targ = 0;

	    for (auto & qubit : win) {
		// Check if the qubit should be measured
		if (not qubit.doNotMeasure()) {
		    // Measure out the qubit and store the measurement result
		    // internally
		    const double z_0_noise{noise_src_.get()};
		    const double x_1_noise{noise_src_.get()};
		    qubit.measure(q, targ, rnd, z_0_noise, x_1_noise);
		} else {
		    // Increment the target qubit to measure so that the
		    // previous qubit (marked do_not_measure) is not
		    // measured on the next round.
		    targ++;
		}
	    }
	}
    
    /**
     * \brief Get the current state
     */
    qsl::Qubits<qsl::Type::Resize, double> state() const
	{
	    return q.getState();
	}

    /**
     * \brief Re-allocate a new column with arbitrary entanglement
     *
     * Use this function to add a new column to the cluster state simulator. 
     * 
     * If there are no columns yet in the simulator, then the argument is 
     * required to be a NodeWindow with one column, of the right height,
     * where the single column has index zero. The edges present in the
     * NodeWindow determine where CZ gates are applied to the new column.
     *
     * If there is one column of qubits in the simulator already, then the
     * argument must be a NodeWindow with two columns, where the second has
     * index zero. The index-zero column is used to establish vertical 
     * entanglement in the new column. The edges between the two columns
     * establish the entanglement links (CZ gates) between the new column
     * and the already existing column
     * 
     */
    template<NodeType Node, bool Base>
    void addColumn(const NodeWindow<Node,Base> & win)
	{
	    ///\todo Could do with a way to check that there is a column
	    /// index -1, etc. Not implemented yet from NodeWindow.

	    if (q.getNumQubits() % N not_eq 0) {
		throw std::logic_error("Cannot addColumn to a ClusterSim "
				       "contain a partially filled column");
	    }
	    
	    // Check there are no more than two columns
	    if (q.getNumQubits() >= 2*N) {
		throw std::logic_error("Cannot addColumn if there are already "
				       "two columns of qubits");
	    }
	    
	    // Check the window height
	    if (win.height() not_eq N) {
		throw std::logic_error("NodeWindow passed to addColumn has "
				       "incorrect height "
				       + std::to_string(win.height()) + " != "
				       + std::to_string(N));
	    }

	    // Check the window width is one when there are no qubits
	    if ((q.getNumQubits() == 0) and (win.width() not_eq 1)) {
		throw std::logic_error("The NodeWindow passed to addColumn must "
				       "have width 1 when the ClusterSim is "
				       "empty.");
	    }

	    // Check the window width is two when there is one column of qubits
	    if ((q.getNumQubits() == N) and (win.width() not_eq 2)) {
		throw std::logic_error("The NodeWindow passed to addColumn must "
				       "have width 2 when the ClusterSim has "
				       "one column of qubits.");
	    }
	    
	    // Add the new qubits to the statevector and
	    // apply the Hadamard everywhere
	    for (std::size_t k = 0; k < N; k++) {
		q.appendQubit();
		q.hadamard(q.getNumQubits()-1);
	    }
	    
	    // Apply CZ between vertical neighbours according to NodeWindow
	    const std::size_t start{q.getNumQubits()-N};
	    for (std::size_t y = 0; y < N-1; y++) {
		if (win.aboveEdge(0,y)) {
		    q.controlZ(start+y, start+y+1);
		}
	    }
	    
	    // Apply CZ between horizontal neighbours according to NodeWindow
	    if (win.width() == 2) {
		for (std::size_t y = 0; y < N; y++) {
		    if (win.rightEdge(-1,y)) {		
			q.controlZ(y,y+N);
		    }
		}
	    }
	}

    /**
     * \brief Entangle a single column in the simulator using a NodeWindow
     *
     * The purpose of this class is to entangle the initial column of the
     * cluster state. This can be done after using the set state function,
     * as part of the implementation of measurement patterns on arbitrary
     * input states.
     *
     */
    template<NodeType Node, bool Base>
    void entangleColumn(const NodeWindow<Node,Base> & win)
	{
	    if (q.getNumQubits() not_eq N) {
		throw std::logic_error("Cannot entangle column on a ClusterSim "
				       "that does not have exactly 1 column");
	    }
	    
	    // Check the window height
	    if (win.height() not_eq N) {
		throw std::logic_error("NodeWindow passed to entangleColumn has "
				       "incorrect height "
				       + std::to_string(win.height()) + " != "
				       + std::to_string(N));
	    }

	    // Check the window width is one when there are no qubits
	    if (win.width() not_eq 1) {
		throw std::logic_error("The NodeWindow passed to entangle"
				       "Column must have width 1.");
	    }
	    
	    // Apply CZ between vertical neighbours according to NodeWindow
	    const std::size_t start{q.getNumQubits()-N};
	    for (std::size_t y = 0; y < N-1; y++) {
		if (win.aboveEdge(0,y)) {
		    q.controlZ(start+y, start+y+1);
		}
	    }
	}

};

#endif
