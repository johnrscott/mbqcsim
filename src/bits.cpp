/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file bits.cpp
 * \brief Implementation of the Bits class
 *
 */

#include <vector>
#include <iostream>
#include "bits.hpp"

/// Return the length of the bitstring
std::size_t Bits::size() const
{
    return bits.size();
}
    
void Bits::pushBit(unsigned bit)
{
    if (bit > 1) {
	throw std::out_of_range("Bit must be zero or one");
    }
    bits.push_back(bit);
}
    
/**
 * \brief Print the object
 *
 * This function prints the bits to the ostream passed as an argument,
 * with most significant bits to the left (i.e. the normal way round).
 */
void Bits::print(std::ostream & os) const
{
    const std::size_t N = bits.size();
    for (std::size_t n = 0; n < N; n++)
	os << bits[N-1-n];
}

/**
 * \brief Get the value of the binary string as an integer
 *
 * \todo Fix the potential bug where unsigned is not big enough
 */
unsigned Bits::val() const
{
    const std::size_t N = bits.size();
    unsigned value{ 0 };
    for (std::size_t n = 0; n < N; n++) {
	value |= bits[n] << n;  
    }
    return value;
}
    
/**
 * \brief Access the nth bit in the object
 *
 * This function returns a reference, so the result can
 * be modified. 
 */
unsigned & Bits::operator () (unsigned n)
{
    if (n >= bits.size()) {
	throw std::out_of_range("n=" + std::to_string(n) + " is out "
				"of range for Bits[]");
    }
    return bits[n];
}

/**
 * \brief Return the nth bit read only
 */
unsigned Bits::operator () (unsigned n) const
{
    const std::size_t N = bits.size();
    if (n >= N) {
	throw std::out_of_range("n=" + std::to_string(n) + " is out "
				"of range for Bits[]");
    }
    return bits[n];
}

/**
 * \brief Return a substring from the bistring
 *
 */
Bits Bits::operator () (unsigned end, unsigned start) 
{
    if (start > end) {
	throw std::out_of_range("Cannot get bit range with start > end");
    }

    if (end >= bits.size()) {
	throw std::out_of_range("end is too large");
    }
	    
    Bits substring;
    for (std::size_t n = start; n <= end; n++) {
	substring.pushBit(bits[n]);
    }
    return substring;
}

Bits & Bits::operator &= (const Bits & rhs)
{
    const std::size_t N = bits.size();
    for (std::size_t n = 0; n < N; n++) {
	bits[n] &= rhs(n);
    }
    return * this;
}

Bits & Bits::operator ^= (const Bits & rhs)
{
    const std::size_t N = bits.size();
    for (std::size_t n = 0; n < N; n++) {
	bits[n] ^= rhs(n);
    }
    return * this;
}

/**
 * \brief Return the result of xor-ing all the bits together
 */
unsigned Bits::xorBits() const {
    unsigned result{ 0 };
    const std::size_t N = bits.size();
    for (std::size_t n = 0; n < N; n++) {
	result ^= bits[n];
    }
    return result;
}


/**
 * \brief Concaternate two Bits objects
 *
 * This function concaternates two bitstrings a and b into a
 * bitstring ab, where a is more significant than b in the
 * resulting bitstring. The operator + is used because addition 
 * is not defined for this bitstring object.
 * 
 */
Bits operator + (const Bits & a, const Bits & b)
{
    Bits result{ b };
    // Write most significant part
    for (std::size_t n = 0; n < a.size(); n++) {
	result.pushBit(a(n));
    }
    return result;
}

Bits operator & (const Bits & a, const Bits & b)
{
    Bits result{ a };
    return result &= b;
}

Bits operator ^ (const Bits & a, const Bits & b)
{
    Bits result{ a };
    return result ^= b;
}

/// Xor operator with literal
Bits operator ^ (const Bits & a, std::integral auto b)
{
    return a ^ Bits(b);
}

/// Xor operator with literal the other way round
Bits operator ^ (std::integral auto a, const Bits & b)
{
    return b ^ a;
}


/**
 * \brief Print Bits object to ostream
 */
std::ostream & operator << (std::ostream & os, const Bits & bits)
{
    bits.print(os);
    return os;
}

