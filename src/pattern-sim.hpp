/**
 * \file pattern-sim.hpp
 * \brief MBQC measurement pattern simulator
 *
 */

#ifndef PATTERN_SIM_HPP
#define PATTERN_SIM_HPP

#include "bits.hpp"
#include "node-window.hpp"
#include "cluster-sim.hpp"

/// Use column col to update byproduct operators byp
template<NodeType Node, bool Base>
static void updateByp(NodeWindow<Node,Base> && col,
		      std::map<std::size_t,Bits> & byp)
{
    for (std::size_t y{0}; y < col.height(); y++) {	
	// Update byproduct operators if there is an outcome
	Node & node{col(0,y)};
	if (not node.isOutput()) {
	    node.updateByp(byp, node.getOutcome());
	}
    }
}

/// Use column PatternNode2 in col to set QubitNode bases in col 
template<NodeType Node, bool Base>
static void setMeasurementBases(NodeWindow<Node,Base> & win, std::size_t x)
{
    for (std::size_t y{0}; y < win.height(); y++) {
	Node & node{win(x,y)};

	// Set the basis including adaptive measurement settings
	if (not node.isZBasis()) {
	    node.setBasis(MeasureXY(node.getAngle(win)));
	}
    }
}



/**
 * \brief Simulate a measurement pattern stored in a NodeWindow
 *
 * Call this function to simulate a measurement pattern defined by a
 * NodeWindow, using a starting state provided as an argument, store
 * the intermediate results of the pattern in the node window, and return
 * the resulting state from the measurement pattern by move.
 *
 * Assumes a NodeWindow with x_zero == 0
 *
 * \todo Remove the arbitrary output indexing in PatternNode2 -- it doesn't
 * really add anything, and a simple logical-qubit indexing scheme would
 * work just as well I think.
 */
template<NodeType Node, bool Base>
qsl::Qubits<qsl::Type::Resize, double>
patternSim(NodeWindow<Node,Base> & win,
	   qsl::Qubits<qsl::Type::Resize, double> && q)
{
    // Create a simulator
    ///\todo check range here!
    Seed sim_seed;
    ClusterSim sim{static_cast<unsigned>(win.height()), sim_seed, NoNoise{}};

    // Check that there are the same number of logical input qubits in the
    // pattern as there are qubits in the input state
    ///\todo Can combine with next for loop maybe
    std::size_t input_count{0};
    for (std::size_t y{0}; y < win.height(); y++) {
	if (win(0,y).isInput()) {
	    input_count++;
	}
    }
    if (q.getNumQubits() not_eq input_count) {
	throw std::logic_error("Cannot compute measurement pattern because "
			       "the number of qubits in the initial state "
			       "does not match the number of input qubits in "
			       "the first column of the measurement pattern");
    }
    
    // Before moving the initial state into the ClusterSim, it is
    // necessary to insert the correct |+) states into the state
    // vector, and then entangle according to the NodeWindow win.
    // Loop over the first column of the NodeWindow to find which
    // qubits are input qubits
    for (std::size_t y{0}; y < win.height(); y++) {
	Node & node{win(0,y)};
	if (not node.isInput()) {
	    // Add a new qubit and put it in the plus state
	    q.addQubit(node.y());
	    q.hadamard(node.y());
	}
    }
    
    // Move the initial quantum state into the simulator
    sim.setState(std::move(q));

    // Entangle the first column of qubits according to the edges in the
    // first column of the NodeWindow
    sim.entangleColumn(win.sub(0,1,0));
    
    // Create a store for byproduct operators
    std::map<std::size_t,Bits> byp{};

    // Use the final column of the NodeWindow to establish which qubits
    // are output qubits, and what are their logical qubit indices.
    /// \todo Initialising here avoids the defect currently in the Bits class
    /// which causes invalid operations between different length Bits objects.
    for (std::size_t y{0}; y < win.height(); y++) {
	Node & node{win(win.width()-1,y)};
	if (node.isOutput()) {
	    byp[node.getLogicalQubitIndex()] = Bits{0,0};
	}
    }

    // Loop over the "inner" columns of the window
    for (long x{1}; x < static_cast<long>(win.width()); x++) {

	// Add the next column
	sim.addColumn(win.sub(x-1,x+1,x));

	// Set measurement bases in the previous column
	setMeasurementBases(win, x-1);

	// Measure the previous column
	sim.measureColumn(win.sub(x-1,x,x-1));    

	// Accumulate outcomes to byproduct operators
	updateByp(win.sub(x-1,x,x-1), byp);
    }

    // Iterate over final column of nodes and set the do-not-measure
    // flag and calculate the true index mapping
    std::map<std::size_t,std::size_t> index_mapper{};
    std::size_t true_index{0};
    const long x_back{static_cast<long>(win.width())-1};
    for (std::size_t y{0}; y < win.height(); y++) {
	Node & node{win(x_back, y)};
	if (node.isOutput()) {
	    node.setDoNotMeasure(true);
	    index_mapper[node.getLogicalQubitIndex()] = true_index++;
	}
    }

    // Set final column measurement settings
    ///\todo Is there an edge case here to deal with, with not measuring
    /// output qubits
    ///\todo NOTE: THIS IS NOT CURRENTLY DOING ANYTHING -- ONLY ONE ROW.
    /// GET READY TO DEBUG IN MULTI COLUMN VERSIONS (CUT OUTS).
    setMeasurementBases(win, x_back);
    
    // Measure the final column (leaving only output qubits)
    sim.measureColumn(win.sub(x_back, x_back+1, x_back));    

    // Accumulate outcomes to byproduct operators
    ///\todo GET READY TO DEBUG THIS FOR MULTIPLE ROWS
    updateByp(win.sub(x_back, x_back+1, x_back), byp);

    // Get the remaining state
    q = sim.getStateByMove();
   
    // Correct each output qubit according to its byproduct operators
    // It is necessary to know which output qubits correspond to which
    // logical qubit. This can be stored in the PatternNode itself along
    // with the output status
    //
    for (const auto & [logical_index,byp] : byp) {
	if (byp(0) == 1) {
	    q.pauliZ(index_mapper[logical_index]);
	}
	if (byp(1) == 1) {
	    q.pauliX(index_mapper[logical_index]);
	}
    }
    
    // Return the state
    ///\todo I believe the choice I'm making by returning by value and
    /// placing a std::move here is that the move constructor will be
    /// called in creating the returned object. Normally std::move is not
    /// what you want, because RVO takes care of it, but here q is a
    /// parameter and I think that prevents RVO. However, returning an
    /// rvalue reference directly (although slightly more inefficient) is
    /// a bit unsafe. This is my reference so far: "https://stackoverflow.
    /// com/questions/30094067/should-i-return-an-rvalue-reference-parameter
    /// -by-rvalue-reference". Need to look into what is the right thing
    /// to do here.
    return std::move(q);
    
}



#endif
