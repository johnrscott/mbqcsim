/**
 * \file coord-arith.cpp
 * \brief Implementations for coordinate arithmetic
 *
 */

#include "coord-arith.hpp"

bool operator==(const Vec2 & lhs, const Vec2 & rhs)
{
    return (lhs.x() == rhs.x()) and (lhs.y() == rhs.y());
}

Vec2 abs(const Vec2 & vec)
{
    return {std::abs(vec.x()), std::abs(vec.y())};
}

Vec2 operator * (long scalar, const Vec2 & vec)
{
    return {scalar * vec.x(), scalar * vec.y()};
}

Vec2 operator / (const Vec2 & vec, long scalar)
{
    return {vec.x() / scalar, vec.y() / scalar};
}

Vec2 operator - (const Vec2 & vec)
{
    return -1 * vec;
}

Vec2 operator + (const Vec2 & lhs, const Vec2 & rhs)
{
    return {lhs.x() + rhs.x(), lhs.y() + rhs.y()};
}

Vec2 operator - (const Vec2 & lhs, const Vec2 & rhs)
{
    return lhs + (-rhs);
}



