/**
 * \file pattern-node.hpp
 *
 * Contains the measurement pattern node class.
 *
 */

#ifndef PATTERN_NODE_HPP
#define PATTERN_NODE_HPP

#include <map>
#include "bits.hpp"
#include <cmath>
#include "colour.hpp"

/**
 * \brief Measurement pattern node
 * 
 * This type of node holds measurement pattern information. Each node holds
 * information about how it should be measured, and what to do with the
 * measurement outcome. It does not store the measurement outcome itself.
 * This type of node stores information that acts like a program for the
 * simulator.
 *
 * The information that is stored contains two parts. The first part, 
 * relating to the measurement of a qubit, stores
 *
 * - What measurement basis to use? (Z-measurement or XY-plane)
 * - How the adaptive measurement setting (basis angle sign) should be calculated
 *    * which other node's outcomes are used in the calculation
 *    * how to use stored byproduct operators in the calculation
 *
 * The second part relates to what to do with the measurement outcome.
 * It includes
 *
 * - Which byproduct operators must be updated using this measurement outcome?
 *
 * Even though adaptive measurement settings may depend on this node's 
 * measurement outcome, this information is not stored in this node. Instead,
 * nodes which use this node's outcome refer to this node as the source of
 * an adaptive measurement setting calculation.
 *
 * In addition, a node may contain the instruction to store the byproduct
 * operators. This is necessary at the beginning of a one-qubit gate boundary,
 * because some adaptive measurement settings depend on stored byproduct 
 * operators.
 * 
 * Implementation note: at this point, only the functionality for the identity
 * gate is implemented. This means that no adaptive measurement settings are
 * required, which simplifies the class considerably.
 */
template<typename Previous>
class PatternNode : public Previous
{
    char label{'Z'}; ///< Label for use in marker shape for printing

    /**
     * \brief Flag to indicate whether the node stores non-default settings
     *
     * This flag is set when the byproduct operator update is set, because
     * this option uniquely defines whether a node has non-default settings.
     * (All cluster qubits that are on the path or adjacent to the path
     * must update the byproduct operators.)
     *
     * \todo Double check the validity of this reasoning above
     */
    bool is_default = true;

    /**
     * \brief Which measurement basis to use
     *
     * False for Z-measurement, true for XY-measurement, other values undefined.  
     *
     */
    bool xy_basis = false;

    /// The measurement basis angle, if XY-measurement
    double angle = 0;
    
    /**
     * \brief How to calculate the adaptive measurement setting
     *
     * This vector stores the indices of nodes whose outcome
     * should be xor-ed together to produce the adaptive measurement
     * setting.
     */
    std::vector<std::size_t> adapt_nodes{};

    /**
     * \brief Stored byproduct contribution to adaptive measurement setting
     *
     * This pair stores two flags in the format (x,z) that determine whether
     * the stored byproduct operators should be xor-ed into the adaptive
     * measurement setting.
     */
    std::pair<unsigned,unsigned> adapt_stored_byp{0,0};

    /**
     * \brief What to do with the measurement outcome
     *
     * This stores a map from path indices to byproduct operator update rules,
     * in the format (x,z). The key stores the path index of the node which
     * is affected by the byproduct operators update.
     * 
     * OLD COMMENTS:
     * 
     * This pair stores two flags in the format (x,z) that determines whether
     * the measurement outcome of this qubit will be added to the byproduct
     * operators of the current logical qubit 
     *
     * Note: I figured out this is not the actual byproduct operators that
     * must be used for the logical correction -- there is a slight subtlety. 
     * If the byproduct operators for a cluster qubit proceeded by an _even_
     * path length is desired, then these byproduct operators can be used
     * directly. If the cluster qubit is proceeded by an _odd_ path length, 
     * the the byproduct operators (x,z) for the even component (the same 
     * path excluding the penultimate cluster qubit) are swapped to form
     * (z,x), and then the measurement outcome from the penultimate cluster
     * qubit is added to x.  
     * 
     * \todo Currently this only supports one-qubit gates. In more complicated
     * scenarios, it is necessary to modify byproduct operators of many logical
     * qubits.
     */
    std::map<std::size_t, Bits> byp_update{};

    /**
     * \brief The index of the pattern node along the path
     *
     * This is used to establish whether the path length is odd or
     * even for the purpose of computing the byproduct operators
     */
    int path_index = -1;     

protected:
    using Previous::index_;
    using Previous::x_;
    using Previous::y_;
    
public:

    bool operator == (const PatternNode &) const & = default;
    
    /**
     * \brief Construct a default measurement pattern node
     *
     * The default measurement pattern is instructions to perform a
     * computational basis measurement (Z-measurement), and to discard
     * the outcome (do not update any byproduct operators). This 
     * operation would cut out a cluster qubit and discard the associated
     * kappa values. It is the desired measurement instructions for any
     * qubit in the cluster state which is not on, or adjacent to, a
     * logical qubit path. 
     */
    using Previous::Previous;

    /// Marker colour for printing
    std::string markerColour() const
	{
	    if (path_index not_eq -1) {
	    	return Colour::GREEN + Colour::BOLD;
	    }
	    if (byp_update.size() > 0) {
		return Colour::RED + Colour::BOLD;
	    }
	    return "";
	}
    
    std::string markerShape() const
    	{
	    if (not xy_basis) {
		return "Z";
	    } else {
		return std::string(1,label);
	    }
	}

    
    
    /// Set the pattern index
    void setPatternIndex(std::size_t path_index_in)
	{
	    path_index = path_index_in;
	}

    /// Get the index of the pattern qubit along the path (if it is on a path)
    std::size_t getPatternIndex() const
	{
	    if (path_index == -1) {
		throw std::logic_error("Cannot get index of pattern qubit");
	    } else {
		return static_cast<std::size_t>(path_index);
	    }
	}
    
    /// Set the basis to XY with angle theta from the X axis
    void setXY(double theta)
	{
	    xy_basis = true;
	    angle = theta;
	}

    
    /// X basis does not have dependencies
    void setX()
	{
	    setXY(0);
	    label = 'X';
	}

    /// Y basis does not have dependencies
    void setY()
	{
	    setXY(M_PI/2);
	    label = 'Y';
	}

    /// Set computational basis measurement
    void setZBasis()
	{
	    xy_basis = false;
	}

    /// Check whether the measurement is in the XY basis (returns true)
    bool isXYBasis() const {
	return xy_basis;
    }

    double getAngle() const {
	if (not xy_basis) {
	    throw std::logic_error("You can't get an angle for a Z-measurement");
	}
	return angle;
    }
    
    /** 
     * \brief Add a key value pair to the byproduct operator update rules
     *
     */
    void pushBypUpdate(const Bits & byp_update_in, std::size_t path_index)
	{
	    if (byp_update.count(path_index) == 1) {
	    	throw std::logic_error("Error: path_index already exits "
				       "for update rule in call to "
				       "pushBypUpdate");
	    }
	    is_default = false;
	    byp_update[path_index] = byp_update_in;
	}

    void pushBypUpdate(const Bits & byp_update_in)
	{
	    if (path_index == -1) {
		throw std::logic_error("Cannot add byproduct rules to "
				       "for this node because it does not "
				       "have a path index");
	    }
	    pushBypUpdate(byp_update_in, path_index);
	}

    void clearBypUpdates()
	{
	    byp_update.clear();
	}
    
    /**
     * \brief Update the byproduct operator update variable
     *
     * This function updates the byproduct operator mask by xor-ing the
     * values of the argument with the internal byp_update variable. 
     *
     * Note: this function is incorrect (nodes do not know which byproduct
     * operators they should update. Commenting out to see what uses it.
     * 
     */
    // void bypUpdate(const Bits & byp_update_in, std::size_t path_index)
    // 	{
    // 	    is_default = false;
    // 	    byp_update ^= byp_update_in;
    // 	}

    /**
     * \brief Get the byproduct operator assocated with this path node
     *
     * This function should be called to get the byproduct operator rules
     * for the current path node. If the current node is not a path node,
     * then an exception is thrown.
     */
    Bits getPathBypUpdate() const
	{
	    if (path_index == -1) {
		throw std::runtime_error("Cannot get path byproduct operator "
					 "because this node is not a path node");
	    }

	    return byp_update.at(path_index);
	}

    /**
     * \brief Call this function to get all the byproduct rules for this node
     *
     */
    std::map<std::size_t, Bits> getBypUpdates() const
	{
	    return byp_update;
	}
    

    /// Chek whether the node has default settings
    bool isDefault() const {
	return is_default;
    }
    
    /// Print the node
    void print(std::ostream & os = std::cout) const
	{
	    Previous::print(os);

	    os << "  (patnode:b=";
	    if (xy_basis) {
		os << "XY(" << angle << "),";
	    } else {
		os << "Z,";
	    }
	    os << "byp={";
	    for (const auto & [index, byp] : byp_update) {
		os << "(" << index << ":";
		if (byp(1) == 1) {
		    os << "x";
		} 
		if (byp(0) == 1) {
		    os << "z";
		}
	    
		os << "),";
	    }
	    os << "},idx=" << path_index;
	    os << ")" << std::endl;
	}
};

#endif
