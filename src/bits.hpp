/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file bits.hpp
 * \brief A class for handling bitstrings
 *
 */

#ifndef BITS_HPP
#define BITS_HPP

#include <vector>
#include <iostream>
#include <algorithm>

/**
 * \brief Class for handling bitstrings
 *
 * This class contains methods for setting and accessing bits, 
 * bitwise arithmetic, etc.  
 */
class Bits
{
    std::vector<unsigned> bits{}; ///< Contains the bits, indexed by bit position
public:

    /**
     * \brief Default constructor, initialise to empty bitstring
     */
    Bits() = default;
    
    /**
     * \brief Construct a Bits object
     *
     * This constructor takes any integer type and stores its
     * bits in a new Bits object.
     *
     */
    Bits(std::integral auto value, std::size_t N)
	{
	    for (std::size_t n = 0; n < N; n++) {
		bits.push_back(1 & (value >> n));
	    }
	}


    /**
     * \brief Constructor for list of bits
     *
     * This constructor is the brace-enclosed initialiser list constructor.
     * Use it like Bits{0,1,1,0}. Be careful that Bits(0,1) is the constructor
     * above, whereas Bits{0,1} is this constructor.
     *
     * The argument {1,0,1,1} makes the bistring 0b1011 ( = 11).
     *
     * Note: there is no checking here, make sure you pass zeros and ones!
     */
    explicit Bits(const std::initializer_list<unsigned> & bits_in)
	: bits{bits_in}
	{
	    if (std::ranges::any_of(bits, [](unsigned bit) {return bit > 1;})) {
		throw std::out_of_range("Invalid value of bit used in "
					"initialiser list constructor of "
					"Bits object.");
	    }
	    // for (const auto & bit : bits) {
	    // 	if (bit > 1) {
	    // }

	    // Need to reverse the input so that the user can specify
	    // the bits in most-significant to least-significant order.
	    std::ranges::reverse(bits);
	}

    
    /// Return the length of the bitstring
    std::size_t size() const;
    
    /**
     * \brief Assign a value to the object from a number
     */
    void set(std::integral auto value, std::size_t N) {
	bits.resize(N);
	for (std::size_t n = 0; n < N; n++) {
	    bits[n] = 1 & (value >> n);
	}
    }

    /**
     * \brief Add a bit in the most significant position
     */
    void pushBit(unsigned bit);
    
    /**
     * \brief Print the object
     *
     * This function prints the bits to the ostream passed as an argument,
     * with most significant bits to the left (i.e. the normal way round).
     */
    void print(std::ostream & os) const;
    
    /**
     * \brief Get the value of the binary string as an integer
     *
     * \todo Fix the potential bug where unsigned is not big enough
     */
    unsigned val() const;
    
    /**
     * \brief Access the nth bit in the object
     *
     * This function returns a reference, so the result can
     * be modified. 
     */
    unsigned & operator () (unsigned n);
    
    /**
     * \brief Return the nth bit read only
     */
    unsigned operator () (unsigned n) const;

    /**
     * \brief The &= operator
     */
    Bits & operator &= (const Bits & rhs);
    
    /**
     * \brief The ^= operator
     *
     * Used for adding together two Bits objects bitwise modulo-2
     */
    Bits & operator ^= (const Bits & rhs);
    
    /**
     * \brief The ^= operator with literal
     */
    Bits & operator ^= (std::integral auto rhs)
	{
	    const std::size_t N = bits.size();
	    for (std::size_t n = 0; n < N; n++) {
		bits[n] ^= 1 & (rhs >> n);
	    }
	    return * this;
	}

    /**
     * \brief Return the result of xor-ing all the bits together
     */
    unsigned xorBits() const;
    
    /**
     * \brief Return a substring from the bistring
     *
     */
    Bits operator () (unsigned end, unsigned start);
    
};


/**
 * \brief Concaternate two Bits objects
 *
 * This function concaternates two bitstrings a and b into a
 * bitstring ab, where a is more significant than b in the
 * resulting bitstring. The operator + is used because addition 
 * is not defined for this bitstring object.
 * 
 */
Bits operator + (const Bits & a, const Bits & b);

/**
 * \brief Xor operator (addition modulo 2)
 *
 * Perform bitwise AND between two bitstrings
 */
Bits operator & (const Bits & a, const Bits & b);

/**
 * \brief Xor operator (addition modulo 2)
 *
 * Perform bitwise addition modulo two between two Bits objects
 */
Bits operator ^ (const Bits & a, const Bits & b);

/// Xor operator with literal
Bits operator ^ (const Bits & a, std::integral auto b);

/// Xor operator with literal the other way round
Bits operator ^ (std::integral auto a, const Bits & b);

/**
 * \brief Print Bits object to ostream
 */
std::ostream & operator << (std::ostream & os, const Bits & bits);

#endif
