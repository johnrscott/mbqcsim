/**
 * \file meas-patterns.hpp
 * \brief Standard measurement patterns
 *
 */

#ifndef MEAS_PATTERNS_HPP
#define MEAS_PATTERNS_HPP

#include "pattern-node-2.hpp"
#include "qubit-node.hpp"

using MeasPatternNode = MakeNode<PatternNode2, QubitNode>;

/**
 * \brief Create NodeWindow for an arbitrary straight identity pattern 
 *
 * \param height The height of the pattern (less than 14)
 * \param width The width of the pattern (arbitrary odd)
 * \param qubit_pos The position of the qubit row (y-coordinate)
 */
BaseNodeWindow<MeasPatternNode>
identityPattern(std::size_t height, std::size_t width, std::size_t qubit_pos)
{
    if (height > 14) {
	throw std::out_of_range("Cannot create identity pattern with height "
				"greater than 14 qubits (memory limit)");
    }

    if (width % 2 not_eq 1) {
	throw std::logic_error("Cannot create identity pattern unless width "
			       "is an odd number");
    }

    if (qubit_pos > height - 1) {
	throw std::out_of_range("Cannot create identity pattern because "
				"logical qubit position is out of range");
    }
    
    BaseNodeWindow<MeasPatternNode> win{height,width};
    setAllEdges(win,true);

    const std::size_t logical_qubit{0}; // Just an index
    
    // Set logical qubit row X measurements
    win(0,qubit_pos).setAsInput();
    for (std::size_t x{0}; x < width-1; x++) {
	win(x,qubit_pos).setX();
	auto x_byp{static_cast<unsigned>(x%2)};
	win(x,qubit_pos).setBypUpdate(logical_qubit, Bits{x_byp, x_byp^1});
    }
    win(width-1,qubit_pos).setAsOutput(logical_qubit);

    // Set byproduct operators above and below
    for (std::size_t x{0}; x < width; x++) {
	auto x_byp{static_cast<unsigned>(x%2)};	
	if (qubit_pos > 0) {
	    win(x,qubit_pos-1).setBypUpdate(logical_qubit, Bits{x_byp, x_byp^1});
	}
	if (qubit_pos < height-1) {
	    win(x,qubit_pos+1).setBypUpdate(logical_qubit, Bits{x_byp, x_byp^1});
	}
    }
    
    return win;
}

#endif
