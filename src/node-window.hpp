/**
 * \file node-window.hpp
 * \brief Class for accessing columns of nodes
 *
 */

#ifndef NODE_WINDOW_HPP
#define NODE_WINDOW_HPP

#include "base-node.hpp"
#include "custom-vector.hpp"
#include "distance-node.hpp"
#include "colour.hpp"
#include <random>
#include "make-node.hpp"
#include "coord-arith.hpp"
#include <sstream>
#include "random.hpp"
#include "seed.hpp"
#include "small-vector.hpp"

// template<typename T>
// concept typename = requires (T x)
// {
//     x.marker();
// };

/**
 * \brief Window into a 2D array of nodes
 *
 * The object of this class is to provide read-only or read-write access
 * to a subset of nodes stored as a column-major vector. One version of 
 * the class (the BaseNodeWindow), owns the full 2D window. SubNodeWindows
 * can be obtained from this object for the purpose of accessing ranges
 * of columns.
 *
 * This class expects to hold any type of node which is constructible using
 * the BaseNode constructor (i.e. BaseNode(index,height)). Other than 
 * that, the class does not assume any Node semantics. For example, it
 * is not possible to set a path successor using this class (which would
 * assume PathNode). The semantics this class supports are:
 *
 * - Adding columns of nodes on the right
 * - Obtaining read/write access to nodes in the array
 * - Obtaining subwindows of nodes
 * - Adding/removing/reading edges between nodes
 *
 * Implemtation notes:
 *
 * Horizontal and vertical edges are stored as std::vector<bool>. This
 * may be implemented using packed bits, as a space saving exercise.
 * Be aware that constructs like &h_edges[0]+3 may not be valid. 
 * 
 *
 */
template<NodeType Node, bool Base>
class NodeWindow
{
    std::size_t H{0}; ///< Height
    std::size_t W{0}; ///< Width

    /// The relative position of the zero column
    long int x_zero{0};
    
    /**
     * \brief The (absolute) column which is taken as x=0 
     *
     * This is necessary to convert the absolute coordinates stored in the
     * nodes to relative coordinates for accessing e.g. h_edges.
     */
    std::size_t x_0{0};

    /**
     * \brief The index of the zero node at (0,0)
     *
     * In NodeWindows where the zero column is equal to the leftmost column,
     * the index corresponding to (x,y) is given by H*x+y. When the x
     * coordinate is shifted as a result of a non-trivial zero column, the
     * index corresponding to (x,y) is given by H*x+y+n_zero. The correction is
     * given by n_zero = H*(x_zero-x_begin). It is better to calculate this up
     * front because all the parameters are known at construction time, and
     * the calculation is required any time an index is calculated from
     * coordinates.
     *
     * Note: it is important to use x_zero in the above expression (the logical
     * zero column) rather than x_0 which is the absolute zero column.
     *
     * \todo This might be an unused variable
     *
     */
    std::size_t n_zero{0};


    /**
     * \brief The absolute index of the node whose logical index is zero
     *
     * This variable is used to transform absolute indices stored in nodes into
     * relative indices for use with the getNode(index) function. If n is the
     * absolute index stored in a node, then the node is located at n - n_base
     * in this subwindow. Since both numbers are unsigned, this will wrap
     * if n < n_base. The value is initialised by recording the index of the
     * base node after the class is constructed.
     * 
     */
    std::size_t n_base{0};
    
    /**
     * \brief Column major vector of nodes
     * 
     * In a column, there are H nodes. The node at coordinate (x,y) is 
     * located at index H*x+y in this vector. 
     */
    Vector<Node,Base> nodes{};

    /** 
     * \brief Vector of horizontal edges
     *
     * In an array of nodes containing more than one column, there can
     * be horizontal edges between nodes. There are H horizontal edges
     * per column. The horizontal edge to the right of node (x,y) is
     * located at index H*x+y. The formula is valid provided there are
     * at least two columns.
     * 
     * The size of h_edges is H*(W-1).
     * 
     */
    Vector<char,Base> h_edges{};


    /**
     * \brief Vector of vertical edges
     *
     * In an array containing more than one row, there can be vertical
     * edges between nodes. There are (H-1) vertical edges per column. 
     * The vertical edge above the node (x,y) is located at (H-1)*x+y. 
     * 
     * \todo It may be worth considering padding this vector with an empty
     * location to bring the indices into the same form as nodes and h_edges.
     * This is probably for a later rendition of the code.
     *
     */
    Vector<char,Base> v_edges{};

    /// Allow access between Base = true/false class types 
    friend class NodeWindow<Node, not Base>;
    
    /**
     * \brief Check whether a rectangle specified by coordinates is valid
     *
     * The function performs checks that the bottom_left and top_right
     * coordinates specifying a rectangle are valid (i.e. not the wrong
     * way round) 
     */
    void
    checkRectangle(const std::pair<long int, long int> & bottom_left,
		   const std::pair<long int, long int> & top_right) const
	{	    
	    // Coordinates for ease of use
	    const long int xbl{bottom_left.first};
	    const long int ybl{bottom_left.second};
	    const long int xtr{top_right.first};
	    const long int ytr{top_right.second};
	    
	    // Check bottom-left and top-right are the right way round
	    if (xbl > xtr) {
		throw std::logic_error("Bottom-left x-coordinate must be "
				       " <= top-right x-coordinate");
	    }
	    if (ybl > ytr) {
		throw std::logic_error("Bottom-left y-coordinate must be "
				       " <= top-right y-coordinate");
	    }
	}

    /**
     * \brief Check column indices
     *
     * Throws logic_error if begin > end. Throws std::out_of_range if the
     * implied range of columns is out of range for this node object.
     *
     * There is no restriction on the zero index, which is just used to shift
     * the logical zero point of the column indices.
     *
     * Make sure to always call this function on an object that has finished
     * constructing (i.e. in the initialiser you probably want to call 
     * other.checkIndices(...) because this function uses the class member
     * W.
     *
     * \return The width of the range given by (end - begin). The function is
     * therefore usable in constructor initialiser lists for calculating the
     * width while checking argument validity.
     */
    std::size_t checkIndices(long int begin, long int end) const
	{
	    // Check begin and end are the right way round
	    if (begin > end) {
		throw std::logic_error("begin index must be <= end index");
	    }

	    // Check begin index
	    ///\todo Generalise to correct limit
	    if (begin + x_zero < 0) {
		throw std::out_of_range("begin index is out of range "
					"(window does not extend that "
					"far to the left)");
	    }

	    // Check end index
	    ///\todo Generalise to correct limit
	    if (end + x_zero > static_cast<long int>(W)) {
		throw std::out_of_range("end index is out of range "
					"(window does not extend that "
					"far to the right). Window "
					"width = " + std::to_string(W) + " "
					"and end = " + std::to_string(end) + ".");
	    }

	    return end - begin; // width
	}


    /**
     * \brief Convert coordinates to a node index
     *
     * Throws std::out_of_range if the coordinates are out of range for
     * the node window.
     *
     */
    std::pair<std::size_t,std::size_t> logicalToReal(long int x, long int y) const
	{
	    // Check x-coordinate is in range
	    const long int x_true{x + x_zero};
	    if (x_true < 0) {
		throw std::out_of_range("x-coordinate out of range. Window "
					"does not extend that far to the left. "
					"Real x = " + std::to_string(x_true) + " "
					"and x_zero = " + std::to_string(x_zero));
	    }

	    if (x_true > static_cast<long int>(W)-1) {
		throw std::out_of_range("x-coordinate out of range. Window "
				       "does not extend that far to the right. "
					"Real x = " + std::to_string(x_true) + " "
					"but width = " + std::to_string(W));
	    }
	    
	    // Check the y-coordinate is in range
	    if (y < 0) {
		throw std::out_of_range("y-coordinate out of range. Window "
					"does not extend that far downwards");
	    } else if (y > static_cast<long int>(H)-1) {
		throw std::out_of_range("y-coordinate out of range. Window "
					"does not extend that far upwards");
	    }
		
	    return {x_true, y};
	}

    /// Convert logical coordinates to a real index
    std::size_t logicalToIndex(long int x, long int y) const
	{
	    auto real{logicalToReal(x,y)};
	    return H*real.first + real.second;
	}
    
public:
    NodeWindow(std::size_t height) : H{height} {}
    NodeWindow(std::size_t height, std::size_t width)
	: H{height}, W{width}
	{
	    // Add the nodes
	    for (std::size_t n = 0; n < H*W; n++) {
		nodes.emplace_back(n,H);
	    }

	    // Add the horizontal edges for the previous row
	    for (std::size_t x = 1; x < W; x++) {
		for (std::size_t y = 0; y < H; y++) {
		    h_edges.push_back(false);
		}
	    }

	    // Add the vertical edges for each column
	    for (std::size_t x = 0; x < W; x++) {
		// Note the H=0 case
		for (int y = 0; y < static_cast<int>(H)-1; y++) {
		    v_edges.push_back(false);
		}
	    }
	}

    /** 
     * \brief Check the column index of a node
     *
     * \todo The presence of this function is a design issue, relating to
     * the lack of a proxy object to act as a node with the correct coordinates
     * for subwindows. This functionality is needed in a number of different
     * situations (for example, in the global breadth first search, where
     * indices are required to store in the queue, but the indices are not
     * directly available from the nodes).
     */
    std::size_t getColumnIndex(const Node & node) const {
	return node.x() - x_0; ///\todo Check this. Test needed.
    }

    /**
     * \brief Construct a NodeWindow<Node,false> from another NodeWindow
     * 
     * Make a SubNodeWindow from a subset of the columns of a NodeWindow. The
     * first column of the new NodeWindow is begin, the last column is
     * end-1, and the column treated as zero index is specified using the
     * argument zero. The zero index is expressed in terms of the coordinates
     * of the original NodeWindow, the same as the other arguments.
     *
     * Implementation notes:
     *
     * The column indices are checked using the checkIndices function, which
     * returns the resulting width. This means it can be used in the initialiser
     * list where it can throw an exception before any attempted Vector
     * operations.
     *  
     */
    template<bool B>
    NodeWindow(const NodeWindow<Node,B> & other,
	       long int begin, long int end, long int zero)
	: H{other.H}, W{other.checkIndices(begin, end)},
	  x_zero{zero-begin}, x_0{other.x_0 + zero}, n_zero{H*x_zero},
	  nodes{other.nodes.getSub(H*begin, H*end)},
	  ///\todo end == 0 is a bug. Maybe end == 1 too.
	  h_edges{other.h_edges.getSub(H*begin, H*(end-1))},
	  v_edges{other.v_edges.getSub((H-1)*begin, (H-1)*end)}
	{
	    if (W > 0 and H > 0) {
		n_base = nodes[0].index();
	    }
	    static_assert(not Base,
			  "The constructor Vector(Vector, "
			  "std::size_t, std::size_t) is not "
			  "available for Base == true Vectors");
	}

    std::size_t height() const { return H; }
    std::size_t width() const { return W; }
    std::size_t size() const { return nodes.size(); }

    /**
     * \brief Print window information for debugging purposes
     *
     */
    void debugPrint() const
	{
	    std::cout << "nodes size: " << nodes.size() << std::endl;
	    std::cout << "v_edges size: " << v_edges.size() << std::endl;
	    std::cout << "h_edges size: " << h_edges.size() << std::endl;
	}

    /// Debugging consistency check
    void checkConsistency(std::ostream & os = std::cout) const
	{
	    os << "W = " << W << std::endl;
	    os << "H = " << H << std::endl;
	    os << "nodes size = " << nodes.size() << std::endl;
	    os << "h_edges size = " << h_edges.size() << std::endl;
	    os << "v_edges size = " << v_edges.size() << std::endl;

	    // If W = 0 or 1, the there are no horizontal edges. Else,
	    // there are (W-1)*H horizontal edges
	    std::size_t h_edges_size{0};
	    if (W > 1) {
		h_edges_size = (W-1)*H;
	    }
	    if (h_edges.size() not_eq h_edges_size) {
		os << "Correct h_edges size = " << h_edges_size << std::endl;
		throw std::logic_error("Consistency failure: h_edges size");
	    }

	    // If H = 0 or 1, then there are no vertical edges. Else,
	    // there are W*(H-1) vertical edges
	    std::size_t v_edges_size{0};
	    if (H > 1) {
		v_edges_size = W*(H-1);
	    }	    
	    if (v_edges.size() not_eq W*(H-1)) {
		os << "Correct v_edges size = " << v_edges_size << std::endl;
		throw std::logic_error("Consistency failure: v_edges size");
	    }


	    if (nodes.size() not_eq W*H) {
		throw std::logic_error("Consistency failure: nodes size");
	    }
	}
    
    /** 
     * \brief Get a subwindow holding a 2D sub-rectangle of nodes
     *
     */
    NodeWindow<Node,false> sub(long int begin, long int end, long int zero) const
	{
	    return NodeWindow<Node,false>{*this, begin, end, zero};
	}
    
    /**
     * \brief Set a particular edge between qubit indices to state (0 or 1)
     *
     * Add a particular edge between two nodes in the graph. This function
     * should only be used for debugging purposes because of the number of 
     * checks it does on the validity of the node coordinates.
     *
     */
    void setEdge(const std::pair<std::size_t,std::size_t> & xy1,
		 const std::pair<std::size_t,std::size_t> & xy2,
		 bool state)
	{
	    // Ease-of-use coordinates
	    const std::size_t x1 = xy1.first;
	    const std::size_t y1 = xy1.second;
	    const std::size_t x2 = xy2.first;
	    const std::size_t y2 = xy2.second;

	    // Check that the coordinates are in range
	    if (x1 > W-1 or x2 > W-1 or y1 > H-1 or y2 > H-1) {
	    	throw std::out_of_range("Node coordinates are out of range");
	    }
	    
	    // Check the nodes are next to each other
	    if (x1 - x2 + 1 > 2 or y1 - y2 + 1 > 2) {
	    	throw std::logic_error("Cannot add edge between "
	    			       "non-adjacent nodes");
	    }

	    // Make sure edge is not diagonal
	    if (x1 != x2 and y1 != y2) {
		throw std::logic_error("Cannot add diagonal edge");
	    }

	    // Make sure nodes are different
	    if (xy1 == xy2) {
		throw std::logic_error("Nodes must be different");
	    }
	    
	    // Add a horizontal edge
	    if (y1 == y2) {
		h_edges[H*std::min(x1,x2) + y1] = state;
	    }

	    // Add a vertical edge
	    if (x1 == x2) {
		v_edges[(H-1)*x1 + std::min(y1,y2)] = state;
	    }

	}
    
    /**
     * \brief Get the edge to the left of a node by index
     *
     * This function throws std::out_of_range for an invalid index (pointing
     * to a non-existent node. Asking for the left edge of a node in the
     * left-most column is not an error -- the function returns false (to
     * indicate there is no edge).
     */
    bool leftEdge(std::size_t index) const
	{
	    const Node & node{getNode(index)};
	    // Check for first column -- in this special case,
	    // return false because there are no edges to the left
	    if (index < H) {
		return false;
	    }
	    return h_edges[H*(node.x()-x_0+x_zero-1) + node.y()];
	}

    /// Coordinate version
    bool leftEdge(long int x, long int y) const
	{
	    auto xy{logicalToReal(x,y)}; // Throws on coordinate errors

	    // Check for first column
	    if (xy.first == 0) {
		return false;
	    }
		
 	    return h_edges[H*(xy.first-1) + xy.second];
	}
    
    /**
     * \brief Get the edge to the right of a node by index
     *
     * This function throws std::out_of_range for an invalid index (pointing
     * to a non-existent node. Asking for the right edge of a node in the
     * right-most column is not an error -- the function returns false (to
     * indicate there is no edge).
     */
    bool rightEdge(std::size_t index) const
	{
	    const Node & node{getNode(index)}; // This might throw

	    // Check for last column -- in this special case,
	    // return false because there are no edges to the left
	    if (index >= size() - H) {
		return false;
	    }

	    return h_edges[H*(node.x()-x_0+x_zero) + node.y()];
	}

    /// Coordinate version
    bool rightEdge(long int x, long int y) const
	{
	    auto xy{logicalToReal(x,y)}; // Throws on coordinate errors

	    // Check for last column
	    if (xy.first == W-1) {
		return false;
	    }
		
 	    return h_edges[H*xy.first + xy.second];
	}

    
    /**
     * \brief Get the edge above a node by index
     *
     * This function throws std::out_of_range for an invalid index (pointing
     * to a non-existent node. Asking for the above-edge of a node in the
     * top row is not an error -- the function returns false (to indicate 
     * there is no edge).
     */
    bool aboveEdge(std::size_t index) const
	{
	    const Node & node{getNode(index)};

	    // Check for top row
	    ///\todo There is an issue with this expression for H==0
	    if (node.y() == H-1) {
		return false;
	    }

	    return v_edges[(H-1)*(node.x()-x_0+x_zero) + node.y()];
	}

    /// Coordinate version
    bool aboveEdge(long int x, long int y) const
	{
	    auto xy{logicalToReal(x,y)}; // Throws on coordinate errors

	    // Check for top row
	    if (xy.second == H-1) {
		return false;
	    }

 	    return v_edges[(H-1)*xy.first + xy.second];
	}
    
    /**
     * \brief Get the edge below a node by index
     *
     * This function throws std::out_of_range for an invalid index (pointing
     * to a non-existent node. Asking for the below-edge of a node in the
     * bottom row is not an error -- the function returns false (to indicate 
     * there is no edge).
     */
    bool belowEdge(std::size_t index) const
	{
	    const Node & node{getNode(index)};

	    // Check for bottom row
	    if (node.y() == 0) {
		return false;
	    }

	    return v_edges[(H-1)*(node.x()-x_0+x_zero) + (node.y()-1)];
	}

    /// Coordinate version
    bool belowEdge(long int x, long int y) const
	{
	    auto xy{logicalToReal(x,y)}; // Throws on coordinate errors

	    // Check for bottom row
	    if (xy.second == 0) {
		return false;
	    }
		
 	    return v_edges[(H-1)*xy.first + (xy.second-1)];
	}

    /** 
     * \brief Get references to the neighbours of a node
     *
     * Return the indices of the neighbours of the current node. This
     * is an important subroutine in the breadth-first search, which
     * iterates through all the neighbours of every node assigning a
     * distance according to which nodes appeared first.
     *
     * The nodes are referenced as indices into the active window,
     * which is not the same as the the index variable in the node
     * (that is the index in the global cluster). 
     *
     * \todo The order in which the nodes are returned in important 
     * for the breadth-first search algorithm -- different orders may 
     * lead to different shortest paths. At the moment, the order is
     * arbitrary, but later on it may be important to fix it in some
     * way (e.g. right-most node first).
     *
     * \todo This is a confusing interface. More than once I have thought
     * the vector is offsets, rather than indices. This is all part of the
     * general lack-of-proxy-objects-for-nodes problem. 
     *
     * \todo This can be optimised, because at the moment the construction
     * of a standard vector is quite time consuming (see a kcachegrind
     * breakdown for proof). A better design would be to just return e.g.
     * a struct with four elements for each neighbour, but this interface
     * change would need to be fixed everywhere in the code. Todo later.
     */
    SmallVector<std::size_t> neighbours(std::size_t index) const
	{
	    // Make an empty vector
	    SmallVector<std::size_t> neighbours;

	    // Check for right neighbours
	    if (rightEdge(index)) {
		neighbours.push_back(index+H);
	    }

	    // Check for left neighour
	    if (leftEdge(index)) {
		neighbours.push_back(index-H);
	    }

	    // Check for neighour above
	    if (aboveEdge(index)) {
		neighbours.push_back(index+1);
	    }

	    // Check for neighour below
	    if (belowEdge(index)) {
		neighbours.push_back(index-1);
	    }

	    return neighbours;
	}

    auto neighbours(const Node & node) const
	{
	    return neighbours(node.index() - n_base);
	}
    
    /**
     * \brief Get the neighbours of a node specified using coordinates
     *
     * The node is specified using logical (x,y) coordinates. The result
     * is a std::vector of indices of nodes which are neighbours of the
     * node at (x,y). The node may then be retrieved using getNode(index). 
     *
     */
    auto neighbours(long int x, long int y) const
	{
	    return neighbours(logicalToIndex(x,y));
	}
    
    /**
     * \brief Get the node at (x,y) coordinates
     * \todo Maybe change to at()
     */
    const Node & getNode(long int x, long int y) const
	{
	    return nodes[logicalToIndex(x,y)];
	}

    /// "Nobody ever got fired for following Scott Meyers" Stackoverflow
    Node & getNode(long int x, long int y)
	{
	    return const_cast<Node &>(
		static_cast<const NodeWindow &>(*this).getNode(x,y));
	}
    
    /**
     * \brief Get the node at a known index
     * \todo Maybe change to at() for standard container consistency (throws)
     */
    const Node & getNode(std::size_t index) const
	{
	    if (index >= nodes.size()) {
		throw std::out_of_range("Index of node is out of range. "
					"Nodes size() = "
					+ std::to_string(nodes.size()) + " "
					"and index = " + std::to_string(index) +
					".");
	    }
	    return nodes[index];
	}

    Node & getNode(std::size_t index)
	{
	    return const_cast<Node &>(
		static_cast<const NodeWindow &>(*this).getNode(index));
	}

    /**
     * \brief Get node by offset from a base node
     *
     * The purpose of this function is to obtain nodes based on an offset,
     * which is the method nodes use to store references to other nodes.
     * This function takes care of the processing required to correct
     * for the logical index vs. the real index of the base node.
     *
     * Implementation notes:
     *
     * The presence of this function is really a design failure. The purpose
     * of storing offsets in nodes was supposed to allow generic code to
     * work directly with nodes' indices. However, this really needs the
     * address-of operator to work properly on nodes, and return a pointer-like
     * object that can have an offset added to it. Such code would make this
     * kind of constuction valid: &node + offset. (This would work for a
     * pointer into a std::vector.) However, the & operator would need to know
     * about the position of the subwindow, so it can't be a method of the
     * Node object itself. I haven't figured out how to make this kind of
     * construction work in a good way.  
     * 
     * \todo CompatNode is supposed to be any node that is compatible with
     * Node (i.e. contains basic node properties), however, it might be
     * the same type. Write a concept for this.
     */
    template<NodeType CompatNode>
    Node & getNode(const CompatNode & base, long int offset)
	{
	    ///\todo Check the casting here
	    return getNode(base.index() - n_base + offset);
	}
    
    ///\todo Remove exception checking eventually
    Node & operator[] (std::size_t index) {
	return getNode(index);
    }

    ///\todo Remove exception checking eventually
    Node & operator() (long int x, long int y) {
	return getNode(x,y);
    }

    void addColumn()
	{
	    static_assert(Base, "Cannot addColumn() to a SubNodeWindow");

	    // Add nodes
	    for (std::size_t n = 0; n < H; n++) {
		nodes.emplace_back(nodes.size(),H);
	    }

	    // Add horizontal edges for the previous column
	    if (W > 0) {
		for (std::size_t y = 0; y < H; y++) {
		    h_edges.push_back(false);
		}
	    }

	    // Add vertical edges for the current column
	    ///\todo Check the H == 0 case
	    for (std::size_t y = 0; y < H-1; y++) {
		v_edges.push_back(false);
	    }	    
	    
	    W++;
	}

    /**
     * \brief Print the full window
     */
    template<template<typename> typename N = DefaultNode>
    void print(std::ostream & os = std::cout) const
	{
	    print<N>(0, W, os);
	}


    /**
     * \brief Print a subset of columns of the window
     */
    template<template<typename> typename N = DefaultNode>
    void print(long int begin, long int end, std::ostream & os = std::cout) const
	{
	    print<N>({begin-x_zero,0}, {end-x_zero,H}, os);
	}
    
    /** 
     * \brief Print out the node window
     *
     * Use this function to print a graphical depiction of the node window
     * to an output stream (default std::cout). The arguments provide the
     * starting column to print, and the width to print. The function will
     * throw a std::out_of_range if the input starting column of the 
     * requested window width result in nodes that are out of range.
     * 
     * The bottom-left coordinates act as the starting coordinates for the
     * printed range. The top-right coordinates act as the first node not-
     * included (i.e. the row and column containing the top-right node are
     * the first row and column not included in the print).
     * 
     * Throws logic_error if the bottom-left input coordinates are not at least
     * as left and at least a low as bottom-right coordinates. Throws 
     * std::out_of_range if input coordinates are out of range for the window. 
     *
     * Logical coordinates are used
     */
    template<template<typename> typename N = DefaultNode>
    void print(const std::pair<long int, long int> & bottom_left, 
	       const std::pair<long int, long int> & top_right,
	       std::ostream & os = std::cout) const
    	{
	    // Types for printing configuration
	    using NodeBase = typename Node::getBase<N>;
	    
	    // Check valid coordinates
	    checkRectangle(bottom_left, top_right);

	    // For checking only
	    logicalToReal(bottom_left.first, bottom_left.second);
	    logicalToReal(top_right.first-1, top_right.second-1);
	    
	    // Coordinates for ease of use
	    const long int xbl{bottom_left.first};
	    const long int ybl{bottom_left.second};
	    const long int xtr{top_right.first};
	    const long int ytr{top_right.second};
	    
    	    // Print all the rows
    	    for (long int y = ytr-1; y >= ybl; y--) {

    	    	// Print all the columns
    	    	for (long int x = xbl; x < xtr; x++) {

    	    	    // The node being printed
		    os << getNode(x,y).NodeBase::markerColour()
		       << getNode(x,y).NodeBase::markerShape()
		       << Colour::RESET;

		    // Print space
		    os << " ";
		    
		    // Only print an edge if not on the final column
		    if (x < xtr-1) {
			if (rightEdge(x,y)) {
			    os << getNode(x,y).NodeBase::edgeColour(getNode(x+1,y))
			       << getNode(x,y).NodeBase::edgeShape(getNode(x+1,y))
			       << Colour::RESET;
			} else {
			    os << " ";
			}

			// Print space
			os << " ";
    	    	    }		    
    	    	}

    	    	// Print a row of vertical edges, below all but final row
		os << std::endl;
    	    	if (y > ybl) {
    	    	    for (long int x = xbl; x < xtr; x++) {
    	    		if (belowEdge(x,y)) {
			    os << getNode(x,y).NodeBase::edgeColour(getNode(x,y-1))
			       << getNode(x,y).NodeBase::edgeShape(getNode(x,y-1))
			       << Colour::RESET
			       << " ";
    	    		} else {
			    os << "  ";
			}

			// Print space
			os << "  ";
    	    	    }
    	    	} else {
		    // Below the last row, print the location of the zero
		    // column (if in range)
    	    	    for (long int x = xbl; x < xtr; x++) {
    	    		if (x - xbl == x_zero) {
    	    		    os << "0(" << x_0 << ")"; 			    
    	    		}
    	    	    }
		}		    
    	    	os << std::endl;
    	    }
    	}  

    /// Begin for range-based for loop
    auto begin() { return nodes.begin(); }
    auto begin() const { return nodes.begin(); }

    /// End for range-based for loop
    auto end() { return nodes.end(); }
    auto end() const { return nodes.end(); }

    /** 
     * \brief Copy the contents of a subwindow into a new subwindow
     *
     */
    friend NodeWindow<Node,Base> copy(const NodeWindow<Node,Base> & win)
	{
	    // Create a new object to write the copy to
	    NodeWindow<Node,Base> dest{win};
	    
	    // Create true copies of the vectors
	    dest.nodes = ::copy(win.nodes);
	    dest.h_edges = ::copy(win.h_edges);
	    dest.v_edges = ::copy(win.v_edges);

	    return dest;
	}

    
};

template<NodeType Node>
using BaseNodeWindow = NodeWindow<Node,true>;

template<NodeType Node>
using SubNodeWindow = NodeWindow<Node,false>;

std::string toString(const std::pair<long,long> & pair);

template<NodeType Node, bool Base>
void setPath(NodeWindow<Node,Base> & win,
	     const std::vector<Vec2> & path, bool state)
{
    if (path.size() == 0) {
	return;
    }
    for (std::size_t n{0}; n < path.size()-1; n++) {
	try {
	    win.setEdge({path[n].x(), path[n].y()},
			{path[n+1].x(), path[n+1].y()},
			state);
	} catch (const std::out_of_range & e) {
	    throw std::out_of_range("Obtained out of range while trying to set "
				    "edges for setPath, while placing edge "
				    + path[n].str() + " -- "
				    + path[n+1].str() + ". Maybe some of "
				    "your path goes outside the valid "
				    "window range?");
	}
    }
}

/// Relative directions for path segments

namespace Dir
{
    struct Direction
    {
	const std::size_t rep{1};
	const long x{0};
	const long y{0};
	Vec2 seg(const Vec2 & prev) const
	    {
		return prev + Vec2{x,y};		
	    }
    };
    
    const Direction L(std::size_t rep);
    const Direction R(std::size_t rep);
    const Direction U(std::size_t rep);
    const Direction D(std::size_t rep);
}

/**
 * \brief Set the edges between a set of nodes passed in the argument
 *
 * \return Returns the final node in the path
 *
 */
template<NodeType Node, bool Base>
Vec2 setPath(NodeWindow<Node,Base> & win, const Vec2 & start,
	     const std::vector<Dir::Direction> & dirs, bool state)
{
    std::vector<Vec2> path{start};
    for (std::size_t n{0}; n < dirs.size(); n++) {
	for (std::size_t m{0}; m < dirs[n].rep; m++) {
	    path.push_back(dirs[n].seg(path.back()));
	}
    }
    setPath(win, path, state);
    return path.back();
}


template<NodeType Node, bool Base>
Vec2 setPath(NodeWindow<Node,Base> & win, const Vec2 & start,
	     const Vec2 & end, bool horizontal_first, bool state)
{
    // Check for empty path
    if (start == end) {
	return start;
    }

    // Compute the path directions
    long xdir{end.x() > start.x()? 1 : -1};
    long ydir{end.y() > start.y()? 1 : -1};
    
    if (horizontal_first) {
	for (long x{start.x()}; x != end.x(); x+=xdir) {
	    win.setEdge({x, start.y()}, {x+xdir, start.y()}, state);
	}
	for (long y{start.y()}; y != end.y(); y+=ydir) {
	    win.setEdge({end.x(), y}, {end.x(), y+ydir}, state);
	}
    } else {
	for (long y{start.y()}; y != end.y(); y+=ydir) {
	    win.setEdge({start.x(), y}, {start.x(), y+ydir}, state);
	}
	for (long x{start.x()}; x != end.x(); x+=xdir) {
	    win.setEdge({x, end.y()}, {x+xdir, end.y()}, state);
	}
    }
	    
    return end;
}


template<NodeType Node, bool Base>
void setHorizontalEdges(NodeWindow<Node,Base> & win, bool state)
{
    for (std::size_t x = 0; x < win.width(); x++) {
	for (std::size_t y = 0; y < win.height(); y++) {
	    if (x < win.width()-1) {
		win.setEdge({x,y},{x+1,y},state);
	    }
	}
    }
}

template<NodeType Node, bool Base>
void setVerticalEdges(NodeWindow<Node,Base> & win, bool state)
{
    for (std::size_t x = 0; x < win.width(); x++) {
	for (std::size_t y = 0; y < win.height(); y++) {
	    if (y < win.height()-1) {
		win.setEdge({x,y},{x,y+1},state);
	    }
	}
    }
}


/**
 * \brief Set all edges in window
 *
 * \todo This might be quite inefficient for large window sizes, due to all
 * the checking in setEdge().
 *
 */
template<NodeType Node, bool Base>
void setAllEdges(NodeWindow<Node,Base> & win, bool state)
{
    setHorizontalEdges(win, state);
    setVerticalEdges(win, state);
}

/**
 * \brief Set random edges in window
 */
template<NodeType Node, bool Base>
void setRandomEdges(NodeWindow<Node,Base> & win, double prob, const Seed<> & seed = Seed<>{})
{
    //std::random_device rd;
    //std::mt19937 gen(rd());
    Random<double> rnd{0,1,seed};
    
    //std::uniform_real_distribution<double> distrib(0,1);

    auto state = [&]() {
		     return (rnd() < prob)? true : false; 
		 };
    
    for (std::size_t x = 0; x < win.width(); x++) {
	for (std::size_t y = 0; y < win.height(); y++) {
	    if (x < win.width()-1) {
		win.setEdge({x,y},{x+1,y}, state());
	    }
	    if (y < win.height()-1) {
		win.setEdge({x,y},{x,y+1}, state());
	    }
	}
    }
}

template<NodeType Node, bool Base>
Vec2 snakePattern(NodeWindow<Node,Base> & win,
		  const Vec2 & start, const Vec2 & end,
		  const bool horizontal, bool parity, long sep)
{
    Vec2 xy{start};
    Vec2 forward, backward;
    long loop_start, loop_end, dir;

    // Check that the separation is compatible with the coordinates
    if (horizontal and ((end.x() - start.x()) % sep) not_eq 0) {
	throw std::logic_error("The separation in snakePattern must divide "
			       "the width of the pattern");
    }
    
    if ((not horizontal) and ((end.y() - start.y()) % sep) not_eq 0) {
	throw std::logic_error("The separation in snakePattern must divide "
			       "the height of the pattern");
    }
    
    if (horizontal) {
	dir = end.y() > start.y()? sep : -sep;
	forward = Vec2{end.x() - start.x(), dir};
	backward = Vec2{start.x() - end.x(), dir};
	loop_start = start.y();
	loop_end = end.y();
    } else {
	dir = end.x() > start.x()? sep : -sep;
	forward = Vec2{dir, end.y() - start.y()};
	backward = Vec2{dir, start.y() - end.y()};
	loop_start = start.x();
	loop_end = end.x();
    }

    bool toggle{true};
    for (long n{loop_start}; n != loop_end; n+=dir) {
	if (toggle) {
	    // Forward direction (towards end)
	    xy = setPath(win, xy, xy + forward, parity, true);
	} else {
	    // Backwards direction (towards start)
	    xy = setPath(win, xy, xy + backward, parity, true);
	}
	toggle = not toggle;
    }

    return setPath(win, xy, end, true, true);     
}

template<NodeType Node, bool Base>
void setCheckerSnakePattern(NodeWindow<Node,Base> & win, std::size_t rep,
			    long width, long height, long sep)
{
    Vec2 xy{0,0};
    for (std::size_t n{0}; n < rep; n++) {
	xy = snakePattern(win, xy, xy + Vec2{width,height}, true, true, sep);
	xy = setPath(win, xy, xy + Vec2{-width,sep}, true, true);
	xy = snakePattern(win, xy, xy + Vec2{width,height}, false, false, sep);
	xy = setPath(win, xy, xy + Vec2{sep,0}, true, true);
	xy = snakePattern(win, xy, xy + Vec2{width,-height}, true, true, sep);
	xy = setPath(win, xy, xy + Vec2{-width,-sep}, true, true);
	xy = snakePattern(win, xy, xy + Vec2{width,-height}, false, false, sep);
	xy = setPath(win, xy, xy + Vec2{sep,0}, true, true);
    }
}

template<NodeType Node, bool Base>
void setCheckerDeadEndPattern(NodeWindow<Node,Base> & win)
{
    setCheckerSnakePattern(win, 2, 4, 2, 2);

    // Function to check if a node is connected to any other nodes
    auto hasEdges = [&](const Node & node) {
			return (win.neighbours(node).size() not_eq 0);
		    };
	
    // Loop over all the nodes in the window
    for (std::size_t x{0}; x < win.width()-1; x++) {
	for (std::size_t y{0}; y < win.height()-1; y++) {

	    const auto & node{win(x,y)};
	    
	    // If the node is connected to any edge, do nothing
	    if (hasEdges(node)) {
		// Do nothing, node is part of path
	    } else {
		// Check if the the node to the left is connected to anything
		const auto & left{win(node.x()+1,node.y())};
		const auto & above{win(node.x(),node.y()+1)};
		if (hasEdges(left)) {
		    // Join node to the left
		    win.setEdge({node.x(),node.y()}, {node.x()+1, node.y()}, 1);
		} else if (hasEdges(above)) {
		    // Join node above
		    win.setEdge({node.x(),node.y()}, {node.x(), node.y()+1}, 1);
		}	
	    }
	}
    }
}

/**
 * \brief Set the edges between a set of nodes passed in the argument
 *
 * \return Returns the final node in the path
 *
 */
template<NodeType Node, bool Base>
Vec2 setPath(NodeWindow<Node,Base> & win, const Vec2 & start,
	     const std::vector<Dir::Direction> & dirs)
{
    return setPathEdges(win, start, dirs, true);
    
    // std::vector<Vec2> path{start};
    // for (std::size_t n{0}; n < dirs.size(); n++) {
    // 	for (std::size_t m{0}; m < dirs[n].rep; m++) {
    // 	    path.push_back(dirs[n].seg(path.back()));
    // 	}
    // }
    // setPath(win, path, state);
    // return path.back();
}


#endif

