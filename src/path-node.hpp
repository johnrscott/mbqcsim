/**
 * \file path-node.hpp
 * \brief A node type that stores path information
 *
 */

#ifndef PATH_NODE_HPP
#define PATH_NODE_HPP

#include "colour.hpp"

/**
 * \brief A node that stores logical qubit path information.
 *
 * A logical qubit is realised as a path through the array of nodes. Such
 * a path is stored as a doubly-linked list of nodes, with each node 
 * pointing to its path successor and path predecessor (if they exist).
 * An assumption in the current program is that the path is unique, so
 * that a given node can only be on one path. There is no path branching
 * support in this class. For example, the shortest path tree that results
 * from a global breadth-first search is not stored using this node
 * (see the DistanceNode).
 *
 * Implementation notes:
 *
 * Predecessors and successors are stored as integer offsets, so that
 * computations can take place in situations where the absolute index
 * of a node may not be equal to its position in the vector of nodes.
 * (This occurs when a subwindow of the nodes is used in computations,
 * for example, the active window in the global breadth-first search.)
 *
 * Forward traversal of a path is terminated when a node is reached 
 * whose successor offset is zero. Reverse traversal of a path is
 * terminated when a node is reached whose predecessor offset is 
 * zero. A node that is not on a path has both offsets set to zero.
 */
template<typename Previous>
class PathNode : public Previous
{
    /**
     * \brief Path index
     *
     * The index of the current node on the path, if the node is on
     * a path. Otherwise, the value is -1 
     */
    long int path_index{-1};
    
    /**
     * \brief Offset to next node on path
     *
     * The value is stored as an offset to the successor node, and
     * the value zero indicates that the node does not have a successor.
     */
    int succ_offset{0};

    /**
     * \brief Offset to previous node on path
     *
     * The value is stored as an offset to the predecessor node, and
     * the value zero indicates that the node does not have a predecessor.
     */
    int pred_offset{0};

    /**
     * \brief Flags for right-nodes
     *
     * A right-node in a column x with respect to a path is any node n such that 
     * all path successors of n lie in or to the right of column x (i.e. column
     * index greater than x). The minimal right-node is the right-node with
     * smallest path index. It is the first right-node that is encountered in
     * a particular column during forward traversal of a path.
     *
     */
    enum class Right {  No=0, Yes=1, Minimal=2 } right_node{Right::No};

    /**
     * \brief Flags for left-nodes
     *
     * A left-node in a column x with respect to a path is any node n such that 
     * all path predecessor of n lie in or to the left of column x (i.e. column
     * index less than x). The maximal left-node is the left-node with
     * largest path index. It is the last left-node that is encountered in
     * a particular column during forward traversal of a path. (Or the first
     * that is encountered during reverse traversal of the path.)
     *
     */
    enum class Left { No=0, Yes=1, Maximal=2 } left_node{Left::No};
    
protected:
    using Previous::index_;
    using Previous::x_;
    using Previous::y_;
   
public:
    /// Inherit constructor
    using Previous::Previous;

    bool operator == (const PathNode &) const & = default;
    
    /// Marker colour for printing
    std::string markerColour() const
	{
	    if ((left() == 2) and (right() == 2)) {
		return Colour::PURPLE + Colour::BOLD;
	    }
	    if (left() == 2) {
		return Colour::BLUE + Colour::BOLD;
	    }
	    if (right() == 2) {
		return Colour::RED + Colour::BOLD;
	    }
	    return "";
	}
    
    /// Marker character for printing
    std::string markerShape() const
	{
	    if (left() and right()) {
		return "◆";
	    }
	    if (left()) {
		return "◂";		
	    }
	    if (right()) {
		return "▸";		
	    }
	    return "•";
	}

    template<typename Node>
    std::string edgeColour(const Node & node) const
	{
	    if (isAdjacent(node)) {
		return Colour::GREEN + Colour::BOLD;
	    }
	    return "";
	}
    
    template<typename Node>
    std::string edgeShape(const Node & node) const
	{	    
	    if (x_+1 == node.x()) {
		switch(isAdjacent(node)) {
		case 1:
		    return "→";
		case -1:
		    return "←";
		default:
		    return "―";		    
		}
	    } else if (y_-1 == node.y()) {
		switch(isAdjacent(node)) {
		case 1:
		    return "↓";
		case -1:
		    return "↑";
		default:
		    return "|";		    
		}
	    }
	    return " ";
	}
    
    /// Print debugging information about this path node
    void print(std::ostream & os = std::cout) const
	{
	    Previous::print(os);
	    os << "  (pnode:so=" << succ_offset
	       << ",po=" << pred_offset
	       << ",p_idx=" << path_index
	       << ")"
	       << std::endl;  
	}

    /// Get the path index. Throw exception if not on path
    std::size_t getPathIndex() const
	{
	    if (path_index == -1) {
		throw std::logic_error("PathNode is not on path, so does "
				       "not have a path index (getPathIndex()");
	    }
	    return path_index;
	}
    
    /// Get the successor offset
    [[nodiscard]] long int getPathSuccessorOffset() const { return succ_offset; }

    /// Get the predecessor offset
    [[nodiscard]] long int getPathPredecessorOffset() const { return pred_offset; }

    /// Returns true if the node is on the path
    [[nodiscard]] bool onPath() const {
	return (path_index != -1);
    }

    /**
     * \brief Set this node as a right-node
     *
     * See the documentation for the right_node variable. To set this node
     * as a minimal right-node, pass true to the function. Otherwise, the
     * node is marked as a regular right-node. 
     *
     * \todo This is a bit of a confusing interface, since setRight(false)
     * looks like clearing the right node status. Maybe rename function.
     */
    void setRight(bool minimal) {
	if (minimal) {
	    right_node = Right::Minimal;
	} else {
	    right_node = Right::Yes;	    
	}
    }

    /// Clear the right-node flag
    void clearRight()
	{
	    right_node = Right::No;
	}

    /// Set this node as the start of the path
    void setPathBegin()
	{
	    path_index = 0;
	}
    
    /**
     * \brief Set this node as a left-node
     *
     * See the documentation for the left_node variable. To set this node
     * as a maximal left-node, pass true to the function. Otherwise, the
     * node is marked as a regular left-node. 
     */
    void setLeft(bool maximal) {
	if (maximal) {
	    left_node = Left::Maximal;
	} else {
	    left_node = Left::Yes;	    
	}
    }

    /// Clear the left-node flag
    void clearLeft()
	{
	    left_node = Left::No;
	}

    /** 
     * \brief Check whether the node is a left-node
     *
     * The function returns 0 if the node is not a left-node. This
     * will convert to bool false. Otherwise, a non-zero value is 
     * returned, which will convert to bool true. If the distinction
     * between regular and maximal left-node is required, the value
     * can be checked
     *
     * \return 0 for not a left-node, 1 for regular left-node, and
     * 2 for maximal left-node.
     */
    int left() const
	{
	    return static_cast<int>(left_node);
	}

    /** 
     * \brief Check whether the node is a right-node
     *
     * Analogous to the left() function.
     *  
     * \return 0 for not a right-node, 1 for regular right-node, and
     * 2 for minimal right-node.
     */
    int right() const
	{
	    return static_cast<int>(right_node);
	}

    /**
     * \brief Check if a node is path-adjacent to this node
     *
     * This function checks if a given node (the argument node) is adjacent
     * to this node (either the path successor or predecessor). An exception
     * (logic_error) is thrown if the argument node is this node (i.e. has
     * the same index as this node).
     *  
     * \return 0 if the argument node is not adjacent to this node. -1 if the
     * argument node is the path predecessor of this node. +1 if the argument
     * node is the path successor of this node.
     */
    int isAdjacent(const PathNode & node) const
	{
	    // If the two nodes are equal, throw exception
	    if (index_ == node.index_) {
		throw std::logic_error("Cannot check path-adjacency "
				       "between a node and itself");
	    }
	    
	    // Get the offset to candidate node
	    int offset = node.index_ - /* this node's */ index_;

	    // Check path successor
	    if (succ_offset == offset) {
		return 1;
	    } else if (node.succ_offset == -offset) {
		return -1;
	    } else {
		return 0;
	    }
	}
    
    /**
     * \brief Set a node as the successor of this node
     *
     * This function does two things. First, it sets the successor offset
     * inside this node to refer to the successor node passed as an 
     * argument. Second, it sets the predecessor offset in the argument
     * node to point to this node.
     *
     * Implementation note:
     *
     * This function is templated on the node type, because the node
     * type is not PathNode (it is a node which inherits from PathNode).
     *
     * \todo Add the correct concept to restrict the template parameter
     * (either by derived_from or by checking existence of variables). 
     *
     * \todo The signature here, taking a PathNode instead of a template,
     * should be replicated throughout all node objects, because otherwise
     * you might be template errors depending on the order of nodes in 
     * MakeNode. By taking a PathNode as an argument, any MakeNode containing
     * PathNode will be valid, because it derives from PathNode and all
     * pointers/references are implicitely upcasted to the base class.
     *
     */
    void setPathSuccessor(PathNode & successor)
	{
	    // Check for self-successor
	    if (successor.index_ == index_) {
		throw std::logic_error("You cannot set a node as the path "
				       "successor of itself.");
	    }
	    
	    // Cast to avoid subtraction errors
	    long int a = successor.index_;
	    long int b = /* this node's */ index_;
	    int offset = static_cast<int>(a - b);

	    // Store the successor and predecessor offsets
	    succ_offset = offset;
	    successor.pred_offset = -offset;   

	    // If there is no predecessor offset, then this is the
	    // start of the path.
	    if (pred_offset == 0) {
		path_index = 0;
	    }

	    // Set the path index of the successor node
	    successor.path_index = path_index + 1;
	}

    bool isEnd() const
	{
	    if (not onPath()) {
		throw std::logic_error("In call to isEnd(): cannot ask if a "
				       "node is the end of a path if it is "
				       "not on any path");
	    } else {
		return (getPathSuccessorOffset() == 0); 
	    }
	}
};

#endif
