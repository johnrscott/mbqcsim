/**
 * \file cluster-sim.cpp
 * \brief Implementation of the cluster state simulator
 *
 */

#include <qsl/qubits.hpp>
#include <qsl/utils/quantum.hpp>
#include <qsl/utils/random.hpp>
#include "cluster-sim.hpp"

template<typename NoiseSource>
ClusterSim<NoiseSource>::ClusterSim(unsigned N_in, const Seed<> & seed,
				    NoiseSource && noise_src_)
    : N{N_in}, q{0}, rnd{0, 1, seed}, noise_src_{std::move(noise_src_)}
{
}

template class ClusterSim<NoNoise>;
template class ClusterSim<GaussianNoise>;

