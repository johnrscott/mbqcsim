/**
 * \file base-node.hpp
 * \brief The base node type 
 *
 */

#ifndef BASE_NODE_HPP
#define BASE_NODE_HPP

#include <cstddef>
#include <iostream>
#include <nlohmann/json.hpp>

/**
 * \brief Contains basic information about a node. Used as the base class
 * for creating node types.
 *
 * This node is the base class for all node objects. It contains the node
 * index (its position in the column-major node vector) and its coordinates
 * (which depend on the height of the 2D node array).
 *
 * Implementation notes:
 *
 * Even though properties of a given node a const, the variables are not
 * declared const to support reassignment of one node object to another.
 * 
 * The internal variables are declared protected so that derived classes
 * can access the variables. A node object is built up by linearly 
 * inheriting all the desired characteristics of the node from a set of
 * classes, for example PathNode and QubitNode. In order for these classes
 * to be able to use the internal variables of BaseNode, the variables are
 * declared protected.
 *
 * \todo Think about support for 3D arrays of nodes.
 */
class BaseNode
{
protected:
    std::size_t index_{0}; ///< Index in vector of nodes
    std::size_t x_{0}; ///< x-coordinate of node (increasing to the left)
    std::size_t y_{0}; ///< y-coordinate of node (increasing upwards)

    /// Used in the constructor to check height != 0
    std::size_t nonZero(std::size_t height) const
	{
	    if (height == 0) {
		throw std::logic_error("Cannot create a BaseNode object "
				       "using height == 0");
	    } else {
		return height;
	    }
	}
public:

    bool operator == (const BaseNode &) const & = default;
    
    template<template<typename>typename Node>
    using getBase = BaseNode;

    BaseNode() = default;
    
    /**
     * \brief Construct a node from its index and the height of 
     * the 2D node array.
     */
    BaseNode(std::size_t index, std::size_t height)
	: index_{index}, x_{index_/nonZero(height)}, y_{index_%height} {}

    /// Marker colour for printing
    std::string markerColour() const
	{
	    return "";
	}
    
    /**
     * \brief A marker for use in printing the node
     *
     * Every node type should provide a function called marker. The
     * function is used when printing graphs. The node marker can contain
     * any one-character-wide string for use as the printed node in the
     * NodeWindow.
     *
     * The base node just returns a solid dot: •
     * 
     */
    std::string markerShape() const
	{
	    return "•";
	}


    template<typename Node>
    std::string edgeColour([[maybe_unused]] const Node & node) const
	{
	    return "";
	}
    
    /// Print an edge between this node an another
    template<typename Node>
    std::string edgeShape(const Node & node) const
	{
	    if (x_+1 == node.x()) {
		return "―";
	    } else if (y_-1 == node.y()) {
		return "|";		
	    }
	    return " ";
	}

    /**
     * \todo This is a potential performance problem for nodes that do 
     * not define their own json() function. What will happen is that, for
     * all those nodes, this function will get called over and over again,
     * returning the same json object, with whatever cost is associated
     * with that. A better method would be to ensure that this base is
     * only called once, and do it in some compile time manner. As it
     * stands, this will not be the bottleneck (because it is used in 
     * esim which is simulating qubits).
     */
    nlohmann::json json() const {
	nlohmann::json j;
	j["x"] = x_;
	j["y"] = y_;
	return j;
    }
    
    /**
     * \brief Simple debugging print function
     *
     * This function prints debugging information about this class
     * followed by a new line. Derived classes with override this
     * function and call the print() method from the base class so
     * that all debugging information is printed in a newline
     * separated list.
     */
    void print(std::ostream & os = std::cout) const
	{
	    os << "(" << x_ << "," << y_ << ") (idx=" << index_ << ")"
	       << std::endl;  
	}
    
    /// Get the index
    [[nodiscard]] std::size_t index() const { return index_; } 

    /// Get the x-coordinate
    [[nodiscard]] std::size_t x() const { return x_; } 

    /// Get the y-coordinate
    [[nodiscard]] std::size_t y() const { return y_; }
};

template<typename T>
concept NodeType = std::derived_from<T, BaseNode>;

#endif
