/**
 * \file path-pattern.hpp
 * \brief Classes that compute measurement patterns along a path
 *
 * Once the search has identified a viable path, it is necessary to map
 * a measurement pattern onto that path. The classes in this file perform 
 * this mapping.
 *
 * Measurement patterns are stored in a node like PatternNode. Only
 * the pattern is stored, as a set of "instructions" that the simulator
 * can read, or an analysis class can analyse. Each node contains 
 * information about how it is to be measured and what should be done 
 * with the measurement outcome.
 * 
 */

#ifndef PATH_PATTERN_HPP
#define PATH_PATTERN_HPP

#include "base-node.hpp"
#include "pattern-node.hpp"

/**
 * \brief Quantum simulator for one-qubit paths through a cluster state
 *
 * A single logical qubit is encoded on a path through the cluster state.
 * This class performs the operations necessary to perform the identity
 * gate on the path, i.e. keep it going without changing the state of 
 * the logical qubit.
 *
 * The identity gate is formed by performing X measurements on qubits
 * along the path. All other qubits are cut out using Z measurements. 
 * Apart from the need to choose between X and Z measurements, there are
 * no adaptive measurement settings (measurements that depend on the
 * outcome of a previous measurement). The choice of X and Z is an
 * adaptive choice, but it depends on the success or failure of an
 * edge in the cluster state, rather than on the outcome of a cluster
 * qubit measurement.
 *
 * At each step n along the logical qubit path, the byproduct operators 
 * are updated according to measurement outcomes along that path. They are
 * initialise to zero, and then updated according to the following 
 * expressions:
 *
 * z ^= (m_n ^ kappa_n),      when n is even
 * x ^= (m_n ^ kappa_n),      when n is odd
 *
 * (n starts from 0.) The kappa_n is formed by adding together all the 
 * Z measurement outcomes from qubits joined to the X measurement on
 * the path by a cluster qubit edge.
 * 
 */
class IdentityGate
{
    /** 
     * \brief The current path length of the logical qubit path
     * 
     * The path length is equal to the number of nodes on the path.  
     */ 
    std::size_t path_length{0};
	
public:

    /// Get the current path length of the pattern
    int getPathLength() const {
	return path_length;
    }

    /**
     * \brief Get the state of the logical qubit
     *
     * Use this function to get the state of the logical qubit
     * at a particular point along the path.
     *
     * The index argument specifies the index of the last qubit on 
     * the path (the unmeasured one)
     */
    qsl::Qubits<qsl::Type::Default, double> state(std::size_t index)
	{
	    // Check that the position is in range
	    if (index > path_length) {
		throw std::out_of_range("Cannot get state of measurement "
					"pattern at requested position "
					+ std::to_string(index));
	    }

	    // Create the simulator
	    qsl::Qubits<qsl::Type::Default, double> q{1};

	    // If the index is even, then apply a hadamard
	    if (index % 2 == 0) {
		q.hadamard(0);
	    }

	    return q;
	}
	
    /**
     * \brief Generate the measurement pattern for the next column
     *
     * Call this function after searching for paths to advance
     * the measurement pattern by one column to the right.
     *
     * This function will generate measurement pattern data 
     * (measurement basis settings) for the window provided in the 
     * argument, for use by the simulator (or for other analysis) 
     *	 
     * By default, all nodes store a Z-measurement which discards the
     * measurement outcome. For nodes on the logical qubit path, an 
     * X-measurement should be used, which alternates adding the 
     * measurement outcome to the x or z byproduct operator. For nodes
     * adjacent to the path, a Z-measurement should be used, but the
     * measurement outcome should be added to the x or z byproduct 
     * operator being modified by the outcome of the adjacent 
     * X-measurement. All other measurement settings remain unmodified.
     * 
     * It is very important that this function is called in such a 
     * way that nodes along the path are only processed once. One valid
     * method to call it is to pass the entire window. In that case,
     * the entire path will be processed.
     *
     * Alternatively, the pattern may be generated incrementally, one
     * column at a time. To use this mode, the function is expecting a
     * right-node whose y-coordinate is given by the start variable. (It
     * must be a right node, because the path must be fully contained
     * to the right of the starting column.) The function will process the
     * path up to (but not including) the first right node in the right-
     * most column of the window. To use this mode, set the incremental
     * flag. The next function call should provide a window whose left-most
     * column is the right-most column of the previous call. The function 
     * returns the y-coordinate of the next starting node. The 
     * 
     */
    template<NodeType Node, bool Base>
    std::size_t generate(NodeWindow<Node,Base> && win, const std::size_t start,
			 bool incremental = false)
	{
	    // First, set all the path measurement bases to X in
	    // the left-most column and the next column
	    //
	    // Implementation note: it is important to advance to
	    // the next column because the next step of the algorithm
	    // works out the cut-out qubits around the path. The cut-
	    // out qubits in the left-most column can only be worked
	    // out using information about the path in the next column.
	    // These cut-outs must be recorded here, since the active
	    // window will advance to the right and access to the
	    // leftmost column will be lost in the next iteration of
	    // the algorithm.
	    //
	    for (auto & node : PathRange(win(0,start), win)) {

		// If incremental, check for right node
		//
		///\todo This feels like a hack, but I can't think of the
		/// right way to do it yet. Once I've got this method
		/// working without errors, I'll try to figure out what
		/// the right thing to do is. (This is also the way the
		/// previous code worked. It may be that this kind of check
		/// is a necessary distinction between the full window
		/// pattern generation and the incremental pattern generation,
		/// -- in that case, it would be better to have two separate
		/// functions.
		if (incremental) {
		    if (node.right() and (win.getColumnIndex(node) not_eq 0)) {
			// Do not process this node, instead, return
			// its coordinate to use as the start next time
			return node.y();
		    }
		}
		
		// Set an X measurement
		node.setX();

		// Set the path index
		node.setPatternIndex(path_length);
		
		// Depending on the path length, set the outcome
		// to be added to the x or z byproduct operator
		if (path_length % 2 == 0) {
		    // Add only to z if path length is even
		    node.pushBypUpdate(Bits{0,1});
		} else {
		    // Add only to x if path length is odd
		    node.pushBypUpdate(Bits{1,0});
		}

		// Work out the contributions from the surrounding
		// cut-out qubits
		auto neighbours{win.neighbours(node)};

		// Loop over the neighbours modifying the byproduct
		// operator update rules according to the path node
		for (std::size_t index : neighbours) {
		    // Check that the neigbour is not on the path
		    Node & neighbour{win[index]};

		    if (not neighbour.onPath()) {
			// Update the byproduct operator rules
			// and store the index to which this byproduct
			// update is linked
			neighbour.pushBypUpdate(node.getPathBypUpdate(),
						node.getPathIndex());
		    } else {

			if (neighbour.getPathIndex() > node.getPathIndex()+1) {
			    throw std::logic_error("Cannot create a valid "
						   "measurement pattern on "
						   "this path because it "
						   "contains loops.");
			}
		    }
		}

		// A node has just been processed, so
		// increment the path length
		path_length++; 
	    }	    
	    return 0; // Not used unless in incremental mode
	    
	}

};

#endif
