/**
 * \brief Path iterator for path nodes
 *
 * Path nodes store path successors that enable forward-traversal of a path.
 * The PathIterator makes this process easier to use in for loops.
 * 
 */

#ifndef PATH_ITERATOR_HPP
#define PATH_ITERATOR_HPP

//#include "node-window.hpp"
#include "base-node.hpp"
#include "node-window.hpp"

class PathSentinel {};

class PathFatalException {};

/**
 * \brief Iterator for forward traversal of a set of path nodes
 *
 * Use this iterator to advance through a path specified through 
 * path successors stored in PathNodes. It is necessary to provide
 * a NodeWindow, which provides the framework for obtaining references
 * to PathNodes (nodes do not store references or pointers to each other 
 * -- only offsets to other nodes).
 *
 */
template<NodeType Node, bool Base>
class PathIterator
{
    Node * node;
    NodeWindow<Node,Base> & win_;

    /**
     * \brief Node object to point to as the end sentinel
     *
     * The purpose of this object is to act as the pointee for the
     * interator that has been incremented to the end sentinel.
     * It should never be dereferenced.
     *
     * Implementation notes:
     *
     * A height of one is used because height zero will throw an
     * exception. Without the exception checking in BaseNode, this
     * causes illegal instruction.
     */
    Node end_sentinel{0,1};
    
public:
    PathIterator(Node & node_in, NodeWindow<Node,Base> & win)
	: node{&node_in}, win_{win}
	{}

    /**
     * \brief Dereference
     *
     * The checking of the end sentinel every time this occurs feels like it
     * might be a performance problem. At the moment, I want correctness rather
     * than performance, but it might be worth coming back to.
     */
    Node & operator * ()
	{
	    if (node == &end_sentinel) {
		throw std::logic_error("Cannot dereference PathIterator "
				       "end_sentinel");
	    } else {
		return * node;
	    }
	}

    Node * operator -> ()
	{
	    if (node == &end_sentinel) {
		// std::cerr << "Internal error: Cannot perform operator-> on "
		// 	  << "PathIterator end_sentinel";
		// abort();
		throw PathFatalException{};
	    } else {
		return node;
	    }
	}


    /**
     * \brief Prefix increment
     *
     * You can only increment a path node. Incrementing the last path node
     * will generate the end sentinel, which you cannot increment. If you try,
     * std::out_of_range will be thrown.
     */
    PathIterator & operator++()
	{
	    if (node == &end_sentinel) {
		throw std::out_of_range("Cannot increment the end sentinel "
					"in PathIterator. You have reached the "
					"end of the path already.");
	    }
	    const long int offset{node->getPathSuccessorOffset()};
	    if (offset == 0) {
		node = &end_sentinel;
	    } else {
		// Calling operator++ and getting out of range for the
		// window is really something that should be passed to
		// the caller to handle.
		//
		// Update: This interface is so much better! Now you can
		// actually see the difference between an out of range node
		// and the end of the path. 
		node = &win_.getNode(*node, offset);
	    }
	    return * this;
	} 
    
    bool operator==(const PathIterator & other) {
	return (node == other.node);
    }

    /**
     * \brief Sentinel for end of path
     *
     * Check if the offset is zero. If it is, return true (this is
     * the end of the path).
     */
    bool operator==(const PathSentinel &)
	{
	    //const long int offset{node->getPathSuccessorOffset()};
	    if (node == &end_sentinel) {
		return true;
	    } else {
		return false;
	    }
	}
};

template<NodeType Node, bool Base>
PathIterator<Node,Base> operator+(const PathIterator<Node,Base> & node,
				  std::size_t offset)
{    
    PathIterator<Node,Base> copy{node};
    for (std::size_t n{0}; n < offset; n++) {
	++copy;
    }
    return copy;
}

template<typename Node, bool Base>
class PathRange
{
    const PathIterator<Node,Base> it;
    
public:

    PathRange(Node & node, NodeWindow<Node,Base> & win)
	: it{node, win}
	{
	    //const long int offset{node.getPathSuccessorOffset()};
	    if (not node.onPath()) {
		throw std::logic_error("Node is not on the path. Cannot "
				       "use it as the start of PathRange.");
	    }
	}
    
    auto begin() const
    {
        // The begin is just the pointer to our string.
        return it;
    }
    auto end() const
    {
        // The end is a different type, the sentinel.
        return PathSentinel{};
    }
};

template<typename Node, bool Base>
bool operator==(const PathSentinel &, const PathIterator<Node,Base> & it)
{
    return (it == PathSentinel{});
}

#endif
