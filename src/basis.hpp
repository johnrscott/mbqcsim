/**
 * \file basis.hpp
 * \brief Contains a class for holding measurement bases
 * 
 */

#ifndef BASIS_HPP
#define BASIS_HPP

#include <qsl/qubits.hpp>
#include "random.hpp"

/**
 * \brief A class for specifying a one-qubit measurement basis
 *
 * One-qubit measurements are made by applying a one-qubit gate and
 * then measuring in the computational basis. Even though a true
 * one-qubit measurement requires three (Euler) angles, it is only
 * necessary to specify two angles for a one-qubit measurment (a
 * Z-rotation followed by an X-rotation), because the final Z-rotation
 * has no effect on the outcome of the measurement (it only introduces
 * a phase).
 *
 */
class Measure
{
    /**
     * \brief Label to indicate basis
     *
     * \todo Remove this because it is taking up unnecessary space. This
     * is only needed for now to print a label in QubitNode. Really, the
     * NodeWindow printing should allow multi-character nodes (then the
     * actual basis could be printed instead).
     */
    const double z_0_; ///< The first rotation (Z-rotation)
    const double x_1_; ///< The second rotation (X-rotation)
    char label_{'Z'};

public:

    double z_0() const { return z_0_; }
    double x_1() const { return x_1_; }
    
    /**
     * \brief Construct the measurement by passing the angles
     */
    Measure(double z_0, double x_1)
	: z_0_{z_0}, x_1_{x_1}
	{ }

    /**
     * \brief Construct the measurement by passing the angles and a label
     */
    Measure(double z_0, double x_1, char label)
	: z_0_{z_0}, x_1_{x_1}, label_{label}
	{ }

    /// Return the label for the basis
    char label() const { return label_; }
    
    virtual void print(std::ostream & os) const {
	os << "(" << z_0_ << "," << x_1_ << ")";
    }

    nlohmann::json json() const {
	nlohmann::json j;
	j["z_0"] = z_0_;
	j["x_1"] = x_1_;
	return j;
    }
    
    /**
     * \brief Measurement qubit 0 of a simulator object
     *
     * Use this function to perform the measurement. Pass a simulator
     * object and a target qubit index. The function returns the result
     * of the measurement (1 or 0).
     *
     * Performing the measurement removes the qubit from the simulator, 
     * reducing the number of qubits in the simulator object by one. Calling
     * this function a second time measures the next-lowest qubit in the
     * simulator.
     *
     * Implementation note: By passing the noise parameters as doubles, 
     * rather than as a a random device, it is possible to control whether
     * noise is added at the cluster sim leve, rather than down here. This
     * is important for now because I want the same code to support both
     * noise and exact measurements -- only because I've run out of time
     * to implement it better. (This isn't really a great design. For one
     * thing, you really want the exact measurements to not add noise
     * at all, not just adding "0" noise.)
     * \todo Come back to this and have a proper think about what is best.
     */
    unsigned operator () (qsl::Qubits<qsl::Type::Resize, double> & q,
			  unsigned targ, Random<double> & rnd,
			  const double z_0_noise, const double x_1_noise) const
	{
	    q.rotateZ(targ, z_0_ + z_0_noise); // X-rotation
	    q.rotateX(targ, x_1_ + x_1_noise); // Z-rotation

	    // Emulate seeded measurement using prob and
	    // postselect (this can be replaced when qsl supports
	    // seeds)
	    const double prob0{q.prob(targ, 0)};
	    const unsigned outcome{rnd() < prob0? 0U : 1U};
	    q.postselectOut(targ, outcome); // Perform the measurement
	    return outcome;
	}    
};

/**
 * \brief Computational basis measurement
 *
 * This is an object, because there is only one type
 * of Z measurement.
 *
 * Implementation node: even thought objects tend to have lower case
 * letters in this code, in this case I think an upper case makes
 * more sense because the then MeasureZ and MeasureXY look more similar.
 * It might be better to just make MeasureZ a class -- but then it would
 * need brackets every time you initialise it.
 *
 */
const Measure MeasureZ{ 0, 0 };

/**
 * \brief Make an XY-measurement at angle phi
 */
class MeasureXY : public Measure
{
    double phi;
public:

    /// Generic basis constructor
    explicit MeasureXY(double phi_in)
	: Measure{-phi_in+M_PI/2, M_PI/2, 'B'}, phi{phi_in}
	{}

    /// Constructor to specify label (e.g. X or Y)
    MeasureXY(double phi_in, char label)
	: Measure{-phi_in+M_PI/2, M_PI/2, label}, phi{phi_in}
	{}

    
    void print(std::ostream & os) const override
	{
	    os << "XY(" << phi << ")"; 
	}
};

const MeasureXY MeasureX{ 0, 'X' }; ///< X measurement
const MeasureXY MeasureY{ M_PI/2, 'Y' }; ///< Y measurement

#endif
