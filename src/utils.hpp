/**
 * \file utils.hpp
 * \brief General utilities
 *
 */

#ifndef UTILS_HPP
#define UTILS_HPP

/// Press enter to continue
void pressEnterToContinue(std::ostream & os = std::cout,
			  std::istream & is = std::cin);

#endif
