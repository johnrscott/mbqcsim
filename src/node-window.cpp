/**
 * \file node-window.cpp
 * \brief Implementations from node-window.hpp
 *
 */

#include "node-window.hpp"

namespace Dir
{
    const Direction L(std::size_t rep) { return Direction{rep, -1, 0}; }
    const Direction R(std::size_t rep) { return Direction{rep, 1, 0}; }
    const Direction U(std::size_t rep) { return Direction{rep, 0, 1}; }
    const Direction D(std::size_t rep) { return Direction{rep, 0, -1}; }
}
