/**
 * \file make-node.hpp
 * \brief Utilities for constructing node types with desired characteristcs
 *
 */

#ifndef MAKE_NODE_HPP
#define MAKE_NODE_HPP

#include "base-node.hpp"
#include <type_traits>

template<template<typename> typename... NodePolicies>
struct MakeNode;

template<template<typename> typename... NodePolicies>
struct MakeNode : BaseNode
{
    using BaseNode::BaseNode;

    ///\todo This needs testing thoroughly, because it appears that it
    /// sometimes does the wrong thing if the base classes do not
    /// define operator== = default.
    bool operator == (const MakeNode &) const & = default;
    
    template<template<typename> typename Node>
    using getBase = BaseNode;
};

/// Induction case
template<typename MakeNodeRest,
	 template<typename> typename First,
	 template<typename> typename Node>
struct BaseFinder
{
    static constexpr bool found {
	std::is_same_v<Node<MakeNodeRest>, First<MakeNodeRest>>
    };
    
    using type = std::conditional_t<found,
				    First<MakeNodeRest>,
				    typename MakeNodeRest::getBase<Node>>;
};

template<typename> struct DefaultNode {};

/// Base case
template<typename MakeNodeRest,
	 template<typename> typename First>
struct BaseFinder<MakeNodeRest, First, DefaultNode>
{
    using type = BaseNode;
};

template<template<typename> typename First, template<typename> typename... Rest>
struct MakeNode<First, Rest...> : First<MakeNode<Rest...>> 
{
    using First<MakeNode<Rest...>>::First;

    template<template<typename> typename Node>
    using getBase = BaseFinder<MakeNode<Rest...>, First, Node>::type;
};

#endif
