/**
 * \file small-vector.hpp
 * \brief A small vector allocated in automatic storage
 *
 */

#ifndef SMALL_VECTOR_HPP
#define SMALL_VECTOR_HPP

#include <array>

/**
 * \brief
 *
 * The only use of this class for the moment is avoiding using std::vector
 * (and the free store) in calls to NodeWindow::neighbours(). Designed for
 * build-in types at the moment (no emplace_back).
 * 
 * Warning: no checking is implemented at the moment. Run a memcheck
 * to check for errors.
 *
 * \todo Written hastily -- not thoroughly checked yet.
 */
template<typename T, std::size_t N = 4>
class SmallVector
{
    std::array<T, N> a_;
    std::size_t end_{0}; ///< A pointer to the end of the array
public:
    /// Get the size of the vector
    std::size_t size() const {
	return end_;
    }

    const T & operator[] (std::size_t index) const
	{
	    return a_[index];
	}

    T & operator[] (std::size_t index)
	{
	    return const_cast<T &>(
		static_cast<const SmallVector &>(*this)[index]);
	}


    /// This will copy construct T
    void push_back(const T & t)
	{
	    // Insert at end_offset
	    a_[end_++] = t;
	}

    /// Begin for range-based for loop
    auto begin() { return a_.begin(); }
    auto begin() const { return a_.begin(); }

    /// End for range-based for loop
    auto end() { return a_.begin() + end_; }
    auto end() const { return a_->begin() + end_; }  
};


#endif
