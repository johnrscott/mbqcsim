/**
 * \file error-sim.hpp
 * \brief Class for simulating errors in a logical qubit path
 *
 */

#ifndef ERROR_SIM_HPP
#define ERROR_SIM_HPP

#include "distance-node.hpp"
#include "pattern-node.hpp"
#include "path-node.hpp"
#include "path-sim.hpp"
#include "path-pattern.hpp"
#include "pathfinding.hpp"
#include "advance-iterator.hpp"

/**
 * \brief Exception for when a simulation cannot be continued
 *
 * This exception is designed for use with the emulatePathfinding function.
 */
template<typename Node>
class ErrorSimTerminated
{
    const std::size_t end_col_; ///< The last node in the path
public:
    explicit ErrorSimTerminated(const std::size_t end_col)
	: end_col_{end_col} {}
    std::size_t end() const noexcept { return end_col_; }
    const char * what() const noexcept { return "Simulation terminated"; }
};

/**
 * \brief Class emulate pathfinding
 *
 * Implementations notes:
 * 
 * The test template parameter is only to disable user input (pressing
 * enter) during the test of simulate()
 */
template<typename Search, typename NoiseSource, bool press_enter = true>
class ErrorSim
{
    std::size_t height_{0};
    /**
     * \brief Store the performance counters from the each search window
     */
    std::vector<nlohmann::json> col_results;    

    Search search;

    /**
     * \brief Whether the print the search process
     */
    bool interactive_{false};

    std::size_t print_width_{20};
    
public:

    /// Constructor for normal use
    explicit ErrorSim(std::size_t height)
	: height_{height}, search{height_}, interactive_{false}
	{
	    if (height_ > 14) {
		throw std::logic_error("Cannot perform an error simulation "
				       "with a height of more than 14 qubits");
	    }
	}

    /// Constructor for interactive printing
    ErrorSim(std::size_t height, bool interactive) : ErrorSim{height}
	{
	    interactive_ = interactive; 
	}

    /**
     * \brief Simulate the columns in a cluster state
     *
     * \return The final column simulated
     */
    template<typename Node, bool Base, typename... NoiseArgs>
    std::size_t simulate(NodeWindow<Node,Base> & win, std::size_t y,
		  const std::size_t block_width, const Seed<> & path_seed,
		  const Seed<> & sim_seed, NoiseArgs&&... args)
	{
	    if (win.height() not_eq height_) {
		throw std::out_of_range("Window height must match ErrorSim "
					"height in simulate() function");
	    }

	    if (block_width > win.width()) {
		throw std::out_of_range("Width of sub-block must be less than or "
					"equal to the NodeWindow width in.");
	    }

	    if (y >= win.height()) {
		throw std::out_of_range("Starting y-coordinate is not in range "
					"in ErrorSim::simulate");	
	    }

	    // For use in the simulator constructor
	    const unsigned height{static_cast<unsigned>(win.height())};
	    
	    // Create the generator for path randomness
	    Generator<unsigned, 0, 1> path_gen{path_seed};

	    std::size_t y_next{y}; // Variable holding next  y-coordinate pattern
	    std::size_t y_gen{y};
	    std::size_t y_max_left{y};
	    win(0,y).setPathBegin();

	    IdentityGate gate;

	    // Make initial all plus state
	    qsl::Qubits<qsl::Type::Resize,double> col{height};
	    for (std::size_t n{0}; n < col.getNumQubits(); n++) {
		col.hadamard(n);
	    }

	    PathSim<NoiseSource> sim {
		height, sim_seed, NoiseSource{std::forward<NoiseArgs>(args)...}
	    };

	    // Distance between simulation and correct state
	    double distance{0};
	    Bits byp{0,0}; // Current byproduct operators
    
	    // Flag to mark the first simulation column
	    bool sim_start{true};
    
	    // Loop over all sub-blocks, performing a path search,
	    // followed by pattern generation, followed by a simulation
	    // Note: the loop is one longer than the window length, because
	    // the simulation needs to get to the end
	    for (std::size_t x{0}; x < win.width()+1; x++) {
	
		// Compute the current block location
		const std::size_t start{x};
		const std::size_t end{std::min(x+block_width,win.width())};

		// Only search the window if there is a region to search
		if (start < end) {
		    auto sub{win.sub(start,end,start)};
		    // Search algorithm and RPT
		    search.search(sub, y);
		    search.makeSPT(sub);
		}
		
		// Extend the path
		try {
		    PathExtension path;
		    // Only extend path if there are at least two columns
		    if (start < end-1) {
			y_next = path.extend(win.sub(start,end,start),
					     y, path_gen);
		    }

		    const long gen_start{
			std::max<long>(0,static_cast<long>(start)-10)
		    };
		    // Only generate the pattern if the window has width
		    // (note: start, not gen_start, because that is the
		    // zero column).
		    if (start < end) {
			y_gen = gate.generate(win.sub(gen_start,end,start),
					      y_gen, true);

			// Compute left nodes for simulation
			y_max_left = writeLeftNodes(
			    win.sub(gen_start, std::min(start+2,win.width()),
				    start),
			    win(start, y_max_left)
			    );

		    }
		    
	    
		    // Simulate the column
		    // Lag by one column so that the pattern endpoint is always one
		    // column ahead.
		    if (start > 0) {
			auto sub{win.sub(start-1,std::min(start+1,end),start-1)};
			col = sim.simulate(std::move(sub), std::move(col),
					   sim_start);
			sim_start = false;	
			auto left_height{sim.getLeftHeight()};
			auto q_correct{gate.state(left_height)};
			auto q{sim.state()};
			byp = sim.getBypOps();
			distance = qsl::fubiniStudy(q,q_correct);

			nlohmann::json column;
			
			// Store the distance and byproduct operators
			column["distance"] = distance;


			/// \todo The information below is good to have, but
			/// causes a memory problem when running large instances of
			/// esim. This memory problem is easily solvable in many
			/// ways (for example, factor the repeats into the python
			/// analysis scripts, rather than as an option to esim),
			/// but the easiest thing to do for now, since I don't
			/// need the information, is just to remove it.
			// nlohmann::json byp_json;
			// byp_json["x"] = byp(1);
			// byp_json["z"] = byp(0);
			// column["byp"] = byp_json;
			
			// // Store the qubit results
			// for (const auto & node : win.sub(start-1,start,start-1)) {
			//     column["nodes"].push_back(node.json());
			// }

			col_results.push_back(column);

		    }

		    // Update the starting y value
		    y = y_next;
	    
		    // Print if interactive mode
		    if (interactive_) {

			// long int block_start{static_cast<long int>(start)};
			// long int block_end{static_cast<long int>(end)};
		    
			long win_start{std::max<long>(static_cast<long>(start)-10,0)};
			long win_end{std::min<long>(win_start+30,win.width())};
		    
			// Compute printable window
			const std::size_t width{end-start};
			std::cout << "Completed search of block " << x << ", "
				  << "width " << width << std::endl;
			std::cout << "Distance nodes:" << std::endl;
			auto sub{win.sub(win_start,win_end,win_start)};
			sub.template print<DistanceNode>();
			std::cout << "Path nodes:" << std::endl;
			sub.template print<PathNode>();
			std::cout << "Pattern nodes:" << std::endl;
			sub.template print<PatternNode>();
			std::cout << "Qubit nodes:" << std::endl;
			sub.template print<QubitNode>();
			std::cout << "Current byproduct operators: "
				  << byp << std::endl;
			std::cout << "Simulation distance: "
				  << distance << std::endl;
			std::cout << "Finished processing column "
				  << x << std::endl;
			if constexpr (press_enter) {
			    pressEnterToContinue();
			}
		    }
	    
		} catch (const std::logic_error & e) {
		    throw ErrorSimTerminated<Node>{start};
		}		
	    }
	    return win.width();
	}

    /**
     * \brief Get JSON performance counter data
     * 
     * Contains two keys: gbfs and path_extend. Both contains lists of the
     * performance counters relating to those two parts of the process.
     *
     * This JSON object aggregates the data from the search and the path
     * extension into one list, so that they can be processed easily together
     * (i.e. the path extension corresponding to a search is known)
     */ 
    nlohmann::json json() const {
	return col_results;
    }
};

#endif
