/**
 * \file coordinates.hpp
 * \brief A model for coordinates and vector arithmetic in 2D
 *
 */

#ifndef COORD_ARITH_HPP
#define COORD_ARITH_HPP

#include <string>
#include <sstream>

/**
 * \brief A 2D vector of integer coordinates (x,y)
 *
 * To be used as either coordinates (specifying position) or as vectors 
 * (supporting vector arithmetic) in two dimensions.
 *
 */
class Vec2
{
    long x_{0};
    long y_{0};
public:
    Vec2() = default;
    Vec2(long x, long y) : x_{x}, y_{y} {}
    long x() const { return x_; }
    long y() const { return y_; }
    long & x() { return x_; }
    long & y() { return y_; }
    std::string str() const
	{
	    std::stringstream ss;
	    ss << "(" << x_ << "," << y_ << ")";
	    return ss.str();   
	}
};

/// Equality comparison
bool operator==(const Vec2 & lhs, const Vec2 & rhs);

/// Vector whose components are absolute values of the argument
Vec2 abs(const Vec2 & vec);

/// Scalar multiplication
Vec2 operator * (long scalar, const Vec2 & vec);

/// Componentwise integer division
Vec2 operator / (const Vec2 & vec, long scalar);

/// Unary minus
Vec2 operator - (const Vec2 & vec); 

/// Vector addition
Vec2 operator + (const Vec2 & lhs, const Vec2 & rhs);
Vec2 operator - (const Vec2 & lhs, const Vec2 & rhs);

#endif
