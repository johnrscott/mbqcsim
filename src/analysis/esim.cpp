/**
 * \file esim.cpp
 * \brief A program for calculating propogation of modulator voltage errors
 *
 */

#include <iostream>
#include "distance-node.hpp"
#include "path-node.hpp"
#include "make-node.hpp"
#include "node-window.hpp"
#include "global-bfs.hpp"
#include "iterative-bfs.hpp"
#include "pathfinding.hpp"
#include "utils.hpp"

#include "error-sim.hpp"

#include <nlohmann/json.hpp>
#include "cmdline.hpp"
#include <fstream>
#include "version.hpp"

using Node = MakeNode<DistanceNode, PathNode, PatternNode, QubitNode>;

int main(int argc, char ** argv)
{
    CommandLine cmd;
    
    const std::string program_name{ "esim" };
    const std::string version{ "v0.1" };
    const std::string short_desc{"A program for simulating the effect " 
				 "of modulator errors"};
    const std::string long_desc{R"xyz(esim is...)xyz"};

    // Map
    enum class Alg
    {
	GBFS, IBFS
    };
    std::map<std::string, Alg> algs {
	{"gbfs", Alg::GBFS}, {"ibfs", Alg::IBFS}
    };
    
    cmd.addOption<bool>('v', "version",
			"Display version information and exit");
    cmd.addOption<Seed<>::seed_t>('s', "seed",
				  "the root_seed used to perform the experiment");
    cmd.addOption<bool>('m', "minimal",
			"Store output information necessary for "
			"repeating the experiment, and exit immediately");
    cmd.addOption<bool>('d', "debug",
			"Step through the calculation as it is performed");
    cmd.addOption<std::string>('i', "infile",
			       "Provide a minimal JSON input file to set "
			       "program options");
    cmd.addOption('a', "algorithm", algs,
		  "which algorithm to use for pathfinding in the experiment");
    cmd.addOption<std::size_t>('H', "window-height",
			       "the height of the window being analysed");
    cmd.addOption<std::size_t>('y', "y-start",
			       "the starting y-coordinate of the path");
    cmd.addOption<std::size_t>('W', "window-width",
			       "the width of the window being analysed");
    cmd.addOption<std::size_t>('B', "block-width",
			       "the width of the active search block");
    cmd.addOption<double>('p', "edge-probability",
			  "The edge probability in the 2D graph");
    cmd.addOption<double>('e', "error-sigma",
			  "The standard deviation of Gaussian noise added to "
			  "measurements (rad)");
    cmd.addOption<double>('f', "error-mu",
			  "The mean of the Gaussian noise added to "
			  "measurements (rad)");
    cmd.addOption<double>('e', "error-sigma",
			  "The standard deviation of noise added to "
			  "measurements (rad)");
    cmd.addOption<std::size_t>('n', "num-repeats",
			       "Number of times to repeat the experiment");
    cmd.addOption<std::string>('o', "output-file",
			       "The relative file path in which to store results");    
    //cmd.setupManPageOption(program_name, short_desc, long_desc, version);
    
    if(cmd.parse(argc, argv) != 0) {
	// An error occured
	std::cerr << "An error occured while parsing the command line arguments"
		  << std::endl;
	return 1;
    }

    // Check for version flag
    if (cmd.get<bool>('v')) {
	std::cout << programHeader("esim") << std::endl;
	return 0;
    }
    
    // Main JSON results object (a list)
    nlohmann::json res;

    // Input file
    nlohmann::json infile;

    // Read from the minimal JSON input file if present
    auto infilename_arg{cmd.get<std::string>('i')};

    // Variables for files from value (also default values)
    std::size_t height_def{8};
    std::size_t width_def{20};
    std::size_t y_start_def{height_def/2};
    std::size_t block_width_def{width_def/2};
    double prob_def{0.9};
    double error_sigma_def{0.0};
    double error_mu_def{0.0};
    std::size_t repeats_def{1};
    std::size_t root_seed_def{Seed<>{}.seed()}; // Generate random seed
    Alg alg_def{Alg::GBFS};
    
    if (infilename_arg) {

	// Read and close input file
	std::ifstream file;
	file.open(infilename_arg.value(), std::ios::in);
	///\todo Catch exceptions
	infile = nlohmann::json::parse(file);
	file.close();	

	try {
	    // Parse the JSON file input
	    height_def = infile.at("window_height");
	    width_def = infile.at("window_width");
	    y_start_def = infile.at("y_start");
	    block_width_def = infile.at("block_width");
	    prob_def = infile.at("edge_probability");
	    error_sigma_def = infile.at("error_sigma");
	    error_mu_def = infile.at("error_mu");
	    repeats_def = infile.at("num_repeats");
	    root_seed_def = infile.at("root_seed").at("seed");
	    std::string alg_temp{infile.at("alg")};
	    if (alg_temp == "gbfs") {
		alg_def = Alg::GBFS;
	    } else if (alg_temp == "ibfs") {
		alg_def = Alg::IBFS;
	    } else {
		std::cout << "Value of key 'alg' in JSON input file is not "
			  << "valid; must be 'gbfs' or 'ibfs'"
			  << std::endl;
		return -1;
	    }
	} catch (const nlohmann::detail::out_of_range & e) {
	    std::cerr << "Error parsing input JSON file (-i): "
		      << e.what() << ". "
		      << "Check that '" << infilename_arg.value() << "' "
		      << "is in the correct format."
		      << std::endl;
	    return -1;
	}
    }
    
    const std::size_t height{cmd.get<std::size_t>('H').value_or(height_def)};

    // Check that the height is within range
    if (height > 14) {
	std::cerr << "Input error: height " << height
		  << " is invalid, must be at most 14"
		  << std::endl;
	return 0;
    }
    
    res["window_height"] = height;    
    const std::size_t width{cmd.get<std::size_t>('W').value_or(width_def)};
    res["window_width"] = width;

    ///\todo There are a few other places this will be required,
    /// but I haven't done them all yet (defaults dependent on passed
    /// command line arguments).
    if (not infilename_arg) {
	y_start_def = height/2;
    }
    const std::size_t y_start{cmd.get<std::size_t>('y').value_or(y_start_def)};
    res["y_start"] = y_start;
    const std::size_t block_width{cmd.get<std::size_t>('B').value_or(block_width_def)};
    res["block_width"] = block_width;
    const double prob{cmd.get<double>('p').value_or(prob_def)};
    res["edge_probability"] = prob;
    const double error_sigma{cmd.get<double>('e').value_or(error_sigma_def)};
    res["error_sigma"] = error_sigma;
    const double error_mu{cmd.get<double>('f').value_or(error_mu_def)};
    res["error_mu"] = error_mu;
    const std::size_t repeats{cmd.get<std::size_t>('n').value_or(repeats_def)};
    res["num_repeats"] = repeats;

    const bool debug{cmd.get<bool>('d')};
    
    // Write algorithm
    const Alg alg{cmd.get<Alg>('a').value_or(alg_def)};
    switch (alg) {
    case Alg::GBFS:
	res["alg"] = "gbfs";
	break;
    case Alg::IBFS:
	res["alg"] = "ibfs";
	break;
    }
        
    // Create a master seed, or use command line argument
    auto root_seed_arg{cmd.get<Seed<>::seed_t>('s').value_or(root_seed_def)};
    Seed root_seed{root_seed_arg};
    res["root_seed"] = root_seed.seed();

    // Following the advice here "https://www.johndcook.com/blog/2016
    // /01/29/random-number-generator-seed-mistakes/", the
    // graph seeds are generated using root_seed + 4*n, the
    // path seeds are generated using root_seed + 4*n + 1, the
    // sim seeds are generated using root_seed + 4*n + 2, and the
    // noise seeds are generated using root_seed + 4*n + 3, where n is
    // the repeat index. The next valid seed is root_seed + 4*repeats
    res["next_seed"] = Seed<>{root_seed.seed() + 4*repeats}.seed();
    
    // Store program build information
    res["build"] = BUILD;
    
    // If set to true, omit main results data from output JSON,
    // and also jump straight to the output section (there is no
    // need to perform the experiment and discard the result).
    const bool minimal{cmd.get<bool>('m').value_or(false)};
    if (minimal) {
	goto WRITE_DATA;
    }
    
    if (debug) {

	std::cout << programHeader(std::string{"esim"}) << std::endl;

	std::cout << "Debug mode" << std::endl;
	std::cout << "Using root seed = " << root_seed.seed() << std::endl;
	std::cout << "Performing N = " << repeats << " repeats" << std::endl;
	std::cout << "Using a random graph with edge probability p = "
		  << prob << std::endl;
	pressEnterToContinue();
    }

    // Repeat the experiment for multiple times (if -r is passed) 
    for (std::size_t n{0}; n < repeats; n++) {

	if (debug) {
	    std::cout << "Starting run n = " << n
		      << " (out of N = " << repeats << ")" << std::endl; 
	}
	
	// Make results object
	nlohmann::json j;
	
	// Make graph seed
	Seed graph_seed{root_seed.seed() + 4*n};

	// Make path seed
	Seed path_seed{root_seed.seed() + 4*n + 1};

	// Make simulation seed
	Seed sim_seed{root_seed.seed() + 4*n + 2};

	// Make noise seed
	Seed noise_seed{root_seed.seed() + 4*n + 3};
	
	// Create graph and set random edges
	BaseNodeWindow<Node> win{height,width};
	setRandomEdges(win, prob, graph_seed);

	std::size_t end_col{0};

	// Perform the pathfinding emulation
	//Generator<unsigned, 0, 1> path_gen{path_seed};    
	Node last;
	switch (alg) {
	case Alg::GBFS: {
	    // Use the global BFS algorithm
	    ErrorSim<GBFS, GaussianNoise> esim{win.height(), debug};
	    try {
		end_col = esim.simulate(win, y_start, block_width,
					path_seed, sim_seed,
					error_mu, error_sigma, noise_seed);
	    } catch (const ErrorSimTerminated<Node> & p) {
		end_col = p.end();
	    }
	    j["sim_results"] = esim.json();
	    break;
	}
	case Alg::IBFS: {
	    // Use the incremental BFS algorithm
	    ErrorSim<GBFS, GaussianNoise> esim{win.height(), debug};
	    try {
		end_col = esim.simulate(win, y_start, block_width,
					path_seed, sim_seed,
					error_mu, error_sigma, noise_seed);
	    } catch (const ErrorSimTerminated<Node> & p) {
		end_col = p.end();
	    }
	    j["sim_results"] = esim.json();
	    break;
	}
	}
	
	// Record final column
	j["end_column"] = end_col;
    
	// Add the results from this repeat to the main results JSON
	if (not minimal) {
	    res["repeat_data"].push_back(j);
	}
    }

WRITE_DATA:
    
    // Write the results to a file
    auto outfilename_arg{cmd.get<std::string>('o')};
    if (outfilename_arg) {
	std::ofstream file;
	file.open(outfilename_arg.value(), std::ios::out);
	file << res << std::endl;
	file.close();	
    } else {
	std::cout << res << std::endl;	
    }
}
