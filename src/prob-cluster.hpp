/**
 * \brief A class for analysing and simulating probabilistic cluster states
 *
 *
 */

#ifndef PROB_CLUSTER
#define PROB_CLUSTER


/**
 * \brief Probabilistic cluster state generator
 *
 * This class contains a 2D rectangular probabilistic cluster state (edges
 * exist only with probability p), of fixed height, which grows one column
 * at a time to the right. It can be used to model the behaviour of a system
 * attempting to implement MBQC on a dynamically generated cluster state.
 *
 * Each node in the graph is indexed in a column-major format, so that the
 * graph can be extended arbitrarily by adding new columns. The object of 
 * the class is to maintain a continuous path through the cluster state, from
 * left to right, which is used as the basis for a logical qubit. The nodes
 * are stored in a NodeWindow object.
 *
 * The class is the base layer for testing the ability of various algorithms
 * to find a viable one-qubit path through the cluster state, The algorithm
 * that is to be used is provided in the template arguments, meaning that an
 * instance of this class is really an instance of a cluster state and an
 * associated pathfinding algorithm.
 *
 * However, it is possible to generate the cluster state without performing
 * the pathfinding algorithm, and it is possible to repeat the pathfinding
 * algorithm with different active window sizes (the set of columns assumed
 * visible to the algorithm at any one time). For probabilistic algorithms,
 * it is possible to repeat the algorithm multiple times on the same cluster,
 * resetting the results before each new run.
 *
 * It is possible to copy construct this class to another class which uses
 * a different algorithm. In that case, the cluster edge data is copied,
 * but obviously not any of the algorithm-specific details (which are not
 * relevant). This allows comparison between different algorithms on the
 * same cluster, without needing to manually copy cluster edge data. 
 *
 * There are a number of utility functions for printing, debugging, writing
 * to file, etc. 
 *
 */
class ProbCluster
{
    using Node = MakeNode<PathNode, DistanceNode>;
    BaseNodeWindow<Node> win;

public:
    ProbCluster(std::size_t height) : win{height} {}
    ProbCluster(std::size_t height, std::size_t width) : win{height,width} {}

    void print() const
	{
	    win.print();
	}

    /// Get the width of the cluster
    [[nodiscard]] std::size_t width() const { return win.width(); }

    /// Get the height of the cluster
    [[nodiscard]] std::size_t height() const { return win.height(); }

    
};


#endif
