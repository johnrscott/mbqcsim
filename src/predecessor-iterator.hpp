/**
 * \brief Iterator for traversing node predecessors
 *
 * 
 * 
 */

#ifndef PREDECESSOR_ITERATOR
#define PREDECESSOR_ITERATOR

#include "node-window.hpp"

class PredecessorSentinel {};

class PredecessorException {};

/**
 * \brief Iterator for traversing node predecessors
 */
template<NodeType Node, bool Base>
class PredecessorIterator
{
    Node * node;
    NodeWindow<Node,Base> & win_;

    /**
     * \brief Node object to point to as the end sentinel
     *
     * The purpose of this object is to act as the pointee for the
     * interator that has been incremented to the end sentinel.
     * It should never be dereferenced.
     *
     * Implementation notes:
     *
     * A height of one is used because height zero will throw an
     * exception. Without the exception checking in BaseNode, this
     * causes illegal instruction.
     */
    Node end_sentinel{0,1};
    
public:
    PredecessorIterator(Node & node_in, NodeWindow<Node,Base> & win)
	: node{&node_in}, win_{win}
	{}

    Node & operator * ()
	{
	    return *node;
	}

    /**
     */
    Node * operator -> ()
	{
	    if (node == &end_sentinel) {
		// std::cerr << "Internal error: Cannot perform operator-> on "
		// 	  << "PredecessorIterator end_sentinel";
		// abort();
		throw PredecessorException{};
	    } else {
		return node;
	    }
	}


    /**
     * \brief Prefix increment operator
     *
     * Be aware that this function moves _backwards_ along the tree. (Being
     * ++, you might expect it to move forwards.) The sense of the ++ is
     * that you are advancing forwards along the reverse path tree. (This
     * use is consistent with the way a reverse iterator works.)
     *
     * This function throws std::out_of_range if an attempt is made to
     * access a node outside the window
     */
    PredecessorIterator & operator++()
	{
	    const long int offset{node->getPredecessorOffset()};
	    if (offset == 0) {
		node = &end_sentinel;
	    } else {
		//try {
		///\todo This is a better interface -- throw the exception and
		/// let the client code handle it as they wish. Make this
		/// consistent across all the iterators (I think only the
		/// RandomAdvanceIterator needs fixing now maybe).
		node = &win_.getNode(*node, offset);
		//} catch (const std::out_of_range & e) {
		// Out of range means the next object is outside
		// the subwindow.
		//node = &end_sentinel;
		//}
	    }
	    return * this;
	} 
    
    bool operator==(const PredecessorIterator & other) {
	return (node == other.node);
    }

    /**
     * \brief Sentinel for end of path
     *
     * Check if the offset is zero. If it is, return true (this is
     * the end of the path).
     */
    bool operator==(const PredecessorSentinel &)
	{
	    //const long int offset{node->getPathSuccessorOffset()};
	    if (node == &end_sentinel) {
		return true;
	    } else {
		return false;
	    }
	}
};

template<typename Node, bool Base>
class PredecessorRange
{
    const PredecessorIterator<Node,Base> it;
    
public:

    PredecessorRange(Node & node, NodeWindow<Node,Base> & win)
	: it{node, win}
	{
	    const long int offset{node.getPredecessorOffset()};
	    if (offset == 0) {
		throw std::logic_error("Node does not have a predecessor. Cannot "
				       "use it as the start of PredecessorRange.");
	    }
	}
    
    auto begin() const
    {
        // The begin is just the pointer to our string.
        return it;
    }
    auto end() const
    {
        // The end is a different type, the sentinel.
        return PredecessorSentinel{};
    }
};

template<NodeType Node, bool Base>
PredecessorIterator<Node,Base> operator+(
    const PredecessorIterator<Node,Base> & node,
    std::size_t offset)
{    
    PredecessorIterator<Node,Base> copy{node};
    for (std::size_t n{0}; n < offset; n++) {
	++copy;
    }
    return copy;
}


// template<typename Node, bool Base>
// bool operator==(const PredecessorSentinel &,
// 		const PredecessorIterator<Node,Base> & it)
// {
//     return (it == PredecessorSentinel{});
// }

#endif
