Testing the program
===================

.. note::

   This section is quite technical -- I plan to come back to it later and make it more readable, but for now I wanted to get down the main points.

Version 0.1 of mbqcsim has incorporated tests from the start, in order to try to make results from the program accurate. Despite the presence of tests, however, checking that the program works is extremely challenging. This is because there are many corner cases in the graph calculations and simulation algorithms which are not obvious, and occur very rarely. It is hard to write tests for these cases without knowing about them already. A few example bugs that have arisen during the development of the program are:

* The invalid calculation of maximal left nodes when a path terminates *before* the final column of the window, but where the terminating node is not the in the right-most column of the path. A graph with this property occurs quite rarely.
* The invalid verification of path simulation using postselected measurement outcomes in the copied verification column. Even though this is an invalid verification technique (the cause of the problem), it nonetheless works and gives the right answer a very large proportion of the time.
* The failure of verification of path simulation based on right nodes, rather than left nodes. Right nodes and left nodes coincide a lot of the time, meaning that the cause of the simulation error is not very obvious. In addition, when right nodes and left nodes do not coincide, the simulation still works because the intervening nodes all lie in the column under consideration. The failure case is when the path backtracks, which occurs very rarely, especially when the edge probability is high. 

I have taken two measures in version 0.1 to try to mitigate the effect of these rare errors:
  
* All random aspects of the program are based on known seeds. This way, random tests that fail can print the failing seed, allowing that case to be repeated and debugged. Once the bug is fixed, depending on the type of edge case, it can be added as a specific test (to ensure the same type of error does not occur again). 
* When I encounter a failure case, I have tried to add an exception to the part of the program that caused the failure to happen (if it was a programming kind of failure, rather than a logic error). That way, if the issue occurs again, it might provide a readable error.
