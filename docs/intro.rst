What is the purpose of this program?
====================================

Previous program versions
=========================

The current version of the program is **Version 0.1**. This section contains a brief overview of previous iterations of the program.

Functional simulator for MBQC-FPGA
**********************************

The main idea in this program -- the use of a recycling quantum simulator -- was developed for the verification of the hardware design in `this repository <https://gitlab.com/johnrscott/mbqc-fpga>`_. That repository contains a draft FPGA design for a control system implementing arbitrary one-qubit gates and the CNOT gate in an idealised photonic quantum computer.

It became clear that the functional verification of that design would be very challenging without a simulator for:

#. Checking that measurement patterns really do what you think; and
#. Checking that the VHDL design really implements the control system for the measurement patterns correctly.

Both these aspects were addressed by creating a simulator which could emulate the behaviour of the VHDL, thereby providing inputs for use in VHDL testbenches. By its nature, the program was highly specific, and not easily usable for any other purpose.

Development of draft version (0.0)
**********************************

The first "proper" version of the current program was designed to improve on the FPGA emulator in two ways:

#. It should support the investigation of incomplete cluster states, with missing edges, and should support simulation of logical qubit paths through this cluster state.
#. It should be able to find paths through an incomplete cluster state.

The main principle of the draft is local storage of node information in a column-major vector of nodes. 

Testing was not included in the draft version.

Development of version 0.1
**************************

Version 0.1 is currently under construction. It is designed to be an improvement over the original draft, incorporating the following new features:

* Testing
* Implementation and usage documentation (you are reading it)
* Easier to add custom algorithms
* Greater degree of safety, sometimes at the expense of optimisation.

The tests aim for 100% line coverage. More thorough tests will be developed in tandem with the development of the program. At the least, tests will cover:

* Bugs encountered during the development of the program;
* Simple use cases of all constructs in the program.

The tests allow basic regression testing when the program is modified. 

Quick introduction to MBQC
==========================

How does the MBQC simulator work?
=================================

How do the graph aspects work?
==============================


