Optimisations
#############

This is a list of optimisations I have performed, and their effects.

* Removing bounds checking from operator[] in Vector in commit 51caddd75a8 improves the speed of pathf by about 10%. Bounds checking is now in at(), which is more consistent with the standard library.
* Changing NodeWindow::neighbours() so that it returns SmallVector (using automatic storage instead of std::vector allocations) shaves about 25% of the execution time of pathf. Roughly halves the execution time of the neighbours() method, which is now limited by NodeWindow::getNode().

License
#######

I support the idea of `Free Software <https://www.gnu.org/philosophy/philosophy.html>`_. As a result, the code and documentation is licensed under the the GPLv3, shown below.

.. literalinclude:: ../COPYING
