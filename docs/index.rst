Welcome to MBQC Simulator's documentation!
==========================================

.. raw:: html
   :file: ../res/path-example.html

.. danger::

   This documentation is under construction. Do not trust anything you read until this notice is gone.
	  
MBQCSIM is a program for analysing and simulating measurement-based quantum computing (MBQC) circuits. This is the main source of documentation for the program. The code is stored in `Gitlab repository <https://gitlab.com/johnrscott/mbqcsim>`_.

.. warning::

   **This is one of those programs where the documentation looks OK but it will be a nightmare to use!**

   This is a program in its very early stages. Although it is designed to be a library, you may find it exceptionally difficult right now to get it to do what you want. This is mainly because I designed it with my own purposes in mind. Over time, I will try to develop it into something more stable. For now, I hope you will find something of interest in the implementation of the program, which I have tried to document here.

The eventual purpose of MBQCSIM is to enable simulation and analyis of measurement patterns, the fundamental "gates" of MBQC. At the moment, it is designed to support arbitrary rectangular cluster states in two dimensions (i.e. a rectangular lattices with arbitrary missing edges).

.. note::
   The two main things MBQCSIM can do are:

   #. Simulate measurement patterns on rectangular arrays of cluster qubits, provided that certain conditions are met; 
   #. Simulate the identity pattern that has been mapped to a path through an incomplete cluster state, for the purpose of analysing the control system necessary for a real implementation of that system.

.. toctree::
   :maxdepth: 2
   :caption: Introduction
	     
   intro

.. toctree::
   :maxdepth: 2
   :caption: Getting started
	     
   start
   
.. toctree::
   :maxdepth: 2
   :caption: Using the program
	     
   usage

.. toctree::
   :maxdepth: 2
   :caption: Python interface
	     
   py_mbqcsim/modules
   
.. toctree::
   :maxdepth: 2
   :caption: Implementation details
	     
   implementation
   pattern-sim
   testing

.. toctree::
   :maxdepth: 2
   :caption: Development

   devel
	     
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
