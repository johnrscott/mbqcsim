Pattern Simulator
#################

The pattern simulator is the top level object for simulating a measurement pattern. The input is a NodeWindow representing a cluster state, which contains the measurement pattern stored locally in PatternNodes. The output is returned in the form of a quantum state, or in the form of a ClusterSim object.

Pattern specification
=====================

Before simulating a measurement pattern, it is necessary to specify it. Patterns are specified by writing to PatternNodes in a NodeWindow. The pattern node P at (x,y) PatternNodes store the following information about a measurement pattern:

* What basis (:math:`XY(\theta)` or :math:`Z`) should the cluster qubit at :math:`(x,y)` be measured in? Use :code:`setXYBasis` and :code:`setZBasis` to set the basis.
* Which byproduct operators are affected by the outcome of the measurement of the cluster qubit at :math:`(x,y)`?  

Implementation notes
====================

The pattern simulator is useful if it can be used as both the basis for verification of general measurement patterns, and also as the basis for the simulation of one-qubit logical qubit paths through an incomplete cluster state.

The main issue relating to the simulation of paths is that byproduct operators can be quite non-local. For example, in a path that snakes backwards past itself, it is necessary not to incorporate the byproduct operators' affect until the simulated path has reached a certain height. Take the following path as an example::

  2 *   * - * - * - * - *
        |
  1 *   * - * - *   *   *
                |
  0 * - * - * - *   *   *
    0   1   2   3   4   5

At column two, the byproduct operators relating to cluster qubits :math:`(1,1)` and :math:`(1,2)` should not be incorporated -- despite the fact that those qubits have been measured (one column ago), and it may already be known how to process those outcomes into byproduct operators. It is only by the simulation of column four that those byproduct operators should be included. This issue appears to contradict the local nature of the PatternSim, which should not need to know anything about the structure of the path in order to process byproduct operators. (In fact, some patterns do not have any "path" associated to them.)

The potential solution is that SubNodeWindows for the simulation of logical qubit paths must begin and end on maximal left-nodes (i.e. *not* just left-nodes). Being a maximal left node, the path is guaranteed to progress to the right, not up and to the left. This means the SubNodeWindow will not be able to incorrectly process byproduct operators in the future of the path, because it *will not know about them*. It is this not-knowing that is the crucial point -- if the pattern knew, then there would have to be a mechanism to record that they should not be included in the PatternSim (this is the mechanism that causes the problem). By not knowing about the byproduct operator updates, the problem is removed.


