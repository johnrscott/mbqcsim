Prerequisites for building
==========================

The most important prerequisite for building mbqcsim is `QSL <https://github.com/lanamineh/qsl>`_, which is a library for simulating quantum circuits. The particular simulator used by mbqcsim is the resizable simulator, which supports the ability to add and remove qubits from a state vector. This simulator is used as the basis for ClusterSim.

To install QSL, follow the instructions outlines `here <https://qsl.readthedocs.io/en/latest/install.html#prerequisites-for-building>`_. Currently, you need to build QSL from source -- it isn't available on any package manager yet.

Compiling and running the program
=================================

The program is built using cmake. After cloning the repository, run the following commands to build the program:

.. code-block:: console
		
   $ mkdir build # Make a folder for the cmake build
   $ cd build/
   $ cmake .. -DCMAKE_BUILD_TYOE=Release
   $ cmake --build .

Once you have built the program, run it using

.. code-block:: console
		
   $ ./bin/v0

The program ``v0`` is defined in the file ``v0/main.cpp``.
   
Tests and debugging
*******************
   
If you want to run the tests, you need to replace the ``cmake ..`` line with

.. code-block:: console
		
   $ cmake .. -DWITH_TESTS=Yes

To perform the tests, run this from inside the ``build/`` directory:

.. code-block:: console
		
   $ ./bin/test-v0

If you want to compile in debugging mode, then use this line instead:
   
.. code-block:: console
		
   $ cmake .. -DCMAKE_BUILD_TYPE=Debug

Debugging mode is useful if you want to see better backtraces in ``gdb ./bin/v0``, for seeing better information in ``valgrind --tool=memcheck ./bin/v0``, etc. 
