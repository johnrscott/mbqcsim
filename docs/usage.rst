Simulating measurement patterns
###############################

To simulate measurement patterns, use the ``patternSim`` function:

.. doxygenfunction:: patternSim

The first argument is a NodeWindow which contains the measurement pattern to be simulated.

Identity patterns
=================
		     
.. code-block:: c++

    #include <iostream>
    #include "pattern-node-2.hpp"
    #include "qubit-node.hpp"
    #include "pattern-sim.hpp"

    using Node = MakeNode<PatternNode2, QubitNode>;

    int main()
    {
	BaseNodeWindow<Node> win{1,3}; // Make a 3x1 window
	setAllEdges(win,true); // Add all edges
	const std::size_t logical_qubit{0}; // Just an index (arbitrary)

	win(0,0).setAsInput(); // Set input qubit
	win(0,0).setBypUpdate(logical_qubit, {0,1}); // Add to z
	win(0,0).setX(); // Add to X
	win(1,0).setBypUpdate(logical_qubit, {1,0}); // Add to x
	win(1,0).setX(); // Add to X
	win(2,0).setAsOutput(logical_qubit); // Set as the output qubit

	// Print the pattern
	win.print<PatternNode2>();

	qsl::Qubits<qsl::Type::Resize,double> q{1}; // Make 1-qubit all-zero state
	auto q_copy{q}; // Copy the state
	q = patternSim(win,std::move(q)); // Apply pattern to q

	// Print the results of measurements in the simulation
	win.print<QubitNode>();

	// Check the measurement pattern simulation worked (expect 0)
	std::cout << "Distance = " << qsl::fubiniStudy(q, q_copy) << std::endl;
    }


Example output from the program is shown below:

    .. raw:: html
       :file: identity-pattern.html

The first  shows the bases of the measurement pattern, and the second part shows the results from the measurements during the simulation.

Arbitrary straight identity patterns can be created using the ``identityPattern`` function:

.. doxygenfunction:: identityPattern

The following example shows the generation of a 9x7 pattern:

.. code-block:: c++

    #include <iostream>
    #include "pattern-sim.hpp"
    #include "meas-patterns.hpp"

    using Node = MakeNode<PatternNode2, QubitNode>;

    int main()
    {
	auto win{identityPattern(7,9,4)};

	// Print the pattern
	win.print<PatternNode2>();

	qsl::Qubits<qsl::Type::Resize,double> q{1}; // Make 1-qubit all-zero state
	auto q_copy{q}; // Copy the state
	q = patternSim(win,std::move(q)); // Apply pattern to q
	win.print<QubitNode>();
	std::cout << "Distance = " << qsl::fubiniStudy(q, q_copy) << std::endl;
    }
		
Example output from the program is:

    .. raw:: html
       :file: identity-pattern-9x7.html


Path search analysis
####################

.. note::
   
   This section contains an analysis of the processing requirements of an ideal model of photonic MBQC using partial cluster states, with a view to placing a lower bound on the difficulty of implementing the model using a digital system.
   
Photonic MBQC is based on the idea that a partial cluster state can be used for quantum computation, provided that logical qubit paths can be found through the partial cluster. An example of a path through a 2D partial cluster state is shown below:

    .. raw:: html
       :file: ../res/random-path-example.html

The measurement pattern (measurement basis settings and byproduct operator update rules) for implementing the identity gate along this path is shown below:
	      
    .. raw:: html
       :file: ../res/random-pattern-example.html

These paths must be found in real time, in a graph which is continually being extended to the right (through the addition of new photonic cluster qubits). At the same time, the graph is continuously being pruned on the left by the measurement of photonic cluster qubits. Therefore, a window (of width :math:`W` and height :math:`H`) of cluster qubits is "alive" at any one time. The cluster qubits inside the window will be called "nodes" below, consistent with the usage in the program. As the window moves one step to the right, at a rate :math:`X_C` (the photonic clock rate), an implementation of photonic MBQC must perform all necessary processing to establish the measurement bases of the cluster qubits in the leftmost column, such that a logical qubit path is propogated through the evolving window of cluster qubits. This processing has the following inputs:

* The success or failure of each individual fusion gate in the newly created rightmost column of the window (called an "edge outcome" below).
* The measurement outcomes from each measured cluster qubit in the leftmost column

.. note::

   There are also additional inputs, if more complicated models of photonic MBQC are adopted. For example, if photon errors are considered, a detector may output additional states other than :math:`|0\rangle` or :math:`|1\rangle`, such as "photon is lost" or "multiple photons detected". These effects are not considered here.

The outputs from the processing of the window include:

* The running byproduct operators associated with the logical qubit
* The measurement outcomes from the final column of the measurement pattern (after the window has moved all the way to the right). Intermediate measurement outcomes are not outputs, but are used in the processing

The following calculations are required to determine the measurement bases of the leftmost column:

* The current path must be extended one column to the right. In doing so, some measurement bases can be determined (i.e. the location of cut-out qubits)
* Adaptive measurement bases of qubits in the leftmost column must be resolved using previously measured qubits.

.. note::

   In this program, only the identity pattern is considered, meaning that no measurement bases need be calculated. This will result in a lower bound on the complexity of the basis-calculating algorithms

Simple implementation model
===========================

.. note::

   This section contains a simple implementation model that will serve as the basis for analysis

* The data about the window of cluster qubits  (i.e. the location of entangled edges) is assumed to be stored in a ring buffer, so that the complexity of adding a new column of data involves writing :math:`2H-1` new edge outcomes, and updating two pointers referring to the start and the end of the active region of the buffer
* Inputs to the program are considered to be present in digital form (i.e. any conversion/amplifying process has already occured). This assumption is necessary because for an initial analysis because of the much greater relative complexity of the analog parts of the system.

Advancing the path
==================

An important part of the model of photonic MBQC is the ability to propagate a path through the partial cluster state. This section describes some methods for doing that.

Window searching
****************

.. note::

   Window searching a potential method to advance the path forward by one column from the leftmost column. If routes are found through the entire window, then one of these can be used to advance the logical qubit path. It is not necessary to use a search method at all (see the next section), but a search method will improve the chance of finding a path.

The most complex part of the algorithm is search for paths, because it potentially involves traversing the entire ring buffer. Possibilities for the search algorithm include

#. A global breadth-first search (GBFS) of the window, starting from the path entry point on the left. This approach has the best chance of finding a path through the window, because the new information added when a new column is added on the right can be used to inform the entire search process. However, it would likely be exceptionally difficult to implement because of the need to visit all :math:`HW` nodes in the window in time :math:`1/X_C` (possibly a very small time).
#. Iterative BFS of the window, where only the newly added column is searched. This would mean that a much smaller number of nodes would need to be visited (although it is not bounded from above by :math:`H`, because the new column could make accessible parts of the window which could not be reached befoore).

.. note::

   Dijkstra's algorithm is not relevant for this problem; it is equivalent to the GBFS because all the edge weights are equal to one. (Edges are either present or not.) In a more complicated model than we consider here, it might be useful to add weights to edges. However, that would increase the difficulty of implementing the algorithm.
   
Shortest Path Tree
------------------

The purpose of searching the window is to find paths through it. However, search algorithms tend to label nodes with distances and predecessors (so that you know if a node is accessible, and where it came from). This construction is called the reverse path tree (RPT). An example of the RPT for a random graph generated by the program, starting from node :math:`(0,3)` is shown below:

    .. raw:: html
       :file: ../res/spt-example.html

Red arrows point to path predecessors. Green dots show accessible nodes.

The ability to progress forwards along a path is not available until the shortest path tree (SPT) has been calculated. This process involves iterating through node predecessors starting from "exit nodes" (nodes in the rightmost column that have been reached), until the root node is reached. At each stage, a node's successor is stored locally inside the node. At this point, a forward path can be found by iterating through node successors. A graphical depiction of the SPT is given by reversing all the arrows in the diagram above.
	      
.. note::

   For search algorithms not based on distances, the concept of "shortest-path" does not make sense. However, the result of iterating over predecessors will still be called the SPT for simplicity. (You could think of SPT standing for "successor path tree" in that case, by analogy with "reverse path tree".)

The GBFS requires the SPT to be calculated from scratch for each new window. The iterative BFS may require less calculation, because some of the SPT may be reusable from the previous window. However, part of the SPT may need to be invalidated as a result of new column information, increasing the complexity of the calculation.
   
In addition to the calculation of successors, another important piece of information is required -- the column right-nodes for each path.

.. note::

   A right-node in a column :math:`x` is a path node :math:`P` (a node on a path) such that all path successors of :math:`P` lie in column :math:`x` or to the right of it. The (unique) minimal right-node is the right-node with the smallest path index (i.e. it occurs before all other right-nodes for column :math:`x`).
	  
Right-nodes are valid end-points for path extensions. They have the property that once a path has reached that point, it cannot backtrack again into the columns to the left. That means that those columns are safe to measure, because it is guaranteed that no more basis modifications need to be made there.

Right nodes are not necessarily the first nodes encountered in a given column, as the example below shows:

    .. raw:: html
       :file: ../res/right-node-example.html

Here, in considering the reverse path from node :math:`(9,4)`, order to allow paths to advance from the root node :math:`(0,3)`, it is necessary to mark node :math:`(1,1)` as the minimal right-node. Otherwise, the path will not know to advance all the way along the snake-like shape, and will get stuck at node :math:`(1,3)`. 

Random path choices
*******************

Instead of searching the window, a random choice could be made for how to extend the path, or some heuristic could be used. For example, the path could be extended forward, up and then forward, or down and then forward, depending on which edges are present. The number of choices to consider here is very large, and will not be discussed in detail further, apart from the simple instances discussed below.

Choosing a path extension
*************************

Once a number of candidates for potential paths has been identified, a choice is made how to extend the path into the next column. One simple option is to choose from the possibilities at random, or simply choose the first possibility. Another option is to choose the path extension which is shortest. A more complicated family of options is to use information from the SPT (for example, which path is most likely to succeed, as measured by number of branch points, etc.)

In creating a path extension, multiple branches may be encountered going from one column to the next, which corresponds to multiple successors in the successor set for a node. A choice must be made at each branch how to proceed.

The following example shows the randomly chosen path extension to column 1, based on the SPT tree shown above.

    .. raw:: html
       :file: ../res/path-advance-example.html

The red arrow is the minimal right-node for column 1. The path extension corresponding to the non-trivial minimal right-node is shown below:

    .. raw:: html
       :file: ../res/right-node-example.html

Processing complexity
=====================

For simplicity, processing complexity is defined roughly in terms of the number of operations that need to be performed for complete a task. For example, it includes the following components:

* How many times a ring buffer entry must be accessed?
* How many times a ring buffer entry must be written?
* How many operations are performed in the calculations relating to the search process (e.g. calculation of distances, or calculation of heuristics).
* How many operations are involved in the calculation of measurement bases
* How many operations are involved in the calculation of byproduct operators

MBQCSIM addresses these questions by emulating the simple implementation model described above, and counting how many times various operations occur. The value that MBQCSIM adds over a theoretical calculation of these quantities is:

#. Due to the various random elements involved in the program, many of the quantities are statistical in nature. Calculating them exactly may either be impossible, may rely on heavily simplifying assumptions, or may be extremely difficult.
#. The emulation of a process demonstrates that the process works, at least in practice. It also makes evident all the steps that are required in the implementation of a process. (Obviously MBQCSIM could itself be wrong, or may have missed important details, important optimisations, etc. It is hoped that the presence of the tested source code and documentation will allow interested users to verify the results for themselves.)
#. The code forms the basis for the functional verification of a hardware implementation of the design. This is important because the construction of a working hardware implementation of photonic MBQC is the **only valid method to establish that photonic MBQC works**.

What follows is a set of investigations into the processing requirements of different parts of the model implementation of photonic MBQC, under various different assumptions. In each case, I have tried to document the intent of the test, how the test was performed, and what were the results.

GBFS ring-buffer writes
***********************

An important property of the implementation is how many times data is written into the ring-buffer. The important feature of writes, over reads, is that they cannot be optimised away (for example, through hardware trickery). Calculated data that must be stored in the ring buffer *must be written at some point*. Reads, on the other hand, could be removed in certain cases (for example, by hardwiring a particular memory location to its destination location in Verilog).

.. note::
   
   Considerations of caching are not relevant for this test. Caching is not considered at all in the model implementation, because it is assumed that the ring-buffer is stored in fast memory (e.g. static RAM, or a platform-dependent fast memory type such as distributed RAM in Xilinx FPGAs).

Data that is written to the ring-buffer during the GBFS includes:

* The distance to all accessible nodes
* The path predecessor of each accessible node
* The path successors of each non-terminal node, obtained during the traversal of the RPT 

In the case of GBFS, each quantity must be calculated afresh for each new window. That means that each quantity must be cleared at the start of the processing of a new window. These clearing writes are also counted in the test.
  
.. note::

   This test analyses the implementation of GBFS included in MBQCSIM. A different implementation will give different (potentially better or worse) results. For the purpose of the test, the model implementation's version of GBFS is assumed to be perfectly emulated by MBQCSIM. It is really a strength of this type of analysis technique that implementation differences between different algorithm implementations can be tested for performance differences.

Implementation overview
-----------------------

The test is implemented by using counters inside the the GBFS class. The state of the counters at the end of a particular program can be interpreted according to the test being run.

The execution time of the program is not a valid measure of processing complexity for these tests, because it runs on an instruction set computer, which is an entirely different type of architecture than the target digital system for the model implementation (a custom hardware design). Even if this could be ignored, execution time (even relative execution time between different subsystems) is not valid, because in many cases, runtime checking has been explicitly preferred to performance. 

Testing
-------

Tests I want to add:
* Hand check counter values
* Check repeatability of results using seeds


Using pathf
===========

``pathf`` is a program which uses the functions in MBQCSIM to analyse the performance of different pathfinding algorithms in terms of the performance counter metrics discussed in the sections above (e.g. memory writes and reads). The type of experiment to be performed is specified using command line arguments, and the results are output in JSON format, for analysis by the script ``pathf.py``.


