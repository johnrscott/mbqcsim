# Folder of experiments involving pathf and esim

To run the scripts in this folder, you need have `pathf` and `esim` installed and accessible on in the `$PATH` variable. The scripts use python3. Execute any of them by running `./scrypt_name.py`. See the scripts themselves for documentation on what they do.
