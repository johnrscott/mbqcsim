from mbqcsim.cache import Cache
from mbqcsim.sweep import Sweep

class CachedSweep:
    '''
    Perform a sweep over the arguments of a program, and get data from the
    program, and cache it, ready for use later. If a sweep is requested that
    is present in the cache, read from the cache instead of re-running the
    program. Data stored in the cache is raw (unprocessed) data, which is
    either a scalar or vector function of the sweep parameters, the repeat
    index, and optionally a set of additional dimensions which is not 
    specified by this class.
    '''

    def __init__(self, ProgClass, seed = None):
        
        self._cache = Cache("cache/")
        self._sweep = Sweep(ProgClass, seed)
        
    def setFixed(self, arg, value):
        '''
        Set the value of a fixed (non-swept) parameter
        '''
        self._sweep.setFixed(arg, value)
        
    def setSweep(self, arg, sweep_values):
        '''
        Add a variable to be swept by passing a value from ProgArgs as the
        first parameter. Set the range of parameters to sweep by passing a list
        of values as the second argument, which is the order in which the 
        parameters will be swept. The values must be compatible with the
        parameter being swept
        
        When the sweep is run, all the sweep variables added using this 
        function will be swept over.
        '''
        self._sweep.setSweep(arg, sweep_values)

    def run(self, ProcessData, note, cache = True):
        '''
        Run the sweep, either by running the program, or by getting cached
        data from a previous run, and return the data.
        '''

        # Get the parameters are create the config file
        self._conf = self._sweep.getConf()

        # Check if data is in the cache.
        if cache:
            try:
                # If so, return it
                print("Found data for this run in the cache, using that.")
                return self._cache.read(self._conf)
            except LookupError:
                # Not found in cache. Continue
                print("Did not find cached results, proceeding with run.")
                pass
            
        # If not, run the sweep, store the data in
        # the cache, and return the data
        results = self._sweep.run(ProcessData)
        results["note"] = note
        return self._cache.write(self._conf, results)
