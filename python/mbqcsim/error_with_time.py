'''
Calculate the error due to noise that accumulates over time using esim

This module contains a class, ErrorWithTime, which is used to investigate how
noise introduces error into the 
'''

from mbqcsim.fidelity import Fidelity
from mbqcsim.esim import EsimArg
import numpy as np


class ErrorWithTime:
    '''
    Compute error with column depth (time)

    Compute error in the identity measurement pattern, realised along a
    dynamically constructed path in an incomplete 2D cluster state, as
    function of various parameters of the cluster state and the pathfinding
    algorithm.

    Attributes
    ----------
    seed : integer
        An integer representing the seed. If you supply the same
        seed with the same values of other parameters, then the same results
        will be generated.
    xp : float:
        The photon clock frequency in Hertz. Defaults to 1GHz.
    n : integer:
        The number of repeats to perform (for the purpose of averaging)
    v_pi : float:
        The voltage at which the modulator causes a pi phase shift.
    v_sigma_max : float:
        The maximum standard deviation of the Gaussian noise in the 
        modulator voltage
    v_sigma_steps : integer:
        The number of points in the v_sigma sweep (from 0 to v_sigma_max)
    t_max : float:
        The results will be simulated up to a maximum time of t_max, defaults
        to 10ns


    data : xarray DataArray
        Contains the results of the experiment (None initially, populated
        after run() has been called.
    '''

    def _volt2rad(self, voltage):
        '''
        Convert a modulator voltage to a value in radians, given
        the V_pi of the modulator.
        '''
        return (np.pi/self.v_pi) * voltage

    def _rad2volt(self, rad):
        '''
        Convert a phase angle to a modulator voltage
        '''
        return (self.v_pi/np.pi) * rad

    
    def __init__(self, seed = None, xp = 1e9, n = 20, v_pi = 5,
                 v_sigma_max = 1e-2, v_sigma_steps = 6, t_max = 10e-9):
        '''
        Construct an instance of the experiment
        '''

        # Store attributes
        self.seed = seed
        self.xp = xp
        self.n = n
        self.v_pi = v_pi
        self.v_sigma_max = v_sigma_max
        self.v_sigma_steps = v_sigma_steps
        self.t_max = t_max
        self.d_max = int(self.xp * self.t_max)
        
        self._fid = Fidelity(self.seed, note="ErrorWithTime")

        # Compute errors in radians
        sigma = [self._volt2rad(v) for v
                 in np.linspace(0, self.v_sigma_max, self.v_sigma_steps)]
        
        # Sweep setup
        self._fid.setFixed(EsimArg.Height, 7)
        self._fid.setFixed(EsimArg.Width, self.d_max)
        self._fid.setFixed(EsimArg.BlockWidth, 10)
        self._fid.setFixed(EsimArg.EdgeProbability, 0.9)
        self._fid.setFixed(EsimArg.Algorithm, "gbfs")
        self._fid.setFixed(EsimArg.YStart, 5)
        self._fid.setFixed(EsimArg.NumRepeats, n)
        self._fid.setSweep(EsimArg.NoiseStdDev, sigma)
        self._fid.setFixed(EsimArg.NoiseMean, 0)
        
    def run(self):
        '''
        Run the experiment

        Returns
        -------
        xarray Dataset
            Containing the results
        '''

        # Run the experiment
        self.data = self._fid.run()["data"]

        # Scale the noise sigma coordinates back to voltages
        self.data = self.data.rename({"NoiseStdDev":"noise_sigma"})        
        self.data = self.data.assign_coords(
            noise_sigma = self._rad2volt(self.data.noise_sigma)
        )
        self.data.noise_sigma.attrs["units"] = "V"
        
        # Average the results over the repeats
        self.data = self.data.mean(dim="n")

        # Convert column to time
        self.data = self.data.rename({"column":"time"})
        self.data = self.data.assign_coords(time=(self.data.time / self.xp))
        self.data.time.attrs["units"] = "s"
        
        # Set attributes
        self.data.attrs["long_name"] = "Average Fidelity"
        return self.data
        
