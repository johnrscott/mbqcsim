'''
Calculate the maximum depth reached by a logical qubit path using pathf

This module contains the class MaxDepthWithProb, that simulates the
pathfinding process and determines the maximum depth reached by the path
as a function of cluster state and algorithm parameters.
'''

from mbqcsim.max_depth import MaxDepth
from mbqcsim.pathf import PathfArg
import numpy as np

class MaxDepthWithProb:
    '''
    Compute the maximum depth reached by the path against edge probability 

    Compute the maximum column (depth) reached by the logical qubit path as
    as function of edge probability and other algorithm parameters.

    Attributes
    ----------
    seed : integer
        An integer representing the seed. If you supply the same
        seed with the same values of other parameters, then the same results
        will be generated.
    n : integer:
        The number of repeats to perform (for the purpose of averaging)
    grid_height : integer
        The height of the grid of cluster qubits used in the experiment.
    d_max : integer
        The maximum depth of the experiment, which corresponds to the width of
        the cluster state being investigated. The max depth reached cannot
        exceed d_max, so it is important to specify d_max large enough so
        that it exceeds the mean maximum column depth (larger d_max will take
        longer to run and take more memory, but there is no other disadvantage
        to using large d_max). One way to establish a suitable value for d_max
        is to run the experiment with n=1 and increase the value of d_max
        until the max_depth reached by the path is less than d_max for
        high probabilities
    p_min : float
        The minimum edge probability to use in the sweep
    p_max : float
        The maximum edge probability to use in the sweep. Due to the large
        amount of time required to simulate IBFS for p=1, it is sometimes a
        good idea to set p_max slightly less than 1.
    p_steps : integer
        The number of steps to use in the edge probability sweep. Probabilities
        will be rounded to 5 decimal places
    bw_min : integer
        The minimum block width (width of the search window for GBFS or IBFS).
        This must be at least 3.
    bw_max : integer
        The maximum block width.
    bw_step_size : integer
        The steps size for the sweep of the block width parameter.

    data : xarray DataArray
        Contains the results of the experiment (None initially, populated
        after run() has been called.
    '''
    
    def __init__(self, seed = None, n = 10, grid_height = 20,
                 d_max = 200, p_min = 0, p_max = 1, p_steps = 11,
                 bw_min = 3, bw_max = 10, bw_step_size = 2):
        '''
        Construct an instance of the experiment
        '''

        if bw_min < 3:
            raise ValueError("bw_min must be at least 3 in MaxDepthWithProb")
        
        # Store attributes
        self.seed = seed
        self.n = n
        self.d_max = d_max
        self.p_min = p_min
        self.p_max = p_max
        self.p_steps = p_steps
        self.bw_min = bw_min
        self.bw_max = bw_max
        self.bw_step_size = bw_step_size
        self.grid_height = grid_height

        # Edge probabilities
        probs = [round(p, 5) for p
                 in np.linspace(self.p_min, self.p_max, self.p_steps)]

        # Block widths
        block_widths = range(self.bw_min, self.bw_max, self.bw_step_size)
        
        self._md = MaxDepth(self.seed, note="MaxDepthWithProb")
        
        # Sweep setup
        self._md.setFixed(PathfArg.Height, self.grid_height)
        self._md.setFixed(PathfArg.Width, self.d_max)
        self._md.setSweep(PathfArg.BlockWidth, block_widths)
        self._md.setSweep(PathfArg.EdgeProbability, probs)
        self._md.setSweep(PathfArg.Algorithm, ["gbfs", "ibfs"])
        self._md.setFixed(PathfArg.YStart, int(self.grid_height/2))
        self._md.setFixed(PathfArg.NumRepeats, self.n)
        
    def run(self):
        '''
        Run the experiment

        Returns
        -------
        xarray Dataset
            Containing the results
        '''

        # Run the experiment
        self.data = self._md.run()["data"]

        # Average the results over the repeats
        self.data = self.data.mean(dim="n")

        # Rename dimensions
        self.data = self.data.rename({"Algorithm":"algorithm"})
        self.data = self.data.rename({"EdgeProbability":"prob"})
        self.data = self.data.rename({"BlockWidth":"block_width"})
        
        # Set attributes
        self.data.attrs["long_name"] = "max_depth"
        return self.data
        
