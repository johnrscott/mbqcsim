import pathlib
from hashlib import sha1
import json
import os
import xarray as xr
import textwrap
from mbqcsim.colour import Colour

class Cache:
    '''
    A class for implementing a cache of JSON results
    '''

    def _getSHA1(self, conf_ds):
        '''
        Compute the  SHA1 hash associated to a config dataset, for using
        as a cache filename. The hash is obtained by serialising the
        (xarray) Dataset conf_ds into JSON, encoding, and then hashing
        the result.
        '''
        d = conf_ds.to_dict() # Create dictionary from dataset
        j = json.dumps(d, sort_keys=True) # Convert to (sorted) JSON
        sha1_hash = sha1(j.encode('utf-8')) # Encode and hash
        return sha1_hash.hexdigest()
        
    '''
    A class for caching results data
    '''

    def __init__(self, cache_folder = "cache"):
        '''
        Create the cache object using the folder cache_folder
        '''
        self._folder = pathlib.Path(cache_folder)
        pathlib.Path(self._folder).mkdir(parents=True, exist_ok=True)


    ## Old version
    # def write(self, conf, data):
    #     '''
    #     Write json data to the cache associated to conf
    #     '''
    #     # Construct the object to write
    #     cachedata = { "conf": conf, "data": data }
    #     # Write the data
    #     filename = pathlib.Path(self._getSHA1(conf) + ".json")
    #     cache_file = self._folder / filename
    #     with cache_file.open("w") as f:
    #         f.write(json.dumps(cachedata))

    def write(self, conf, da):
        '''
        '''

        # Create the dataset from the config dictionary
        ds = xr.Dataset(conf)

        # Make the cachefile name based on the (config-only) dataset
        filename = pathlib.Path(self._getSHA1(ds) + ".nc")

        # Now add the data array to the dataset object
        ds["data"] = da
        ds.to_netcdf(self._folder / filename)

        # For the purposes of using the data, this function also returns the
        # dataset for processing
        return ds
    
    def read(self, conf):
        '''
        Try to read data associated to conf from the cache
        '''

        # Create the dataset from the config dictionary
        ds = xr.Dataset(conf)        
        
        # Check if the file is in the cache
        filename = pathlib.Path(self._getSHA1(ds) + ".nc")
        cache_file = self._folder / filename
        if cache_file.exists():
            # Read and return the file from the cache
            return xr.open_dataset(cache_file)
        else:
            raise LookupError("No data found in cache associated to this config.")

    def readByHash(self, short_hash):
        '''
        Read an entry from the cache by short hash value (any amount of the
        beginning of the filename in the cache, or the short hash printed
        by summarise).
        '''
        pathlist = self._folder.glob(short_hash + "*")
        files = [p for p in pathlist]
        total = len(files)
        if total == 0:
            raise ValueError(f"Could not find {short_hash}*.nc in the cache. "
                             "Are you sure it is there?")
        elif total > 1:
            raise ValueError(f"The short hash \"{short_hash}\" is not enough to "
                             f"uniquely identity a file in the cache. There "
                             f"are {total} matching files: "
                             f"{[p.name+',' for p in files]}.")

        # If you get here, there was one matching file
        return xr.open_dataset(files[0])
        
    def summarise(self):
        '''
        Summarise the contents of the cache by printing a list of experiments
        it contains.
        '''

        pathlist = self._folder.glob('*.nc')
        
        for path in pathlist:

            # Print filename
            print(Colour.green(path.name))

            # Get the dataset and extract the config
            ds = xr.open_dataset(path)
            print("  Metadata: ")
            print(f"    Note={ds['note'].values}")
            
            # Print the fixed parameters
            print("  Fixed params: ")
            msg = ""
            for key, val in ds.data_vars.items():
                if key != "data":
                    # Check if the value is a list or not
                    msg += f"{key}={val.values}, "
            print(textwrap.fill(msg, width=80,
                                initial_indent="    ",
                                subsequent_indent="    "))

            # Print the sweep parameters
            print("  Sweep params: ")
            for key, val in ds.coords.items():
                if key != "data":
                    # Check if the value is a list or not
                    short_list = textwrap.shorten(str(val.values), width=50)
                    print(f"    {key}={short_list}")

            # Print newline
            print()
                    
    def clear(self):
        '''
        Clear the cache (delete all the files)
        '''
        pass
