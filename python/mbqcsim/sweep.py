from mbqcsim.pathf import Pathf, PathfArg
import enum
import itertools
import xarray as xr
import numpy as np

class Sweep:
    '''
    Collect multiple sets of data from pathf by varying one parameter
    and leaving all the others fixed.
    '''
    
    def _checkArg(self, arg):
        '''
        Check arg is a valid argument for the program. If it is invalid,
        raise ValueError
        '''
        if arg not in self._prog.args():
            raise ValueError(f"Argument {str(arg)} is not a valid argument "
                             f"for {self._prog.name()}.")

    def _checkParams(self):
        '''
        Check that the parameters stored in the params member variable
        are complete. Throw a ValueError if not. (User must specify all
        the parameters explicitly.)
        '''
        # First, check that the user has explitly set all the parameters
        for arg in self._prog.args():
            if ((arg not in self._params["sweeps"].keys())
                and (arg not in self._params["fixed"].keys())):
                raise ValueError(
                    (f"You must explicitly set the values "
                     f"of all swept and fixed parameters before "
                     f"calling run(). Missing argument: {arg.name}."))

        
    def __init__(self, ProgClass, seed = None):
        self._prog = ProgClass(seed)
        self._seed = self._prog.getSeed()
        self._sweep_args = []
        self._sweep_values = []
        self._da = xr.DataArray()

        # Params is dictionary containing the parameters to be swept
        # and the parameters that are fixed. The user must explicitly
        # set the swept and fixed parameters, and their values, using
        # setSweep and setFixed
        self._params = { "sweeps": {}, "fixed": {} }
        
    def setFixed(self, arg, value):
        '''
        Set the value of a fixed (non-swept) parameter
        '''
        self._checkArg(arg)
        self._params["fixed"][arg] = value
        self._prog.setArg(arg, value)
        
    def setSweep(self, arg, sweep_values):
        '''
        Add a variable to be swept by passing a value from ProgArgs as the
        first parameter. Set the range of parameters to sweep by passing a list
        of values as the second argument, which is the order in which the 
        parameters will be swept. The values must be compatible with the
        parameter being swept
        
        When the sweep is run, all the sweep variables added using this 
        function will be swept over.
        '''

        self._checkArg(arg)

        # Try to convert sweep_values to a list
        if not isinstance(sweep_values, list):
            try:
                sweep_values = list(sweep_values)
            except:
                raise ValueError("Error: sweep_values must be a list or "
                                 "be convertible to a list")
                
        # Specifically exclude sweeping the number of repeats
        if arg == self._prog.args().NumRepeats:
            raise ValueError("You cannot sweep {arg.name}. It must be provided "
                             "as a fixed parameter")
        
        self._params["sweeps"][arg] = sweep_values
        self._sweep_args.append(arg)
        self._sweep_values.append(sweep_values)
        self._da = self._da.expand_dims({arg.name: sweep_values}).copy()

    def getConf(self):
        '''
        Return the dictionary containing the configuration of the sweep.
        The configuration contains everything needed to repeat the sweep,
        including all the fixed and swept parameters, the seed, and the
        program name. A ValueError is thrown if any of the parameter have
        not been set
        '''

        # Check all the params have been set
        self._checkParams()

        # Flatten the parameters into a single leve dictionary
        conf = {}
        for arg, value in self._params["fixed"].items():
            conf[arg.name] = value
        for arg, value in self._params["sweeps"].items():
            conf[arg.name] = value
        # Also store the seed
        conf["seed"] = self._seed
    
        return conf
        
    def run(self, ProcessData):
        '''
        Run the sweep, processing data using a function process_data.
        The data_process is a function object that takes one argument, 
        which is the output data dictionary from the program run. It should
        return a dictionary. The dictonaries returned by data_process
        are all stored in a list, which is returned by this function. The
        list is ordered in the same order as the sweep parameters, as 
        returned by getSweepValues()
        '''

        # Create a process data object
        pd = ProcessData()

        # Check the user has set all the parameters
        self._checkParams()

        num_repeats = self._prog.getArg(self._prog.args().NumRepeats)
        self._da = pd.setDims(self._da, self._params["fixed"])
        
        for value_set in itertools.product(*self._sweep_values):

            # Set the values of arguments for this run of the program
            sweep_params = dict(zip(self._sweep_args, value_set))
            for prog_arg, value in sweep_params.items():
                self._prog.setArg(prog_arg, value)

            # Run the program
            print(self._prog)
            d = self._prog.run()

            # Create dictionary to specify coordinates
            coords = dict(zip([arg.name for arg in self._sweep_args], value_set))

            # Process the results
            da = pd.process(d, sweep_params)
            self._da.loc[coords] = da
            
        return self._da
    
    def getSweepValues(self):
        '''
        Return the swept params as a list. Used to index the results from
        run().
        '''
        pass
    
    def __str__(self):
        '''
        Return the string representation of the object
        '''
        msg = f"Sweep default command: {str(self._prog)}\n"
        msg += "Sweeps:\n"
        msg += str(self._da)
        return msg
