class Colour:
    '''
    Some colour codes
    '''
    _HEADER = '\033[95m'
    _BLUE = '\033[94m'
    _CYAN = '\033[96m'
    _GREEN = '\033[92m'
    _WARNING = '\033[93m'
    _FAIL = '\033[91m'
    _RESET = '\033[0m'
    _BOLD = '\033[1m'
    _UNDERLINE = '\033[4m'

    def green(string):
        return Colour._GREEN + string + Colour._RESET
    
