#!/usr/bin/python3
import subprocess
import json
import os
import numpy as np
import matplotlib.pyplot as plt
from .esim import Esim
from .cache import Cache

class ErrorDepth:
    '''
    Calculate the error as a function of depth for different
    Gaussian modulator error rates
    '''

    def __init__(self, seed = None):
        '''
        Construct an experiment, optionally specifying a seed
        '''

        # Create the cache
        self._cache = Cache("cache/")

        # Create esim wrapper with seed
        if seed is not None:
            self._esim = Esim(seed)
        else:
            self._esim = Esim() # Generate random seed

        # Set default values
        self._esim.setWidth(2000)
        self._esim.setHeight(8)
        self._esim.setBlockWidth(10)
        self._esim.setEdgeProbability(0.9)
        self._esim.setAlgorithm("gbfs")
        self._esim.setNoiseMu(0.0)
        self._esim.setNoiseSigma(0.0)
        self._esim.setAlgorithm("gbfs")

        # Create the config dictionary
        self._conf = {
            "experiment_name": "error_depth",
            "root_seed": self._esim.getSeed(),
            "sigma": [round(p,8) for p in np.linspace(0, 0.001, 10)]
        }
                
    def setNoiseSigmaRange(self, start, end, num_steps):
        '''
        Create the range of noise sigma values to loop over. Results 
        are rounded to 8.d.p.
        '''
        self._conf["sigma"] = [
            round(p,8) for p in np.linspace(start, end, num_steps)
        ]

    def setNumRepeats(self, num_repeats):
        '''
        Set the number of repeats for each parameter set
        '''
        self._esim.setNumRepeats(num_repeats)
        
    def run(self):

        # Set the default command
        self._conf["default_cmd"] = str(self._esim)
        
        # Try to read data from the cache
        try:
            depth_sets = self._cache.read(self._conf)
            print("Found data in the cache. Skipping to analysis.")
        except LookupError as e:

            # List of lists containining the results for each
            # error signa. Each of these lists contains one list
            # per depth, in increasing order of depth.
            depth_sets = []
            
            # Loop over the sigma values
            for sigma in self._conf["sigma"]:

                # Set the value for sigma
                self._esim.setNoiseSigma(sigma)

                # Print and run
                print(self._esim)
                d = self._esim.run()

                # List of lists containing distance values, indexed
                # by the depth k
                distances = []
                
                # Loop over repeat data
                for rep in d["repeat_data"]:
                    sim_results = rep["sim_results"]
                    end_column = rep["end_column"]
                    for k in range(len(sim_results)):

                        col = sim_results[k]

                        # If there is not yet a list at k, add it
                        if k >= len(distances):
                            distances.append([])

                        # Append the distance 
                        distances[k].append(col["distance"])

                # Store the distance data for sigma
                depth_sets.append(distances)
                    
            # Store the configuration in the outdata
            self._cache.write(self._conf, depth_sets)


        # Loop over the data in the run
        for s in range(len(depth_sets)):

            # Get the set of distances for this sigma
            distances = depth_sets[s]

            print(distances)

            # Compute the mean distance at each depth
            mean_distances = []
            std_distances = []
            for d in distances:
                mean_distances.append(np.mean(d))
                std_distances.append(np.std(d, ddof=1))

            # Plot standard deviations
            mean_distances_np = np.array(mean_distances)
            std_distances_np = np.array(std_distances)
                
            # Plot the results
            plt.plot(np.cos(mean_distances_np),
                     label=self._conf["sigma"][s])


            # plt.fill_between(range(len(mean_distances_np)),
            #                  mean_distances_np + 2*std_distances_np,
            #                  mean_distances_np - 2*std_distances_np,
            #                  alpha = 0.2)
            
        plt.legend()
        plt.show()
