from mbqcsim.cached_sweep import CachedSweep
from mbqcsim.pathf import Pathf, PathfArg
import numpy as np
import xarray as xr

class PathEmulate:

    def __init__(self, seed = None, note = ""):            
        self._cached_sweep = CachedSweep(Pathf, seed)
        self._note = "PathEmulate: " + note
        
    def setFixed(self, arg, value):
        '''
        Set the value of a fixed (non-swept) parameter
        '''
        self._cached_sweep.setFixed(arg, value)
        
    def setSweep(self, arg, sweep_values):
        '''
        Set the range of values of a sweep parameter
        '''
        self._cached_sweep.setSweep(arg, sweep_values)

    def run(self, cache = True):
        '''
        Run the experiment. To temporarily prevent caching, set cache = False
        '''

        # Get the experimental results (either from the cache or by running)
        ds = self._cached_sweep.run(self._ProcessData, self._note, cache)
        
        return ds
        
    class _ProcessData:
        '''
        Get the max column for each repeat
        '''

        def setDims(self, da, params):
            '''
            '''            
            self._params = params

            # The counts dimension contains the various counts recorded
            # by the program. To see what information is available, run
            # pathf -agbfs | jq ".repeat_data[0].path_emulate[0]" and
            # pathf -aibfs | jq ".repeat_data[0].path_emulate[0]". The
            # counts depend on the algorithm, which is set here
            self._count_coords = [
                # For gbfs and ibfs
                "path_succ_writes", "distance_writes",
                "pred_writes", "queue_push", "succ_writes",
                # For ibfs only
                "failed_nodes", "prune_reads", "succ_deletes"
            ]

            dims = {
                # Dimension for repeats
                "n" : self._params[PathfArg.NumRepeats],

                # The block dimension contains data about each search block.
                "block" : self._params[PathfArg.Width],

                # The count dimension stores the counts for each block
                "count" : self._count_coords
            }
            return da.expand_dims(dims).copy()

        def process(self, results, sweep_params):

            # Combine the sweep and fixed params. You need to do
            # this so that all the parameters for this particular
            # run in the sweep are available.
            params = {**self._params, **sweep_params}
    
            # Create a new xarray
            a = np.empty((self._params[PathfArg.NumRepeats],
                          self._params[PathfArg.Width],
                          len(self._count_coords)))
            a[:] = np.nan
            da = xr.DataArray(a, dims=("n", "block", "count"),
                              coords = {"count" : self._count_coords})
            
            # Loop over the repeats
            for n, rep in enumerate(results["repeat_data"]):
                # Loop over the blocks
                for k, block in enumerate(rep["path_emulate"]):
                    width = block["search"]["width"]
                    # Exclude block if its width does not match
                    # param block width, to avoid processing the end
                    # of the search window (where the blocks get smaller)
                    if params[PathfArg.BlockWidth] == width:

                        # Store the count data common to gbfs and ibfs
                        coords = {"n":n ,"block":k,
                                  "count":"path_succ_writes"}
                        da.loc[coords] = (block["path_extend"]
                                          ["counts"]
                                          ["path_succ_writes"])
                        coords = {"n":n ,"block":k,
                                  "count":"distance_writes"}
                        da.loc[coords] = (block["search"]
                                          ["counts"]
                                          ["distance_writes"])
                        coords = {"n":n ,"block":k,
                                  "count":"pred_writes"}
                        da.loc[coords] = (block["search"]
                                          ["counts"]
                                          ["pred_writes"])
                        coords = {"n":n ,"block":k,
                                  "count":"queue_push"}
                        da.loc[coords] = (block["search"]
                                          ["counts"]
                                          ["queue_push"])
                        coords = {"n":n ,"block":k,
                                  "count":"succ_writes"}
                        da.loc[coords] = (block["search"]
                                          ["counts"]
                                          ["succ_writes"])

                        # Store additional count data for ibfs
                        if params[PathfArg.Algorithm] == "ibfs":
                            coords = {"n":n ,"block":k,
                                      "count":"failed_nodes"}
                            da.loc[coords] = (block["search"]
                                              ["counts"]
                                              ["failed_nodes"])
                            coords = {"n":n ,"block":k,
                                      "count":"prune_reads"}
                            da.loc[coords] = (block["search"]
                                              ["counts"]
                                              ["prune_reads"])
                            coords = {"n":n ,"block":k,
                                      "count":"succ_deletes"}
                            da.loc[coords] = (block["search"]
                                              ["counts"]
                                              ["succ_deletes"])

                            
            return da

        
