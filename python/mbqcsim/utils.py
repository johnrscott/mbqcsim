def volt2rad(voltage, v_pi):
    '''
    Convert a modulator voltage to a value in radians, given
    the V_pi of the modulator.
    '''
    return np.pi * voltage/v_pi
