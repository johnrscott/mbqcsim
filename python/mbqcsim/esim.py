from .args import Args
import subprocess
import json
import enum

class EsimArg(enum.Enum):
    Height = "H"
    Width = "W"
    BlockWidth = "B"
    EdgeProbability = "p"
    Algorithm = "a"
    YStart = "y"
    NumRepeats = "n"
    NoiseStdDev = "e"
    NoiseMean = "f"

'''
An object that wraps the esim function

Use one instance of this class for an entire experiment. You may seed the
class in the constructor to repeat results, or provide no seed to generate
a random seed.
'''
class Esim(Args):

    def name(self):
        '''
        The name of the program
        '''
        return "esim"
    
    def _getSeed(self):
        '''
        Obtain a random seed from esim
        '''
        # Run the program once to get a root seed. This does
        # not need any specific arguments, any root seed generated
        # by the program will do.
        stdout = subprocess.check_output(["esim", "-m"])
        j = json.loads(stdout)
        return j["root_seed"]

    def _setArg(self, arg, value):
        ''' Set an arbitrary argument '''
        self._args.setValue(arg, value)

    def _getArg(self, arg):
        ''' Get an arbitrary argument '''
        return self._args.getValue(arg)
        
    def __init__(self, seed = None):
        '''
        Initialise the esim executable and give it sensible default parameters
        '''
        default_args = { "a": "gbfs", "H": 7, "W": 50, "B": 10,
                         "p": 0.9, "n": 5, "e": 0.0, "f": 0.0 }
        self._args = Args(default_args)
        
        # Seed the esim object if a seed was provided, otherwise
        # generate a random seed
        if seed is not None:
            self._setArg("s", seed)
        else:
            self._setArg("s", self._getSeed())
        
    def run(self):
        '''
        Run the esim executable with the arguments stored in this class.
     
        Repeated calls to run() will use the next seed provided by esim 

        return: A dictionary of data returned by this run of the program
        raises: RuntimeError: if the esim command failed
        '''

        try:
            stdout = subprocess.check_output(["esim"] + self._args.asList())
            d = json.loads(stdout)
        except subprocess.CalledProcessError as e:
            raise RuntimeError(e.output)
    
        # Set the next seed value. This must be in the innermost loop
        # (with the call to pathf) so that each run of the program
        # gets a new seed
        self._setArg("s", d["next_seed"])

        return d

    def args(self):
        '''
        Get the enum of valid arguments for this program
        '''
        return EsimArg
    
    def setArg(self, esim_arg, value):
        '''
        Set an esim argument. The esim_arg is a value from the EsimArg enum,
        and the value is its corresponding value
        '''
        self._setArg(esim_arg.value, value)

    def getArg(self, esim_arg):
        '''
        Get an esim argument. The esim_arg is a value from the EsimArg enum,
        and the value is its corresponding value.
        '''
        return self._getArg(esim_arg.value)
    
    # def setWidth(self, width):
    #     '''Set the total window width'''
    #     self._setArg("W", width)

    # def setHeight(self, height):
    #     '''Set the total window height'''
    #     if height > 14:
    #         raise ValueError("Cannot simulate height > 14 using esim")
    #     self._setArg("H", height)

    # def setBlockWidth(self, block_width):
    #     '''Set the block window width'''
    #     self._setArg("B", block_width)

    # def setEdgeProbability(self, prob):
    #     '''Set the edge probability'''
    #     self._setArg("p", prob)

    # def setNumRepeats(self, n):
    #     '''Set the edge probability'''
    #     self._setArg("n", n)

    # def setAlgorithm(self, alg):
    #     '''Set the the search algorithm'''
    #     self._setArg("a", alg)

    # def setNoiseMu(self, mean):
    #     '''Set the mean Gaussian modulator error'''
    #     self._setArg("f", mean)

    # def setNoiseSigma(self, sigma):
    #     '''Set the the standard deviation Gaussian modulator error'''
    #     self._setArg("e", sigma)
        
    def getSeed(self):
        '''
        Get the seed 
        
        You may _not_ set the seed, apart from in the constructor. Do not 
        try. Whatever you end up doing with that, it will probably be wrong)
        '''
        return self._getArg("s")
        
    '''
    Print the command to be run
    '''
    def __str__(self):
        return "esim " + str(self._args)
        
