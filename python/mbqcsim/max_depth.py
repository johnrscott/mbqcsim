from mbqcsim.cached_sweep import CachedSweep
from mbqcsim.pathf import Pathf, PathfArg
import numpy as np
import xarray as xr

class MaxDepth:
    '''
    Get the maximum column depth reached by the path using pathf, as
    a function of arbitrary swept parameters of pathf. The result is
    an xarray dataset containing a "data" field containing the DataArray
    of results.

    Tips for performing this experiment

    Don't use a very large number of samples (use n = 500 or n = 1000), unless
    you want to wait ages for the results. Narrow the probability window to
    the range of interest (0.4 to 0.9 roughly). Split that range up into many
    steps (say 30 or 40) to get a smoothish plot). Bring the total window width
    down to get the results faster (say W = 50 or 100), because the results
    do not change too much with window width.
    '''

    def __init__(self, seed = None, note = ""):            
        self._cached_sweep = CachedSweep(Pathf, seed)
        self._note = "MaxDepth: " + note
        
    def setFixed(self, arg, value):
        '''
        Set the value of a fixed (non-swept) parameter
        '''
        self._cached_sweep.setFixed(arg, value)
        
    def setSweep(self, arg, sweep_values):
        '''
        Set the range of values of a sweep parameter
        '''
        self._cached_sweep.setSweep(arg, sweep_values)

    def run(self, cache = True):
        '''
        Run the experiment. To temporarily prevent caching, set cache = False
        '''

        # Get the experimental results (either from the cache or by running)
        ds = self._cached_sweep.run(self._ProcessData, self._note, cache)

        # Set attributes
        ds["data"].attrs["long_name"] = "max_depth"
        
        return ds
        
    class _ProcessData:
        '''
        Get the max column for each repeat
        '''

        def setDims(self, da, params):
            '''
            Now need to add in an extra dimension for the column. The 
            params argument holds the values of the fixed params of the
            sweep.
            '''
            self._params = params
            dims = {
                "n" : self._params[PathfArg.NumRepeats],
            }
            return da.expand_dims(dims).copy()

        def process(self, results, sweep_params):
            data = [rep["max_column"] for rep in results["repeat_data"]]
            return xr.DataArray(data, dims=("n")).copy()

        
