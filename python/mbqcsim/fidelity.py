from mbqcsim.cached_sweep import CachedSweep
from mbqcsim.esim import Esim, EsimArg
import numpy as np
import xarray as xr

class Fidelity:
    '''
    Perform a parameter sweep using esim, extract the fidelity at each column,
    cache the results, and return the data as an xarray Dataset. The dataset
    contains a field called "data" which contains the results of the sweep as
    a DataArray. The number of dimensions is determined by the number of 
    sweeps that were selected.
    
    If cached data is present for a run, return that instead. Cached data
    will be returned if a run of the experiment will produce exactly the 
    same results (i.e. all parameters and seeds determining the run are
    equal).
    '''

    def __init__(self, seed = None, note = ""):            
        self._cached_sweep = CachedSweep(Esim, seed)
        self._note = "Fidelity: " + note

    def setFixed(self, arg, value):
        '''
        Set the value of a fixed (non-swept) parameter
        '''
        self._cached_sweep.setFixed(arg, value)
        
    def setSweep(self, arg, sweep_values):
        '''
        Set the range of values of a sweep parameter
        '''
        self._cached_sweep.setSweep(arg, sweep_values)

    def run(self, cache = True):
        '''
        Run the experiment. To temporarily prevent caching, set cache = False
        '''

        # Get the experimental results (either from the cache or by running)
        ds = self._cached_sweep.run(self._ProcessData, self._note, cache)

        # Set attributes
        ds["data"].attrs["long_name"] = "fidelity"
        
        return ds
        
    class _ProcessData:
        '''
        Get the fidelity of the simulation at each column of each repeat.
        '''
        
        def setDims(self, da, params):
            '''
            Now need to add in an extra dimension for the column. The 
            params argument holds the values of the fixed params of the
            sweep.
            '''
            self._params = params
            dims = {
                "n" : self._params[EsimArg.NumRepeats],
                "column" : self._params[EsimArg.Width]
            }
            return da.expand_dims(dims).copy()

    
        def process(self, results, sweep_params):
            # Create a new xarray
            a = np.empty((self._params[EsimArg.NumRepeats],
                          self._params[EsimArg.Width]))
            a[:] = np.nan
            da = xr.DataArray(a, dims=("n", "column"))
            # Loop over all the repeats in the experiment, computing the
            # average for each repeat.
            for n, rep in enumerate(results["repeat_data"]):
                for k, col in enumerate(rep["sim_results"]):
                    try:
                        da[n,k] = np.cos(col["distance"])
                    except KeyError as e:
                        # Continue to the next value
                        pass

            return da
