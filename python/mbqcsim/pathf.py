from mbqcsim.args import Args
import subprocess
import json
import enum

class PathfArg(enum.Enum):
    Height = "H"
    Width = "W"
    BlockWidth = "B"
    EdgeProbability = "p"
    Algorithm = "a"
    YStart = "y"
    NumRepeats = "n"
    
class Pathf:
    '''
    An object that wraps the pathf function

    Use one instance of this class for an entire experiment. You may seed the
    class in the constructor to repeat results, or provide no seed to generate
    a random seed.
    '''
    
    def _getSeed(self):
        '''
        Obtain a random seed from pathf
        '''
        # Run the program once to get a root seed. This does
        # not need any specific arguments, any root seed generated
        # by the program will do. (Better to use pathf to get a
        # seed than python, because it does it properly.)
        stdout = subprocess.check_output(["pathf", "-m"])
        j = json.loads(stdout)
        return j["root_seed"]

    def _setArg(self, arg, value):
        ''' Set an arbitrary argument '''
        self._args.setValue(arg, value)

    def _getArg(self, arg):
        ''' Get an arbitrary argument '''
        return self._args.getValue(arg)

    def __init__(self, seed = None):
        '''
        Initialise the pathf executable and give it sensible default parameters
        '''
        default_args = { "a": "gbfs", "H": 10, "W": 50, "B": 10,
                         "p": 0.9, "n": 5 }
        self._args = Args(default_args)
        
        # Seed the pathf object if a seed was provided, otherwise
        # generate a random seed
        if seed is not None:
            self._setArg("s", seed)
        else:
            self._setArg("s", self._getSeed())
            
    def run(self):
        '''
        Run the pathf executable with the arguments stored in this class.
     
        Repeated calls to run() will use the next seed provided by pathf

        return: A dictionary of data returned by this run of the program
        raises: RuntimeError: if the pathf command failed
        '''
        try:
            stdout = subprocess.check_output(["pathf"] + self._args.asList())
            d = json.loads(stdout)
        except subprocess.CalledProcessError as e:
            raise RuntimeError(e.output)
        
        # Set the next seed value. This must be in the innermost loop
        # (with the call to pathf) so that each run of the program
        # gets a new seed
        self._setArg("s", d["next_seed"])

        return d

    def args(self):
        '''
        Get the enum of valid arguments for this program
        '''
        return PathfArg
    
    def setArg(self, pathf_arg, value):
        '''
        Set a pathf argument. The pathf_arg is a value from the PathfArg enum,
        and the value is its corresponding value
        '''
        self._setArg(pathf_arg.value, value)

    def getArg(self, pathf_arg):
        '''
        Get a pathf argument. The pathf_arg is a value from the PathfArg enum,
        and the value is its corresponding value.
        '''
        return self._getArg(pathf_arg.value)
        
    def getSeed(self):
        '''
        Get the seed 
        
        You may **not** set the seed, apart from in the constructor. Do not 
        try. Whatever you end up doing with that, it will probably be wrong
        '''
        return self._getArg("s")
        
    def __str__(self):
        '''
        Print the command to be run
        '''
        return "pathf " + str(self._args)
        
