#!/usr/bin/python3
from mbqcsim.pathf import Pathf, PathfArg
from mbqcsim.depth_with_prob import MaxDepthWithProb
from mbqcsim.memory_ops import MemoryOperations
from mbqcsim.error_with_time import ErrorWithTime
import matplotlib.pyplot as plt

def fig9():
    '''
    Figure 9: Average maximum path depth achieved using GBFS
    '''
    e = MaxDepthWithProb(seed=12406134726338567098, p_min=0.4,
                         p_max=1, p_steps=25, n=1000, bw_min=3, bw_max=15,
                         bw_step_size=1, d_max=2000)
    da = e.run()
    print(da)
    da.sel(algorithm="gbfs").plot.line(x="prob", marker="o", markerfacecolor='none')
    plt.show()

def fig10():
    '''
    Figure 10: Average maximum path depth achieved using IBFS
    '''    
    e = MaxDepthWithProb(seed=12406134726338567098, p_min=0.4,
                         p_max=1, p_steps=25, n=1000, bw_min=3, bw_max=15,
                         bw_step_size=1, d_max=2000)
    da = e.run()
    print(da)

    ax = plt.gca()
    ax.set_ylim([-10, 100]) # Optionally change the y-limit here

    da.sel(algorithm="ibfs").plot.line(x="prob", marker="o", markerfacecolor='none')
    plt.show()

def fig11():
    '''
    Figure 11: Average number of predecessors written to memory
    during each block search process when using GBFS
    '''
    e = MemoryOperations(seed = 6903564118784409788,
                         p_min=0, p_max=1, p_steps=21, n=1000,
                         bw_min=3, bw_max=15,
                         bw_step_size=1, d_max=100)
    da = e.run()
    print(da)
    da.sel(algorithm="gbfs",mem_op="pred_writes").plot.line(x="prob", marker="o",
                                                            markerfacecolor='none')
    plt.show()
    
def fig12():
    '''
    Figure 12: Average number of predecessors written to memory
    during each block search process when using IBFS
    '''
    e = MemoryOperations(seed = 6903564118784409788,
                         p_min=0, p_max=1, p_steps=21, n=1000,
                         bw_min=3, bw_max=15,
                         bw_step_size=1, d_max=100)
    da = e.run()
    print(da)
    da.sel(algorithm="ibfs",mem_op="pred_writes").plot.line(x="prob", marker="o",
                                                            markerfacecolor='none')
    plt.show()

def fig14():
    ''''
    Figure 14: average fidelity of the logical qubit, as a function of
    elapsed time. Note that the paper version uses
    '''
    e = ErrorWithTime(seed=18429267364050288035, n=1000, v_pi=1, v_sigma_max=0.5e-2,
                      v_sigma_steps=24, xp=1e9, t_max=1e-6)
    da = e.run()
    print(da)

    da = da.assign_coords(time=(1e6 * da.time))
    da.time.attrs["units"] = "us"

    # Scale voltage to millivolts
    da = da.assign_coords(noise_sigma=(1e3 * da.noise_sigma))
    da["note"] = "Fidelity with time, V_pi=1V"

    da.sel(time=slice(None,None,50)).plot(x="time")
    plt.show()

    
