'''
Function argument class
'''

class Args:

    def __init__(self, default_params_dict):
        ''' Initialise the class '''
        self.arg_dict = default_params_dict

    def setValue(self, short_name, value):
        ''' Set an argument '''
        self.arg_dict[short_name] = value

    def getValue(self, short_name):
        ''' Get an argument '''
        return self.arg_dict[short_name]

    def asList(self):
        arg_list = []
        for short_name, value in self.arg_dict.items():
            arg_list.append("-" + short_name + str(value))
        return arg_list

    def __str__(self):
        arg_string = ""
        for short_name, value in self.arg_dict.items():
            arg_string += "-" + short_name + str(value) + " "
        return arg_string
