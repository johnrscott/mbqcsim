## MBQC Simulator 

This repository contains a library and two example utilities (`pathf` and `esim`) for analysing and simulating measurement-based quantum computing (MBQC) circuits on complete or incomplete rectangular cluster states. The program was developed for checking the validity of measurement patterns and analysing the behaviour of algorithms for photonic quantum computing using incomplete cluster states. The code is work in progress.

### What is here

The two programs that you can compile and use are called `pathf` (for pathfinding analysis) and `esim` (for error simulation). You can also use any other aspect of the library, which provides functions for simulating other more general measurement patterns (not just paths). 

### Installation

You can compile this program on **Ubuntu 20.04 LTS** or more recent.

You need to install the quantum simulator QSL first -- instructions are provided [here](https://github.com/lanamineh/qsl). Then you can clone this repository and build the program by running the following commands from the root directory of `mbqcsim`:

You will also need to install some prerequisites before building the program (this list is currently incomplete -- I'll come back to this soon to make sure all the dependencies are thoroughly worked out)

```bash
# For the main build process
sudo apt install lcov python3 python3-pytest
# For the python library
python3 -m pip install xarray matplotlib netCDF4
```

Finally, you can build and install the program using.

```bash
mkdir build # Make a build directory
cd build # Change to the build directory
CC=gcc-10 CXX=g++-10 cmake .. # Configure the build to use g++-10
cmake --build . # Build the library
sudo cmake --install . # Install pathf and esim
```

### Usage

MBQCSIM comprises a library and some analysis programs. Currently, the library is under development, and is not easily usable, although I hope to fix that in the future. The analysis programs are described below.

#### pathf

`pathf` is a program for analysing the behaviour of various pathfinding algorithms attempting to find logical qubit paths in an incomplete rectangular 2D cluster state. It is controlled via command line arguments, and provides output in JSON format. The synopsis is given below:

```
$ ./bin/pathf -h
OPTIONS
    -h, --help
      display this help and exit

    -s, --seed
      the root_seed used to perform the experiment; argument type integer

    -m, --minimal
      Only store output information necessary for repeating the experiment; no argument

    -i, --infile
      Provide a minimal JSON input file to set program options; argument type string

    -a, --algorithm
      which algorithm to use for the experiment; argument type string

    -H, --window-height
      the height of the window being analysed; argument type integer

    -y, --y-start
      the starting y-coordinate of the path; argument type integer

    -W, --window-width
      the width of the window being analysed; argument type integer

    -B, --block-width
      the width of the active search block; argument type integer

    -p, --edge-probability
      The edge probability in the 2D graph; argument type double

    -n, --num-repeats
      Number of times to repeat the experiment; argument type integer

    -o, --output-file
      The relative file path in which to store results; argument type string
```

To get a flavour of what `pathf` does, run `pathf -d`. You will step through the calculation of paths in a particular (default sized) window.

A few examples are provided below:

```bash
# Analyse a 20x200 cluster state containing random edges with probability, 
# 90%, using the global breadth-first search algorithm with block size of 10;
# repeat the experiment 100 times, and store the results in the file out.json
./bin/pathf -a gbfs -H20 -W200 -B10 -p0.9 -n100 -o out.json

# Analyse a 40x1000 cluster state containing random edges with probability, 
# 80%, using the incremental breadth-first search algorithm with block size 
# of 20; repeat the experiment 10 times, and print the results to stdout.
./bin/pathf -H40 -W1000 -B20 -p0.85 -n10
```

In analysing the output from `pathf`, the utility `jq` is very useful. It will probably be already installed, but if not, you can obtain it using `sudo apt install jq`. `jq` provides easy inspection of JSON files. For example, by piping the results from `pathf` to `jq`, the results can be inspected directly:

```bash
$ ./bin/pathf -H20 -W200 -B10 -p0.9 -m | jq
{
  "alg": "gbfs",
  "block_width": 10,
  "edge_probability": 0.9,
  "num_repeats": 1,
  "root_seed": {
    "seed": 17779220936506340000
  },
  "window_height": 20,
  "window_width": 200,
  "y_start": 5
}
```

The `-m` option caused the main output to be surpressed, but the results can be recreated by passing the same arguments along with the seed. The result is very big, but `jq` can be used to inspect the keys:

```bash
$ ./bin/pathf -H20 -W200 -B10 -p0.9 -s 17779220936506340000 | jq keys
[
  "alg",
  "block_width",
  "edge_probability",
  "num_repeats",
  "repeat_data",
  "root_seed",
  "window_height",
  "window_width",
  "y_start"
]
```

From there, anything in the JSON output can be inspected, for example, the performance counters for one of the search blocks:

```bash
$ ./bin/pathf -H20 -W200 -B10 -p0.9 -s 17779220936506340000 | jq .repeat_data[0].path_emulate[3].search
{
  "counts": {
    "distance_writes": 399,
    "pred_writes": 399,
    "queue_push": 399,
    "succ_writes": 1083
  },
  "height": 20,
  "width": 10
}
```

At any stage, the keys can be inspected using a command like `jq ".repeat_data[0]|keys"`. (You will need the quotes.) See `man jq` for a thorough description of how to use `jq`.

The results from `pathf` can be analysed in a number of ways, but one simple approach is to pipe the results to a simple analysis script called `pathf.py`:

```bash
# Compute summary statistics from the search
./bin/pathf -H20 -W200 -B10 -p0.9 -s 17779220936506340000 | ./bin/pathf.py
```

#### esim

`esim` is a program for the analysis of errors that arise from noise in the modulator voltages required to implement photonic MBQC. The synopsis of the program is provided below:

```
$ ./bin/esim -h
OPTIONS
    -h, --help
      display this help and exit

    -v, --version
      Display version information and exit; no argument

    -s, --seed
      the root_seed used to perform the experiment; argument type integer

    -m, --minimal
      Store output information necessary for repeating the experiment, and exit immediately; no argument

    -d, --debug
      Step through the calculation as it is performed; no argument

    -i, --infile
      Provide a minimal JSON input file to set program options; argument type string

    -a, --algorithm
      which algorithm to use for pathfinding in the experiment; argument type string

    -H, --window-height
      the height of the window being analysed; argument type integer

    -y, --y-start
      the starting y-coordinate of the path; argument type integer

    -W, --window-width
      the width of the window being analysed; argument type integer

    -B, --block-width
      the width of the active search block; argument type integer

    -p, --edge-probability
      The edge probability in the 2D graph; argument type double

    -e, --error-sigma
      The standard deviation of noise added to measurements (rad); argument type double

    -f, --error-mu
      The mean of the Gaussian noise added to measurements (rad); argument type double

    -e, --error-sigma
      The standard deviation of noise added to measurements (rad); argument type double

    -n, --num-repeats
      Number of times to repeat the experiment; argument type integer

    -o, --output-file
      The relative file path in which to store results; argument type string
```

Many options are the same as `pathf`, and have the same meaning. The output format is also JSON, and can be analysed in the same way as for `pathf`, using `jq`.

### Tests

I have tried to test the code thoroughly. Two steps I have take to improve the validity of the program output are listed below:

- Most of the code is covered by tests, in the sense of line coverage (the metric reported at the top of the repository). Most of the basic tests (especially for lower level features of the code), are designed from the bottom up, and test every aspect of the program. However, for higher level bits of the code, the tests are more in the style of end-to-end testing, where a test is performed on a larger chunk of the code and the correct results are used to justify the working of that block.
- Nearly every random action taken in the program (and everything calculated by `pathf` and `esim`) is repeatable, by providing seeds. Repeatability based on seeds provides two benefits. Firstly, it allowed the isolatation and fixing of many edge cases in the behaviour of the program. Secondly, it allows results from the program to be re-generated and re-analysed, which will improve the likelihood of finding any problems with it. In addition, large amounts of data can be effectively stored in a very small space by just storing the configuration and seeds (something that both `pathf` and `esim` do).

To run the tests, you need to compile with `-DWITH_TESTS=yes`. Then run `./run-tests` from the build directory. If they pass, that means that all the c++ code works (within the scope of what is tested), and that the output from `pathf` and `esim` agrees with a set of hardcoded test outputs.

### Reproducing paper figures

The paper [Timing constraints due to real-time graph traversal algorithms on incomplete cluster states in photonic measurement-based quantum computing](https://arxiv.org/abs/2208.14938) uses the code in this repository. Some of the figures can be reproduced by following the instructions below.

These steps were performed on a clean installation of Ubuntu. These instructions were tested on an Ubuntu 22.04 virtual machine, but should work on any relatively recent version of Ubuntu.

```bash
# These instructions assume that you perform all operations
# starting in the home (~) directory. If you use a different
# directory, you may need to modify some of the commands below
cd ~

# Install required packages
sudo apt update
sudo apt install g++ cmake lcov python3 python3-pytest python3-pip

# Clone the QSL (quantum simulator) repository
git clone https://github.com/lanamineh/qsl.git
# This test used the following commit (master branch):
git checkout e7f1f96339
# Build and install the simulator
cd qsl/ && mkdir build && cd build/
cmake ..
cmake --build .
sudo cmake --install .

# Now clone and build this (mbqcsim) repository
cd ~
git clone https://gitlab.com/johnrscott/mbqcsim.git
# This test used the following commit (master branch)
git checkout cc5a0bf5b964e
cd mbqcsim/ && mkdir build && cd build/
cmake .. -DWITH_TESTS=yes
cmake --build .
# Run the tests to ensure the programs work
./run-tests
sudo cmake --install .

# Run the following command to reproduce the failure case shown in Figure 8.
# The program will run interactively and show each stage of the search.
pathf -s3996592937216137949 -d -aibfs -p0.9 -B4 -H9 -W10000

# The other figures require the python package (python/mbqcsim). In order
# to make this accessible to python, add a line like the following to 
# your ~/.bashrc. Make sure you replace the path with the location of the
# python folder in this repository.
#
# export PYTHONPATH=/home/ubuntu/mbqcsim/python/
#
# Then reload the profile
source ~/.bashrc

# Before running the python code, install required packages
python3 -m pip install xarray matplotlib netCDF4

# Figures 9, 10, 11, 12 and 14 may be reproduced by calling the corresponding
# python functions located in python/mbqcsim/paper.py. The scripts will
# create a cache/ folder which stores the simulation results. Running the
# functions from the same directory will ensure that the cache results are
# used the second time the experiments are performed (useful because they
# take a long time to run). These instructions assume everything is run
# from the home folder (~). Note that data is only stored in the cache
# after a run has successfully completed; partial results are not stored.
cd ~

# Open the file scripts/paper-figures.py and uncomment one of the figure
# function calls. Then run (from ~) the following command, which starts
# the run in the background (since it is long running).
nohup mbqcsim/scripts/paper-figures.py &

# View the nohup.out file to see the progress of the simulation. Note that
# the nohup.out file may contain no output for a while due to buffering.
# Once the process is finished, it will plot the graph (if graphics is 
# available). (The script is finished once the .nc file is written to the 
# cache folder.) If graphics is not available, then copy the cache folder
# to a location where graphics is available, and run the same script. 

# To view what is stored in the cache, use the summarise script.
cd ~
./mbqcsim/scripts/summarise.py

# Approximate runtimes
# (using a 2-cpu 8GiB-memory multipass instance on a small server)
#
# fig9() - 13 hours
# fig10() - same dataset as fig9()
# fig11() - 4 hours
# fig12() - same dataset as fig11()
# fig14() - 14 hours
```
