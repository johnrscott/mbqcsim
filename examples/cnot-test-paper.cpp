/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file cnot-test-paper.cpp
 * \brief CNOT pattern test
 *
 * Test the reduced CNOT
 * 
 */

#include <qsl/qubits.hpp>
#include <qsl/utils.hpp>
#include <iostream>

/**
 * \brief Measure the end qubit in a particular basis and remove it
 *
 * \param phi The measurement angle
 * \param s The measurement setting, which adds a sign to the angle
 */
unsigned measure(qsl::Qubits<qsl::Type::Resize, double> & q,
		 double phi, unsigned s, unsigned qubit)
{
    
    // Measure and remove qubit 0 in a basis depending on phi and s
    if (s == 0) {
	q.rotateZ(qubit, -phi+M_PI/2);
    } else if (s == 1) {
	q.rotateZ(qubit, phi+M_PI/2);
    } else {
	throw std::logic_error("Unrecognised measurement setting");
    }
    q.rotateX(qubit, M_PI/2); // X-rotation
    return q.measureOut(qubit); // Perform the measurement
}


int main ()
{
    const unsigned nqubits{ 15 };
    qsl::Qubits<qsl::Type::Resize, double> q{nqubits};

    // Make the equal superposition state
    for (std::size_t n = 0; n < nqubits; n++) {
        q.hadamard(n);
    }

    // Make the cluster state using controlZ

    // Control row
    q.controlZ(0,1);
    q.controlZ(1,2);
    q.controlZ(2,3);
    q.controlZ(3,4);
    q.controlZ(4,5);
    q.controlZ(5,6);

    // Vertical line
    q.controlZ(3,7);
    q.controlZ(7,11);
    
    // Target row
    q.controlZ(8,9);
    q.controlZ(9,10);
    q.controlZ(10,11);
    q.controlZ(11,12);
    q.controlZ(12,13);
    q.controlZ(13,14);

    // Measure out
    const double X_BASIS{ 0 };
    const double Y_BASIS{ M_PI/2 };

    // Measure control row
    unsigned s0 = measure(q, X_BASIS, 0, 0);
    unsigned s1 = measure(q, Y_BASIS, 0, 0);
    unsigned s2 = measure(q, Y_BASIS, 0, 0);
    unsigned s3 = measure(q, Y_BASIS, 0, 0);
    unsigned s4 = measure(q, Y_BASIS, 0, 0);
    unsigned s5 = measure(q, Y_BASIS, 0, 0);

    // Measure middle qubit
    unsigned s7 = measure(q, Y_BASIS, 0, 1);
    
    // Measure target row
    unsigned s8 = measure(q, X_BASIS, 0, 1);
    unsigned s9 = measure(q, X_BASIS, 0, 1);
    unsigned s10 = measure(q, X_BASIS, 0, 1);
    unsigned s11 = measure(q, Y_BASIS, 0, 1);
    unsigned s12 = measure(q, X_BASIS, 0, 1);
    unsigned s13 = measure(q, X_BASIS, 0, 1);

    // Compute byproduct operators
    unsigned x_c = s1 ^ s2 ^ s4 ^ s5;
    unsigned z_c = 1 ^ s0 ^ s2 ^ s3 ^ s4 ^ s7 ^ s8 ^ s10;
    unsigned x_t = s1 ^ s2 ^ s7 ^ s9 ^ s11 ^ s13;
    unsigned z_t = s8 ^ s10 ^ s12;

    // Print byproduct ops
    std::cout << "x_c = " << x_c << std::endl;
    std::cout << "z_c = " << z_c << std::endl;  
    std::cout << "x_t = " << x_t << std::endl;
    std::cout << "z_t = " << z_t << std::endl;
  
    // Correct the state
    if (x_c == 1) {
    	q.pauliX(0);
    }
    if (z_c == 1) {
    	q.phase(0,M_PI);
    }
    if (x_t == 1) {
    	q.pauliX(1);
    }
    if (z_t == 1) {
    	q.phase(1,M_PI);
    }
    
    q.print();
    
    qsl::Qubits<qsl::Type::Resize, double> q1{2};
    q1.hadamard(0);
    q1.hadamard(1);
    q1.controlNot(0,1);

    std::cout << "Distance = " << qsl::fubiniStudy(q.getState(), q1.getState())
	      << std::endl;
}
