/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file one-qubit-cut-out.cpp
 * \brief Arbitrary one-qubit gate surrounded by cut-out qubits
 *
 */

#include <qsl/qubits.hpp>
#include <qsl/utils/quantum.hpp>
#include <qsl/utils/random.hpp>

#include <iostream>

int main()
{
    using Sim = qsl::Qubits<qsl::Type::Resize, double>;

    // The one-qubit gate is performed on a pattern of five
    // logical qubits

    // Need three rows of three qubits. The middle row
    // is the actual gate, and the surrounding two rows
    // are where qubits will be cut out. Qubit pattern:
    //
    // 0 - 3 - 6 - 9  - 12
    // -   -   -   -    -
    // 2 - 5 - 8 - 11 - 14
    // -   -   -   -    -
    // 1 - 4 - 7 - 10 - 13
    //
    // The order is so that you're always measuring
    // qubit zero
    //
    // Dashes mean CHPASE gates
    Sim q{ 15 };

    ///\todo Change phase to rotateZ to avoid sign problem
    
    // Make equal superposition state
    for (unsigned k = 0; k < 15; k++) {
	q.hadamard(k);
    }

    // Make the cluster state (vertical entanglement)
    q.controlZ(0,2); // CZ
    q.controlZ(2,1); // CZ
    q.controlZ(3,5); // CZ
    q.controlZ(5,4); // CZ
    q.controlZ(6,8); // CZ
    q.controlZ(8,7); // CZ
    q.controlZ(9,11); // CZ
    q.controlZ(11,10); // CZ
    q.controlZ(12,14); // CZ
    q.controlZ(14,13); // CZ

    // Make the cluster state (horizontal entanglement)
    q.controlZ(0,3); // CZ
    q.controlZ(3,6); // CZ
    q.controlZ(6,9); // CZ
    q.controlZ(9,12); // CZ
    q.controlZ(2,5); // CZ
    q.controlZ(5,8); // CZ
    q.controlZ(8,11); // CZ
    q.controlZ(11,14); // CZ
    q.controlZ(1,4); // CZ
    q.controlZ(4,7); // CZ
    q.controlZ(7,10); // CZ
    q.controlZ(10,13); // CZ

    // Generate a random phase
    qsl::Random<double> rand(0,2*M_PI);
    const double xi = rand.getNum();
    const double eta = rand.getNum();
    const double zeta = rand.getNum();

    // In each measured column, first measure out the
    // qubits in the rows above and below and record
    // the results. 

    // COLUMN 0 (X measurement) ==========
    
    // Measure qubit 0 and 1 in computational basis
    const unsigned cutout0_up = q.measureOut(0);
    const unsigned cutout0_down = q.measureOut(0);
    const unsigned k0 = cutout0_up ^ cutout0_down;
    
    // Now measure the middle qubit in the X basis
    q.rotateZ(0, M_PI/2); // Z-rotation 
    q.rotateX(0, M_PI/2); // X-rotation 
    const unsigned s0 = q.measureOut(0);

    std::cout << "up: " << cutout0_up
	      << ", down: " << cutout0_down
	      << ", k: " << k0
	      << ", s: "<< s0
	      << std::endl;

    // COLUMN 1 (XY-plane measurement) ==========
    
    // Measure qubit 3 and 4 in computational basis
    const unsigned cutout1_up = q.measureOut(0);
    const unsigned cutout1_down = q.measureOut(0);
    const unsigned k1 = cutout1_up ^ cutout1_down;
    
    // Measure qubit 5 in xi basis and remove
    unsigned setting = s0 ^ k0;
    double phi = -xi;
    if (setting == 0) {
        q.rotateZ(0, -phi+M_PI/2); // Z-rotation 
    } else {
	q.rotateZ(0, phi+M_PI/2); // Z-rotation
    }
    q.rotateX(0, M_PI/2); // X-rotation 
    const unsigned s1 = q.measureOut(0);

    std::cout << "up: " << cutout1_up
	      << ", down: " << cutout1_down
	      << ", k: " << k1
	      << ", s: "<< s1
	      << std::endl;

    // COLUMN 2 (XY-plane measurement) ==========
    
    // Measure qubit 3 and 4 in computational basis
    const unsigned cutout2_up = q.measureOut(0);
    const unsigned cutout2_down = q.measureOut(0);
    const unsigned k2 = cutout2_up ^ cutout2_down;
    
    // Measure qubit 5 in xi basis and remove
    setting = s1 ^ k1;
    phi = -eta;
    if (setting == 0) {
        q.rotateZ(0, -phi+M_PI/2); // Z-rotation 
    } else {
	q.rotateZ(0, phi+M_PI/2); // Z-rotation
    }
    q.rotateX(0, M_PI/2); // X-rotation 
    const unsigned s2 = q.measureOut(0);

    std::cout << "up: " << cutout2_up
	      << ", down: " << cutout2_down
	      << ", k: " << k2
	      << ", s: "<< s2
	      << std::endl;

    // COLUMN 3 (XY-plane measurement) ==========
    
    // Measure qubit 3 and 4 in computational basis
    const unsigned cutout3_up = q.measureOut(0);
    const unsigned cutout3_down = q.measureOut(0);
    const unsigned k3 = cutout3_up ^ cutout3_down;
    
    // Measure qubit 5 in xi basis and remove
    setting = s0 ^ k0 ^ s2 ^ k2;
    phi = -zeta;
    if (setting == 0) {
        q.rotateZ(0, -phi+M_PI/2); // Z-rotation 
    } else {
	q.rotateZ(0, phi+M_PI/2); // Z-rotation
    }
    q.rotateX(0, M_PI/2); // X-rotation 
    const unsigned s3 = q.measureOut(0);

    std::cout << "up: " << cutout3_up
	      << ", down: " << cutout3_down
	      << ", k: " << k3
	      << ", s: "<< s3
	      << std::endl;
    
    // Column 4 (Final) =========

    // Measure qubit 6 and 7 in computational basis
    const unsigned cutout4_up = q.measureOut(0);
    const unsigned cutout4_down = q.measureOut(0);    
    const unsigned k4 = cutout4_up ^ cutout4_down;

    std::cout << "up: " << cutout2_up
	      << ", down: " << cutout2_down
	      << ", k: " << k2
	      << std::endl;
    
    // Byproduct operators =========

    // Byproduct operators
    unsigned z = s0 ^ k0 ^ s2 ^ k2 ^ k4;
    unsigned x = s1 ^ k1 ^ s3 ^ k3;
    
    // The final state should be equal to the |+) state up
    // to the byproduct operators
    Sim q_correct{ 1 };
    q_correct.hadamard(0); // Create the plus state

    // Apply the rotations - U = Rx(\xi)
    q_correct.rotateX(0, xi);
    q_correct.rotateZ(0, eta);
    q_correct.rotateX(0, zeta);
    
    // Use the byproduct operators to correct the state
    std::cout << "x = " << x << std::endl;
    std::cout << "z = " << z << std::endl;
    if (x == 1) {
    	q.pauliX(0);
    }
    if (z == 1) {
    	q.pauliZ(0);
    }
    q.print();
    q_correct.print();

    // Check the distance between states
    std::cout << "Distance = " << qsl::fubiniStudy(q.getState(),
						   q_correct.getState())
	      << std::endl;
}
