/*
 *  Copyright 2021 John Scott
 *
 *  This file is part of mbqcsim.
 *
 *  mbqcsim is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  mbqcsim is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with mbqcsim.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
/**
 * \file hadamard-cut-out.cpp
 * \brief Hadamard gate surrounded by cut-out qubits
 *
 */

#include <qsl/qubits.hpp>
#include <qsl/utils/quantum.hpp>
#include <qsl/utils/random.hpp>

#include <iostream>

int main()
{
    using Sim = qsl::Qubits<qsl::Type::Resize, double>;

    // The Hadamard gate is performed on a pattern of two
    // logical qubits

    // Need three rows of three qubits. The middle row
    // is the actual gate, and the surrounding two rows
    // are where qubits will be cut out. Qubit pattern:
    //
    // 0 - 3
    // -   -
    // 2 - 5
    // -   -
    // 1 - 4
    //
    // The order is so that you're always measuring
    // qubit zero
    //
    // Dashes mean CHPASE gates
    Sim q{ 6 };

    ///\todo Change phase to rotateZ to avoid sign problem
    
    // Make equal superposition state
    for (unsigned k = 0; k < 6; k++) {
	q.hadamard(k);
    }

    // Make the cluster state (vertical entanglement)
    q.controlZ(0,2); // CZ
    q.controlZ(2,1); // CZ
    q.controlZ(3,5); // CZ
    q.controlZ(5,4); // CZ

    // Make the cluster state (horizontal entanglement)
    q.controlZ(0,3); // CZ
    q.controlZ(2,5); // CZ
    q.controlZ(1,4); // CZ

    // In each measured column, first measure out the
    // qubits in the rows above and below and record
    // the results. 

    // COLUMN 0 ==========
    
    // Measure qubit 0 and 1 in computational basis
    const unsigned cutout0_up = q.measureOut(0);
    const unsigned cutout0_down = q.measureOut(0);
    const unsigned k0 = cutout0_up ^ cutout0_down;
    
    // Now measure the middle qubit in the X basis
    q.rotateZ(0, M_PI/2); // Z-rotation 
    q.rotateX(0, M_PI/2); // X-rotation 
    const unsigned s0 = q.measureOut(0);

    std::cout << "up: " << cutout0_up
	      << ", down: " << cutout0_down
	      << ", k: " << k0
	      << ", s: "<< s0
	      << std::endl;
    
    // COLUMN 1 ==========
    
    // Measure qubit 3 and 4 in computational basis
    const unsigned cutout1_up = q.measureOut(0);
    const unsigned cutout1_down = q.measureOut(0);
    const unsigned k1 = cutout1_up ^ cutout1_down;
    
    // Leave the middle qubit in the final column unmeasured
    
    // Byproduct operators =========

    // Byproduct operators
    unsigned z = k1;
    unsigned x = s0 ^ k0;
    
    // The final state should be equal to the |+) state up
    // to the byproduct operators
    Sim q_correct{ 1 };
    q_correct.hadamard(0); // Create the plus state

    // Apply the Hadamard gate under test
    // Note: this hadamard gate is the one that the pattern realises.
    // The one above is used to set the initial state of the cluster.
    // Having two of them is not a bug, even though it looks like it!
    q_correct.hadamard(0);
    
    // Use the byproduct operators to correct the state
    std::cout << "x = " << x << std::endl;
    std::cout << "z = " << z << std::endl;
    if (x == 1) {
    	q.pauliX(0);
    }
    if (z == 1) {
    	q.pauliZ(0);
    }
    q.print();
    q_correct.print();

    // Check the distance between states
    std::cout << "Distance = " << qsl::fubiniStudy(q.getState(),
						   q_correct.getState())
	      << std::endl;
}
