# Build the documentation
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = docs
BUILDDIR      = docs
APIDOCDIR = docs/py_mbqcsim	
.PHONY: docs
docs:
	doxygen docs/Doxyfile
	rm -rf docs/html
	rm -rf $(APIDOCDIR)
	sphinx-apidoc -o $(APIDOCDIR) scripts/py_mbqcsim
	@$(SPHINXBUILD) -M html "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
	mv docs/simdocs docs/html/simdocs


.PHONY: clean
clean:
	-rm -rf $(SOURCEDIR)/xml $(SOURCEDIR)/doxygen-html $(SOURCEDIR)/html $(SOURCEDIR)/doctrees
